
import HockeyApp from 'react-native-hockeyapp';

//Config information for HockeyApp.
//HockeyApp.AuthenticationType.EmailSecret
module.exports = {

  hockeyIOSAppId:"2bab4f21b2564d3a9f287997738aff58",
  hockeyAndroidAppId:"c94fa96ef9fc4d74b707efa20b57b896",
  autoSendReports:true,
  authenticationType: HockeyApp.AuthenticationType.Anonymous,
  appIOSSecret:"327909fa7bbb2936548cc0f6a3c4734e",
  appAndroidSecret: "4f7ab85ddd3a8a4995ca89ac1c1b79e0",

  app:{
    forgotPasswordLink: 'https://passwordreset.microsoftonline.com/?username=',
    customerServicePhoneNumber:'(423) 602-9002'
  }

}
