import React, { Component } from 'react';

import { StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import palette from '../../styles/palette.js';

import { BasicPageHeader } from '../../components/_common/drawer-nav';
import {
  ClockInView,
  ClockInStoreListView,
  ClockInHavingTroubleView,
  ClockInSearchResultsView,
  GrillbarrowView
} from '../../components/tech/clocking';

const TechClockInStackNavigator = StackNavigator({
  ClockIn: {
    screen: ClockInView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  StoreList: {
    screen: ClockInStoreListView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  HavingTrouble: {
    screen: ClockInHavingTroubleView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  SearchResults: {
    screen: ClockInSearchResultsView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  Grillbarrow: {
    screen: GrillbarrowView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
});

export default TechClockInStackNavigator;
