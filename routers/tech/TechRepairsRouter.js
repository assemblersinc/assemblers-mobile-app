import React, { Component } from 'react';
import {
  View,
  TouchableOpacity
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import palette from '../../styles/palette.js';
import {
  RepairsView,
  RepairTicketDetailView,
  RepairCategorySelectionView,
  AddRepairNotesView,
  AbleToAssembleView,
  AttachStickerView,
  RepairProductDetailsView,
  EnterStickerNumberView,
  RepairTicketInvoiceView
} from '../../components/_common/store-repairs';

import { CameraView, PhotoPreview } from '../../components/_common/camera';
import { BasicPageHeader, DrawerPageHeader } from '../../components/_common/drawer-nav';

const TechRepairRouter = StackNavigator({
  Repairs: {
    screen: RepairsView,
    navigationOptions: ({ navigation }) => ({
      title: 'Store Repairs',
      header: <DrawerPageHeader navigation={navigation} />
    })
  },
  RepairTicketDetail: {
    screen: RepairTicketDetailView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  EnterStickerNumber: {
    screen: EnterStickerNumberView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  RepairProductDetails: {
    screen: RepairProductDetailsView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  SelectRepairCategory: {
    screen: RepairCategorySelectionView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  Camera: {
    screen: CameraView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  PhotoPreview: {
    screen: PhotoPreview,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  AddRepairNotes: {
    screen: AddRepairNotesView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  AttachSticker: {
    screen: AttachStickerView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  RepairTicketInvoice: {
    screen: RepairTicketInvoiceView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  }
});

export default TechRepairRouter;
