import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import StoreRequestView from '../../components/_common/store-request/StoreRequestView.js';

const TechStoreRequestRouter = StackNavigator({
  StoreRequest: {
    screen: StoreRequestView,
    navigationOptions: ({ navigation }) => ({
      title: 'Store Request',
      header: null
    })
  }
});

export default TechStoreRequestRouter;
