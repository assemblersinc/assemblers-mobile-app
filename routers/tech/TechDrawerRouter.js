import React, { Component } from 'react';
import { DrawerNavigator, DrawerView, DrawerItems, addNavigationHelpers } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import TechContentComponent from '../../components/_common/drawer-nav/TechContentComponent';

import {
  TechStoreRequestRouter,
  TechRepairsRouter,
  TechMyHoursRouter,
  TechClockInRouter
} from './';

import {
  AnnouncementsRouter,
  ChatRouter,
  StoreWalkRouter,
  WorkListRouter,
  TodaysInvoiceRouter,
  InvoiceHistoryRouter
} from '../common';

import styles from '../../styles';

const DrawerRoutes = {
  Announcements: {
    screen: AnnouncementsRouter
  },
  Chat: {
    screen: ChatRouter,
  },
  StoreWalk: {
    screen: StoreWalkRouter,
  },
  WorkList: {
    screen: WorkListRouter,
  },
  TodaysInvoice: {
    screen: TodaysInvoiceRouter,
  },
  StoreRequest: {
    screen: TechStoreRequestRouter,
  },
  StoreRepairs: {
    screen: TechRepairsRouter,
  },
  MyHours: {
    screen: TechMyHoursRouter,
  },
  InvoiceHistory: {
    screen: InvoiceHistoryRouter,
  },
  ClockIn: {
    screen: TechClockInRouter,
  }
};


const DrawerOptions = {
  contentComponent: props => <TechContentComponent navProps={props} />
};

export default TechDrawerNavigator = DrawerNavigator(DrawerRoutes, DrawerOptions);
