import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { StackNavigator } from 'react-navigation';
import palette from '../../styles/palette.js';
import MyHoursView from '../../components/_common/tech-data/TimesheetView.js';

const MyHoursRouter = StackNavigator({
  MyHours: {
    screen: MyHoursView,
    navigationOptions: ({ navigation }) => ({
      drawerLabel: 'My Hours',
      header:
      <View style={{height: 70, backgroundColor: palette.white, flexDirection: 'row', alignItems: 'flex-end', paddingLeft: 20, paddingRight: 20, paddingTop: 20 }}>
        <TouchableOpacity onPress={ () => navigation.navigate('DrawerOpen') }>
          <Icon name="menu" size={30} color={palette.mediumBlue} />
        </TouchableOpacity>
      </View>
    })
  }
});

export default MyHoursRouter;
