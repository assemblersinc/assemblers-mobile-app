import TechClockInRouter from './TechClockInRouter';
import TechDrawerRouter from './TechDrawerRouter';
import TechMyHoursRouter from './TechMyHoursRouter';
import TechRepairsRouter from './TechRepairsRouter';
import TechStoreRequestRouter from './TechStoreRequestRouter';

module.exports = {
  TechClockInRouter: TechClockInRouter,
  TechDrawerRouter: TechDrawerRouter,
  TechMyHoursRouter: TechMyHoursRouter,
  TechRepairsRouter: TechRepairsRouter,
  TechStoreRequestRouter: TechStoreRequestRouter
}
