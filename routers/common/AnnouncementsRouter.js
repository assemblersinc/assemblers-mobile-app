import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { DrawerPageHeader } from '../../components/_common/drawer-nav';
import { AnnouncementsView } from '../../components/_common/announcements';
import palette from '../../styles/palette';

const AnnouncementsRouter = StackNavigator({
  Announcements: {
    screen: AnnouncementsView,
    navigationOptions: ({ navigation }) => ({
      header: <DrawerPageHeader navigation={navigation} />
    })
  }
});

export default AnnouncementsRouter;
