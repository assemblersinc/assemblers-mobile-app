import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {
  StartInvoiceView,
  InvoiceDetailView,
  CantGetFinalizedView,
  PrintNameView,
  SurveyView,
  SignatureView,
  GetKeyRecView,
  GetPOCodeView,
  ThankYouView,
  SubmitInvoiceView
} from '../../components/_common/todays-invoice';

import { CameraView, PhotoPreview } from '../../components/_common/camera';

import { BasicPageHeader } from '../../components/_common/drawer-nav';
import styles from '../../styles';


const TodaysInvoiceRouter = StackNavigator({
  StartInvoice: {
    screen: StartInvoiceView,
    navigationOptions: ({ navigation }) => ({
      title: "My Open Invoices",
      drawerLabel: "My Open Invoices",
      header: null
    })
  },
  InvoiceDetail: {
    screen: InvoiceDetailView,
    navigationOptions: ({ navigation }) => ({
      title: 'Invoice',
      header: null
    })
  },
  CantGetFinalized: {
    screen: CantGetFinalizedView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  PrintName: {
    screen: PrintNameView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  Survey: {
    screen: SurveyView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  Signature: {
    screen: SignatureView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  GetPOCode: {
    screen: GetPOCodeView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} version={'blue'} />
    })
  },
  GetKeyRec: {
    screen: GetKeyRecView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} version={'blue'} />
    })
  },
  Camera: {
    screen: CameraView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  PhotoPreview: {
    screen: PhotoPreview,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  ThankYou: {
    screen: ThankYouView,
    navigationOptions: ({ navigation }) => ({
      header: null,
    })
  },
  SubmitInvoice: {
    screen: SubmitInvoiceView,
    navigationOptions: ({ navigation }) => ({
      header: null,
    })
  }
});

export default TodaysInvoiceRouter;
