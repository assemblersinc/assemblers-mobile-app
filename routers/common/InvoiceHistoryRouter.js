import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { StackNavigator } from 'react-navigation';
import { DrawerPageHeader, BasicPageHeader } from '../../components/_common/drawer-nav';
import { MyPreviousInvoicesView } from '../../components/_common/invoice-history';
import { InvoiceDetailView } from '../../components/_common/todays-invoice';

import palette from '../../styles/palette.js';

const PreviousInvoicesRouter = StackNavigator({
  MyPreviousInvoices: {
    screen: MyPreviousInvoicesView,
    navigationOptions: ({ navigation }) => ({
      drawerLabel: 'My Previous Invoices',
      header: <DrawerPageHeader navigation={navigation} />
    })
  },
  InvoiceDetail: {
    screen: InvoiceDetailView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
});

export default PreviousInvoicesRouter;
