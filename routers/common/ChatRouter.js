import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import ChatListView from '../../components/_common/chat/ChatListView';
import { DrawerPageHeader } from '../../components/_common/drawer-nav';
import ChatView from '../../components/_common/chat/ChatView';
import styles from '../../styles';

const ChatRouter = StackNavigator({
  ChatList: {
    screen: ChatListView,
    navigationOptions: ({ navigation }) => ({
      drawerLabel: 'Chat',
      header: null,
    })
  },
  ChatDetail: {
    screen: ChatView,
    navigationOptions: ({ navigation }) => ({
      header: null,
    })
  }
});

export default ChatRouter;
