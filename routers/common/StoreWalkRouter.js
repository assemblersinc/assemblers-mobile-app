import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from '../../styles';
import BasicPageHeader from '../../components/_common/drawer-nav/BasicPageHeader';

import { StoreWalkView, StoreWalkItemDetailView, AddSKUView, AddServiceView, SubmitPriceRequestView } from '../../components/_common/store-walk';
import { CameraView, PhotoPreview } from '../../components/_common/camera';
import { RepairProductDetailsView, RepairCategorySelectionView, AddRepairNotesView, AttachStickerView, EnterStickerNumberView  } from '../../components/_common/store-repairs';

const StoreWalkRouter = StackNavigator({
  StoreWalk: {
    screen: StoreWalkView,
    navigationOptions: ({ navigation }) => ({
      drawerLabel: 'Store Walk',
      header: null
    })
  },
  Camera: {
    screen: CameraView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  PhotoPreview: {
    screen: PhotoPreview,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  SubmitPriceRequest: {
    screen: SubmitPriceRequestView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  PhotoPreview: {
    screen: PhotoPreview,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  AddSKU: {
    screen: AddSKUView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  AddService: {
    screen: AddServiceView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  Confirmation: {
    screen: StoreWalkItemDetailView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  RepairProductDetails: {
    screen: RepairProductDetailsView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  SelectRepairCategory: {
    screen: RepairCategorySelectionView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  AddRepairNotes: {
    screen: AddRepairNotesView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  AttachSticker: {
    screen: AttachStickerView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  EnterStickerNumber: {
    screen: EnterStickerNumberView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
});

export default StoreWalkRouter;
