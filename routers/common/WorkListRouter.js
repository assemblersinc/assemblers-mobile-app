import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import palette from '../../styles/palette.js';
import { BasicPageHeader, DrawerPageHeader } from '../../components/_common/drawer-nav';

import { WorkListView, ItemDetailView, WorkListServicesView, WorkListSkuView } from '../../components/_common/work-list';
import { CameraView, PhotoPreview } from '../../components/_common/camera';
import { RepairProductDetailsView, RepairCategorySelectionView, AddRepairNotesView, AbleToAssembleView, AttachStickerView  } from '../../components/_common/store-repairs';
import { SubmitPriceRequestView } from '../../components/_common/store-walk';

const WorkListRouter = StackNavigator({
  WorkList: {
    screen: WorkListView,
    navigationOptions: ({ navigation }) => ({
      drawerLabel: 'Work List',
      header: <DrawerPageHeader navigation={navigation} />
    })
  },
  WorkListSkuView: {
    screen: WorkListSkuView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  WorkListServicesView: {
    screen: WorkListServicesView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  SubmitPriceRequest: {
    screen: SubmitPriceRequestView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  Confirmation: {
    screen: ItemDetailView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  RepairProductDetails: {
    screen: RepairProductDetailsView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  SelectRepairCategory: {
    screen: RepairCategorySelectionView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  Camera: {
    screen: CameraView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  PhotoPreview: {
    screen: PhotoPreview,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  AddRepairNotes: {
    screen: AddRepairNotesView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  AbleToAssemble: {
    screen: AbleToAssembleView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  AttachSticker: {
    screen: AttachStickerView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
});

export default WorkListRouter;
