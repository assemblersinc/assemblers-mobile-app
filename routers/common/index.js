import AnnouncementsRouter from './AnnouncementsRouter';
import ChatRouter from './ChatRouter';
import InvoiceHistoryRouter from './InvoiceHistoryRouter';
import StoreWalkRouter from './StoreWalkRouter';
import TodaysInvoiceRouter from './TodaysInvoiceRouter';
import WorkListRouter from './WorkListRouter';

module.exports = {
  AnnouncementsRouter: AnnouncementsRouter,
  ChatRouter: ChatRouter,
  InvoiceHistoryRouter: InvoiceHistoryRouter,
  StoreWalkRouter: StoreWalkRouter,
  TodaysInvoiceRouter: TodaysInvoiceRouter,
  WorkListRouter: WorkListRouter
}
