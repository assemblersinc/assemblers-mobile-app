import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import { DashboardView } from '../../components/manager-common';
import styles from '../../styles';

const ManagerDashboardRouter = StackNavigator({
  DashboardView: {
    screen: DashboardView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
});

export default ManagerDashboardRouter;
