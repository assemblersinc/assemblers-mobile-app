import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import { BasicPageHeader } from '../../components/_common/drawer-nav';
import { ClientSelectionListView } from '../../components/_common';
import { ManagerStoreRequestView, StoreRequestView } from '../../components/_common/store-request';

const ManagerStoreRequestRouter = StackNavigator({
  StoreRequestList: {
    screen: ManagerStoreRequestView,
    navigationOptions: ({ navigation }) => ({
      drawerLabel: 'Store Request',
      header: null
    })
  },
  StoreRequest:{
    screen: StoreRequestView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  ClientSelection: {
    screen: ClientSelectionListView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  }
});

export default ManagerStoreRequestRouter;
