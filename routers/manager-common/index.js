import AreaManagerDrawerRouter from './AreaManagerDrawerRouter';
import FieldOpsDrawerRouter from './FieldOpsDrawerRouter';
import FieldOpsTechsRouter from './FieldOpsTechsRouter';
import ManagerInvoicesRouter from './ManagerInvoicesRouter';
import ManagerRepairsRouter from './ManagerRepairsRouter';
import ManagerStoreRequestRouter from './ManagerStoreRequestRouter';
import MyTechsRouter from './MyTechsRouter';

module.exports = {
  AreaManagerDrawerRouter: AreaManagerDrawerRouter,
  FieldOpsDrawerRouter: FieldOpsDrawerRouter,
  FieldOpsTechsRouter: FieldOpsTechsRouter,
  ManagerInvoicesRouter: ManagerInvoicesRouter,
  ManagerRepairsRouter: ManagerRepairsRouter,
  ManagerStoreRequestRouter: ManagerStoreRequestRouter,
  MyTechsRouter: MyTechsRouter
}
