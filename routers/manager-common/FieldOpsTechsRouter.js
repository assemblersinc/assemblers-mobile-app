import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import { DrawerPageHeader, BasicPageHeader } from '../../components/_common/drawer-nav';
import { AreaTechsView, TechDetailView, ReadOnlyWorkListView } from '../../components/manager-common/tech-management';
import { TimesheetView } from '../../components/_common/tech-data';
import { MyPreviousInvoicesView } from '../../components/_common/invoice-history';
import { InvoiceDetailView } from '../../components/_common/todays-invoice';

const TechsRouter = StackNavigator({
  AreaTechs: {
    screen: AreaTechsView,
    navigationOptions: ({ navigation }) => ({
      drawerLabel: 'Area Techs',
      header: <DrawerPageHeader navigation={navigation} />
    })
  },
  TechDetail: {
    screen: TechDetailView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  Timesheet: {
    screen: TimesheetView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  TechWorkList: {
    screen: ReadOnlyWorkListView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  TechInvoices: {
    screen: MyPreviousInvoicesView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  InvoiceDetail: {
    screen: InvoiceDetailView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
});

export default TechsRouter;
