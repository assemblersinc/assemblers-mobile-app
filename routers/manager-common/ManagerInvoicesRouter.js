import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { BasicPageHeader } from '../../components/_common/drawer-nav';
import { ClientSelectionListView } from '../../components/_common';
import ManagerInvoicesView from '../../components/_common/invoice-history/ManagerInvoicesView';
import InvoiceDetailView from '../../components/_common/todays-invoice/InvoiceDetailView';

const ManagerInvoicesRouter = StackNavigator({
  InvoicesList: {
    screen: ManagerInvoicesView,
    navigationOptions: ({ navigation }) => ({
      title: 'Area Invoices',
      header: null
    })
  },
  ClientSelection: {
    screen: ClientSelectionListView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  InvoiceDetail: {
    screen: InvoiceDetailView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
});

export default ManagerInvoicesRouter;
