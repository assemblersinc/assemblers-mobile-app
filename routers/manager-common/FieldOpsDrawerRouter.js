import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import { DrawerNavigator, DrawerView, DrawerItems } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { Header, NavListItem, ManagerContentComponent } from '../../components/_common/drawer-nav';

import ManagerDashboardRouter from './ManagerDashboardRouter';
import FieldOpsTechsRouter from './FieldOpsTechsRouter';
import ManagerStoreRequestRouter from './ManagerStoreRequestRouter';
import ManagerRepairsRouter from './ManagerRepairsRouter';

import {
  AnnouncementsRouter,
  ChatRouter,
  StoreWalkRouter,
  WorkListRouter,
  TodaysInvoiceRouter,
  InvoiceHistoryRouter
} from '../common';

import styles from '../../styles';

const DrawerRoutes = {
  Chat: {
    screen: ChatRouter,
  },
  Dashboard: {
    screen: ManagerDashboardRouter
  },
  Announcements: {
    screen: AnnouncementsRouter
  },
  MyTechs: {
    screen: FieldOpsTechsRouter,
  },
  StoreWalk: {
    screen: StoreWalkRouter,
  },
  WorkList: {
    screen: WorkListRouter
  },
  TodaysInvoice: {
    screen: TodaysInvoiceRouter
  },
  ManagerStoreRequest: {
    screen: ManagerStoreRequestRouter,
  },
  ManagerStoreRepairs: {
    screen: ManagerRepairsRouter,
  },
  InvoiceHistory: {
    screen: InvoiceHistoryRouter
  }
};

const DrawerOptions = {
  contentComponent: props => <ManagerContentComponent navProps={props} />
};


export default FieldOpsDrawerNavigator = DrawerNavigator(DrawerRoutes, DrawerOptions);
