import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { DrawerPageHeader, BasicPageHeader } from '../../components/_common/drawer-nav';
import { MyTechsView, ManageTechsView, SearchTechsView, TechDetailView, ReadOnlyWorkListView } from '../../components/manager-common/tech-management';
import { TimesheetView } from '../../components/_common/tech-data';
import { MyPreviousInvoicesView } from '../../components/_common/invoice-history';
import { InvoiceDetailView } from '../../components/_common/todays-invoice';

const MyTechsRouter = StackNavigator({
  MyTechs: {
    screen: MyTechsView,
    navigationOptions: ({ navigation }) => ({
      drawerLabel: 'My Techs',
      header: null
    })
  },
  ManageTechs: {
    screen: ManageTechsView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  SearchTechs: {
    screen: SearchTechsView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  TechDetail: {
    screen: TechDetailView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  Timesheet: {
    screen: TimesheetView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  TechWorkList: {
    screen: ReadOnlyWorkListView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  TechInvoices: {
    screen: MyPreviousInvoicesView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  InvoiceDetail: {
    screen: InvoiceDetailView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
});

export default MyTechsRouter;
