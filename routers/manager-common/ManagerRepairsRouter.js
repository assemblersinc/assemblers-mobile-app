import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { BasicPageHeader } from '../../components/_common/drawer-nav';
import { CameraView, PhotoPreview } from '../../components/_common/camera';
import { ClientSelectionListView } from '../../components/_common';
import {
  ManagerRepairsView,
  RepairsView,
  RepairTicketDetailView,
  RepairCategorySelectionView,
  AddRepairNotesView,
  AbleToAssembleView,
  AttachStickerView,
  RepairProductDetailsView,
  EnterStickerNumberView,
  RepairTicketInvoiceView
} from '../../components/_common/store-repairs';

const ManagerRepairsRouter = StackNavigator({
  RepairsList: {
    screen: ManagerRepairsView,
    navigationOptions: ({ navigation }) => ({
      title: 'Store Repairs',
      header: null
    })
  },
  ClientSelection: {
    screen: ClientSelectionListView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  Repairs: {
    screen: RepairsView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  RepairTicketDetail: {
    screen: RepairTicketDetailView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  EnterStickerNumber: {
    screen: EnterStickerNumberView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  RepairProductDetails: {
    screen: RepairProductDetailsView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  SelectRepairCategory: {
    screen: RepairCategorySelectionView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  Camera: {
    screen: CameraView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  PhotoPreview: {
    screen: PhotoPreview,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  AddRepairNotes: {
    screen: AddRepairNotesView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  AttachSticker: {
    screen: AttachStickerView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  },
  RepairTicketInvoice: {
    screen: RepairTicketInvoiceView,
    navigationOptions: ({ navigation }) => ({
      header: <BasicPageHeader navigation={navigation} />
    })
  }
});

export default ManagerRepairsRouter;
