import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import { DrawerNavigator, DrawerView, DrawerItems } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { Header, NavListItem, ManagerContentComponent } from '../../components/_common/drawer-nav';

import TechClockInRouter from '../tech/TechClockInRouter';
import MyTechsRouter from './MyTechsRouter';
import ManagerInvoicesRouter from './ManagerInvoicesRouter';
import ManagerRepairsRouter from './ManagerRepairsRouter';
import ManagerStoreRequestRouter from './ManagerStoreRequestRouter';
import ManagerDashboardRouter from './ManagerDashboardRouter';

import {
  AnnouncementsRouter,
  ChatRouter,
  StoreWalkRouter,
  WorkListRouter,
  TodaysInvoiceRouter,
  InvoiceHistoryRouter
} from '../common';

import styles from '../../styles';

const DrawerRoutes = {
  Chat: {
    screen: ChatRouter,
  },
  Dashboard: {
    screen: ManagerDashboardRouter
  },
  Announcements: {
    screen: AnnouncementsRouter
  },
  MyTechs: {
    screen: MyTechsRouter,
  },
  StoreWalk: {
    screen: StoreWalkRouter,
  },
  WorkList: {
    screen: WorkListRouter
  },
  TodaysInvoice: {
    screen: TodaysInvoiceRouter
  },
  InvoiceHistory: {
    screen: InvoiceHistoryRouter
  },
  ManagerInvoices: {
    screen: ManagerInvoicesRouter
  },
  ManagerStoreRequest: {
    screen: ManagerStoreRequestRouter,
  },
  ManagerStoreRepairs: {
    screen: ManagerRepairsRouter,
  },
  CheckIn: {
    screen: TechClockInRouter
  }

};

const DrawerOptions = {
  contentComponent: props => <ManagerContentComponent navProps={props} />
};


export default ManagerDrawerNavigator = DrawerNavigator(DrawerRoutes, DrawerOptions);
