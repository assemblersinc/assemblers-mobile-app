import React, { Component } from 'react';
import {
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { StackNavigator } from 'react-navigation';

import { LoginView, LoginHelpView, PermissionsView, ForgotPasswordView } from '../../components/login';
import styles from '../../styles';

const LoginStackNavigator = StackNavigator({
  Login: {
    screen: LoginView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  NeedHelp: {
    screen: LoginHelpView,
    navigationOptions: ({ navigation }) => ({
      header:
      <View style={{ height: 70, backgroundColor: styles.palette.mediumBlue, flexDirection: 'row', alignItems: 'center' }}>
        <Icon name="chevron-left" size={40}  color={styles.palette.white} style={{ padding: 10, paddingTop: 20 }} onPress={ () => navigation.goBack() } />
      </View>,
    })
  },
  ForgotPassword: {
    screen: ForgotPasswordView,
    navigationOptions: ({ navigation }) => ({
      header: 
      <View style={{ height: 70, backgroundColor: styles.palette.mediumBlue, flexDirection: 'row', alignItems: 'center' }}>
        <Icon name="chevron-left" size={40}  color={styles.palette.white} style={{ padding: 10, paddingTop: 20 }} onPress={ () => navigation.goBack() } />
      </View>,
    })
  }, 
  Permissions: {
    screen: PermissionsView,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
});

module.exports = {
  LoginStackNavigator: LoginStackNavigator
}
