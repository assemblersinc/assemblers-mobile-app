# Customise this file, documentation can be found here:
# https://github.com/fastlane/fastlane/tree/master/fastlane/docs
# All available actions: https://docs.fastlane.tools/actions
# can also be listed using the `fastlane actions` command

# Change the syntax highlighting to Ruby
# All lines starting with a # are ignored when running `fastlane`

# If you want to automatically update fastlane if a new version is available:
# update_fastlane

# This is the minimum version number required.
# Update this, if you use features of a newer version
fastlane_version "2.53.1"

# BlueFletch fastlane slack channel
ENV["SLACK_URL"] = "https://hooks.slack.com/services/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"

default_platform :ios

# These get used for both Android and iOS builds/lanes

before_all do
    if is_ci?
#        ensure_git_status_clean(show_uncommitted_changes: true)
#        ensure_git_branch(branch: "master")
        # this section only runs when the build is initiated by a continuous integration server such as Jenkins
        setup_jenkins
    else
        puts "Fastlane being ran on dev computer, skipping Jenkins setup"
    end
end

after_all do |lane|
# This block is called, only if the executed lane was successful


      puts "Fastlane lane ran successfully."

      slack(message: "Success. Hooray!")
end

error do |lane, exception|

    puts "Fastlane lane failed."

        slack(
          message: "Fail. Boooo. Error: #{exception.message}.",
          success: false
        )
end

platform :ios do
  before_all do

    if is_ci?
        clear_derived_data
    else
        puts "Fastlane being ran on dev computer, skipping Jenkins setup"
    end

    cocoapods(clean: true, podfile: './ios/Podfile')
  end

  desc "Runs all the tests"
  lane :test do
    scan
  end

  desc "Submit a new Beta Build to Apple TestFlight"
  desc "This will also make sure the profile is up to date"
  lane :beta do

    sigh(
      adhoc: false,
      force: true,
    )

    gym(scheme: "AssemblersIncMobile", project: './ios/AssemblersIncMobile.xcodeproj') # Build your app - more options available

    hockey(
      api_token: "05dc9d24bc5d4e02988ab184aeec5c33",
      ipa: "AssemblersIncMobile.ipa",
      release_type: "0" # Zero means beta
    )

   test_flight

    # sh "your_script.sh"
    # You can also use other beta testing services here (run `fastlane actions`)
  end

  desc "Uploads build to TestFlight"
  lane :test_flight do
    pilot
  end

  desc "Deploy a new version to the App Store"
  lane :release do
    # match(type: "appstore")
    # snapshot
    sigh(
        adhoc: false,
        force: true,
    )

    gym(scheme: "AssemblersIncMobile", project: './ios/AssemblersIncMobile.xcodeproj') # Build your app - more options available

    hockey(
      api_token: "05dc9d24bc5d4e02988ab184aeec5c33",
      ipa: "AssemblersIncMobile.ipa",
      release_type: "1" # Zero means store build
    )

    test_flight
    deliver(force: true)
    # frameit
  end

  # You can define as many lanes as you want

  after_all do |lane|
    # This block is called, only if the executed lane was successful

  end
end

platform :android do

  lane :build do
    gradle(task: 'clean', project_dir: "android/") # Clean the Gradle project
    gradle(task: "assemble", build_type: "Release", project_dir: "android/") # Build the Release APK
  end

  desc "Submit a new Beta Build to Play Store"
  lane :beta do
    build
    hockey_upload

    # Commented out until we get upload signing finished
#    supply(track: "beta", apk: "android/app/build/outputs/apk/app-release.apk") # Upload the APK to the Play Store (alpha)
  end

  lane :release do
    build
    hockey_upload
# Commented out until we get upload signing finished
#    supply(track: "rollout", apk: "android/app/build/outputs/apk/app-release.apk") # Upload the APK to the Play Store (alpha)
  end

  after_all do |lane|
    slack(message: "Successfully deployed new app beta update to Hockey.")
  end

  lane :hockey_upload do
    hockey(
              api_token: "05dc9d24bc5d4e02988ab184aeec5c33",
              apk: "android/app/build/outputs/apk/app-release.apk",
              release_type: "2", # Zero means beta
              status: "1"
            )
  end
end

# More information about multiple platforms in fastlane: https://github.com/fastlane/fastlane/blob/master/fastlane/docs/Platforms.md
# All available actions: https://docs.fastlane.tools/actions

# fastlane reports which actions are used. No personal data is recorded.
# Learn more at https://github.com/fastlane/fastlane#metrics
