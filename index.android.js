/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import AssemblersIncMobile from './app.js';


class Main extends Component {
  render() {
    return (
      <AssemblersIncMobile />
    );
  }
}

//Register the main navigator as the main app component
AppRegistry.registerComponent('AssemblersIncMobile', () => AssemblersIncMobile);
