import AddButton from './worklistAddButton@3x.png';
import AddTechGrey from './add-tech-grey@3x.png';
import AddTechBlue from './add-tech@3x.png';
import AILogo from './aiLogo@3x.png';
import AddQtyIcon from './addQty@3x.png';
import BannerBackground from './add-item-background.png';
import BigChatBubble from './big-chat-bubble@3x.png';
import Calendar from './calendar-icon@3x.png'
import CameraIcon from './cameraIcon@3x.png';
import ChatIcon from './chatIcon@3x.png';
import Client from './client-icon@3x.png';
import Clock from './clock@3x.png';
import MinusQtyIcon from './minusQty@3x.png';
import MinusTech from './minus-tech@3x.png';
import PhotoBackground from './photoBackground@3x.png';
import RemoveButton from './removeButton@3x.png';
import RepairWrench from './repair-wrench@3x.png';
import ScanButton from './scan-button@3x.png';
import ScanConfirmPopover from './scanConfPopover@3x.png';
import ScanOverlay from './scanOverlay@3x.png';
import PhoneQRImage from './phone-image@3x.png';
import Wrench from './wrench@3x.png';


module.exports = {
  AddButton: AddButton,
  AddQtyIcon: AddQtyIcon,
  AddTechGrey: AddTechGrey,
  AddTechBlue: AddTechBlue,
  AILogo: AILogo,
  BannerBackground: BannerBackground,
  BigChatBubble: BigChatBubble,
  Calendar: Calendar,
  CameraIcon: CameraIcon,
  ChatIcon: ChatIcon,
  Client: Client,
  Clock: Clock,
  MinusQtyIcon: MinusQtyIcon,
  MinusTech: MinusTech,
  PhotoBackground: PhotoBackground,
  PhoneQRImage: PhoneQRImage,
  RemoveButton: RemoveButton,
  RepairWrench: RepairWrench,
  ScanButton: ScanButton,
  ScanConfirmPopover: ScanConfirmPopover,
  ScanOverlay: ScanOverlay,
  Wrench: Wrench
};
