import { combineReducers } from 'redux';
import AppReducer from './AppReducer';
import LoginReducer from './LoginReducer';
import UserReducer from './UserReducer';
import ClockingReducer from './ClockingReducer';
import StoreWalkReducer from './StoreWalkReducer';
import ServicesReducer from './ServicesReducer';
import ProductsReducer from './ProductsReducer';
import ClientReducer from './ClientReducer';
import TimeEstimateReducer from './TimeEstimateReducer';
import WorkListReducer from './WorkListReducer';
import ChatReducer from './ChatReducer';
import AnnouncementsReducer from './AnnouncementsReducer';
import TimesheetReducer from './TimesheetReducer';
import StoreRequestReducer from './StoreRequestReducer';
import InvoiceReducer from './InvoiceReducer';
import InvoiceHistoryReducer from './InvoiceHistoryReducer';
import SurveyReducer from './SurveyReducer';
import RepairsReducer from './RepairsReducer';
import MyTechsReducer from './MyTechsReducer';
import ReportingReducer from "./ReportingReducer";

export default combineReducers({
  appReducer: AppReducer,
  loginReducer: LoginReducer,
  userReducer: UserReducer,
  clockingReducer: ClockingReducer,
  storeWalk: StoreWalkReducer,
  servicesReducer: ServicesReducer,
  productsReducer: ProductsReducer,
  clientReducer: ClientReducer,
  timeEstimate: TimeEstimateReducer,
  workListReducer: WorkListReducer,
  chatReducer: ChatReducer,
  announcementsReducer: AnnouncementsReducer,
  timesheetReducer: TimesheetReducer,
  storeRequestReducer: StoreRequestReducer,
  invoiceReducer: InvoiceReducer,
  invoiceHistoryReducer: InvoiceHistoryReducer,
  surveyReducer: SurveyReducer,
  repairsReducer: RepairsReducer,
  myTechsReducer: MyTechsReducer,
  reportingReducer: ReportingReducer
});
