const INITIAL_STATE = {
  "managerList": null,
  "location": null,
  "items": []
};

import {
  GET_MANAGER_STORE_REQUEST_SUCCESS,
  GET_MANAGER_STORE_REQUEST_FAIL,
  GET_STORE_REQUEST_START,
  SELECT_STORE_REQUEST,
  GET_STORE_REQUEST_SUCCESS,
  GET_STORE_REQUEST_FAIL,
  LOGOUT_USER_SUCCESS
} from '../types';

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case GET_MANAGER_STORE_REQUEST_SUCCESS:
      const requests = [];
      action.payload.forEach(function(location) {
        location.requests.forEach(function(request) {
          var locationObj = {};
          locationObj.Location = location.Location;
          locationObj.address = location.address;
          locationObj.city = location.city;
          locationObj.state = location.state;
          locationObj.zip = location.zip;
          locationObj.clientId = location.clientId;
          locationObj.phoneNumber = location.phoneNumber;

          request.location = locationObj;
          requests.push(request);
        })
      })
      console.log(requests);
      return { ...state, "managerList": requests }
    case GET_MANAGER_STORE_REQUEST_FAIL:
      return { ...state, "managerList": null}
    case SELECT_STORE_REQUEST:
      var allRequests = [];
      action.payload.requestproduct.forEach(function(product) {
        allRequests.push(product);
      });
      action.payload.requestservice.forEach(function(service) {
        allRequests.push(service);
      });
      console.log(action.payload.location);
      return { ...state, "location": action.payload.location, "items": allRequests }
    case GET_STORE_REQUEST_SUCCESS:
      var location = action.payload[0].location;
      var requestItems = [];
      action.payload.forEach(function(element) {
        element.requestproduct.forEach(function(product) {
          requestItems.push(product);
        })
      });
      console.log(requestItems);
      return { ...state, "location": location, "items": requestItems }
    case GET_STORE_REQUEST_FAIL:
      return { ...state, "location": null, "items": [] };
    case LOGOUT_USER_SUCCESS:
      return { ...INITIAL_STATE };
    default:
      return state;
  }
}
