const INITIAL_STATE ={
  "count": 0,
  "announcements": [],
  "error": false
};

import {
  GET_ANNOUNCEMENTS_SUCCESS,
  GET_ANNOUNCEMENTS_FAIL,
  LOGOUT_USER_SUCCESS
} from '../types';


export default (state = INITIAL_STATE,  action) => {

  switch(action.type) {
    case GET_ANNOUNCEMENTS_SUCCESS:
      return { ...state, "announcements": action.payload.rows, "error": false };
    case GET_ANNOUNCEMENTS_FAIL:
      return { ...INITIAL_STATE, "error": true };
    case LOGOUT_USER_SUCCESS:
      return { ...INITIAL_STATE };
    default:
      return state;
  }

}
