
import _ from 'lodash';

const INITIAL_STATE = {
  "messages": [],
  "chatmessages": [],
  "currentMessageText":"",
  "nameModalVisible": true,
  "noManager": false,
  "senderId": null,
  "receiverId":null,
  "quickMessage": false,
  "countOfPriorityMsgs": 0,
  "tech": null,
  "chatWithTech": false
};

import {
  MESSAGE_SENT,
  MESSAGE_RECEIVED,
  GET_CHAT_MESSAGES_SUCCESS,
  GET_CHAT_MESSAGES_FAIL,
  GET_LAST_CHAT_MESSAGES_SUCCESS,
  GET_LAST_CHAT_MESSAGES_FAIL,
  CONVERSATION_CLICKED,
  UPDATE_CHAT_READ_MESSAGE,
  MESSAGE_CHANGED,
  SEND_GENERATED_MESSAGE,
  CLEAR_MESSAGE_TEXT,
  LOGOUT_USER_SUCCESS,
  CLEAR_QUICK_MESSAGE,
  UPDATE_CHAT_PRIORITY_MESSAGE_COUNT,
  SET_TECH_CHAT
} from '../types';

export default (state = INITIAL_STATE, action) => {
  
  switch(action.type) {
    case MESSAGE_CHANGED:
      return { ...state, "currentMessageText": action.payload };
    case MESSAGE_SENT:
      return { ...state, "currentMessageText": "", "chatmessages" : action.payload.rows};
    case MESSAGE_RECEIVED:
    console.log( 'Mesasge received updating list ' + state.messages)
      return { ...state, "messages": state.messages.push(action.payload)};
    case GET_CHAT_MESSAGES_SUCCESS:
      return { ...state, "chatmessages": action.payload.rows};
    case GET_CHAT_MESSAGES_FAIL:
      return { ...INITIAL_STATE}
    case GET_LAST_CHAT_MESSAGES_SUCCESS:
      let counts = _.filter(action.payload, function(o) { return o.priority == 1; }).length
      return { ...state, "messages": action.payload,  "countOfPriorityMsgs" : counts};
    case GET_LAST_CHAT_MESSAGES_FAIL:
      return { ...INITIAL_STATE};
    case UPDATE_CHAT_READ_MESSAGE:
      return { ...state};
    case UPDATE_CHAT_PRIORITY_MESSAGE_COUNT:
      console.log('counts on messsage', counts);
      return  { ...state, "countOfPriorityMsgs" : counts};
    case SEND_GENERATED_MESSAGE:
      return { ...state, "currentMessageText": action.payload.message + action.payload.location, "quickMessage":true }
    case CLEAR_MESSAGE_TEXT:
      return { ...state, "currentMessageText": "" }
    case CLEAR_QUICK_MESSAGE:
      return { ...state,  "quickMessage":false , "chatWithTech": false}
    case LOGOUT_USER_SUCCESS:
      return { ...INITIAL_STATE };
    case SET_TECH_CHAT:
      return { ...state, "tech": action.payload.tech.tech, "chatWithTech":true }
    default:
      return state;
  }
}
