const INITIAL_STATE = {
  "userId": "",
  "password": "",
  "userIdError": "",
  "passwordError": "",
  "loginError": "",
  "isPasswordVisible": false,
  "fieldOpsAreas": []
}

import {
  LOGIN_ID_CHANGED,
  LOGIN_PASSWORD_CHANGED,
  LOGIN_USER_START,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_SHOW_PASSWORD,
  LOGIN_HIDE_PASSWORD,
  LOGOUT_USER_SUCCESS,
  GET_AREAS_SUCCESS,
  GET_AREAS_FAIL
} from '../types';

export default (state = INITIAL_STATE, action) => {

  switch(action.type) {
    case LOGIN_ID_CHANGED:
      var isUserIdError = (action.payload.length < 3 || action.payload.length > 100) ? true : false;
      return { ...state, "userId": action.payload, "userIdError": isUserIdError };
    case LOGIN_PASSWORD_CHANGED:
      var isPasswordError = (action.payload.length < 1 || action.payload.length > 100) ? true : false;
      return { ...state, "password": action.payload, "passwordError": isPasswordError };
    case LOGIN_USER_START:
      return{ ...state, "loginError": "" };
    case LOGIN_USER_SUCCESS:
      return { ...INITIAL_STATE };
    case LOGIN_USER_FAIL:
      return { ...state, "loginError": action.payload, "password": "" };
    case LOGIN_SHOW_PASSWORD:
      return { ...state, "isPasswordVisible": true };
    case LOGIN_HIDE_PASSWORD:
      return { ...state, "isPasswordVisible": false };
    case LOGOUT_USER_SUCCESS:
      return { ...INITIAL_STATE };
    case GET_AREAS_SUCCESS:
      return { ...state, "fieldOpsAreas": action.payload }
    case GET_AREAS_FAIL:
      return { ...state, "fieldOpsAreas": [] }
    default:
      return state;
  }
}
