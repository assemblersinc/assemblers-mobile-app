const INITIAL_STATE = {
  "isLoggedIn": false,
  "notificationToken": ""
};

import {
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  PERMISSIONS_COMPLETED,
  CLOCKED_IN_SUCCESS,
  CLOCKED_OUT_SUCCESS,
  ALREADY_CLOCKED_IN,
  CHECK_IN_SUBCONTRACTOR,
  CHECK_OUT_SUBCONTRACTOR,
  MEAL_IN_SUCCESS,
  MEAL_OUT_SUCCESS,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER_FAIL,
  POST_NOTIFICATION_TOKEN_SUCCESS,
  POST_NOTIFICATION_TOKEN_FAIL,
  MANAGER_STORE_FOUND,
  MANAGER_STORE_NOT_FOUND,
  FIELDOPS_AREA_SELECTED,
  UPDATE_LOCATION_TYPE
} from '../types';

export default (state = INITIAL_STATE, action) => {

  switch(action.type) {
    case LOGIN_USER_SUCCESS:
      const user = action.payload;
      console.log(action.payload);
      return {
        ...state,
        "areaId": user.areaId,
        "clockStatus": (user.clockStatus != null) ? user.clockStatus : 'clock-out',
        "mealStatus": (user.mealStatus != null) ? user.mealStatus : 'meal-out',
        "email": user.email,
        "firstName": user.firstName,
        "geoarea": user.geoarea,
        "lastName": user.lastName,
        "phone": user.phone,
        "role": user.role,
        "roleId": user.roleId,
        "techId": user.techId,
        "timeSheetId": user.timeSheetId,
        "claimedby" : user.claimedby,
        "timesheet": user.timesheet,
        "token": {
          "token": user.token,
          "refreshToken": user.refreshToken,
        },
        "userId": user.userId,
        "username": user.username,
      }
    case LOGIN_USER_FAIL:
      return { ...INITIAL_STATE };
    case PERMISSIONS_COMPLETED:
      return{ ...state, "isLoggedIn": true }
    case CLOCKED_IN_SUCCESS:
      return { ...state, "timesheet": action.payload }
    case CHECK_IN_SUBCONTRACTOR:
      let timesheetObj = { "location": action.payload, "locationId": action.payload.locationId };
      console.log(timesheetObj);
      return { ...state, "timesheet": timesheetObj }
    case ALREADY_CLOCKED_IN:
      return { ...state, "timesheet": action.payload }
    case CLOCKED_OUT_SUCCESS:
      return { ...state, "timesheet": null}
    case CHECK_OUT_SUBCONTRACTOR:
      return { ...state, "timesheet": null}
    case MEAL_IN_SUCCESS:
    case MEAL_OUT_SUCCESS:
      return { ...state, "timesheet": action.payload }
    case LOGOUT_USER_SUCCESS:
      return { ...INITIAL_STATE };
    case POST_NOTIFICATION_TOKEN_SUCCESS:
      return { ...state, "notificationToken": action.payload.notificationToken };
     case POST_NOTIFICATION_TOKEN_FAIL:
      return { ...state };
    case MANAGER_STORE_FOUND:
      const newTimesheet = { "locationId": action.payload.location[0].locationId, "location": action.payload.location[0] };
      return { ...state, "timesheet": newTimesheet }
    case UPDATE_LOCATION_TYPE:
      return { ...state, "locationType": action.payload }
    case MANAGER_STORE_NOT_FOUND:
      return { ...state, "timesheet": null }
    case FIELDOPS_AREA_SELECTED:
      return { ...state, "areaId": action.payload.area.areaId, "geoarea": action.payload.area, "region": action.payload.region }
    default:
      return state;
  }
}
