const INITIAL_STATE = {
  'name': null,
  'questions': []
};

import {

  GET_SURVEY_SUCCESS,
  GET_SURVEY_FAIL,
  SURVEY_QUESTION_ANSWERED,
  CLEAR_SURVEY_ANSWERS,
  LOGOUT_USER_SUCCESS,
  ADD_PRINTED_NAME
} from '../types.js';

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case GET_SURVEY_SUCCESS:
      return {...state, 'questions': action.payload};
    case GET_SURVEY_FAIL:
      return INITIAL_STATE;
    case SURVEY_QUESTION_ANSWERED:
      var newState = JSON.parse(JSON.stringify(state.questions));
      newState[action.payload.index].answer= action.payload.answer;
      return { ...state, 'questions': newState };
    case CLEAR_SURVEY_ANSWERS:
      var newState = JSON.parse(JSON.stringify(state.questions));
      for(var i = 0; i < newState.length; i++) {
        newState[i].answer = null;
      }
      return {...state, 'questions': newState};
    case LOGOUT_USER_SUCCESS:
      return INITIAL_STATE
    case ADD_PRINTED_NAME:
      return { ...state, 'name': action.payload };
    default:
      return state;
  }
}
