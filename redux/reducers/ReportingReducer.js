const INITIAL_STATE = {
  totals: {
    name: "TOTAL",
    serviceDays: 0,
    servicedOnDay: 0,
    servicedOnOther: 0,
    revenue: 0,
    requestedItems: 0,
    assembledItems: 0
  },
  selectedClient: null,
  clientTotals: [],
  error: ""
};

import {
  GET_REPORTS_FAIL,
  GET_REPORTS_SUCCESS,
  SELECT_CLIENT_REPORT
} from "../types";


export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_REPORTS_SUCCESS:
      action.payload.totals.name = "TOTAL";
      return {...state, totals: action.payload.totals, clientTotals: action.payload.clientTotals, error: ""};
    case GET_REPORTS_FAIL:
      return {...INITIAL_STATE, error: action.payload};
    case SELECT_CLIENT_REPORT:
      return {...state, selectedClient: action.payload};
    default:
      return state;
  }
}
