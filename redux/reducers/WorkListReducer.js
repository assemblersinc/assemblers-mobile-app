const INITIAL_STATE = {
  "workList": [],
  "error": "",
  "selectedItem": null
};

import {
  GET_WORKLIST_SUCCESS,
  GET_WORKLIST_FAIL,
  SELECT_WORKLIST_ITEM,
  LOGOUT_USER_SUCCESS
} from '../types';

export default (state = INITIAL_STATE, action) => {

  switch(action.type) {
    case GET_WORKLIST_SUCCESS:
      //make one list of items from all of the individual request objects
      action.payload.items.forEach(function(element) {
        element.isWorkList = true;
      })
      return { ...state, "workList": action.payload.items, "photos": action.payload.photos, "error": "" }
    case GET_WORKLIST_FAIL:
      return { ...INITIAL_STATE, "error": action.payload }
    case SELECT_WORKLIST_ITEM:
      return { ...state, "selectedItem": action.payload }
    case LOGOUT_USER_SUCCESS:
      return { ...INITIAL_STATE }
    default:
      return state;
  }

}
