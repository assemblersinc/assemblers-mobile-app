const INITIAL_STATE ={
  "clients": []
};

import {
  GET_CLIENTS_START,
  GET_CLIENTS_SUCCESS,
  GET_CLIENTS_FAIL,
  LOGOUT_USER_SUCCESS
} from '../types';


export default (state = INITIAL_STATE,  action) => {

  switch(action.type) {
    case GET_CLIENTS_SUCCESS:
      return { ...state, "clients": action.payload };
    case GET_CLIENTS_FAIL:
      return { ...INITIAL_STATE };
    case LOGOUT_USER_SUCCESS:
      return { ...INITIAL_STATE };
    default:
      return state;
  }

}
