const INITIAL_STATE = [];

import {
  GET_TIMESHEETS_START,
  GET_TIMESHEETS_SUCCESS,
  GET_TIMESHEETS_FAIL,
  LOGOUT_USER_SUCCESS
} from '../types';

export default (state = INITIAL_STATE, action) => {

  switch(action.type) {
    case GET_TIMESHEETS_SUCCESS:
      return action.payload;
    case GET_TIMESHEETS_FAIL:
      return INITIAL_STATE;
    case LOGOUT_USER_SUCCESS:
      return INITIAL_STATE;
    default:
      return state;
  }
}
