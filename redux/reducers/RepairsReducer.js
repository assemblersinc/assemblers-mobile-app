const INITIAL_STATE = {
  "managerList": null,
  "items": null,
  "product": null,
  "categories": [],
  "searchText": "",
  "selectedCategory": null,
  "photo": null,
  "comment": null,
  "selectedRepairTicket": null,
  "stickerSearchError": false
};

import {
  GET_MANAGER_REPAIRS_SUCCESS,
  GET_MANAGER_REPAIRS_FAIL,
  SELECT_REPAIR_LOCATION,
  GET_STORE_REPAIRS_SUCCESS,
  GET_STORE_REPAIRS_FAIL,
  GET_REPAIR_CATS_SUCCESS,
  GET_REPAIR_CATS_FAIL,
  CREATE_REPAIR_TICKET_SUCCESS,
  REPAIR_CAT_SEARCH_TEXT_CHANGED,
  REPAIR_PRODUCT_INFO_ADDED,
  CLEAR_REPAIR_PRODUCT_INFO,
  UPLOAD_REPAIR_PHOTO_SUCCESS,
  REPAIR_NOTE_FINISHED,
  REPAIR_CAT_SELECTED,
  REPAIR_TICKET_SELECTED,
  GET_REPAIR_SKUSCAN_SUCCESS,
  GET_REPAIR_SKUSCAN_FAIL,
  UPDATE_REPAIR_TICKET_SUCCESS,
  REMOVE_REPAIR_SUCCESS,
  ADD_REPAIR_TO_INVOICE_SUCCESS,
  CLEAR_STICKER_ERROR,
  LOGOUT_USER_SUCCESS
} from '../types';

export default (state = INITIAL_STATE, action) => {

  switch(action.type) {
    case GET_MANAGER_REPAIRS_SUCCESS:
      return { ...state, "managerList": action.payload }
    case GET_MANAGER_REPAIRS_FAIL:
      return { ...state, "managerList": null }
    case SELECT_REPAIR_LOCATION:
      console.log(action.payload);
      var repairLocation = action.payload.repairs;
      var locationObj = {};
      locationObj.Location = action.payload.Location;
      locationObj.address = action.payload.address;
      locationObj.city = action.payload.city;
      locationObj.state = action.payload.state;
      locationObj.zip = action.payload.zip;

      return { ...state, "items": repairLocation, "location": locationObj }
    case GET_STORE_REPAIRS_SUCCESS:
      return { ...state, "items": action.payload }
    case GET_STORE_REPAIRS_FAIL:
      return { ...state, "items": null }
    case GET_REPAIR_CATS_SUCCESS:
      return { ...state, "categories": action.payload }
    case GET_REPAIR_CATS_FAIL:
      return { ...state, "categories": [] }
    case REPAIR_PRODUCT_INFO_ADDED:
      return { ...state, "product": action.payload }
    case CLEAR_REPAIR_PRODUCT_INFO:
      return { ...INITIAL_STATE }
    case CREATE_REPAIR_TICKET_SUCCESS:
      return { ...INITIAL_STATE }
    case REPAIR_CAT_SEARCH_TEXT_CHANGED:
      return { ...state, "searchText": action.payload };
    case REPAIR_CAT_SELECTED:
      return { ...state, "selectedCategory": action.payload };
    case UPLOAD_REPAIR_PHOTO_SUCCESS:
      return { ...state, "photo": action.payload }
    case REPAIR_NOTE_FINISHED:
      return { ...state, "comment": action.payload }
    case REPAIR_TICKET_SELECTED:
      return { ...state, "selectedRepairTicket": action.payload }
    case GET_REPAIR_SKUSCAN_SUCCESS:
      return { ...state, "stickerSearchError": false, "selectedRepairTicket": action.payload }
    case GET_REPAIR_SKUSCAN_FAIL:
      return { ...state, "stickerSearchError": true }
    case CLEAR_STICKER_ERROR:
      return { ...state, "stickerSearchError": false }
    case UPDATE_REPAIR_TICKET_SUCCESS:
      return { ...state, "selectedRepairTicket": action.payload }
    case REMOVE_REPAIR_SUCCESS:
      return { ...state, "selectedRepairTicket": action.payload }
    case ADD_REPAIR_TO_INVOICE_SUCCESS:
      return { ...state, "selectedRepairTicket": action.payload }
    case LOGOUT_USER_SUCCESS:
      return { ...INITIAL_STATE }
    default:
      return state;
  }
}
