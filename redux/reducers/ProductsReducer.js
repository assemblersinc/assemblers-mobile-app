const INITIAL_STATE = {
  "results": [],
  "searchText": "",
  "error": ""
};

import {
  SKU_TEXT_CHANGED,
  SEARCH_SKU_START,
  SEARCH_SKU_SUCCESS,
  SEARCH_SKU_FAIL,
  LOGOUT_USER_SUCCESS
} from '../types';

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case SKU_TEXT_CHANGED:
      console.log(action.payload);
      return { ...state, "searchText": action.payload }
    case SEARCH_SKU_START:
      return { ...state, "searchText": action.payload }
    case SEARCH_SKU_SUCCESS:
      return { ...state, "results": action.payload }
    case SEARCH_SKU_FAIL:
      return { ...state, "results": [], error: action.payload }
    case LOGOUT_USER_SUCCESS:
      return { ...INITIAL_STATE }
    default:
      return state;
  }

}
