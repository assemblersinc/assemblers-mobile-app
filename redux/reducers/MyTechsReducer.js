const INITIAL_STATE = {
    "myTechs": null,
    "allTechs": null,
    "areaTechs": null
};

import {
  GET_TECHS_SUCCESS,
  GET_TECHS_FAIL,
  GET_ALL_TECHS_SUCCESS,
  GET_ALL_TECHS_FAIL,
  GET_AREA_TECHS_SUCCESS,
  GET_AREA_TECHS_FAIL,
  LOGOUT_USER_SUCCESS
} from '../types';

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case GET_TECHS_SUCCESS:
      return { ...state, "myTechs": action.payload }
    case GET_TECHS_FAIL:
      return { ...state, "myTechs": null }
    case GET_ALL_TECHS_SUCCESS:
      return { ...state, "allTechs": action.payload.unclaimtechs }
    case GET_ALL_TECHS_FAIL:
      return { ...state, "allTechs": null }
    case GET_AREA_TECHS_SUCCESS:
      var clockedIn = [];
      var clockedOut = [];

      action.payload.forEach(function(tech) {
        if(tech.clockStatus && tech.clockStatus == 'clock-in') {
          clockedIn.push(tech);
        } else {
          clockedOut.push(tech);
        }
      })
      return { ...state, "areaTechs": { "clockedin": clockedIn, "clockedout": clockedOut } }
    case GET_AREA_TECHS_FAIL:
      return { ...state, "areaTechs": null }
    default:
      return state;
  }
}
