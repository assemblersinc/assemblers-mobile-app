const INITIAL_STATE = {
	"count": 0,
	"invoices": [],
	"masterList": null
};

import {
	GET_MANAGER_INVOICES_START,
  GET_MANAGER_INVOICES_SUCCESS,
  GET_MANAGER_INVOICES_FAIL,
	GET_INVOICE_HISTORY_SUCCESS,
  GET_INVOICE_HISTORY_FAIL,
	LOGOUT_USER_SUCCESS
} from '../types';


export default (state = INITIAL_STATE, action) => {

	switch(action.type) {
		case GET_MANAGER_INVOICES_SUCCESS:
			return { ...state, "managerList": action.payload }
		case GET_MANAGER_INVOICES_FAIL:
			return { ...state, "managerList": null }
		case GET_INVOICE_HISTORY_SUCCESS:
			return { ...state, "invoices": action.payload.rows };
		case GET_INVOICE_HISTORY_FAIL:
			return { ...INITIAL_STATE };
		case LOGOUT_USER_SUCCESS:
			return { ...INITIAL_STATE };
		default:
			return state;
	}

};
