// import data from './data/StoreWalkSampleData.json';
//
// export default () => data;

const INITIAL_STATE = {
  "storeWalk": {},
  "error": false
};

import {
  GET_STOREWALK_SUCCESS,
  GET_STOREWALK_FAIL,
  START_STOREWALK_SUCCESS,
  LOGOUT_USER_SUCCESS
} from '../types';

export default (state = INITIAL_STATE, action) => {

  switch(action.type) {
    case GET_STOREWALK_SUCCESS:
      if(action.payload) {
        var storeWalk = action.payload;
        var productsServices;

        if(storeWalk.requestproduct && storeWalk.requestservice && storeWalk.requestproduct.length > 0 && storeWalk.requestservice.length > 0) {
          var mergedResults = [];
          storeWalk.requestproduct.forEach(function(element) {
            mergedResults.push(element);
          });
          storeWalk.requestservice.forEach(function(element) {
            mergedResults.push(element);
          });
          productsServices = mergedResults;
        } else if(storeWalk.requestproduct && storeWalk.requestproduct.length > 0) {
          productsServices = storeWalk.requestproduct;
        } else if(storeWalk.requestservice && storeWalk.requestservice.length > 0) {
          productsServices = storeWalk.requestservice;
        } else {
          productsServices = [];
        }
        storeWalk.productsServices = productsServices;
        return { ...state, "storeWalk": storeWalk, "error": false }
      } else return { ...INITIAL_STATE }
    case GET_STOREWALK_FAIL:
      return { ...INITIAL_STATE, "error": true }
    case START_STOREWALK_SUCCESS:
      return { ...state, "storeWalk": action.payload }
    case LOGOUT_USER_SUCCESS:
      return { ...INITIAL_STATE }
    default:
      return state;
  }

}
