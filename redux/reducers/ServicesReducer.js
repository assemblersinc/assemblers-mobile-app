const INITIAL_STATE = {
  "services": [],
  "searchResults": [],
  "searchText": "",
  "error": ""
};

import {
  GET_SERVICES_LIST_SUCCESS,
  GET_SERVICES_LIST_FAIL,
  SEARCH_SERVICES,
  LOGOUT_USER_SUCCESS
} from '../types';

export default (state = INITIAL_STATE, action) => {

  switch(action.type) {
    case GET_SERVICES_LIST_SUCCESS:
      return { ...state, "services": action.payload, "searchResults": action.payload, "error": "" }
    case GET_SERVICES_LIST_FAIL:
      return { ...INITIAL_STATE, "error": action.payload }
    case SEARCH_SERVICES:
      //if they don't enter anything, show the whole list
      const searchText = action.payload;
      if(searchText == '') {
        return { ...state, "searchText": "", "searchResults": JSON.parse(JSON.stringify(state.services)) }
      } else {
        //otherwise search for their text inside the service name
        var matches = [];
        JSON.parse(JSON.stringify(state)).services.forEach(function(element) {
          if(element.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1) matches.push(element);
        });

        return { ...state, "searchText": searchText, "searchResults": matches }
      }
    case LOGOUT_USER_SUCCESS:
      return { ...INITIAL_STATE }
    default:
      return state;
  }

}
