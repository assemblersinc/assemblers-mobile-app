const INITIAL_STATE = {
  "isClockedIn": false,
  "isOnMeal": false,
  "selectedStore": {},
  "storeNumberText": "",
  "cityText": "",
  "stateText": "",
  "zipText": "",
  "searchResults": [],
  "error": null,
  "origin" : null,
  "needsUpdate": false,
  "hasValidation": false
}

import {
  ENTER_CLOCK_IN_FLOW,
  LOGIN_USER_SUCCESS,
  EXIT_CLOCK_IN_FLOW,
  OPEN_CLOCK_IN_PROMPT,
  CLOCK_IN_STORE_NAME_CHANGED,
  CLOCK_IN_STORE_NUMBER_CHANGED,
  CURRENT_STORE_SEARCH_START,
  CURRENT_STORE_SEARCH_SUCCESS,
  CURRENT_STORE_SEARCH_FAIL,
  CLOCKED_IN_SUCCESS,
  CLOCKED_IN_FAIL,
  CLOCKED_OUT_SUCCESS,
  CLOCKED_OUT_FAIL,
  STAY_CLOCKED_IN,
  MEAL_IN_SUCCESS,
  MEAL_IN_FAIL,
  MEAL_OUT_SUCCESS,
  MEAL_OUT_FAIL,
  SELECT_CLIENT,
  SELECT_STORE,
  GET_CLOCKIN_LOCATION_SUCCESS,
  GET_CLOCKIN_LOCATION_FAIL,
  SET_CLOCKIN_ERROR,
  UPDATE_CITY_TEXT,
  UPDATE_STATE_TEXT,
  UPDATE_ZIP_TEXT,
  STORE_SEARCH_SUCCESS,
  STORE_SEARCH_FAIL,
  CHECK_IN_SUBCONTRACTOR,
  CHECK_OUT_SUBCONTRACTOR,
  CLEAR_NEEDS_UPDATE,
  CLEAR_SELECTED_STORE,
  TRAVEL_STORE_START,
  TRAVEL_STORE_SUCCESS,
  TRAVEL_STORE_FAIL,
  GRILLBARROW_SUBMIT_SUCCESS,
  LOGOUT_USER_SUCCESS
} from '../types';

export default (state = INITIAL_STATE, action) => {

  switch(action.type) {
    case ENTER_CLOCK_IN_FLOW:
      return { ...state, "origin": action.payload }
    case LOGIN_USER_SUCCESS:
      const user = action.payload;
      const clockStatus = (user.clockStatus && user.clockStatus == 'clock-in') ? true : false;
      const mealStatus = (user.mealStatus && user.mealStatus == 'meal-in') ? true : false;
      return { ...state, "isClockedIn": clockStatus, "isOnMeal": mealStatus }
    case LOGOUT_USER_SUCCESS:
      return { ...INITIAL_STATE }
    case OPEN_CLOCK_IN_PROMPT:
      return { ... state, "nearbyStore": action.payload }
    case EXIT_CLOCK_IN_FLOW:
      return { ...state, "storeNameText": '', "storeNumberText": '', "error": null }
    case CLOCK_IN_STORE_NAME_CHANGED:
      return { ...state, "storeNameText": action.payload, "hasValidation": false }
    case CLOCK_IN_STORE_NUMBER_CHANGED:
      return { ... state, "storeNumberText": action.payload, "hasValidation": false }
    case CURRENT_STORE_SEARCH_SUCCESS:
      return { ...state, "selectedStore": action.payload, "storeNumberText": action.payload.location[0].storeNumber, "error": null, "hasValidation": true }
    case CURRENT_STORE_SEARCH_FAIL:
      return { ...state, "selectedStore": {}, "hasValidation": false }
    case CLOCKED_IN_SUCCESS:
      return { ...state, "isClockInFlow": false, "isClockedIn": true, "selectedStore": {}, "storeNumberText": "", "needsUpdate": true, "error": null }
    case CLOCKED_IN_FAIL:
      return { ...state, "isClockedIn": false, error: action.payload }
    case CLOCKED_OUT_SUCCESS:
      return { ...INITIAL_STATE, "needsUpdate": true, "isClockedIn": false, "error": null }
    case CLOCKED_OUT_FAIL:
      return{ ...state, "isClockedIn": true }
    case STAY_CLOCKED_IN:
      return { ...state, "isClockedIn": true }
    case MEAL_OUT_SUCCESS:
      return { ...state, "isOnMeal": true, "needsUpdate": true, }
    case MEAL_OUT_FAIL:
      return { ...state, "isOnMeal": false }
    case MEAL_IN_SUCCESS:
      return { ...state, "isOnMeal": false, "needsUpdate": true, }
    case MEAL_IN_FAIL:
      return { ...state, "isOnMeal": true }
    case SELECT_CLIENT:
      return { ...state, "selectedStore": action.payload, "storeNumberText": "", "error": null, "hasValidation": false }
    case SELECT_STORE:
      return { ...state, "selectedStore": action.payload.client, "storeNumberText": action.payload.storeNumber, "error": null, "hasValidation": true }
    case GET_CLOCKIN_LOCATION_SUCCESS:
      return { ...state, "selectedStore": action.payload, "storeNumberText": action.payload.location[0].storeNumber, "error": null, "hasValidation": true }
    case GET_CLOCKIN_LOCATION_FAIL:
      return { ...state, "storeNumberText": '', "hasValidation": false }
    case SET_CLOCKIN_ERROR:
      return { ...state, "error": action.payload }
    case UPDATE_CITY_TEXT:
      return { ...state, "cityText": action.payload }
    case UPDATE_STATE_TEXT:
      return { ...state, "stateText": action.payload }
    case UPDATE_ZIP_TEXT:
      return { ...state, "zipText": action.payload }
    case STORE_SEARCH_SUCCESS:
      return { ...state, "searchResults": action.payload }
    case STORE_SEARCH_FAIL:
      return { ...state, "searchResults": [], "error": action.payload }
    case CHECK_IN_SUBCONTRACTOR:
      return { ...state, "isClockedIn": true, "selectedStore": {}, "storeNameText": "", "storeNumberText": "", "needsUpdate": true }
    case CHECK_OUT_SUBCONTRACTOR:
      return { ...state, "isClockedIn": false }
    case GRILLBARROW_SUBMIT_SUCCESS:
      return { ...state, "needsUpdate": true }
    case CLEAR_NEEDS_UPDATE:
      return { ...state, "needsUpdate": false }
    case CLEAR_SELECTED_STORE:
      return { ...state, "selectedStore": {}, "storeNumberText": "", "error": null}
    default:
      return state;
  }
}
