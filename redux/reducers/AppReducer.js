const INITIAL_STATE = {
  "isLoading": false,
  "bannerVisible": false,
  "bannerText": null,
  "bannerColor": null,
  "bannerTextColor": null,
  "isClockInFlow": false,
  "clockInPromptModalVisible": false,
  "clockOutPromptModalVisible": false,
  "leftStoreModalVisible": false,
  "inactivityModalVisible": false,
  "grillbarrowModalVisible": false,
  "areaManagerLocationModalVisible": false,
  "foundLocation": null,
  "fieldOpsModalVisible": false,
  "errorModalVisible": false,
  "errorText": "",
  "chatMessageCount":0,
};

import {
  LOGIN_USER_START,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGOUT_USER_START,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER_FAIL,
  SHOW_CONNECTIVITY_BANNER,
  HIDE_CONNECTIVITY_BANNER,
  SHOW_FIELDOPS_MODAL,
  HIDE_FIELDOPS_MODAL,
  CURRENT_STORE_SEARCH_START,
  CURRENT_STORE_SEARCH_SUCCESS,
  CURRENT_STORE_SEARCH_FAIL,
  SHOW_AREA_MANAGER_LOCATION_MODAL,
  HIDE_AREA_MANAGER_LOCATION_MODAL,
  ENTER_CLOCK_IN_FLOW,
  EXIT_CLOCK_IN_FLOW,
  OPEN_CLOCK_IN_PROMPT,
  CLOSE_CLOCK_IN_PROMPT,
  OPEN_CLOCK_OUT_PROMPT,
  CLOSE_CLOCK_OUT_PROMPT,
  CLOCK_IN_START,
  CLOCKED_IN_SUCCESS,
  CLOCKED_IN_FAIL,
  SHOW_CLOCKIN_LEFT_STORE_MODAL,
  HIDE_CLOCKIN_LEFT_STORE_MODAL,
  SHOW_INACTIVITY_MODAL,
  HIDE_INACTIVITY_MODAL,
  CLOCK_OUT_START,
  CLOCKED_OUT_SUCCESS,
  CLOCKED_OUT_FAIL,
  STAY_CLOCKED_IN,
  CHECK_IN_SUBCONTRACTOR,
  CHECK_OUT_SUBCONTRACTOR,
  TRAVEL_STORE_START,
  TRAVEL_STORE_FAIL,
  MEAL_IN_START,
  MEAL_IN_SUCCESS,
  MEAL_IN_FAIL,
  MEAL_OUT_START,
  MEAL_OUT_SUCCESS,
  MEAL_OUT_FAIL,
  GET_CLOCKIN_LOCATION_START,
  GET_CLOCKIN_LOCATION_SUCCESS,
  GET_CLOCKIN_LOCATION_FAIL,
  GET_CLIENTS_START,
  GET_CLIENTS_SUCCESS,
  GET_CLIENTS_FAIL,
  STORE_SEARCH_START,
  STORE_SEARCH_SUCCESS,
  STORE_SEARCH_FAIL,
  FIND_MANAGER_STORE_START,
  MANAGER_STORE_FOUND,
  MANAGER_STORE_NOT_FOUND,
  GET_TIMESHEETS_START,
  GET_TIMESHEETS_SUCCESS,
  GET_TIMESHEETS_FAIL,
  GET_ANNOUNCEMENTS_START,
  GET_ANNOUNCEMENTS_SUCCESS,
  GET_ANNOUNCEMENTS_FAIL,
  GET_CHAT_MESSAGES_START,
  GET_CHAT_MESSAGES_SUCCESS,
  GET_CHAT_MESSAGES_FAIL,
  UPDATE_CHAT_MESSAGE_COUNT,
  GET_LAST_CHAT_MESSAGES_START,
  GET_LAST_CHAT_MESSAGES_SUCCESS,
  GET_LAST_CHAT_MESSAGES_FAIL,
  GET_MANAGER_STORE_REQUEST_START,
  GET_MANAGER_STORE_REQUEST_SUCCESS,
  GET_MANAGER_STORE_REQUEST_FAIL,
  GET_STORE_REQUEST_START,
  GET_STORE_REQUEST_SUCCESS,
  GET_STORE_REQUEST_FAIL,
  GET_WORKLIST_START,
  GET_WORKLIST_SUCCESS,
  GET_WORKLIST_FAIL,
  SEARCH_WORKLIST_SERVICE_START,
  SEARCH_WORKLIST_SERVICE_SUCCESS,
  SEARCH_WORKLIST_SERVICE_FAIL,
  SEARCH_WORKLIST_SKU_START,
  SEARCH_WORKLIST_SKU_SUCCESS,
  SEARCH_WORKLIST_SKU_FAIL,
  ADD_TO_INVOICE_START,
  ADD_TO_INVOICE_SUCCESS,
  ADD_TO_INVOICE_FAIL,
  GET_SERVICES_LIST_START,
  GET_SERVICES_LIST_SUCCESS,
  GET_SERVICES_LIST_FAIL,
  SEARCH_SKU_START,
  SEARCH_SKU_SUCCESS,
  SEARCH_SKU_FAIL,
  GET_SCANSKU_START,
  GET_SCANSKU_SUCCESS,
  GET_SCANSKU_FAIL,
  SEND_PRICING_REQUEST_START,
  SEND_PRICING_REQUEST_SUCCESS,
  SEND_PRICING_REQUEST_FAIL,
  GRILLBARROW_SUBMIT_START,
  GRILLBARROW_SUBMIT_SUCCESS,
  GRILLBARROW_SUBMIT_FAIL,
  SHOW_GRILLBARROW_MODAL,
  HIDE_GRILLBARROW_MODAL,
  START_STOREWALK_START,
  START_STOREWALK_SUCCESS,
  START_STOREWALK_FAIL,
  GET_STOREWALK_START,
  GET_STOREWALK_SUCCESS,
  GET_STOREWALK_FAIL,
  ADD_STOREWALK_ITEM_START,
  ADD_STOREWALK_ITEM_SUCCESS,
  ADD_STOREWALK_ITEM_FAIL,
  UPLOAD_STOREWALK_PHOTO_START,
  UPLOAD_STOREWALK_PHOTO_SUCCESS,
  UPLOAD_STOREWALK_PHOTO_FAIL,
  UPLOAD_STOREWALK_NOTE_START,
  UPLOAD_STOREWALK_NOTE_SUCCESS,
  UPLOAD_STOREWALK_NOTE_FAIL,
  UPDATE_STOREWALK_ITEM_QTY_START,
  UPDATE_STOREWALK_ITEM_QTY_SUCCESS,
  UPDATE_STOREWALK_ITEM_QTY_FAIL,
  SUBMIT_STOREWALK_START,
  SUBMIT_STOREWALK_SUCCESS,
  SUBMIT_STOREWALK_FAIL,
  GET_INVOICE_HISTORY_START,
  GET_INVOICE_HISTORY_SUCCESS,
  GET_INVOICE_HISTORY_FAIL,
  GET_SURVEY_START,
  GET_SURVEY_SUCCESS,
  GET_SURVEY_FAIL,
  GET_MANAGER_REPAIRS_START,
  GET_MANAGER_REPAIRS_SUCCESS,
  GET_MANAGER_REPAIRS_FAIL,
  GET_STORE_REPAIRS_START,
  GET_STORE_REPAIRS_SUCCESS,
  GET_STORE_REPAIRS_FAIL,
  GET_REPAIR_CATS_START,
  GET_REPAIR_CATS_SUCCESS,
  GET_REPAIR_CATS_FAIL,
  GET_REPAIR_SKUSCAN_START,
  GET_REPAIR_SKUSCAN_SUCCESS,
  GET_REPAIR_SKUSCAN_FAIL,
  UPLOAD_REPAIR_PHOTO_START,
  UPLOAD_REPAIR_PHOTO_SUCCESS,
  UPLOAD_REPAIR_PHOTO_FAIL,
  CREATE_REPAIR_TICKET_START,
  CREATE_REPAIR_TICKET_SUCCESS,
  CREATE_REPAIR_TICKET_FAIL,
  UPDATE_REPAIR_TICKET_START,
  UPDATE_REPAIR_TICKET_SUCCESS,
  UPDATE_REPAIR_TICKET_FAIL,
  REMOVE_REPAIR_START,
  REMOVE_REPAIR_SUCCESS,
  REMOVE_REPAIR_FAIL,
  ADD_REPAIR_TO_INVOICE_START,
  ADD_REPAIR_TO_INVOICE_SUCCESS,
  ADD_REPAIR_TO_INVOICE_FAIL,
  GET_MANAGER_INVOICES_START,
  GET_MANAGER_INVOICES_SUCCESS,
  GET_MANAGER_INVOICES_FAIL,
  GET_INVOICES_START,
  GET_INVOICES_SUCCESS,
  GET_INVOICES_FAIL,
  ADD_PO_START,
  ADD_PO_SUCCESS,
  ADD_PO_FAIL,
  ADD_KEYREC_START,
  ADD_KEYREC_SUCCESS,
  ADD_KEYREC_FAIL,
  ADD_STAMP_START,
  ADD_STAMP_SUCCESS,
  ADD_STAMP_FAIL,
  SUBMIT_INVOICES_START,
  SUBMIT_INVOICES_SUCCESS,
  SUBMIT_INVOICES_FAIL,
  SEND_SURVEY_WITH_SIGNATURE_START,
  SEND_SURVEY_WITH_SIGNATURE_SUCCESS,
  SEND_SURVEY_WITH_SIGNATURE_FAIL,
  DECREASE_INVOICE_ITEM_QTY_START,
  DECREASE_INVOICE_ITEM_QTY_SUCCESS,
  DECREASE_INVOICE_ITEM_QTY_FAIL,
  GET_TECHS_START,
  GET_TECHS_SUCCESS,
  GET_TECHS_FAIL,
  GET_ALL_TECHS_START,
  GET_ALL_TECHS_SUCCESS,
  GET_ALL_TECHS_FAIL,
  GET_AREA_TECHS_START,
  GET_AREA_TECHS_SUCCESS,
  GET_AREA_TECHS_FAIL,
  CLAIM_TECH_START,
  CLAIM_TECH_SUCCESS,
  CLAIM_TECH_FAIL,
  DELETE_TECH_START,
  DELETE_TECH_SUCCESS,
  DELETE_TECH_FAIL,
  GET_REPORTS_START,
  GET_REPORTS_SUCCESS,
  GET_REPORTS_FAIL
} from '../types';

export default (state = INITIAL_STATE, action) => {

  console.log(action);

  switch(action.type) {
    case LOGIN_USER_START:
      return{ ...state, "isLoading": true };
    case LOGIN_USER_SUCCESS:
      return { ...state, "isLoading": false };
    case LOGIN_USER_FAIL:
      return { ...state, "isLoading": false };
    case LOGOUT_USER_START:
      return { ...state, "isLoading": true };
    case LOGOUT_USER_SUCCESS:
      return { ...INITIAL_STATE};
    case LOGOUT_USER_FAIL:
      return { ...state, "isLoading": false, "error": action.payload }
    case SHOW_CONNECTIVITY_BANNER:
      return { ...state, "bannerVisible": true, "bannerText": action.payload.text, "bannerColor": action.payload.color, "bannerTextColor": action.payload.textColor }
    case HIDE_CONNECTIVITY_BANNER:
      return { ...state, "bannerVisible": false, "bannerText": null, "bannerColor": null, "bannerTextColor": null }
    case SHOW_FIELDOPS_MODAL:
      return { ...state, "fieldOpsModalVisible": true }
    case HIDE_FIELDOPS_MODAL:
      return { ...state, "fieldOpsModalVisible": false }
    case CURRENT_STORE_SEARCH_START:
      return { ...state, "isLoading": true }
    case CURRENT_STORE_SEARCH_SUCCESS:
      return { ...state, "isLoading": false }
    case CURRENT_STORE_SEARCH_FAIL:
      return { ...state, "isLoading": false}
    case SHOW_AREA_MANAGER_LOCATION_MODAL:
      return { ...state, "areaManagerLocationModalVisible": true, "foundLocation": action.payload }
    case HIDE_AREA_MANAGER_LOCATION_MODAL:
      return { ...state, "areaManagerLocationModalVisible": false, "foundLocation": null }
    case ENTER_CLOCK_IN_FLOW:
      return { ...state, "isClockInFlow": true }
    case EXIT_CLOCK_IN_FLOW:
      return { ...state, "isClockInFlow": false }
    case CLOCK_IN_START:
      return { ...state, "isLoading": true }
    case CLOCKED_IN_SUCCESS:
      return { ...state, "isLoading": false,  "clockInPromptModalVisible": false }
    case CLOCKED_IN_FAIL:
      return { ...state, "isLoading": false }
    case CHECK_IN_SUBCONTRACTOR:
      return { ...state, "isClockInFlow": false }
    case CHECK_OUT_SUBCONTRACTOR:
      return { ...state, "isClockInFlow": false,  "clockOutPromptModalVisible": false }
    case TRAVEL_STORE_START:
      return { ...state, "isLoading": true}
    case TRAVEL_STORE_FAIL:
      return { ...state, "isLoading": false }
    case SHOW_CLOCKIN_LEFT_STORE_MODAL:
      return { ...state, "leftStoreModalVisible": true }
    case HIDE_CLOCKIN_LEFT_STORE_MODAL:
      return { ...state, "leftStoreModalVisible": false }
    case SHOW_INACTIVITY_MODAL:
      return { ...state, "inactivityModalVisible": true }
    case HIDE_INACTIVITY_MODAL:
      return { ...state, "inactivityModalVisible": false }
    case CLOCKED_OUT_SUCCESS:
      return { ...state, "isLoading": false, "isClockInFlow": false,  "clockOutPromptModalVisible": false }
    case CLOCKED_OUT_FAIL:
      return { ...state, "isLoading": false }
    case CLOCK_OUT_START:
      return { ...state, "isLoading": true }
    case OPEN_CLOCK_IN_PROMPT:
      return { ... state, "clockInPromptModalVisible": true }
    case CLOSE_CLOCK_IN_PROMPT:
      return { ...state, "clockInPromptModalVisible": false }
    case OPEN_CLOCK_OUT_PROMPT:
      return { ...state, "clockOutPromptModalVisible": true }
    case CLOSE_CLOCK_OUT_PROMPT:
      return { ...state, "clockOutPromptModalVisible": false }
    case STAY_CLOCKED_IN:
      return { ...state, "isClockInFlow": false }
    case GET_CLOCKIN_LOCATION_START:
      return { ...state, "isLoading": true }
    case GET_CLOCKIN_LOCATION_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_CLOCKIN_LOCATION_FAIL:
      return { ...state, "isLoading": false }
    case MEAL_IN_START:
      return { ...state, "isLoading": true }
    case MEAL_IN_SUCCESS:
      return { ...state, "isLoading": false }
    case MEAL_IN_FAIL:
      return { ...state, "isLoading": false }
    case MEAL_OUT_START:
      return { ...state, "isLoading": true }
    case MEAL_OUT_SUCCESS:
      return { ...state, "isLoading": false }
    case MEAL_OUT_FAIL:
      return { ...state, "isLoading": false }
    case GET_CLIENTS_START:
      return { ...state, "isLoading": true }
    case GET_CLIENTS_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_CLIENTS_FAIL:
      return { ...state, "isLoading": false }
    case STORE_SEARCH_START:
      return { ...state, "isLoading": true }
    case STORE_SEARCH_SUCCESS:
      return { ...state, "isLoading": false }
    case STORE_SEARCH_FAIL:
      return { ...state, "isLoading": false }
    case GET_ANNOUNCEMENTS_START:
      return { ...state, "isLoading": true }
    case GET_ANNOUNCEMENTS_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_ANNOUNCEMENTS_FAIL:
      return { ...state, "isLoading": false }
    case GET_CHAT_MESSAGES_START:
      return { ...state, "isLoading": true }
    case GET_CHAT_MESSAGES_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_CHAT_MESSAGES_FAIL:
      return { ...state, "isLoading": false }
    case GET_LAST_CHAT_MESSAGES_START:
      return { ...state, "isLoading": true }
    case GET_LAST_CHAT_MESSAGES_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_LAST_CHAT_MESSAGES_FAIL:
      return { ...state, "isLoading": false }
    case GET_TIMESHEETS_START:
      return { ...state, "isLoading": true }
    case GET_TIMESHEETS_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_TIMESHEETS_FAIL:
      return { ...state, "isLoading": false }
    case GET_MANAGER_STORE_REQUEST_START:
      return { ...state, "isLoading": true }
    case GET_MANAGER_STORE_REQUEST_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_MANAGER_STORE_REQUEST_FAIL:
      return { ...state, "isLoading": false }
    case GET_STORE_REQUEST_START:
      return { ...state, "isLoading": true }
    case GET_STORE_REQUEST_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_STORE_REQUEST_FAIL:
      return { ...state, "isLoading": false }
    case GET_WORKLIST_START:
      return { ...state, "isLoading": true }
    case GET_WORKLIST_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_WORKLIST_FAIL:
      return { ...state, "isLoading": false }
    case ADD_TO_INVOICE_START:
      return { ...state, "isLoading": true }
    case ADD_TO_INVOICE_SUCCESS:
      return { ...state, "isLoading": false }
    case ADD_TO_INVOICE_FAIL:
      return { ...state, "isLoading": false }
    case GET_SERVICES_LIST_START:
      return { ...state, "isLoading": true }
    case GET_SERVICES_LIST_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_SERVICES_LIST_FAIL:
      return { ...state, "isLoading": false }
    case GET_SCANSKU_START:
      return { ...state, "isLoading": true }
    case GET_SCANSKU_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_SCANSKU_FAIL:
      return { ...state, "isLoading": false }
    case SEND_PRICING_REQUEST_START:
      return { ...state, "isLoading": true }
    case SEND_PRICING_REQUEST_SUCCESS:
      return { ...state, "isLoading": false }
    case SEND_PRICING_REQUEST_FAIL:
      return { ...state, "isLoading": false }
    case GRILLBARROW_SUBMIT_START:
      return { ...state, "isLoading": true }
    case GRILLBARROW_SUBMIT_SUCCESS:
      return { ...state, "isLoading": false }
    case GRILLBARROW_SUBMIT_FAIL:
      return { ...state, "isLoading": false }
    case SHOW_GRILLBARROW_MODAL:
      return { ... state, "grillbarrowModalVisible": true }
    case HIDE_GRILLBARROW_MODAL:
      return { ... state, "grillbarrowModalVisible": false }
    case START_STOREWALK_START:
      return { ...state, "isLoading": true }
    case START_STOREWALK_SUCCESS:
      return { ...state, "isLoading": false }
    case START_STOREWALK_FAIL:
      return { ...state, "isLoading": false }
    case GET_STOREWALK_START:
      return { ...state, "isLoading": true }
    case GET_STOREWALK_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_STOREWALK_FAIL:
      return { ...state, "isLoading": false }
    case ADD_STOREWALK_ITEM_START:
        return { ...state, "isLoading": true }
    case ADD_STOREWALK_ITEM_SUCCESS:
        return { ...state, "isLoading": false }
    case ADD_STOREWALK_ITEM_FAIL:
        return { ...state, "isLoading": false }
    case UPDATE_STOREWALK_ITEM_QTY_START:
        return { ...state, "isLoading": true }
    case UPDATE_STOREWALK_ITEM_QTY_SUCCESS:
        return { ...state, "isLoading": false }
    case UPDATE_STOREWALK_ITEM_QTY_FAIL:
        return { ...state, "isLoading": false }
    case UPLOAD_STOREWALK_PHOTO_START:
        return { ...state, "isLoading": true }
    case UPLOAD_STOREWALK_PHOTO_SUCCESS:
        return { ...state, "isLoading": false }
    case UPLOAD_STOREWALK_PHOTO_FAIL:
        return { ...state, "isLoading": false }
    case UPLOAD_STOREWALK_NOTE_START:
        return { ...state, "isLoading": true }
    case UPLOAD_STOREWALK_NOTE_SUCCESS:
        return { ...state, "isLoading": false }
    case UPLOAD_STOREWALK_NOTE_FAIL:
        return { ...state, "isLoading": false }
    case SUBMIT_STOREWALK_START:
        return { ...state, "isLoading": true }
    case SUBMIT_STOREWALK_SUCCESS:
        return { ...state, "isLoading": false }
    case SUBMIT_STOREWALK_FAIL:
        return { ...state, "isLoading": false }
    case GET_SURVEY_START:
        return { ...state, "isLoading": true }
    case GET_SURVEY_SUCCESS:
        return { ...state, "isLoading": false }
    case GET_SURVEY_FAIL:
        return { ...state, "isLoading": false }
    case UPDATE_CHAT_MESSAGE_COUNT:
        return { ...state, "chatMessageCount": action.payload }
    case GET_MANAGER_INVOICES_START:
      return { ...state, "isLoading": true }
    case GET_MANAGER_INVOICES_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_MANAGER_INVOICES_FAIL:
      return { ...state, "isLoading": false }
    case GET_INVOICES_START:
      return { ...state, "isLoading": true }
    case GET_INVOICES_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_INVOICES_FAIL:
      return { ...state, "isLoading": false }
    case GET_INVOICE_HISTORY_START:
      return { ...state, "isLoading": true }
    case GET_INVOICE_HISTORY_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_INVOICE_HISTORY_FAIL:
      return { ...state, "isLoading": false }
    case GET_MANAGER_REPAIRS_START:
      return { ...state, "isLoading": true }
    case GET_MANAGER_REPAIRS_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_MANAGER_REPAIRS_FAIL:
      return { ...state, "isLoading": false }
    case GET_STORE_REPAIRS_START:
      return { ...state, "isLoading": true }
    case GET_STORE_REPAIRS_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_STORE_REPAIRS_FAIL:
      return { ...state, "isLoading": false }
    case GET_REPAIR_CATS_START:
      return { ...state, "isLoading": true }
    case GET_REPAIR_CATS_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_REPAIR_CATS_FAIL:
      return { ...state, "isLoading": false }
    case GET_REPAIR_SKUSCAN_START:
      return { ...state, "isLoading": true }
    case GET_REPAIR_SKUSCAN_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_REPAIR_SKUSCAN_FAIL:
      return { ...state, "isLoading": false }
    case UPLOAD_REPAIR_PHOTO_START:
      return { ...state, "isLoading": true }
    case UPLOAD_REPAIR_PHOTO_SUCCESS:
      return { ...state, "isLoading": false }
    case UPLOAD_REPAIR_PHOTO_FAIL:
      return { ...state, "isLoading": false }
    case CREATE_REPAIR_TICKET_START:
      return { ...state, "isLoading": true }
    case CREATE_REPAIR_TICKET_SUCCESS:
      return { ...state, "isLoading": false }
    case CREATE_REPAIR_TICKET_FAIL:
      return { ...state, "isLoading": false }
    case UPDATE_REPAIR_TICKET_START:
      return { ...state, "isLoading": true }
    case UPDATE_REPAIR_TICKET_SUCCESS:
      return { ...state, "isLoading": false }
    case UPDATE_REPAIR_TICKET_FAIL:
      return { ...state, "isLoading": false }
    case REMOVE_REPAIR_START:
      return { ...state, "isLoading": true }
    case REMOVE_REPAIR_SUCCESS:
      return { ...state, "isLoading": false }
    case REMOVE_REPAIR_FAIL:
      return { ...state, "isLoading": false }
    case ADD_REPAIR_TO_INVOICE_START:
      return { ...state, "isLoading": true }
    case ADD_REPAIR_TO_INVOICE_SUCCESS:
      return { ...state, "isLoading": false }
    case ADD_REPAIR_TO_INVOICE_FAIL:
      return { ...state, "isLoading": false }
    case ADD_PO_START:
      return { ...state, "isLoading": true }
    case ADD_PO_SUCCESS:
      return { ...state, "isLoading": false }
    case ADD_PO_FAIL:
      return { ...state, "isLoading": false }
    case ADD_KEYREC_START:
      return { ...state, "isLoading": true }
    case ADD_KEYREC_SUCCESS:
      return { ...state, "isLoading": false }
    case ADD_KEYREC_FAIL:
      return { ...state, "isLoading": false }
    case ADD_STAMP_START:
      return { ...state, "isLoading": true }
    case ADD_STAMP_SUCCESS:
      return { ...state, "isLoading": false }
    case ADD_STAMP_FAIL:
      return { ...state, "isLoading": false }
    case SUBMIT_INVOICES_START:
      return { ...state, "isLoading": true }
    case SUBMIT_INVOICES_SUCCESS:
      return { ...state, "isLoading": false }
    case SUBMIT_INVOICES_FAIL:
      return { ...state, "isLoading": false }
    case SEND_SURVEY_WITH_SIGNATURE_START:
      return { ...state, "isLoading": true }
    case SEND_SURVEY_WITH_SIGNATURE_SUCCESS:
      return { ...state, "isLoading": false }
    case SEND_SURVEY_WITH_SIGNATURE_FAIL:
      return { ...state, "isLoading": false }
    case DECREASE_INVOICE_ITEM_QTY_START:
      return { ...state, "isLoading": true }
    case DECREASE_INVOICE_ITEM_QTY_SUCCESS:
      return { ...state, "isLoading": false }
    case DECREASE_INVOICE_ITEM_QTY_FAIL:
      return { ...state, "isLoading": false }
    case GET_TECHS_START:
      return { ...state, "isLoading": true }
    case GET_TECHS_SUCCESS:
      return { ...state }
    case GET_TECHS_FAIL:
      return { ...state, "isLoading": false }
    case GET_ALL_TECHS_START:
      return { ...state, "isLoading": true }
    case GET_ALL_TECHS_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_ALL_TECHS_FAIL:
      return { ...state, "isLoading": false }
    case GET_AREA_TECHS_START:
      return { ...state, "isLoading": true }
    case GET_AREA_TECHS_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_AREA_TECHS_FAIL:
      return { ...state, "isLoading": false }
    case CLAIM_TECH_START:
      return { ...state, "isLoading": true }
    case CLAIM_TECH_SUCCESS:
      return { ...state, "isLoading": false }
    case CLAIM_TECH_FAIL:
      return { ...state, "isLoading": false }
    case DELETE_TECH_START:
      return { ...state, "isLoading": true }
    case DELETE_TECH_SUCCESS:
      return { ...state, "isLoading": false }
    case DELETE_TECH_FAIL:
      return { ...state, "isLoading": false }
    case GET_REPORTS_START:
      return { ...state, "isLoading": true }
    case GET_REPORTS_SUCCESS:
      return { ...state, "isLoading": false }
    case GET_REPORTS_FAIL:
      return { ...state, "isLoading": false }
    default:
      return state;
  }
}
