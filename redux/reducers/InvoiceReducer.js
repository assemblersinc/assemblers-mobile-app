const INITIAL_STATE = {
	"count": 0,
	"invoices": [],
	"currentInvoice": {},
	"index": null,
	"mannagerList": null
};

import {
	GET_MANAGER_INVOICES_SUCCESS,
	GET_MANAGER_INVOICES_FAIL,
	CURRENT_INVOICE_CHANGE,
	GET_INVOICES_START,
	GET_INVOICES_SUCCESS,
	GET_INVOICES_FAIL,
	ADD_PO_START,
	ADD_PO_SUCCESS,
	ADD_PO_FAIL,
	ADD_KEYREC_START,
	ADD_KEYREC_SUCCESS,
	ADD_KEYREC_FAIL,
	ADD_STAMP_START,
	ADD_STAMP_SUCCESS,
	ADD_STAMP_FAIL,
	SEND_SURVEY_WITH_SIGNATURE_START,
	SEND_SURVEY_WITH_SIGNATURE_SUCCESS,
	SEND_SURVEY_WITH_SIGNATURE_FAIL,
	DECREASE_INVOICE_ITEM_QTY_START,
	DECREASE_INVOICE_ITEM_QTY_SUCCESS,
	DECREASE_INVOICE_ITEM_QTY_FAIL
} from '../types';

export default (state = INITIAL_STATE, action) => {

	switch(action.type) {
		case CURRENT_INVOICE_CHANGE:
			return { ...state, "currentInvoice": action.payload.invoice, "index": action.payload.index };
		case GET_MANAGER_INVOICES_SUCCESS:
			return { ...state, "managerList": action.payload }
		case GET_MANAGER_INVOICES_FAIL:
			return { ...state, "managerList": null }
		case GET_INVOICES_START:
			return state;
		case GET_INVOICES_SUCCESS:
			return { ...state, "invoices": action.payload.rows };
		case GET_INVOICES_FAIL:
			return INITIAL_STATE;
		case ADD_PO_SUCCESS:
			return { ...state, "currentInvoice": action.payload, "invoices": updateInvoice(state.invoices, action.payload) };
		case ADD_PO_FAIL:
			return state;
		case ADD_KEYREC_SUCCESS:
			return { ...state, "currentInvoice": action.payload, "invoices": updateInvoice(state.invoices, action.payload) };
		case ADD_KEYREC_FAIL:
			return state;
		case ADD_STAMP_SUCCESS:
			return { ...state, "currentInvoice": action.payload, "invoices": updateInvoice(state.invoices, action.payload) };
		case ADD_STAMP_FAIL:
			return state;
		case SEND_SURVEY_WITH_SIGNATURE_SUCCESS:
			return { ...state, "invoices": updateInvoice(state.invoices, action.payload)};
		case SEND_SURVEY_WITH_SIGNATURE_FAIL:
			return state;
		case DECREASE_INVOICE_ITEM_QTY_SUCCESS:
			return { ...state, "currentInvoice": action.payload, "invoices": updateInvoice(state.invoices, action.payload) };
		case DECREASE_INVOICE_ITEM_QTY_FAIL:
			return state;
		default:
			return state;
	}
}

function updateInvoice(invoices, invoice) {

	for(var i = 0; i < invoices.length; i++) {

		if (invoices[i].invoiceId == invoice.invoiceId) {
			invoices[i] = invoice;

			return invoices;
		}
	}

	return invoices;
}
