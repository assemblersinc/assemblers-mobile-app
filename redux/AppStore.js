import { AsyncStorage } from 'react-native';
import { createStore, compose, applyMiddleware } from 'redux';
import reducers from './reducers';
import { autoRehydrate } from 'redux-persist'
import { persistStore } from 'redux-persist';
import thunk from 'redux-thunk';

const store = createStore(
  reducers,
  {},
  compose(
    autoRehydrate(),
    applyMiddleware(thunk)
  )
);
persistStore(store, {storage: AsyncStorage});

export default store;
