import Toast from 'react-native-root-toast';
import moment from 'moment';
import services from '../../services';
import utils from '../../utils';
import constants from '../../constants.json';
// const PROD_LOCATION_TIMER = 1000*60*30;
// const TEST_LOCATION_TIMER = 1000*60;
const PROD_INACTIVITY_TIMER = 180;
const TEST_INACTIVITY_TIMER = 5;

import {
  ENTER_CLOCK_IN_FLOW,
  EXIT_CLOCK_IN_FLOW,
  OPEN_CLOCK_IN_PROMPT,
  CLOSE_CLOCK_IN_PROMPT,
  OPEN_CLOCK_OUT_PROMPT,
  CLOSE_CLOCK_OUT_PROMPT,
  OPEN_INACTIVITY_MODAL,
  CLOSE_INACTIVITY_MODAL,
  CLOCK_IN_STORE_NAME_CHANGED,
  CLOCK_IN_STORE_NUMBER_CHANGED,
  CLOCK_IN_START,
  CLOCKED_IN_FAIL,
  CLOCKED_IN_SUCCESS,
  CLOCK_OUT_START,
  CLOCKED_OUT_FAIL,
  CLOCKED_OUT_SUCCESS,
  STAY_CLOCKED_IN,
  MEAL_IN_START,
  MEAL_IN_SUCCESS,
  MEAL_IN_FAIL,
  MEAL_OUT_START,
  MEAL_OUT_SUCCESS,
  MEAL_OUT_FAIL,
  GET_TIMESHEETS_START,
  GET_TIMESHEETS_SUCCESS,
  GET_TIMESHEETS_FAIL,
  SELECT_CLIENT,
  SELECT_STORE,
  UPDATE_CITY_TEXT,
  UPDATE_STATE_TEXT,
  UPDATE_ZIP_TEXT,
  STORE_SEARCH_START,
  STORE_SEARCH_SUCCESS,
  STORE_SEARCH_FAIL,
  GET_CLOCKIN_LOCATION_START,
  GET_CLOCKIN_LOCATION_SUCCESS,
  GET_CLOCKIN_LOCATION_FAIL,
  SET_CLOCKIN_ERROR,
  // LOCATION_TIMER_START,
  // LOCATION_TIMER_TICK,
  // LOCATION_TIMER_STOP,
  SHOW_CLOCKIN_LEFT_STORE_MODAL,
  HIDE_CLOCKIN_LEFT_STORE_MODAL,
  SHOW_INACTIVITY_MODAL,
  HIDE_INACTIVITY_MODAL,
  SHOW_GRILLBARROW_MODAL,
  HIDE_GRILLBARROW_MODAL,
  GRILLBARROW_SUBMIT_START,
  GRILLBARROW_SUBMIT_SUCCESS,
  GRILLBARROW_SUBMIT_FAIL,
  CHECK_IN_SUBCONTRACTOR,
  CHECK_OUT_SUBCONTRACTOR,
  CLEAR_NEEDS_UPDATE,
  TRAVEL_STORE_START,
  TRAVEL_STORE_SUCCESS,
  TRAVEL_STORE_FAIL,
  ALREADY_CLOCKED_IN
} from '../types';

//let locationTimer = null;

//Clock in
export const enterClockInFlow = (origin, callback) => {
  if(callback) {
    callback();
  }
  return (dispatch) => {
    dispatch({ type: ENTER_CLOCK_IN_FLOW, payload: origin });
  }
}

export const exitClockInFlow = () => {
  return({ type: EXIT_CLOCK_IN_FLOW });
}

export const closeClockInPromptModal = () => {
  return({ type: CLOSE_CLOCK_IN_PROMPT });
}

export const openClockOutPromptModal = () => {
  return({ type: OPEN_CLOCK_OUT_PROMPT });
}

export const closeClockOutPromptModal = () => {
  return({ type: CLOSE_CLOCK_OUT_PROMPT });
}

export const closeInactivityModal = () => {
  return({ type: CLOSE_INACTIVITY_MODAL });
}

export const selectClient = (client) => {
  return {
    type: SELECT_CLIENT,
    payload: client
  };
}

export const selectStore = (client, storeNumber) => {
  return {
    type: SELECT_STORE,
    payload: { client: client, storeNumber: storeNumber }
  };
}

export const updateCityText = (cityText) => {
  return({
    type: UPDATE_CITY_TEXT,
    payload: cityText
  });
}

export const updateStateText = (stateText) => {
  return({
    type: UPDATE_STATE_TEXT,
    payload: stateText
  });
}

export const updateZipText = (zipText) => {
  return({
    type: UPDATE_ZIP_TEXT,
    payload: zipText
  });
}

export const searchForStores = (token, clientId, cityText, stateText, zipText, viewCallback, viewEmptyCallback, viewErrorCallback) => {
  return (dispatch) => {
    dispatch({ type: STORE_SEARCH_START });
    services.geolocation.storeSearch(token, clientId, cityText, stateText, zipText,
      (locations) => searchForStoresSuccess(dispatch, locations, viewCallback, viewEmptyCallback),
      (error) => searchForStoresFail(dispatch, error, viewErrorCallback)
    );
  }
}

searchForStoresSuccess = (dispatch, locations, viewCallback, viewEmptyCallback) => {
  dispatch({ type: STORE_SEARCH_SUCCESS, payload: locations });
  if(locations == "" || locations.length < 1) {
    viewEmptyCallback();
  } else {
    viewCallback();
  }
}

searchForStoresFail = (dispatch, error, viewErrorCallback) => {
  dispatch({ type: STORE_SEARCH_FAIL, payload: error });
  viewErrorCallback();
}

export const clockinStoreNumberChanged = (text) => {
  return({ type: CLOCK_IN_STORE_NUMBER_CHANGED, payload: text });
}

export const getClockInLocation = (clientId, storeNumber, token) => {
  console.log('getClockInLocation', clientId, storeNumber);

  return (dispatch) => {
    dispatch({ type: GET_CLOCKIN_LOCATION_START });
    services.clocking.getClockInLocation(clientId, storeNumber, token,
      (location) => getClockInLocationSuccess(dispatch, location),
      (error) => getClockInLocationFail(dispatch, error)
    );
  }
}

const getClockInLocationSuccess = (dispatch, location) => {
  console.log('getClockInLocationSuccess');
  dispatch({ type: GET_CLOCKIN_LOCATION_SUCCESS, payload: location });
}

const getClockInLocationFail = (dispatch, error) => {
  console.log('getClockInLocationFail');
  dispatch({ type: SET_CLOCKIN_ERROR, payload: error });
  dispatch({ type: GET_CLOCKIN_LOCATION_FAIL });
}

export const setClockinError = (error) => {
  return({ type: SET_CLOCKIN_ERROR, payload: error });
}

export const clockInUser = (userId, locationId, token, roleId, grillbarrowCallback) => {
  return (dispatch) => {
    dispatch({ type: CLOCK_IN_START });
    services.clocking.clockInUser(userId, locationId, token,
      (timesheet) => clockInSuccess(dispatch, timesheet, token, roleId, userId, grillbarrowCallback),
      (newTimesheet) => clockInAlreadyClockedIn(dispatch, newTimesheet, token, roleId, userId, grillbarrowCallback),
      (error) => clockInFail(dispatch, error)
    );
  };
}

const clockInAlreadyClockedIn = (dispatch, newTimesheet, token, roleId, userId, grillbarrowCallback) => {
  dispatch({ type: ALREADY_CLOCKED_IN, payload: newTimesheet });
  clockInSuccess(dispatch, newTimesheet, token, roleId, userId);
}

export const clockInSuccess = (dispatch, timesheet, token, roleId, userId, grillbarrowCallback) => {

  dispatch({ type: CLOCKED_IN_SUCCESS, payload: timesheet });

  // if(roleId == constants.roleIds.tech) {
  //   startLocationTimer(dispatch, token, timesheet);
  // }
  //
  // console.log(timesheet);
  //show BOPIS toast if at Home Depot
  if(timesheet.locationId == constants.clients.thd) {
    setTimeout(function() {
      let toast = Toast.show('Check in at the counter for BOPIS items.', {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true
      });
    }, 1000);
  }

  if(utils.dateTime.isGrillbarrowDay() && timesheet.location.storeassemblycounts == null && grillbarrowCallback) {
    grillbarrowCallback();
  } else {
    dispatch({ type: EXIT_CLOCK_IN_FLOW });
  }

}

export const clockInFail = (dispatch, error) => {
  dispatch({ type: CLOCKED_IN_FAIL, payload: error });
}

export const stayClockedIn = () => {
  return { type: STAY_CLOCKED_IN };
}

export const mealOutUser = (token, timeId, userId, viewSuccessCallback) => {
  return (dispatch) => {
    dispatch({ type: MEAL_OUT_START });
    services.clocking.mealOutUser(token, timeId, userId,
      (timesheet) => mealOutUserSuccess(timesheet, dispatch, viewSuccessCallback),
      () => mealOutUserFail(dispatch)
    );
  }
}

const mealOutUserSuccess = (timesheet, dispatch, viewSuccessCallback) => {
  dispatch({ type: MEAL_OUT_SUCCESS, payload: timesheet });
  if(viewSuccessCallback) viewSuccessCallback();
}

const mealOutUserFail = (dispatch) => {
  dispatch({ type: MEAL_OUT_FAIL });
}

export const mealInUser = (token, timeId, userId, viewSuccessCallback) => {
  return (dispatch) => {
    dispatch({ type: MEAL_IN_START });
    services.clocking.mealInUser(token, timeId, userId,
      (timesheet) => mealInUserSuccess(timesheet, dispatch, viewSuccessCallback),
      () => mealInUserFail(dispatch)
    );
  }
}

const mealInUserSuccess = (timesheet, dispatch, viewSuccessCallback) => {
  dispatch({ type: MEAL_IN_SUCCESS, payload: timesheet });
  if(viewSuccessCallback) viewSuccessCallback();
}

const mealInUserFail = (dispatch) => {
  dispatch({ type: MEAL_IN_FAIL });
}

//Clock out
export const clockOutUser = (userId, timeId, token, viewCallback) => {
  return (dispatch) => {
    dispatch({ type: CLOCK_OUT_START });
    services.clocking.clockOutUser(userId, timeId, token,
      () => clockOutSuccess(dispatch, viewCallback),
      (error) => clockOutFail(dispatch, error)
    );
  };
}

export const clockOutSuccess = (dispatch, viewCallback) => {
  dispatch({ type: CLOCKED_OUT_SUCCESS });
  dispatch({ type: EXIT_CLOCK_IN_FLOW });
  dispatch({ type: HIDE_CLOCKIN_LEFT_STORE_MODAL });
  //stopLocationTimer();
  if(viewCallback) {
    viewCallback();
  }
}

export const clockOutFail = (dispatch, error) => {
  dispatch({ type: CLOCKED_OUT_FAIL });
}

export const travelToAnotherStore = (token, timeId, userId, locationId, roleId) => {
  return (dispatch) => {
    dispatch({ type: TRAVEL_STORE_START });
    services.clocking.travelToAnotherStore(token, timeId, userId, locationId,
      (timesheet) => travelToAnotherStoreSuccess(dispatch, timesheet, token, roleId, userId),
      (error) => travelToAnotherStoreFail(dispatch, error)
    );
  }
}

const travelToAnotherStoreSuccess = (dispatch, timesheet, token, roleId, userId) => {
  //stopLocationTimer();
  setTimeout(function() {
    clockInSuccess(dispatch, timesheet, token, roleId, userId);
  }, 500);
}

const travelToAnotherStoreFail = (dispatch, error) => {
  dispatch({ type: CLOCKED_OUT_FAIL });
}

export const getRecentTimesheets = (userId, token) => {
  return (dispatch) => {
    dispatch({ type: GET_TIMESHEETS_START });
    services.clocking.getRecentTimesheets(userId, token,
      (timesheets) => getRecentTimesheetsSuccess(dispatch, timesheets),
      (error) => getRecentTimesheetsFail(dispatch, error)
    );
  }
}

export const submitGrillbarrow = (token, userId, locationId,
  onRackCount, holesCount, stagedCount, boxedCount,repairsCount,
  grillCount, wheelbarrowCount, viewCallback, viewErrorCallback) => {
  return (dispatch) => {
    dispatch({ type: GRILLBARROW_SUBMIT_START });
    services.storeWalk.submitGrillbarrow(token, userId, locationId,
      onRackCount, holesCount, stagedCount, boxedCount, repairsCount, grillCount, wheelbarrowCount,
      () => submitGrillbarrowSuccess(dispatch, viewCallback),
      (error) => submitGrillbarrowFail(dispatch, error, viewErrorCallback)
    );
  }
}

const submitGrillbarrowSuccess = (dispatch, viewCallback) => {
  if(viewCallback) viewCallback();
  dispatch({ type: GRILLBARROW_SUBMIT_SUCCESS });
  dispatch({ type: EXIT_CLOCK_IN_FLOW });
}

const submitGrillbarrowFail = (dispatch, error, viewErrorCallback) => {
  dispatch({ type: GRILLBARROW_SUBMIT_FAIL, payload: error })
  setTimeout(function() {
    if(viewErrorCallback) viewErrorCallback();
  }, 500);
}

// const startLocationTimer = (dispatch, token, timesheet) => {
//   console.log('startLocationTimer');
//   clearInterval(locationTimer);
//   locationTimer = setInterval(() => dispatch(locationTimerTick(dispatch, token, timesheet)), PROD_LOCATION_TIMER);
//   dispatch({ type: LOCATION_TIMER_START });
// }
//
// const locationTimerTick = (dispatch, token, timesheet) => {
//   console.log('locationTimerTick');
//   return(dispatch) => {
//     dispatch({ type: LOCATION_TIMER_TICK });
//     services.geolocation.findNearbyStore(token,
//       (stores) => locationTickSuccess(dispatch, stores, timesheet),
//       (error) => locationTickFail(dispatch, error)
//     );
//   }
// };
//
// const locationTickSuccess = (dispatch, stores, timesheet) => {
//   console.log('Currently at ', stores);
//   console.log('Clocked in at ', timesheet);
//
//   const clockedInStore = timesheet.locationID;
//   var nearbyStores = [];
//
//   if(typeof stores === 'object') {
//     nearbyStores.push(stores.location[0].locationId);
//   } else {
//     stores.forEach(function(element) {
//       nearbyStores.push(element.location[0].locationId);
//     });
//   }
//
//   nearbyStores.forEach(function(store) {
//     console.log(store, timesheet);
//     if(store != timesheet.locationId) {
//       showLeftStoreModal();
//     }
//   })
// }

export const showLeftStoreModal = () => {
  return { type: SHOW_CLOCKIN_LEFT_STORE_MODAL };
}

export const hideClockInLeftStoreModal = () => {
  return { type: HIDE_CLOCKIN_LEFT_STORE_MODAL};
}

// const locationTickFail = (dispatch, error) => {
//   console.log('locationTickFail');
// }

// const stopLocationTimer = () => {
//   clearInterval(locationTimer);
//   return { type: LOCATION_TIMER_STOP }
// }

export const getLastSeenTime = (token, userId) => {
  console.log('getLastSeenTime');
  return(dispatch) => {
    services.user.getLastSeenTime(token, userId,
      (lastSeen) => getLastSeenTimeSuccess(dispatch, lastSeen),
      (error) => getLastSeenTimeFail(dispatch, error)
    );
  }
}

const getLastSeenTimeSuccess = (dispatch, lastSeen) => {
  console.log('getLastSeenTimeSuccess', lastSeen);
  const lastSeenTime = moment(lastSeen);
  var duration = moment().diff(lastSeenTime, 'minutes');
  console.log('duration', duration);
  if(duration >= PROD_INACTIVITY_TIMER) {
    dispatch({ type: SHOW_INACTIVITY_MODAL });
  }
}

const getLastSeenTimeFail = (dispatch, error) => {
  console.log('getLastSeenTimeFail', error);
}

export const hideInactivityModal = () => {
  return {
    type: HIDE_INACTIVITY_MODAL
  }
}

export const getRecentTimesheetsSuccess = (dispatch, timesheets) => {
  dispatch({ type: GET_TIMESHEETS_SUCCESS, payload: timesheets });
}

export const getRecentTimesheetsFail = (dispatch, error) => {
  dispatch({ type: GET_TIMESHEETS_FAIL, payload: error})
}

export const checkInSubcontractor = (location) => {
  return { type: CHECK_IN_SUBCONTRACTOR, payload: location };
}

export const checkOutSubcontractor = () => {
  return { type: CHECK_OUT_SUBCONTRACTOR };
  closeClockOutPromptModal();
}

export const clearNeedsUpdate = () => {
  return { type: CLEAR_NEEDS_UPDATE };
}
