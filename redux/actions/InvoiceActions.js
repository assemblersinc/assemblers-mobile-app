import services from '../../services';

import {
  GET_MANAGER_INVOICES_START,
  GET_MANAGER_INVOICES_SUCCESS,
  GET_MANAGER_INVOICES_FAIL,
  GET_INVOICES_START,
  GET_INVOICES_SUCCESS,
  GET_INVOICES_FAIL,
  GET_INVOICE_HISTORY_START,
  GET_INVOICE_HISTORY_SUCCESS,
  GET_INVOICE_HISTORY_FAIL,
  DECREASE_INVOICE_ITEM_QTY_START,
  DECREASE_INVOICE_ITEM_QTY_SUCCESS,
  DECREASE_INVOICE_ITEM_QTY_FAIL,
  PRINTED_NAME_CHANGED,
  SIGNATURE_COMPLETED,
  ADD_PO_START,
  ADD_PO_SUCCESS,
  ADD_PO_FAIL,
  ADD_KEYREC_START,
  ADD_KEYREC_SUCCESS,
  ADD_KEYREC_FAIL,
  ADD_STAMP_START,
  ADD_STAMP_SUCCESS,
  ADD_STAMP_FAIL,
  SUBMIT_INVOICES_START,
  SUBMIT_INVOICES_SUCCESS,
  SUBMIT_INVOICES_FAIL,
  CURRENT_INVOICE_CHANGE,
  SEND_SURVEY_WITH_SIGNATURE_START,
  SEND_SURVEY_WITH_SIGNATURE_SUCCESS,
  SEND_SURVEY_WITH_SIGNATURE_FAIL,
  SHOW_INVOICE_SUBMIT_MODAL,
  HIDE_INVOICE_SUBMIT_MODAL
} from '../types';

export const setCurrentInvoice = (invoice, index) => {
  return (dispatch) => {
    dispatch({ type: CURRENT_INVOICE_CHANGE, payload: {
      'invoice': invoice,
      'index': index }
    });
  };
}

/*
  Invoice History
*/

export const getManagerInvoiceList = (token, areaId, viewCallback) => {
  return (dispatch) => {
    dispatch({ type: GET_MANAGER_INVOICES_START });
    services.invoices.getManagerInvoiceList(token, areaId,
      (invoices) => getManagerInvoiceListSuccess(dispatch, invoices, viewCallback),
      (error) => getManagerInvoiceListFail(dispatch, error)
    );
  }
}

const getManagerInvoiceListSuccess = (dispatch, invoices, viewCallback) => {
  dispatch({ type: GET_MANAGER_INVOICES_SUCCESS, payload: invoices });
  viewCallback();
}

const getManagerInvoiceListFail = (dispatch, error) => {
  dispatch({ type: GET_MANAGER_INVOICES_FAIL });
}

export const getMyInvoiceHistory = (query, token, viewCallback) => {
  return (dispatch) => {
    dispatch({ type: GET_INVOICE_HISTORY_START });
    services.invoices.getMyInvoiceHistory(query, token,
      (invoices) => getMyInvoiceHistorySuccess(dispatch, invoices, viewCallback),
      (error) => getMyInvoiceHistoryFail(dispatch, error)
    );
  }
}

export const getMyInvoiceHistorySuccess = (dispatch, invoices, viewCallback) => {
  console.log('getMyInvoiceHistory Success');
  dispatch({ type: GET_INVOICE_HISTORY_SUCCESS, payload: invoices });
  viewCallback();
}

export const getMyInvoiceHistoryFail = (dispatch, error) => {
  console.log('getMyInvoiceHistory failed');
  dispatch({ type: GET_INVOICE_HISTORY_FAIL, payload: error});
}

/*
* Add PO to Invoice
*/
export const invoiceAddPO = ({invoice, token}, callback) => {
  return (dispatch) => {
    dispatch({ type: ADD_PO_START });
    services.invoices.updateInvoice(invoice, token,
      (invoiceData) => updateInvoiceSuccess(dispatch, invoiceData, ADD_PO_SUCCESS, callback),
      (error) => updateInvoiceFail(dispatch, error, ADD_PO_FAIL)
    );
  }
}

/*
* Add Key Rec to Invoice
*/
export const invoiceAddKeyRec = ({invoice, token}, callback) => {
  return (dispatch) => {
    dispatch({ type: ADD_KEYREC_START });
    services.invoices.updateInvoice(invoice, token,
      (invoice) => updateInvoiceSuccess(dispatch, invoice, ADD_KEYREC_SUCCESS, callback),
      (error) => updateInvoiceFail(dispatch, error, ADD_KEYREC_FAIL)
    );
  }
}

/*
* Update Invoice Success & Failure Methods
*/
export const updateInvoiceSuccess = (dispatch, invoice, type, callback) => {
  console.log('updateInvoiceFail Success');
  dispatch({ type: type, payload: invoice });

  if (callback) callback(invoice);
}

export const updateInvoiceFail = (dispatch, error, type) => {
  console.log('updateInvoiceFail failed');
  dispatch({ type: type, payload: error});
}

/*
* Complete Invoice(s)
*/
export const completeInvoice = ({invoices, token}, callback) => {
  return (dispatch) => {
    dispatch({ type: SUBMIT_INVOICES_START });
    services.invoices.updateInvoices(invoices, token,
      (invoices) => completeInvoicesSuccess(dispatch, invoices, callback),
      (error) => completeInvoicesFail(dispatch, error)
      );
  }
}

export const completeInvoicesSuccess = (dispatch, invoices, callback) => {
  console.log('completeInvoiceSuccess');
  dispatch({ type: SUBMIT_INVOICES_SUCCESS, payload: invoices });

  if(callback) callback();
}


export const completeInvoicesFail = (dispatch, error) => {
  console.log('completeInvoiceFail');
  dispatch({ type: SUBMIT_INVOICES_FAIL, payload: error});
}


/*
* Upload Stamp Photo
*/
export const uploadStampImage = ({token, invoice, imagePath}, callback) => {
  return (dispatch) => {
    dispatch({ type: ADD_STAMP_START });
    services.invoices.uploadStampPhoto(token, invoice, imagePath,
      (invoice) => uploadStampImageSuccess(dispatch, invoice, callback),
      (error) => uploadStampImageFail(dispatch, error)
    );
  }

}

export const uploadStampImageSuccess = (dispatch, invoice, callback) => {
  console.log('uploadStampImageSuccess');
  dispatch({ type: ADD_STAMP_SUCCESS, payload: invoice });

  if(callback) callback();
}

export const uploadStampImageFail = (dispatch, error) => {
  console.log('uploadStampImageFail');
  dispatch({ type: ADD_STAMP_FAIL, payload: error});
}


/*
* Save Invoice with Survey & Signature
*/
export const saveSurveyWithSignature = ({data, token}, callback) => {
  return (dispatch) => {
    dispatch({ type: SEND_SURVEY_WITH_SIGNATURE_START});
    services.invoices.saveSurveyWithSignature(data, token,
      (invoices) => saveSurveyWithSignatureSuccess(dispatch, invoices, callback),
      (error) => saveSurveyWithSignatureFail(dispatch, error)
    );
  }
}

export const saveSurveyWithSignatureSuccess = (dispatch, invoices, callback) => {
  console.log('saveSurveyWithSignatureSuccess');
  dispatch({ type: SEND_SURVEY_WITH_SIGNATURE_SUCCESS, payload: invoices });

  if(callback) callback();
}


export const saveSurveyWithSignatureFail = (dispatch, error) => {
  console.log('saveSurveyWithSignatureFail');
  dispatch({ type: SEND_SURVEY_WITH_SIGNATURE_FAIL, payload: error});
}

/*
* Decrease Invoice Product Quantity
*/

export const decreaseInvoiceItemQuantity = (item, token) => {
  return (dispatch) => {
    dispatch({ type: DECREASE_INVOICE_ITEM_QTY_START});
    services.invoices.decreaseInvoiceItemQuantity(item, token,
      (invoice) => decreaseInvoiceItemQuantitySuccess(dispatch, invoice),
      (error) => decreaseInvoiceItemQuantityFail(dispatch, error)
    );
  }
}

export const decreaseInvoiceItemQuantitySuccess = (dispatch, invoice) => {
  console.log('decreaseInvoiceItemQuantitySuccess');
  dispatch({ type: DECREASE_INVOICE_ITEM_QTY_SUCCESS, payload: invoice });
}

export const decreaseInvoiceItemQuantityFail = (dispatch, error) => {
  console.log('decreaseInvoiceItemQuantityFail');
  dispatch({ type: DECREASE_INVOICE_ITEM_QTY_FAIL, payload: error });
}

/*
* Invoice Start
*/
export const getTodaysInvoices = (query, token) => {
  return(dispatch) => {
    dispatch({ type: GET_INVOICES_START});
    services.invoices.getTodaysInvoices(query, token,
      (invoices) => getTodaysInvoicesSuccess(dispatch, invoices),
      (error) => getTodaysInvoicesFail(dispatch, error)
      );
  }
}

export const getTodaysInvoicesSuccess = (dispatch, invoices) => {
  console.log('getTodaysInvoices Success');
  dispatch({ type: GET_INVOICES_SUCCESS, payload: invoices });
}

export const getTodaysInvoicesFail = (dispatch, error) => {
  console.log('getTodaysInvoices failed');
  dispatch({ type: GET_INVOICES_FAIL, payload: error});
}

export const showInvoiceSubmitModal = () => {
  return { type: SHOW_INVOICE_SUBMIT_MODAL };
}

export const hideInvoiceSubmitModal = () => {
  return { type: HIDE_INVOICE_SUBMIT_MODAL };
}
