import services from '../../services';

import {
  GET_ANNOUNCEMENTS_START,
  GET_ANNOUNCEMENTS_SUCCESS,
  GET_ANNOUNCEMENTS_FAIL
} from '../types';

export const getAnnouncements = (query, token, errorCallback) => {
  return (dispatch) => {
    dispatch({ type: GET_ANNOUNCEMENTS_START });
    services.announcements.getAnnouncements(query, token,
      (announcements) => getAnnouncementsSuccess(dispatch, announcements),
      (error) => getAnnouncementsFail(dispatch, error, errorCallback)
    );
  }
}

const getAnnouncementsSuccess = (dispatch, announcements) => {
  dispatch({ type: GET_ANNOUNCEMENTS_SUCCESS, payload: announcements });
}

const getAnnouncementsFail = (dispatch, error, errorCallback) => {
  dispatch({ type: GET_ANNOUNCEMENTS_FAIL, payload: error});

  setTimeout(function() {
    console.log('timeout');
    errorCallback();
  }, 1000);
}
