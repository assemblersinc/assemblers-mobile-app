import services from '../../services';

import {
  GET_WORKLIST_START,
  GET_WORKLIST_SUCCESS,
  GET_WORKLIST_FAIL,
  SELECT_WORKLIST_ITEM,
  SEARCH_WORKLIST_SERVICE_START,
  SEARCH_WORKLIST_SERVICE_SUCCESS,
  SEARCH_WORKLIST_SERVICE_FAIL,
  SEARCH_WORKLIST_SKU_START,
  SEARCH_WORKLIST_SKU_SUCCESS,
  SEARCH_WORKLIST_SKU_FAIL,
  ADD_TO_INVOICE_START,
  ADD_TO_INVOICE_SUCCESS,
  ADD_TO_INVOICE_FAIL
} from '../types';

export const getWorkList = (locationId, token, userId) => {
  return (dispatch) => {
    dispatch({ type: GET_WORKLIST_START });
    services.workList.getWorkList(locationId, token, userId,
      (workList) => getWorkListSuccess(dispatch, workList),
      (error) => getWorkListFail(dispatch, error)
    );
  }
}

export const selectWorkListItem = (item) => {
  return { type: SELECT_WORKLIST_ITEM, payload: item };
}

const getWorkListSuccess = (dispatch, workList) => {
  dispatch({ type: GET_WORKLIST_SUCCESS, payload: workList });
}

const getWorkListFail = (dispatch, error) => {
  dispatch({ type: GET_WORKLIST_FAIL, payload: error})
}

export const addItemToInvoice = (token, userId, requestProductId, clientId, locationId,
  isService, serialNumber, qty, viewSuccessCallback, viewErrorCallback) => {
  return (dispatch) => {
    dispatch({ type: ADD_TO_INVOICE_START });
    services.workList.addItemToInvoice(token, userId, requestProductId, clientId,
      locationId, isService, serialNumber, qty,
      () => addToInvoiceSuccess(dispatch, viewSuccessCallback),
      (error) => addToInvoiceFail(dispatch, error, viewErrorCallback)
    );
  }
}

export const addOnDemandItemToInvoice = (token, userId, productId, product,
  clientId, locationId, isService, serialNumber, qty, viewSuccessCallback, viewErrorCallback) => {
  return (dispatch) => {
    dispatch({ type: ADD_TO_INVOICE_START });
    services.workList.addOnDemandItemToInvoice(token, userId, productId, product,
      clientId, locationId, isService, serialNumber, qty,
      () => addToInvoiceSuccess(dispatch, viewSuccessCallback),
      (error) => addToInvoiceFail(dispatch, error, viewErrorCallback)
    );
  }
}

const addToInvoiceSuccess = (dispatch, viewSuccessCallback) => {
  dispatch({ type: ADD_TO_INVOICE_SUCCESS });
  setTimeout(function() {
    viewSuccessCallback();
  }, 500);

}

const addToInvoiceFail = (dispatch, error, viewErrorCallback) => {
  dispatch({ type: ADD_TO_INVOICE_FAIL });
  viewErrorCallback();
}
