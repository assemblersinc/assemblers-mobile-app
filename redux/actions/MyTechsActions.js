import services from '../../services';

import {
  GET_TECHS_START,
  GET_TECHS_SUCCESS,
  GET_TECHS_FAIL,
  GET_ALL_TECHS_START,
  GET_ALL_TECHS_SUCCESS,
  GET_ALL_TECHS_FAIL,
  CLAIM_TECH_START,
  CLAIM_TECH_SUCCESS,
  CLAIM_TECH_FAIL,
  DELETE_TECH_START,
  DELETE_TECH_SUCCESS,
  DELETE_TECH_FAIL,
  GET_AREA_TECHS_START,
  GET_AREA_TECHS_SUCCESS,
  GET_AREA_TECHS_FAIL
} from '../types';

export const getMyTechs = (managerId, token, callback) => {
  console.log('getMyTechs');
  return (dispatch) => {
    dispatch({ type: GET_TECHS_START });
    services.myTechs.getMyTechs(managerId, token,
      (techs) => getTechsSuccess(dispatch, techs, managerId, token, callback),
      (error) => getTechsFail(dispatch, error)
    );
  }
}

const getTechsSuccess = (dispatch, techs, managerId, token, callback) => {
  dispatch({ type: GET_TECHS_SUCCESS, payload: techs });
  callback(managerId, token);
}

const getTechsFail = (dispatch, error) => {
  dispatch({ type: GET_TECHS_FAIL, payload: error });
}

export const getAllTechs = (managerId, token) => {
  return (dispatch) => {
    dispatch({ type: GET_ALL_TECHS_START });
    services.myTechs.getAllTechs(managerId, token,
      (techs) => getAllTechsSuccess(dispatch, techs, managerId, token),
      (error) => getAllTechsFail(dispatch, error)
    );
  }
}

const getAllTechsSuccess = (dispatch, techs) => {
  dispatch({ type: GET_ALL_TECHS_SUCCESS, payload: techs });
}

const getAllTechsFail = (dispatch, error) => {
  dispatch({ type: GET_ALL_TECHS_FAIL, payload: error });
}

export const claimTech = (managerId, techId, token, viewCallback, viewErrorCallback) => {
  console.log(managerId, techId);
  return (dispatch) => {
    dispatch({ type: CLAIM_TECH_START });
    services.myTechs.claimTech(managerId, techId, token,
      () => claimTechSuccess(dispatch, viewCallback),
      (error) => claimTechFail(dispatch, error, viewErrorCallback)
    );
  }
}

const claimTechSuccess = (dispatch, viewCallback) => {
  dispatch({ type: CLAIM_TECH_SUCCESS });
  viewCallback();
}

const claimTechFail = (dispatch, error, viewErrorCallback) => {
  dispatch({ type: CLAIM_TECH_FAIL });
  viewErrorCallback();
}

export const deleteTech = (managerId, techId, token, viewCallback, viewErrorCallback) => {
  return (dispatch) => {
    dispatch({ type: DELETE_TECH_START });
    services.myTechs.dropTech(managerId, techId, token,
      () => deleteTechSuccess(dispatch, viewCallback),
      (error) => deleteTechFail(dispatch, error, viewErrorCallback)
    );
  }
}

const deleteTechSuccess = (dispatch, viewCallback) => {
  dispatch({ type: DELETE_TECH_SUCCESS });
  viewCallback();
}

const deleteTechFail = (dispatch, error, viewCallback) => {
  dispatch({ type: DELETE_TECH_FAIL });
  viewErrorCallback();
}

export const getAreaTechs = (token, areaId) => {
  return(dispatch) => {
    dispatch({ type: GET_AREA_TECHS_START });
    services.myTechs.getAreaTechs(token, areaId,
      (techs) => getAreaTechsSuccess(dispatch, techs),
      () => getAreaTechsFail(dispatch)
    );
  }
}

const getAreaTechsSuccess = (dispatch, techs) => {
  dispatch({ type: GET_AREA_TECHS_SUCCESS, payload: techs });
}

const getAreaTechsFail = (dispatch) => {
  dispatch({ type: GET_AREA_TECHS_FAIL });
}
