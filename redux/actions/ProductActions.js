import services from '../../services';

import {
  SKU_TEXT_CHANGED,
  GET_SCANSKU_START,
  GET_SCANSKU_SUCCESS,
  GET_SCANSKU_FAIL,
  SEND_PRICING_REQUEST_START,
  SEND_PRICING_REQUEST_SUCCESS,
  SEND_PRICING_REQUEST_FAIL,
  SEARCH_SKU_START,
  SEARCH_SKU_SUCCESS,
  SEARCH_SKU_FAIL
} from '../types';

export const skuTextChanged = (token, skuText, clientId) => {
  if(skuText.length > 2) {
    return (dispatch) => {
      dispatch({ type: SEARCH_SKU_START, payload: skuText });
      services.products.partialSkuSearch(token, skuText, clientId,
        (results) => searchSkusSuccess(dispatch, results),
        (error) => searchSkusFail(dispatch, error)
      );
    };
  }

  return({ type: SKU_TEXT_CHANGED, payload: skuText});
}

const searchSkusSuccess = (dispatch, results) => {
  dispatch({ type: SEARCH_SKU_SUCCESS, payload: results });
}

const searchSkusFail = (dispatch, error) => {
  dispatch({ type: SEARCH_SKU_FAIL, payload: error });
}

export const clearProductResults = () => {
  return { type: SEARCH_SKU_SUCCESS, payload: [] };
}

export const searchScannedBarcode = (token, sku, clientId, viewCallback, viewErrorCallback) => {
  console.log(sku, clientId);

  return (dispatch) => {
    dispatch({ type: GET_SCANSKU_START });
    services.products.findScannedSku(token, sku, clientId,
      (product) => scannedSkuSearchSuccess(dispatch, product, viewCallback),
      (error) => scannedSkuSearchFail(dispatch, error, viewErrorCallback)
    );
  };
}

const scannedSkuSearchSuccess = (dispatch, product, viewCallback) => {
  console.log('scannedSkuSearchSuccess', product);
  dispatch({ type: GET_SCANSKU_SUCCESS });
  viewCallback(product);
}

const scannedSkuSearchFail = (dispatch, error, viewErrorCallback) => {
  dispatch({ type: GET_SCANSKU_FAIL });
  if(viewErrorCallback) viewErrorCallback();
}

export const submitPricingRequest = (token, userId, locationId, clientId, product,
  viewCallback, viewErrorCallback) => {
    return (dispatch) => {
      dispatch({ type: SEND_PRICING_REQUEST_START });
      services.products.submitPricingRequest(token, userId, locationId, clientId, product,
        () => submitPricingRequestSuccess(dispatch, viewCallback),
        (error) => submitPricingRequestFail(dispatch, error, viewErrorCallback)
      );
    };
}

const submitPricingRequestSuccess = (dispatch, viewCallback) => {
  dispatch({ type: SEND_PRICING_REQUEST_SUCCESS });
  viewCallback();
}

const submitPricingRequestFail = (dispatch, error, viewErrorCallback) => {
  dispatch({ type: SEND_PRICING_REQUEST_FAIL });
  setTimeout(function() {
    viewErrorCallback();
  }, 500);
}
