import services from '../../services';
import constants from '../../constants.json';

import {
  LOGIN_ID_CHANGED,
  LOGIN_PASSWORD_CHANGED,
  LOGIN_USER_START,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_SHOW_PASSWORD,
  LOGIN_HIDE_PASSWORD,
  PERMISSIONS_COMPLETED,
  LOGOUT_USER_START,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER_FAIL,
  SHOW_FIELDOPS_MODAL,
  HIDE_FIELDOPS_MODAL
} from '../types';

//Login form
export const loginIdChanged = (userId) => {
  return {
    type: LOGIN_ID_CHANGED,
    payload: userId
  };
}

export const loginPasswordChanged = (password) => {
  return {
    type: LOGIN_PASSWORD_CHANGED,
    payload: password
  };
}

export const showPassword = () => {
  return { type: LOGIN_SHOW_PASSWORD };
}

export const hidePassword = () => {
  return { type: LOGIN_HIDE_PASSWORD };
}

export const loginUser = ({userId, password}, callback) => {
  return (dispatch) => {
    dispatch({ type: LOGIN_USER_START });
    services.login.logInUser(userId, password,
      (user) => loginUserSuccess(dispatch, user, callback),
      (error) => loginUserFail(dispatch, error)
    );
  };
}

export const loginUserSuccess = (dispatch, user, callback) => {
  dispatch({ type: LOGIN_USER_SUCCESS, payload: user });
  callback();

  if(user.roleId == constants.roleIds.fieldops) {
    dispatch({ type: SHOW_FIELDOPS_MODAL });
  }
}

export const loginUserFail = (dispatch, error) => {
  dispatch({ type: LOGIN_USER_FAIL, payload: error });
}

export const showFieldopsSelectionModal = () => {
  return { type: SHOW_FIELDOPS_MODAL }
}

export const hideFieldopsSelectionModal = () => {
  return { type: HIDE_FIELDOPS_MODAL }
}

export const logoutUser = (token, userId) => {
  return (dispatch) => {
    dispatch({ type: LOGOUT_USER_START });
    services.login.logOutUser( token, userId,
      () => logoutUserSuccess(dispatch),
      (error) => logoutUserFail(dispatch, error)
    )
  }
}

export const logoutUserSuccess = (dispatch) => {
  dispatch({ type: LOGOUT_USER_SUCCESS });
}

export const logoutUserFail = (dispatch, error) => {
  dispatch({ type: LOGOUT_USER_FAIL, payload: error })
}

export const permissionsCompleted = () => {
  return({ type: PERMISSIONS_COMPLETED });
}
