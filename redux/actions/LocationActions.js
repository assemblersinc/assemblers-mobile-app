import services from '../../services';

import {
  CURRENT_STORE_SEARCH_START,
  CURRENT_STORE_SEARCH_SUCCESS,
  CURRENT_STORE_SEARCH_FAIL,
  FIND_MANAGER_STORE_START,
  MANAGER_STORE_FOUND,
  MANAGER_STORE_NOT_FOUND,
  CLEAR_SELECTED_STORE,
  SHOW_FIELDOPS_MODAL,
  HIDE_FIELDOPS_MODAL,
  GET_AREAS_START,
  GET_AREAS_SUCCESS,
  GET_AREAS_FAIL,
  FIELDOPS_AREA_SELECTED,
  UPDATE_LOCATION_TYPE,
  SHOW_AREA_MANAGER_LOCATION_MODAL,
  HIDE_AREA_MANAGER_LOCATION_MODAL
} from '../types';

//Find store within 500m of user
export const findCurrentStore = (token, isModal) => {
  return (dispatch) => {
    dispatch({ type: CURRENT_STORE_SEARCH_START });
    services.geolocation.findNearbyStore(token,
      (store, isModal) => findCurrentStoreSuccess(dispatch, store),
      (error, isModal) => findCurrentStoreFail(dispatch, error)
    );
  }
}

const findCurrentStoreSuccess = (dispatch, store, isModal) => {
  if(isModal) {
    dispatch({ type: OPEN_CLOCK_IN_PROMPT, payload: store });
  } else {
    dispatch({ type: CURRENT_STORE_SEARCH_SUCCESS, payload: store });
  }
}

const findCurrentStoreFail = (dispatch, error, isModal) => {
  if(isModal) {

  } else {
    dispatch({ type: CURRENT_STORE_SEARCH_FAIL, payload: error });
  }
}

export const clearSelectedStore = () => {
  return { type: CLEAR_SELECTED_STORE }
}

export const findManagerStore = (token, viewCallback, viewErrorCallback) => {
  return (dispatch) => {
    dispatch({ type: FIND_MANAGER_STORE_START });
     services.geolocation.findNearbyStore(token,
        (store) => findManagerStoreSuccess(dispatch, store, viewCallback),
        (error) => findManagerStoreFail(dispatch, error, viewErrorCallback)
      );
  }
}

const findManagerStoreSuccess = (dispatch, store, viewCallback) => {
 dispatch({ type: MANAGER_STORE_FOUND, payload: store });
 if(viewCallback) viewCallback();
 if(viewCallback) viewCallback(store);
}


const findManagerStoreFail = (dispatch, error, viewErrorCallback) => {
 dispatch({ type: MANAGER_STORE_NOT_FOUND });
 if(viewErrorCallback) viewErrorCallback();
}

export const checkAreaManagerLocation = (token, timesheetLocationId) => {
  console.log('checkAreaManagerLocation', timesheetLocationId);
  return (dispatch) => {
    dispatch({ type: FIND_MANAGER_STORE_START });
     services.geolocation.findNearbyStore(token,
        (store) => checkAreaManagerLocationSuccess(dispatch, timesheetLocationId, store),
        (error) => findManagerStoreFail(dispatch, error, null)
      );
  }
}

const checkAreaManagerLocationSuccess = (dispatch, timesheetLocationId, newLocation) => {
  console.log('checkAreaManagerLocationSuccess', timesheetLocationId, newLocation.location[0].locationId);
  if(newLocation && newLocation.location && timesheetLocationId == newLocation.location[0].locationId) {
    hideAreaManagerLocationModal();
  } else {
    dispatch({ type: SHOW_AREA_MANAGER_LOCATION_MODAL, payload: newLocation.location[0] });
  }
}

export const hideAreaManagerLocationModal = () => {
  return ({ type: HIDE_AREA_MANAGER_LOCATION_MODAL });
}

export const getAreasRegions = (token) => {
  return (dispatch) => {
    dispatch({ type: GET_AREAS_START });
    services.geolocation.getAreasRegions( token,
      (regions) => getAreasRegionsSuccess(dispatch, regions),
      (error) => getAreasRegionsFail(dispatch, error)
    )
  }
}

const getAreasRegionsSuccess = (dispatch, regions) => {
  dispatch({ type: GET_AREAS_SUCCESS, payload: regions });
}

const getAreasRegionsFail = (dispatch, error) => {
  dispatch({ type: GET_AREAS_FAIL });
}

export const fieldOpsSelectAreaRegion = (area, region) => {
  return { type: FIELDOPS_AREA_SELECTED, payload: { area: area, region: region } };
}
