import services from '../../services';

import {
  GET_SERVICES_LIST_START,
  GET_SERVICES_LIST_SUCCESS,
  GET_SERVICES_LIST_FAIL,
  SEARCH_SERVICES
} from '../types';

export const getServicesList = (token) => {
  return (dispatch) => {
    dispatch({ type: GET_SERVICES_LIST_START });
    services.products.getServicesList(token,
      (serviceList) => getServicesSuccess(dispatch, serviceList),
      (error) => getServicesFail(dispatch, error)
    );
  }
}

const getServicesSuccess = (dispatch, workList) => {
  dispatch({ type: GET_SERVICES_LIST_SUCCESS, payload: workList });
}

const getServicesFail = (dispatch, error) => {
  dispatch({ type: GET_SERVICES_LIST_FAIL, payload: error})
}

export const searchServices = (text) => {
  return({ type: SEARCH_SERVICES, payload: text });
}
