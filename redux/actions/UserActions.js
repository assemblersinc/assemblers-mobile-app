import services from '../../services';

import {
  POST_NOTIFICATION_TOKEN_SUCCESS,
  POST_NOTIFICATION_TOKEN_FAIL,
  SHOW_CONNECTIVITY_BANNER,
  HIDE_CONNECTIVITY_BANNER
} from '../types';

export const updateNotificationTokenRegistration = (token, registration, userId) => {

  console.log('updateNotificationTokenRegistration');
  return (dispatch) => {
    services.user.updateNotificationTokenRegistrationService(token, registration, userId,
      (response) => updatedNotificationTokenSuccess(dispatch, response),
      (error) => updatedNotificationTokenFail(dispatch, error)
    );
  }
}

export const updatedNotificationTokenSuccess = (dispatch, response) => {
    console.log('updateNotificationTokenRegistration success ');
  dispatch({ type: POST_NOTIFICATION_TOKEN_SUCCESS, payload: response });
}

export const updatedNotificationTokenFail = (dispatch, error) => {
  dispatch({ type: POST_NOTIFICATION_TOKEN_FAIL, payload: error})
}

export const showBanner = (text, color, textColor) => {
  return { type: SHOW_CONNECTIVITY_BANNER, payload: { text, color, textColor } }
}

export const hideBanner = () => {
  return { type: HIDE_CONNECTIVITY_BANNER }
}
