import services from '../../services';

import {
  GET_SURVEY_START,
  GET_SURVEY_SUCCESS,
  GET_SURVEY_FAIL,
  SURVEY_QUESTION_ANSWERED,
  CLEAR_SURVEY_ANSWERS,
  ADD_PRINTED_NAME
} from '../types';

/*
** Get Survey Questions
*/

export const getSurveyQuestions = (token) => {
  return (dispatch) => {
    dispatch({ type: GET_SURVEY_START });
    services.survey.getSurveyQuestions(token,
      (questions) => getSurveyQuestionSuccess(dispatch, questions),
      (error) => getSurveyQuestionsFail(dispatch, error)
    );
  }
}

const getSurveyQuestionSuccess = (dispatch, questions) => {
  dispatch({ type: GET_SURVEY_SUCCESS, payload: questions });
}

const getSurveyQuestionsFail = (dispatch, error) => {
  dispatch({ type: GET_SURVEY_FAIL, payload: error });
}


export const answerSurveyQuestion = (index, answer) => {
  return {
    type: SURVEY_QUESTION_ANSWERED,
    payload: { index: index, answer: answer }
  }
}

export const clearSurveyAnswers = () => {
  return({
    type: CLEAR_SURVEY_ANSWERS
  })
}

export const addSurveyPrintedName = (name) => {
  return (dispatch) =>{
    dispatch ({
      type: ADD_PRINTED_NAME,
      payload: name
    });
  };
}
