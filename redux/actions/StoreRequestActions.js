import services from '../../services';

import {
  GET_MANAGER_STORE_REQUEST_START,
  GET_MANAGER_STORE_REQUEST_SUCCESS,
  GET_MANAGER_STORE_REQUEST_FAIL,
  SELECT_STORE_REQUEST,
  GET_STORE_REQUEST_START,
  GET_STORE_REQUEST_SUCCESS,
  GET_STORE_REQUEST_FAIL
} from '../types';

export const getManagerStoreRequestList = (token, areaId, viewCallback) => {
  return (dispatch) => {
    dispatch({ type: GET_MANAGER_STORE_REQUEST_START });
    services.storeRequest.getManagerStoreRequestList(token, areaId,
      (results) => getManagerStoreRequestListSuccess(dispatch, results, viewCallback),
      (error) => getManagerStoreRequestListFail(dispatch, error)
    );
  }
}

const getManagerStoreRequestListSuccess = (dispatch, results, viewCallback) => {
  dispatch({ type: GET_MANAGER_STORE_REQUEST_SUCCESS, payload: results });
  viewCallback();
}

const getManagerStoreRequestListFail = (dispatch, error) => {
  dispatch({ type: GET_MANAGER_STORE_REQUEST_FAIL, payload: error });
}

export const selectRequest = (request) => {
  console.log(request);
  return { type: SELECT_STORE_REQUEST, payload: request }
}

export const getStoreRequest = (token, locationId) => {
  return (dispatch) => {
    dispatch({ type: GET_STORE_REQUEST_START });
    services.storeRequest.getStoreRequest(token, locationId,
      (results) => getStoreRequestSuccess(dispatch, results),
      (error) => getStoreRequestFail(dispatch, error)
    );
  }
}

export const getStoreRequestSuccess = (dispatch, results) => {
  dispatch({ type: GET_STORE_REQUEST_SUCCESS, payload: results });
}

export const getStoreRequestFail = (dispatch, error) => {
  dispatch({ type: GET_STORE_REQUEST_FAIL, payload: error})
}
