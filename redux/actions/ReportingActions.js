import {GET_REPORTS_FAIL, GET_REPORTS_START, GET_REPORTS_SUCCESS} from "../types";
import * as services from "../../services/index";

export const getReports = (token, clientId, startDate, areaId, viewErrorCallback) => {
  return (dispatch) => {
    dispatch({ type: GET_REPORTS_START });
    services.reports.getReports(token, clientId, startDate, areaId,
      (reports) => getReportsSuccess(dispatch, reports),
      (error) => getReportsFail(dispatch, error, viewErrorCallback)
    );
  }
};

const getReportsSuccess = (dispatch, reports) => {
  dispatch({ type: GET_REPORTS_SUCCESS, payload: reports });
};

const getReportsFail = (dispatch, error, viewErrorCallback) => {
  dispatch({ type: GET_REPORTS_FAIL, payload: error});
  if(viewErrorCallback) viewErrorCallback();
};
