import services from '../../services';
import { Alert } from 'react-native';

import {
  GET_STOREWALK_START,
  GET_STOREWALK_SUCCESS,
  GET_STOREWALK_FAIL,
  START_STOREWALK_START,
  START_STOREWALK_SUCCESS,
  START_STOREWALK_FAIL,
  UPDATE_STOREWALK_ITEM_QTY_START,
  UPDATE_STOREWALK_ITEM_QTY_SUCCESS,
  UPDATE_STOREWALK_ITEM_QTY_FAIL,
  UPLOAD_STOREWALK_PHOTO_START,
  UPLOAD_STOREWALK_PHOTO_SUCCESS,
  UPLOAD_STOREWALK_PHOTO_FAIL,
  UPLOAD_STOREWALK_NOTE_START,
  UPLOAD_STOREWALK_NOTE_SUCCESS,
  UPLOAD_STOREWALK_NOTE_FAIL,
  ADD_STOREWALK_ITEM_START,
  ADD_STOREWALK_ITEM_SUCCESS,
  ADD_STOREWALK_ITEM_FAIL,
  SUBMIT_STOREWALK_START,
  SUBMIT_STOREWALK_SUCCESS,
  SUBMIT_STOREWALK_FAIL,
  EXIT_CLOCK_IN_FLOW
} from '../types';

export const startStoreWalk = (token, userId, locationId) => {
  return (dispatch) => {
    dispatch({ type: START_STOREWALK_START });
    services.storeWalk.startStoreWalk(token, userId, locationId,
      (storeWalk) => getStoreWalkSuccess(dispatch, storeWalk),
      (error) => getStoreWalkFail(dispatch, error)
    );
  }
}

const startStoreWalkSuccess = (dispatch, storeWalk) => {
  dispatch({ type: START_STOREWALK_SUCCESS, payload: storeWalk })
}

const startStoreWalkFail = (dispatch, error) => {
  dispatch({ type: START_STOREWALK_FAIL, payload: error })
}

export const getStoreWalk = (token, locationId) => {
  return (dispatch) => {
    dispatch({ type: GET_STOREWALK_START });
    services.storeWalk.getStoreWalk(token, locationId,
      (storeWalk) => getStoreWalkSuccess(dispatch, storeWalk),
      (error) => getStoreWalkFail(dispatch, error)
    );
  }
}

const getStoreWalkSuccess = (dispatch, workList) => {
  dispatch({ type: GET_STOREWALK_SUCCESS, payload: workList });
}

const getStoreWalkFail = (dispatch, error) => {
  dispatch({ type: GET_STOREWALK_FAIL, payload: error})
}

export const addStoreWalkItem = (token, userId, requestId, product, qty, location, viewCallback, errorCallback) => {
  return (dispatch) => {
    dispatch({ type: ADD_STOREWALK_ITEM_START });
    services.storeWalk.addStoreWalkItem(token, userId, requestId, product, qty, location,
      () => addStoreWalkItemSuccess(dispatch, viewCallback),
      (error) => addStoreWalkItemFail(dispatch, error, errorCallback)
    );
  }
}

export const addStoreWalkItemWithLoc = (token, userId, requestId, product, qty, locType, viewCallback, errorCallback) => {
  return (dispatch) => {
    dispatch({ type: ADD_STOREWALK_ITEM_START });
    services.storeWalk.addStoreWalkItem(token, userId, requestId, product, qty,
      () => addStoreWalkItemSuccess(dispatch, viewCallback),
      (error) => addStoreWalkItemFail(dispatch, error, errorCallback)
    );
  }
}

const addStoreWalkItemSuccess = (dispatch, viewCallback) => {
  dispatch({ type: ADD_STOREWALK_ITEM_SUCCESS });
  viewCallback();
}

const addStoreWalkItemFail = (dispatch, error, errorCallback) => {
  dispatch({ type: ADD_STOREWALK_ITEM_FAIL, payload: error});
  setTimeout(function() {
    errorCallback();
  }, 500);
}

export const updateStoreWalkItemQty = (token, requestId, product, qty, refreshStoreWalk) => {
  return (dispatch) => {
    dispatch({ type: UPDATE_STOREWALK_ITEM_QTY_START });
    services.storeWalk.updateStoreWalkItemQty(token, requestId, product, qty,
      () => updateStoreWalkItemQtySuccess(dispatch, refreshStoreWalk),
      (error) => updateStoreWalkItemQtyFail(dispatch, error)
    );
  }
}

const updateStoreWalkItemQtySuccess = (dispatch, refreshStoreWalk) => {
  refreshStoreWalk();
}

const updateStoreWalkItemQtyFail = (dispatch, error) => {
  dispatch({ type: UPDATE_STOREWALK_ITEM_QTY_FAIL, payload: error});
}

export const submitStoreWalk = (token, userId, locationId, refreshStoreWalk) => {
  return (dispatch) => {
    dispatch({ type: SUBMIT_STOREWALK_START });
    services.storeWalk.submitStoreWalk(token, userId, locationId,
      () => submitStoreWalkSuccess(dispatch, refreshStoreWalk),
      (error) => submitStoreWalkFail(dispatch, error)
    );
  }
}

const submitStoreWalkSuccess = (dispatch, refreshStoreWalk) => {
  refreshStoreWalk();
}

const submitStoreWalkFail = (dispatch, error) => {
  dispatch({ type: SUBMIT_STOREWALK_FAIL, payload: error});
}

export const uploadStoreWalkPhoto = (token, requestId, imagePath, notesPhotosObject, userObject, refreshStoreWalk) => {
  return (dispatch) => {
    dispatch({ type: UPLOAD_STOREWALK_PHOTO_START });
    services.storeWalk.uploadStoreWalkPhoto(token, requestId, imagePath, notesPhotosObject, userObject,
      () => uploadStoreWalkPhotoSuccess(dispatch, refreshStoreWalk),
      (error) => uploadStoreWalkPhotoFail(dispatch, error)
    );
  }
}

const uploadStoreWalkPhotoSuccess = (dispatch, refreshStoreWalk) => {
  refreshStoreWalk();
}

const uploadStoreWalkPhotoFail = (dispatch, error) => {
  dispatch({ type: UPLOAD_STOREWALK_PHOTO_FAIL, payload: error});
}

export const addStoreWalkComment = (token, requestId, comment, index, notesPhotosObject, refreshStoreWalk) => {
  return (dispatch) => {
    dispatch({ type: UPLOAD_STOREWALK_PHOTO_START });
    services.storeWalk.addStoreWalkNote(token, requestId, comment, index, notesPhotosObject,
      () => addStoreWalkCommentSuccess(dispatch, refreshStoreWalk),
      (error) => addStoreWalkCommentFail(dispatch, error)
    );
  }
}

const addStoreWalkCommentSuccess = (dispatch, refreshStoreWalk) => {
  refreshStoreWalk();
}

const addStoreWalkCommentFail = (dispatch, error) => {
  dispatch({ type: UPLOAD_STOREWALK_PHOTO_FAIL, payload: error});
}
