import services from '../../services';

import {
  GET_MANAGER_REPAIRS_START,
  GET_MANAGER_REPAIRS_SUCCESS,
  GET_MANAGER_REPAIRS_FAIL,
  GET_STORE_REPAIRS_START,
  GET_STORE_REPAIRS_SUCCESS,
  GET_STORE_REPAIRS_FAIL,
  GET_REPAIR_CATS_START,
  GET_REPAIR_CATS_SUCCESS,
  GET_REPAIR_CATS_FAIL,
  GET_REPAIR_SKUSCAN_START,
  GET_REPAIR_SKUSCAN_SUCCESS,
  GET_REPAIR_SKUSCAN_FAIL,
  CLEAR_STICKER_ERROR,
  REPAIR_CAT_SEARCH_TEXT_CHANGED,
  REPAIR_CAT_SELECTED,
  UPLOAD_REPAIR_PHOTO_START,
  UPLOAD_REPAIR_PHOTO_SUCCESS,
  UPLOAD_REPAIR_PHOTO_FAIL,
  REPAIR_PRODUCT_INFO_ADDED,
  CLEAR_REPAIR_PRODUCT_INFO,
  REPAIR_NOTE_TEXT_CHANGED,
  REPAIR_NOTE_FINISHED,
  CREATE_REPAIR_TICKET_START,
  CREATE_REPAIR_TICKET_SUCCESS,
  CREATE_REPAIR_TICKET_FAIL,
  UPDATE_REPAIR_TICKET_START,
  UPDATE_REPAIR_TICKET_SUCCESS,
  UPDATE_REPAIR_TICKET_FAIL,
  REMOVE_REPAIR_START,
  REMOVE_REPAIR_SUCCESS,
  REMOVE_REPAIR_FAIL,
  ADD_REPAIR_TO_INVOICE_START,
  ADD_REPAIR_TO_INVOICE_SUCCESS,
  ADD_REPAIR_TO_INVOICE_FAIL,
  SELECT_REPAIR_LOCATION,
  REPAIR_TICKET_SELECTED
} from '../types';

export const getManagerRepairsList = (token, areaId, viewCallback) => {
  return (dispatch) => {
    dispatch({ type: GET_MANAGER_REPAIRS_START });
    services.repairs.getManagerRepairsList(token, areaId,
      (results) => getManagerRepairsListSucess(dispatch, results, viewCallback),
      (error) => getManagerRepairsListFail(dispatch, error)
    );
  };
}

const getManagerRepairsListSucess = (dispatch, results, viewCallback) => {
  dispatch({ type: GET_MANAGER_REPAIRS_SUCCESS, payload: results })
  viewCallback();
}

const getManagerRepairsListFail = (dispatch, error) => {
  dispatch({ type: GET_MANAGER_REPAIRS_FAIL, payload: error })
}

export const selectRepairLocation = (location) => {
  return { type: SELECT_REPAIR_LOCATION, payload: location}
}

export const getStoreRepairsList = (token, locationId) => {
  return (dispatch) => {
    dispatch({ type: GET_STORE_REPAIRS_START });
    services.repairs.getStoreRepairsList(token, locationId,
      (results) => getStoreRepairsListSuccess(dispatch, results),
      (error) => getStoreRepairsListFail(dispatch, error)
    );
  };
}

const getStoreRepairsListSuccess = (dispatch, results) => {
  dispatch({ type: GET_STORE_REPAIRS_SUCCESS, payload: results })
}

const getStoreRepairsListFail = (dispatch, error) => {
  dispatch({ type: GET_STORE_REPAIRS_FAIL, payload: error });
}

export const getRepairCategories = (token, viewCallback) => {
  return (dispatch) => {
    dispatch({ type: GET_REPAIR_CATS_START });
    services.repairs.getRepairCategories(token,
      (results) => getRepairCategoriesSuccess(dispatch, results, viewCallback),
      (error) => getRepairCategoriesFail(dispatch, error)
    );
  };
}

const getRepairCategoriesSuccess = (dispatch, results, viewCallback) => {
  dispatch({ type: GET_REPAIR_CATS_SUCCESS, payload: results });
  viewCallback();
}

const getRepairCategoriesFail = (dispatch, error) => {
  dispatch({ type: GET_REPAIR_CATS_FAIL, payload: error });
}

export const searchScannedRepairSticker = (token, sticker, viewCallback, errorCallback) => {
  console.log(sticker);
  return (dispatch) => {
    dispatch({ type: GET_REPAIR_SKUSCAN_START });
    services.repairs.searchScannedRepairStickerService(token, sticker,
      (results) => searchScannedRepairStickerSuccess(dispatch, results, viewCallback),
      (error) => searchScannedRepairStickerFail(dispatch, error, errorCallback)
    );
  };
}

const searchScannedRepairStickerSuccess = (dispatch, results, viewCallback) => {
  dispatch({ type: GET_REPAIR_SKUSCAN_SUCCESS, payload: results });
  viewCallback(results[0]);
}

const searchScannedRepairStickerFail = (dispatch, error, errorCallback) => {
  dispatch({ type: GET_REPAIR_SKUSCAN_FAIL, payload: error });
  errorCallback();
}

export const clearStickerError = () => {
  return { type: CLEAR_STICKER_ERROR };
}

export const createRepairTicket = (token, userId, locationId, sticker, product,
  repairCategory, imageData, viewCallback) => {
  return (dispatch) => {
    dispatch({ type: CREATE_REPAIR_TICKET_START });
    services.repairs.createRepairTicket(token, userId, locationId, sticker, product,
      repairCategory, imageData,
      (results) => createRepairTicketSuccess(dispatch, viewCallback),
      (error) => createRepairTicketFail(dispatch, error)
    );
  };
}

const createRepairTicketSuccess = (dispatch, viewCallback) => {
  dispatch({ type: CREATE_REPAIR_TICKET_SUCCESS });
  setTimeout(function() {
    viewCallback();
  }, 1000);

}

const createRepairTicketFail = (dispatch, error) => {
  dispatch({ type: CREATE_REPAIR_TICKET_FAIL, payload: error });
}

export const updateRepairTicketWithPhoto = (token, locationId, storeRepairId, photoObject, viewCallback) => {
  return (dispatch) => {
    dispatch({ type: UPDATE_REPAIR_TICKET_START });
    services.repairs.updateRepairTicketWithPhoto(token, locationId, storeRepairId, photoObject,
      (ticket) => updateRepairTicketSuccess(dispatch, ticket, viewCallback),
      (error) => updateRepairTicketFail(dispatch, error)
    );
  };
}

export const updateRepairTicket = (token, storeRepairId, userId,
  repairId, locationId, imageData, viewCallback) => {
  return (dispatch) => {
    dispatch({ type: UPDATE_REPAIR_TICKET_START });
    services.repairs.updateRepairTicket(token, storeRepairId, userId, repairId, locationId, imageData,
      (ticket) => updateRepairTicketSuccess(dispatch, ticket, viewCallback),
      (error) => updateRepairTicketFail(dispatch, error)
    );
  };
}

const updateRepairTicketSuccess = (dispatch, ticket, viewCallback) => {
  dispatch({ type: UPDATE_REPAIR_TICKET_SUCCESS, payload: ticket });
  viewCallback();
}

const updateRepairTicketFail = (dispatch, error) => {
  dispatch({ type: UPDATE_REPAIR_TICKET_FAIL, payload: error });
}

export const uploadRepairPhoto = (token, imagePath, viewCallback) => {
  return (dispatch) => {
    dispatch({ type: UPLOAD_REPAIR_PHOTO_START });
    services.repairs.uploadRepairPhoto(token, imagePath,
      (photo) => uploadRepairPhotoSuccess(dispatch, photo, viewCallback),
      (error) => uploadRepairPhotoFail(dispatch, error)
    );
  };
}

const uploadRepairPhotoSuccess = (dispatch, photo, viewCallback) => {
  dispatch({ type: UPLOAD_REPAIR_PHOTO_SUCCESS, payload: photo });
    viewCallback();
}

const uploadRepairPhotoFail = (dispatch, error) => {
  dispatch({ type: UPLOAD_REPAIR_PHOTO_FAIL, payload: error });
}

export const repairCategorySearch = (searchText) => {
  return {
    type: REPAIR_CAT_SEARCH_TEXT_CHANGED,
    payload: searchText
  }
}

export const repairCategorySelected = (category) => {
  return{
    type: REPAIR_CAT_SELECTED,
    payload: category
  }
}

export const productInfoAdded = (product) => {
  return {
    type: REPAIR_PRODUCT_INFO_ADDED,
    payload: product
  }
}

export const clearProductInfo = () => {
  return {
    type: CLEAR_REPAIR_PRODUCT_INFO
  }
}

export const repairCommentAdded = (comment) => {
  return {
    type: REPAIR_NOTE_FINISHED,
    payload: comment
  }
}

export const removeRepairFromTicket = (token, storeRepairId, viewCallback, viewEmptyCallback, errorViewCallbck) => {
  return (dispatch) => {
    dispatch({ type: REMOVE_REPAIR_START });
    services.repairs.removeRepairFromTicket(token, storeRepairId,
      (ticket) => removeRepairFromTicketSuccess(dispatch, ticket, viewCallback),
      () => removeRepairCloseTicket(dispatch, viewEmptyCallback),
      (error) => removeRepairFromTicketFail(dispatch, error, errorViewCallbck)
    );
  };
}

removeRepairFromTicketSuccess = (dispatch, ticket, viewCallback) => {
  dispatch({ type: REMOVE_REPAIR_SUCCESS, payload: ticket });
  viewCallback();
}

removeRepairCloseTicket = (dispatch, viewEmptyCallback) => {
  dispatch({ type: REMOVE_REPAIR_SUCCESS });
  viewEmptyCallback();
}

removeRepairFromTicketFail = (dispatch, error, errorViewCallbck) => {
  dispatch({ type: REMOVE_REPAIR_FAIL });
  errorViewCallbck();
}

export const addRepairToInvoice = (token, repairsList, viewCallback) => {
  return (dispatch) => {
    dispatch({ type: ADD_REPAIR_TO_INVOICE_START });
    services.repairs.addRepairToInvoice(token, repairsList,
      (photo) => addRepairToInvoiceSuccess(dispatch, viewCallback),
      (error) => addRepairToInvoiceFail(dispatch, error)
    );
  };
}

const addRepairToInvoiceSuccess = (dispatch, viewCallback) => {
  dispatch({ type: ADD_REPAIR_TO_INVOICE_SUCCESS });
  viewCallback();
}

const addRepairToInvoiceFail = (dispatch) => {
  dispatch({ type: ADD_REPAIR_TO_INVOICE_FAIL });
}

export const selectRepairTicket = (ticket) => {
  return {
    type: REPAIR_TICKET_SELECTED,
    payload: ticket
  }
}
