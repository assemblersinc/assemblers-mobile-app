import services from '../../services';

import {
  MESSAGE_SENT,
  MESSAGE_RECEIVED,
  GET_CHAT_MESSAGES_START,
  GET_CHAT_MESSAGES_SUCCESS,
  GET_CHAT_MESSAGES_FAIL,
  GET_LAST_CHAT_MESSAGES_START,
  GET_LAST_CHAT_MESSAGES_SUCCESS,
  GET_LAST_CHAT_MESSAGES_FAIL,
  MESSAGE_SENT_FAIL,
  CONVERSTATION_CLICKED,
  MESSAGE_CHANGED,
  SEND_GENERATED_MESSAGE,
  CLEAR_MESSAGE_TEXT, 
  UPDATE_CHAT_MESSAGE_COUNT,
  UPDATE_CHAT_PRIORITY_MESSAGE_COUNT, 
  CLEAR_QUICK_MESSAGE,
  SET_TECH_CHAT
} from '../types';

export const getLastHistoryMessages  = (token, userId) => {
  console.log('Chat:: getLastHistoryMessages', userId)
  return (dispatch) => {
    dispatch({ type: GET_LAST_CHAT_MESSAGES_START });
    services.chat.getLastHistoryMessages(token, userId,
      (messages) => getLastChatMessageHistorySuccess(dispatch, messages),
      (error) => getLastChatMessageHistoryFail(dispatch, error)
    );
  }
}

export const getLastChatMessageHistorySuccess = (dispatch, messages) =>{
  console.log('getLastChatMessageHistorySuccess');
  dispatch({ type: GET_LAST_CHAT_MESSAGES_SUCCESS, payload: messages });
}


export const getLastChatMessageHistoryFail = (dispatch, messages) =>{
  console.log('getLastChatMessageHistoryFail');
  dispatch({ type: GET_LAST_CHAT_MESSAGES_FAIL, payload: messages });
}


export const messageTextChange = (text) => {
  return {
    type: MESSAGE_CHANGED,
    payload: text
  };
}

export const getChatMessages = (query, token) => {

  console.log('Chat:: getChatMessages');

  return (dispatch) => {
    console.log('inside dispatch');
    dispatch({ type: GET_CHAT_MESSAGES_START });
    services.chat.getChatMessages(token, query,
      (messages) => getMessagesSuccess(dispatch, messages),
      (error) => getMessagesFail(dispatch, error)
    );
  }
}

const getMessagesSuccess = (dispatch, messages) => {
  dispatch({ type: GET_CHAT_MESSAGES_SUCCESS, payload: messages });
}

const getMessagesFail= (dispatch, messages) => {
  dispatch({ type: GET_CHAT_MESSAGES_FAIL, payload: messages });
}

export const sendMessage = (token, message, refreshFunction) => {
  console.log('Chat:: sendMessage',message)
  return (dispatch) => {
    services.chat.sendMessage(token, message.senderId, message.receiverId, message.message, message.locationId,
      (messages) => messageSentSuccess(dispatch, messages, refreshFunction),
      (error) => messageSentFail(dispatch, error)
    );
  }
}

const messageSentSuccess = (dispatch, message, refreshFunction) => {
  console.log('sent chat message succeeded', message);
  dispatch({ type: MESSAGE_SENT, payload: message });
 // refreshFunction();

  clearCurrentText();
}

const messageSentFail = (dispatch, message) => {
  console.log('sent chat message failed');
  dispatch({ type: MESSAGE_SENT_FAIL, payload: message });
}

export const messageReceived = (dispatch, message,refreshFunction) => {
  console.log('received chat message succeeded');
  return({ type: MESSAGE_RECEIVED, payload: message });
 // refreshFunction();
}

export const updateChatMessageCounts = (counts) =>{
  console.log('updateChatMessageCounts');
  return ({ type: UPDATE_CHAT_MESSAGE_COUNT, payload: counts });
}

export const setTechChat = (tech) =>{
  return {
    type: SET_TECH_CHAT,
    payload: { "tech": tech, "chatWithTech": true }
  }
}
export const sendGeneratedMessage = (message, location) => {
  return {
    type: SEND_GENERATED_MESSAGE,
    payload: { "message": message, "location": location }
  }
}

export const clearCurrentText = () => {
  return {
    type: CLEAR_MESSAGE_TEXT
  }
}
export const clearQuickMessage = () => {
  return ({ type: CLEAR_QUICK_MESSAGE }); 
}


