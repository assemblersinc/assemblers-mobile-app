import services from '../../services';

import {
  GET_CLIENTS_START,
  GET_CLIENTS_SUCCESS,
  GET_CLIENTS_FAIL
} from '../types';

export const getClients = (query, token) => {

  return (dispatch) => {
    dispatch({ type: GET_CLIENTS_START });
    services.client.getClients(query,token,
      (client) => getClientSuccess(dispatch, client),
      (error) => getClientFail(dispatch, error)
    );
  }
}

export const getClientSuccess = (dispatch, clients) => {
  console.log('getClients Success');
  dispatch({ type: GET_CLIENTS_SUCCESS, payload: clients });
}

export const getClientFail = (dispatch, error) => {
  console.log('getClients failed');
  dispatch({ type: GET_CLIENTS_FAIL, payload: error})
}
