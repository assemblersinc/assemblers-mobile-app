//login flow
export const LOGIN_ID_CHANGED = "login_id_changed";
export const LOGIN_PASSWORD_CHANGED = "login_password_changed";
export const LOGIN_USER_START = "login_user_start";
export const LOGIN_USER_SUCCESS = "login_user_success";
export const LOGIN_USER_FAIL = "login_user_fail";
export const LOGIN_SHOW_PASSWORD = "login_show_password";
export const LOGIN_HIDE_PASSWORD = "login_hide_password";
export const LOGOUT_USER_START = "logout_user_start";
export const LOGOUT_USER_SUCCESS = "logout_user_success";
export const LOGOUT_USER_FAIL = "logout_user_fail";
export const PERMISSIONS_COMPLETED = "permissions_completed";
export const UPDATE_LOCATION_TYPE = "update_location_type";
export const SHOW_FIELDOPS_MODAL = "show_fieldops_modal";
export const HIDE_FIELDOPS_MODAL = "hide_fieldops_modal";
export const GET_AREAS_START = "get_areas_start";
export const GET_AREAS_SUCCESS = "get_areas_success";
export const GET_AREAS_FAIL = "get_areas_fail";
export const FIELDOPS_AREA_SELECTED = "fieldops_area_selected";

//USER
export const POST_NOTIFICATION_TOKEN_SUCCESS = "notification_token_success";
export const POST_NOTIFICATION_TOKEN_FAIL = "notification_token_fail";
export const SHOW_CONNECTIVITY_BANNER = "show_connectivity_banner";
export const HIDE_CONNECTIVITY_BANNER = "hide_connectivity_banner";

//clock in flow
export const ENTER_CLOCK_IN_FLOW = "enter_clock_in_flow";
export const EXIT_CLOCK_IN_FLOW = "exit_clock_in_flow";
export const OPEN_CLOCK_IN_PROMPT = "open_clock_in_prompt";
export const CLOSE_CLOCK_IN_PROMPT = "close_clock_in_promp";
export const OPEN_CLOCK_OUT_PROMPT = "open_clock_out_prompt";
export const CLOSE_CLOCK_OUT_PROMPT = "close_clock_out_prompt";
export const OPEN_INACTIVITY_MODAL = "open_inactivity_modal";
export const CLOSE_INACTIVITY_MODAL = "close_inactivity_modal";
export const CLOCK_IN_STORE_NAME_CHANGED = "clock_in_store_name_changed";
export const CLOCK_IN_STORE_NUMBER_CHANGED = "clock_in_store_number_changed";
export const CURRENT_STORE_SEARCH_START = "current_store_search_start";
export const CURRENT_STORE_SEARCH_SUCCESS = "current_store_search_success";
export const CURRENT_STORE_SEARCH_FAIL = "current_store_search_fail";
export const CLOCK_IN_START = "clockin_start";
export const CLOCKED_IN_SUCCESS = "clockin_success";
export const CLOCKED_IN_FAIL = "clockin_fail";
export const CLOCK_OUT_START = "clockout_start";
export const CLOCKED_OUT_SUCCESS = "clockout_success";
export const CLOCKED_OUT_FAIL = "clockout_fail";
export const STAY_CLOCKED_IN = "stay_clocked_in";
export const MEAL_IN_START = "meal_in_start";
export const MEAL_IN_SUCCESS = "meal_in_success";
export const MEAL_IN_FAIL = "meal_in_fail";
export const MEAL_OUT_START = "meal_out_start";
export const MEAL_OUT_SUCCESS = "meal_out_success";
export const MEAL_OUT_FAIL = "meal_out_fail";
export const SELECT_CLIENT = "select_client";
export const SELECT_STORE = "select_store";
export const UPDATE_CITY_TEXT = "update_city_text";
export const UPDATE_STATE_TEXT = "update_state_text";
export const UPDATE_ZIP_TEXT = "update_zip_text";
export const STORE_SEARCH_START = "store_search_start";
export const STORE_SEARCH_SUCCESS = "store_search_success";
export const STORE_SEARCH_FAIL = "store_search_fail";
export const GET_CLOCKIN_LOCATION_START = "get_clockin_location_start";
export const GET_CLOCKIN_LOCATION_SUCCESS = "get_clockin_location_success";
export const GET_CLOCKIN_LOCATION_FAIL = "get_clockin_location_fail";
export const SHOW_CLOCKIN_LEFT_STORE_MODAL = "show_clockin_left_store_modal";
export const HIDE_CLOCKIN_LEFT_STORE_MODAL = "hide_clockin_left_store_modal";
export const SET_CLOCKIN_ERROR = "set_clockin_error";
export const LOCATION_TIMER_START = "location_timer_start";
export const LOCATION_TIMER_TICK = "location_timer_tick";
export const LOCATION_TIMER_STOP = "location_timer_stop";
export const SHOW_INACTIVITY_MODAL = "show_inactivity_modal";
export const HIDE_INACTIVITY_MODAL = "hide_inactivity_modal";
export const CHECK_IN_SUBCONTRACTOR = "check_in_subcontractor";
export const CHECK_OUT_SUBCONTRACTOR = "check_out_subcontractor";
export const CLEAR_NEEDS_UPDATE = "clear_needs_update";
export const CLEAR_SELECTED_STORE = "clear_selected_store";
export const TRAVEL_STORE_START = "travel_store_start";
export const TRAVEL_STORE_SUCCESS = "travel_store_success";
export const TRAVEL_STORE_FAIL = "travel_store_fail";
export const ALREADY_CLOCKED_IN = "already_clocked_in";

//announcements
export const GET_ANNOUNCEMENTS_START = "get_announcements_start";
export const GET_ANNOUNCEMENTS_SUCCESS = "get_announcements_success";
export const GET_ANNOUNCEMENTS_FAIL = "get_announcements_fail";

//chat
export const MESSAGE_SENT = "message_sent";
export const MESSAGE_SENT_FAIL = "message_sent_fail";
export const MESSAGE_RECEIVED = "message_received";
export const GET_CHAT_MESSAGES_START = "get_chat_message_start";
export const GET_CHAT_MESSAGES_SUCCESS = "get_chat_message_success";
export const GET_CHAT_MESSAGES_FAIL = "get_chat_message_fail";
export const GET_LAST_CHAT_MESSAGES_START = "get_last_chat_messages_start";
export const GET_LAST_CHAT_MESSAGES_SUCCESS ="get_last_chat_messages_success";
export const GET_LAST_CHAT_MESSAGES_FAIL = "get_last_chat_messages_fail";
export const CONVERSATION_CLICKED = "converstation_clicked";
export const GET_MESSAGES_START = "get_messages_start";
export const UPDATE_CHAT_MESSAGE_COUNT = "update_chat_message_count";
export const UPDATE_CHAT_READ_MESSAGE = "update_chat_message_count";
export const MESSAGE_CHANGED = "message_changed";
export const SEND_GENERATED_MESSAGE = "send_generated_message";
export const CLEAR_MESSAGE_TEXT = "clear_message_text";
export const UPDATE_CHAT_PRIORITY_MESSAGE_COUNT  = "update_chat_priority_message_count";
export const CLEAR_QUICK_MESSAGE = "clear_quick_message";
export const SET_TECH_CHAT = "set_tech_chat";

//work list
export const GET_WORKLIST_START = "get_worklist_start";
export const GET_WORKLIST_SUCCESS = "get_worklist_success";
export const GET_WORKLIST_FAIL = "get_worklist_fail";
export const SELECT_WORKLIST_ITEM = "select_worklist_item";
export const SEARCH_WORKLIST_SERVICE_START = "search_worklist_service_start";
export const SEARCH_WORKLIST_SERVICE_SUCCESS = "search_worklist_service_success";
export const SEARCH_WORKLIST_SERVICE_FAIL = "search_worklist_service_fail";
export const SEARCH_WORKLIST_SKU_START = "search_worklist_sku_start";
export const SEARCH_WORKLIST_SKU_SUCCESS = "search_worklist_sku_success";
export const SEARCH_WORKLIST_SKU_FAIL = "search_worklist_sku_fail";
export const ADD_TO_INVOICE_START = "add_to_invoice_start";
export const ADD_TO_INVOICE_SUCCESS = "add_to_invoice_success";
export const ADD_TO_INVOICE_FAIL = "add_to_invoice_fail";

//store walk
export const GRILLBARROW_SUBMIT_START = "grillbarrow_submit_start";
export const GRILLBARROW_SUBMIT_SUCCESS = "grillbarrow_submit_success";
export const GRILLBARROW_SUBMIT_FAIL = "grillbarrow_submit_fail";
export const SHOW_GRILLBARROW_MODAL = "show_grillbarrow_modal";
export const HIDE_GRILLBARROW_MODAL = "hide_grillbarrow_modal";
export const START_STOREWALK_START = "start_storewalk_start";
export const START_STOREWALK_SUCCESS = "start_storewalk_success";
export const START_STOREWALK_FAIL = "start_storewalk_fail";
export const GET_STOREWALK_START = "get_storewalk_start";
export const GET_STOREWALK_SUCCESS = "get_storewalk_success";
export const GET_STOREWALK_FAIL = "get_storewalk_fail";
export const UPDATE_STOREWALK_ITEM_QTY_START = "increase_storewalk_item_qty_start";
export const UPDATE_STOREWALK_ITEM_QTY_SUCCESS = "increase_storewalk_item_qty_success";
export const UPDATE_STOREWALK_ITEM_QTY_FAIL = "increase_storewalk_item_qty_fail";
export const UPLOAD_STOREWALK_PHOTO_START = "upload_storewalk_photo_start";
export const UPLOAD_STOREWALK_PHOTO_SUCCESS = "upload_storewalk_photo_success"
export const UPLOAD_STOREWALK_PHOTO_FAIL = "upload_storewalk_photo_fail";
export const UPLOAD_STOREWALK_NOTE_START = "upload_storewalk_note_start";
export const UPLOAD_STOREWALK_NOTE_SUCCESS = "upload_storewalk_note_success"
export const UPLOAD_STOREWALK_NOTE_FAIL = "upload_storewalk_note_fail";
export const ADD_STOREWALK_ITEM_START = "add_storewalk_item_start";
export const ADD_STOREWALK_ITEM_SUCCESS = "add_storewalk_item_success";
export const ADD_STOREWALK_ITEM_FAIL = "add_storewalk_item_fail";
export const SUBMIT_STOREWALK_START = "submit_storewalk_start";
export const SUBMIT_STOREWALK_SUCCESS = "submit_storewalk_success";
export const SUBMIT_STOREWALK_FAIL = "submit_storewalk_fail";

//products
export const SKU_TEXT_CHANGED = "sku_text_changed";
export const GET_SCANSKU_START = "get_scansku_start";
export const GET_SCANSKU_SUCCESS = "get_scansku_success";
export const GET_SCANSKU_FAIL = "get_scansku_fail";
export const SEARCH_SKU_START = "search_sku_start";
export const SEARCH_SKU_SUCCESS = "search_sku_success";
export const SEARCH_SKU_FAIL = "search_sku_fail";
export const SEND_PRICING_REQUEST_START = "send_pricing_request_start";
export const SEND_PRICING_REQUEST_SUCCESS = "send_pricing_request_success";
export const SEND_PRICING_REQUEST_FAIL = "send_pricing_request_fail";
export const GET_SERVICES_LIST_START = "get_services_list_start";
export const GET_SERVICES_LIST_SUCCESS = "get_services_list_success";
export const GET_SERVICES_LIST_FAIL = "get_services_list_fail";
export const SEARCH_SERVICES = "search_services";

//repairs
export const GET_MANAGER_REPAIRS_START = "get_manager_repairs_start";
export const GET_MANAGER_REPAIRS_SUCCESS = "get_manager_repairs_success";
export const GET_MANAGER_REPAIRS_FAIL = "get_manager_repairs_fail";
export const GET_STORE_REPAIRS_START = "get_store_repairs_start";
export const GET_STORE_REPAIRS_SUCCESS = "get_store_repairs_success";
export const GET_STORE_REPAIRS_FAIL = "get_store_repairs_fail";
export const GET_REPAIR_CATS_START = "get_repair_cats_start";
export const GET_REPAIR_CATS_SUCCESS = "get_repair_cats_success";
export const GET_REPAIR_CATS_FAIL = "get_repair_cats_fail";
export const GET_REPAIR_SKUSCAN_START = "get_repair_skuscan_start";
export const GET_REPAIR_SKUSCAN_SUCCESS = "get_repair_skuscan_success";
export const GET_REPAIR_SKUSCAN_FAIL = "get_repair_skuscan_fail";
export const REPAIR_CAT_SEARCH_TEXT_CHANGED = "repair_cat_search_text_changed";
export const REPAIR_CAT_SELECTED = "repair_cat_selected";
export const REPAIR_PRODUCT_INFO_ADDED = "repair_product_info_added";
export const CLEAR_REPAIR_PRODUCT_INFO = "clear_repair_product_info";
export const UPLOAD_REPAIR_PHOTO_START = "upload_repair_photo_start";
export const UPLOAD_REPAIR_PHOTO_SUCCESS = "upload_repair_photo_success";
export const UPLOAD_REPAIR_PHOTO_FAIL = "upload_repair_photo_fail";
export const REPAIR_NOTE_TEXT_CHANGED = "repair_note_text_changed";
export const REPAIR_NOTE_FINISHED = "repair_note_finished";
export const CREATE_REPAIR_TICKET_START = "create_repair_ticket_start";
export const CREATE_REPAIR_TICKET_SUCCESS = "create_repair_ticket_success";
export const CREATE_REPAIR_TICKET_FAIL = "create_repair_ticket_fail";
export const UPDATE_REPAIR_TICKET_START = "update_repair_ticket_start";
export const UPDATE_REPAIR_TICKET_SUCCESS = "update_repair_ticket_success";
export const UPDATE_REPAIR_TICKET_FAIL = "update_repair_ticket_fail";
export const ADD_REPAIR_TO_INVOICE_START = "add_repair_to_invoice_start";
export const ADD_REPAIR_TO_INVOICE_SUCCESS = "add_repair_to_invoice_success";
export const ADD_REPAIR_TO_INVOICE_FAIL = "add_repair_to_invoice_fail";
export const SELECT_REPAIR_LOCATION = "select_repair_location";
export const REPAIR_TICKET_SELECTED = "repair_ticket_selected";
export const REMOVE_REPAIR_START = "remove_repair_start";
export const REMOVE_REPAIR_SUCCESS = "remove_repair_success";
export const REMOVE_REPAIR_FAIL = "remove_repair_fail";
export const CLEAR_STICKER_ERROR = "clear_sticker_error";

//store request
export const GET_MANAGER_STORE_REQUEST_START = "get_manager_store_request_start";
export const GET_MANAGER_STORE_REQUEST_SUCCESS = "get_manager_store_request_success";
export const GET_MANAGER_STORE_REQUEST_FAIL = "get_manager_store_request_fail";
export const SELECT_STORE_REQUEST = "select_store_request";
export const GET_STORE_REQUEST_START = "get_store_request_start";
export const GET_STORE_REQUEST_SUCCESS = "get_store_request_success";
export const GET_STORE_REQUEST_FAIL = "get_store_request_fail";

//invoices
export const GET_MANAGER_INVOICES_START = "get_manager_invoices_start";
export const GET_MANAGER_INVOICES_SUCCESS = "get_manager_invoices_success";
export const GET_MANAGER_INVOICES_FAIL = "get_manager_invoices_fail";
export const GET_INVOICES_START = "get_invoices_start";
export const GET_INVOICES_SUCCESS = "get_invoices_success";
export const GET_INVOICES_FAIL = "get_invoices_fail";
export const GET_INVOICE_HISTORY_START = "get_invoice_history_start";
export const GET_INVOICE_HISTORY_SUCCESS = "get_invoice_history_success";
export const GET_INVOICE_HISTORY_FAIL = "get_invoice_history_fail";
export const DECREASE_INVOICE_ITEM_QTY_START = "decrease_invoice_item_qty_start";
export const DECREASE_INVOICE_ITEM_QTY_SUCCESS = "decrease_invoice_item_qty_success";
export const DECREASE_INVOICE_ITEM_QTY_FAIL = "decrease_invoice_item_qty_fail";
export const PRINTED_NAME_CHANGED = "printed_name_changed";
export const SIGNATURE_COMPLETED = "signature_completed";
export const ADD_PO_START = "add_po_start";
export const ADD_PO_SUCCESS = "add_po_success";
export const ADD_PO_FAIL = "add_po_fail";
export const ADD_KEYREC_START = "add_keyrec_start";
export const ADD_KEYREC_SUCCESS = "add_keyrec_success";
export const ADD_KEYREC_FAIL = "add_keyrec_fail";
export const ADD_STAMP_START = "add_stamp_start";
export const ADD_STAMP_SUCCESS = "add_stamp_success";
export const ADD_STAMP_FAIL = "add_stamp_fail";
export const GET_SURVEY_START = "get_survey_start";
export const GET_SURVEY_SUCCESS = "get_survey_success";
export const GET_SURVEY_FAIL = "get_survey_fail";
export const SUBMIT_INVOICES_START = "submit_invoices_start";
export const SUBMIT_INVOICES_SUCCESS = "submit_invoices_success";
export const SUBMIT_INVOICES_FAIL = "submit_invoices_fail";
export const CURRENT_INVOICE_CHANGE = "current_invoice_change";
export const SHOW_INVOICE_SUBMIT_MODAL = "show_invoice_submit_modal";
export const HIDE_INVOICE_SUBMIT_MODAL = "hide_invoice_submit_modal";

//survey
export const SEND_SURVEY_WITH_SIGNATURE_START = "send_survey_with_signature_start";
export const SEND_SURVEY_WITH_SIGNATURE_SUCCESS = "send_survey_with_signature_success";
export const SEND_SURVEY_WITH_SIGNATURE_FAIL = "send_survey_with_signature_fail";
export const GET_SURVEY_QUESTION_START = "get_survey_question_start";
export const GET_SURVEY_QUESTION_SUCCESS = "survey_question_success";
export const GET_SURVEY_QUESTION_FAIL = "get_survey_question_fail";
export const SURVEY_QUESTION_ANSWERED = "survey_question_answered";
export const CLEAR_SURVEY_ANSWERS = "clear_survey_answers";
export const ADD_PRINTED_NAME = "survey_add_printed_name";

//timesheets
export const GET_TIMESHEETS_START = "get_timesheets_start";
export const GET_TIMESHEETS_SUCCESS = "get_timesheets_success";
export const GET_TIMESHEETS_FAIL = "get_timesheets_fail";

//manager
export const GET_TECHS_START = "get_techs_start";
export const GET_TECHS_SUCCESS = "get_techs_success";
export const GET_TECHS_FAIL = "get_techs_fail";
export const GET_ALL_TECHS_START = "get_all_techs_start";
export const GET_ALL_TECHS_SUCCESS = "get_all_techs_success";
export const GET_ALL_TECHS_FAIL = "get_all_techs_fail";
export const CLAIM_TECH_START = "claim_tech_start";
export const CLAIM_TECH_SUCCESS = "claim_tech_success";
export const CLAIM_TECH_FAIL = "claim_tech_fail";
export const DELETE_TECH_START = "delete_tech_start";
export const DELETE_TECH_SUCCESS = "delete_tech_success";
export const DELETE_TECH_FAIL = "delete_tech_fail";
export const FIND_MANAGER_STORE_START = "find_manager_store_start";
export const MANAGER_STORE_FOUND = "manager_store_found";
export const MANAGER_STORE_NOT_FOUND = "manager_store_not_found";
export const SHOW_AREA_MANAGER_LOCATION_MODAL = "show_area_manager_location_modal";
export const HIDE_AREA_MANAGER_LOCATION_MODAL = "hide_area_manager_location_modal";
export const GET_AREA_TECHS_START = "get_area_techs_start";
export const GET_AREA_TECHS_SUCCESS = "get_area_techs_success";
export const GET_AREA_TECHS_FAIL = "get_area_techs_fail";

//client
export const GET_CLIENTS_START= "get_clients_start";
export const GET_CLIENTS_SUCCESS = "get_clients_success";
export const GET_CLIENTS_FAIL = "get_clients_fail";

//field ops
export const GET_REPORTS_START = "get_reports_start";
export const GET_REPORTS_SUCCESS = "get_reports_success";
export const GET_REPORTS_FAIL = "get_reports_fail";
export const SELECT_CLIENT_REPORT = "select_client_report";
