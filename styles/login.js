import React, { Component } from 'react';
import {
  StyleSheet
} from 'react-native';

import palette from './palette.js';

module.exports = StyleSheet.create({
  container: {
    backgroundColor: palette.mediumBlue,
    flex: 1,
    padding: 20,
    paddingTop: 40
  },
  header: {
    color: palette.white,
    fontSize: 36,
    fontWeight: 'bold',
    marginTop: 40,
    marginBottom: 40
  },
  headerDark: {
    color: palette.black,
    fontSize: 36,
    fontWeight: 'bold',
    marginTop: 40,
    marginBottom: 20
  },
  instructions: {
    fontSize: 24,
    color: palette.black
  },
  textInput: {
    color: palette.white,
    flex: 1,
    fontWeight: 'bold'
  },
  textInputDark: {
    color: palette.black,
    flex: 1,
    fontWeight: 'bold'
  },
  passwordViewButton60: {
    width: 50,
    height: 55,
    marginTop: 30,
    borderBottomWidth: 2,
    borderBottomColor: palette.white60
  },
  passwordViewButton100: {
    width: 50,
    height: 55,
    marginTop: 30,
    borderBottomWidth: 2,
    borderBottomColor: palette.white
  },
  passwordViewText: {
    paddingTop: 25,
    color: palette.white
  },
  actionButtonContainer: {
    alignItems: 'flex-end',
    marginTop: 20,
    marginBottom: 20
  },
  actionButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  errorView: {
    height: 40,
    marginTop: 10
  },
  errorText: {
    color: palette.white
  }
})
