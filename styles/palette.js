import React, { Component } from 'react';

module.exports = {
  black: 'black',
  white: 'white',
  red: '#FF3B30',
  green: '#00DF4F',
  darkGray: 'rgb(70, 70, 70)',
  mediumGray: '#7A7A7A',
  lightGray: '#CBCBCB',
  paleGray: '#F2F2F2',
  borderGray: '#D4D4D4',
  completedGray: '#979797',
  white60: 'rgba(255, 255, 255, .6)',
  mediumBlue: '#0074de',
  paleBlue: '#589EE3',
  mediumBlue30: 'rgba(33,115,216, .8)',
  bluishGrey: '#878f93',
  orange: '#FF641D'
};
