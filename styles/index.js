import React, { Component } from 'react';
import fonts from './fonts.js';
import palette from './palette.js';
import modal from './modal.js';
import login from './login.js';

module.exports = {
  fonts: fonts,
  palette: palette,
  modal: modal,
  login: login,
};
