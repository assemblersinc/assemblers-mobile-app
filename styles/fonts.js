import React, { Component } from 'react';
import {
  StyleSheet
} from 'react-native';

import palette from './palette.js';

module.exports = StyleSheet.create({
  headerBlueGrey: {
    fontSize: 28,
    fontFamily: 'LibreFranklin-Bold',
    color: palette.bluishGrey
  },
  bold28: {
    fontSize: 28,
    fontFamily: 'LibreFranklin-Bold',
    color: palette.darkGray
  },
  h2: {
    fontSize: 24,
    fontFamily: 'LibreFranklin-Light',
    color: palette.darkGray,
    lineHeight: 28,
  },
  h2White: {
    fontSize: 24,
    fontFamily: 'LibreFranklin-Light',
    color: palette.white,
    lineHeight: 30,
  },
  headerBlueGreySmall: {
    fontSize: 21,
    fontFamily: 'LibreFranklin-Bold',
    color: palette.bluishGrey
  },
  sectionHeader: {
    fontSize: 20,
    fontFamily: 'LibreFranklin-Medium',
    color: palette.darkGray,
    lineHeight: 24
  },
  itemNoLink: {
    fontSize: 20,
    fontFamily: 'LibreFranklin-Medium',
    color: palette.darkGray,
    lineHeight: 28
  },
  mediumLink: {
    fontSize: 18,
    fontFamily: 'LibreFranklin-Medium',
    color: palette.mediumBlue
  },
  mediumLinkRed: {
    fontSize: 18,
    fontFamily: 'LibreFranklin-Medium',
    color: palette.red
  },
  qty: {
    fontSize: 20,
    fontFamily: 'LibreFranklin-Light',
    color: palette.darkGray
  },
  nav: {
    fontSize: 18,
    fontFamily: 'LibreFranklin-Regular',
    color: palette.darkGray
  },
  navCurrent: {
    fontSize: 18,
    fontFamily: 'LibreFranklin-Regular',
    color: palette.mediumBlue
  },
  navInactive: {
    fontSize: 18,
    fontFamily: 'LibreFranklin-Regular',
    color: palette.lightGray
  },
  boldItemName: {
    fontSize: 17,
    fontFamily: 'LibreFranklin-Bold',
    color: palette.darkGray,
    lineHeight: 20
  },
  body3Bold: {
    fontSize: 15,
    fontFamily: 'LibreFranklin-Bold',
    color: palette.darkGray
  },
  body3Semi: {
    fontSize: 15,
    fontFamily: 'LibreFranklin-SemiBold',
    color: palette.darkGray
  },
  smallLink: {
    fontSize: 15,
    fontFamily: 'LibreFranklin-SemiBold',
    color: palette.mediumBlue
  },
  whiteText: {
    fontSize: 15,
    fontFamily: 'LibreFranklin-SemiBold',
    color: palette.white
  },
  whiteTextLight: {
    fontSize: 15,
    fontFamily: 'LibreFranklin-Light',
    color: palette.white
  },
  whiteTextMedium: {
    fontSize: 20,
    fontFamily: 'LibreFranklin-Regular',
    color: palette.white
  },
  blueTextMedium: {
    fontSize: 15,
    fontFamily: 'LibreFranklin-Medium',
    color: palette.mediumBlue
  },
  formInput: {
    fontSize: 15,
    fontFamily: 'LibreFranklin-SemiBold',
    color: palette.bluishGrey
  },
  body2: {
    fontSize: 15,
    fontFamily: 'LibreFranklin-Medium',
    color: palette.darkGray
  },
  body2Light: {
    fontSize: 14,
    fontFamily: 'LibreFranklin-Regular',
    color: palette.darkGray
  },
  body1: {
    fontSize: 14,
    fontFamily: 'LibreFranklin-Regular',
    color: palette.darkGray,
    lineHeight: 22
  },
  body1Blue: {
    fontSize: 14,
    fontFamily: 'LibreFranklin-Regular',
    color: palette.mediumBlue,
    lineHeight: 22
  },
  orangeText: {
    fontSize: 18,
    fontFamily: 'LibreFranklin-Regular',
    color: palette.orange,
    lineHeight: 22
  }
});
