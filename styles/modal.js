import React, { Component } from 'react';
import {
  StyleSheet
} from 'react-native';

module.exports = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0, 0, 0, .6)',
    flex: 1,
    justifyContent: 'center',
    paddingLeft: '5%',
    paddingRight: '5%'
  },
  containerBottom: {
    backgroundColor: 'rgba(0, 0, 0, .6)',
    flex: 1,
    justifyContent: 'flex-end',
    paddingLeft: '5%',
    paddingRight: '5%'
  },
  modal: {
    padding: 20,
    paddingTop: 40,
    paddingBottom: 40,
    backgroundColor: 'white',
    alignItems: 'center'
  },
  textLarge: {
    fontSize: 28,
    textAlign: 'center',

  },
  text: {
    fontSize: 18,
    lineHeight: 28,
  },
  boldText: {
    fontSize: 18,
    lineHeight: 28,
    fontWeight: 'bold'
  }
});
