Assemblers, Inc. Mobile App
=======================

Assemblers, Inc. Mobile is a React Native app for both iOS and Android.

The app may be used by Assemblers, Inc. users with the following roles:
Tech
Area Manager
Field Ops Manager
Contractor
Subcontractor

The app has a diverse feature set that includes:
Clock in/out and meal break tracking
Work Lists
Store Walks
Today's Invoices
Invoice History
Tech Management
Timesheet History

----------


Setup
-------------
To set up your machine for development, please follow these steps:
1. Install both XCode and Android Studio
2. Install Node
	- https://nodejs.org/en/
	- Command line: brew install node
3. Install Watchmen
	- https://facebook.github.io/watchman/docs/install.html
	- Command line: brew install watchman
4. Install the React Native CLI
	- Command line: npm install -g react-native-cli
**Additional information** may be found here: http://facebook.github.io/react-native/docs/getting-started.html under the "Building Projects with Native Code" tab.

After following all above steps:
1. Open a new command line window.
2. Command line: npm install
3. Command line: react-native run-ios OR react-native run-android
**Note**: run-ios will launch a simulator, but run-android requires that a simulator is already launched or that a device is already plugged in.


Structure
--------------
All top-level application files and modal orchestrators are located in the roles directory.
All UI components are organized by feature in the components directory.
All navigation-related files are located in the routers directory.
All state management (reducers and action creators) are located in the redux directory.

Oddities and Notes
----------------------------

- React Native has a bug that causes Action Sheet layer conflict when using Alerts and Modals. To get around this bug, there are Modal Orchestrator files that insure that only one Action Sheet layer item is shown at any given time.
- The camera library we are using is only allowed one instance per stack navigator. If you add an additional Camera View to a stack navigator, you will notice that the UI and callbacks for the camera do not get cleared on navigation as you would expect. To get around this issue, we have implemented a "go back, go forward" scheme on navigation away from the camera. In other words, when you are finished with a camera screen, navigate back to the previous screen and then forward to the next desired screen. This is prevalent in the Work List, Store Walk, and Repairs sections.

Libraries and Dependencies
---------------------------
    "axios": "0.16.2",
    "lodash": "4.17.4",
    "moment": "2.18.1",
    "numeral": "2.0.6",
    "react": "16.0.0-alpha.6",
    "react-native": "0.44.3",
    "react-native-calendar": "0.12.3",
    "react-native-camera": "0.10.0",
    "react-native-communications": "2.2.1",
    "react-native-hockeyapp": "0.5.1",
    "react-native-keyboard-aware-scroll-view": "0.2.9",
    "react-native-material-kit": "0.4.1",
    "react-native-permissions": "0.2.7",
    "react-native-push-notification": "2.2.1",
    "react-native-root-toast": "1.1.2",
    "react-native-signature-capture": "0.4.5",
    "react-native-star-rating": "1.0.7",
    "react-native-vector-icons": "4.1.1",
    "react-navigation": "1.0.0-beta.10",
    "react-redux": "5.0.5",
    "redux": "3.7.0",
    "redux-persist": "4.8.0",
    "redux-thunk": "2.2.0"
