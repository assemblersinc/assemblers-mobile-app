import React from 'react';
import { Navigator } from 'react-native';
import _ from 'lodash';
import constants from '../constants.json';
import baseRequest from './base-request';

//add token to these calls
function findNearbyStore(token, callback, errorCallback) {

  console.log('getCurrentStore');

  /**
   * Gets navigation location position (Lat/Long)
   */
  navigator.geolocation.getCurrentPosition(
    (position) => {
      var locationJSON = {
        "lat": position.coords.latitude,
        "lon": position.coords.longitude
      };
      console.log(locationJSON);

      baseRequest.getBaseRequest(token, 'location/findnearby', 'POST', locationJSON)
      .then(function(response) {
        console.log(response);
        if(response && response.status == 200 && response.data && response.data.length > 0 ) {
          console.log(response.data[0]);
          callback(response.data[0]);
        }
        else {
          errorCallback(response.message);
        }
      })
      .catch((err) => {
        console.log('location find near by error ' + err);
        errorCallback(err);
      });
    },
    (error) => {
      //location error
      errorCallback(error);
    },
    {enableHighAccuracy: false, timeout: 10000, maximumAge: 10000}
  );
}

function storeSearch(token, clientId, city, state, zip, callback, errorCallback) {

  var searchJSON = {
    "clientId": clientId,
    "city": city,
    "state": state,
    "zip": zip,
  };

  let uri = "location/searchlocation?" + baseRequest.toQueryString(searchJSON);

  baseRequest.getBaseRequest(token, uri, 'GET', null)
    .then(function(response) {
      console.log(response);
      if(response.status == 200 || response.status == 201 || response.status == 204) {
        callback(response.data);
      }
      else {
        errorCallback(constants.errorMessages.appErrorTryAgain);
      }
    })
    .catch(function(err) {
      console.log('storeSearch error ' + err);
      errorCallback(err);
  });
}

function getAreasRegions(token, callback, errorCallback) {
    baseRequest.getBaseRequest(token, 'region', 'GET', null)
    .then(function(response) {
      console.log(response);
      if(response.status == 200) {
        callback(response.data);
      }
      else {
        errorCallback(constants.errorMessages.appErrorTryAgain);
      }
    })
    .catch(function(err) {
      console.log(err);
      errorCallback(err);
  });
}


module.exports = {
  findNearbyStore: findNearbyStore,
  storeSearch: storeSearch,
  getAreasRegions: getAreasRegions
}
