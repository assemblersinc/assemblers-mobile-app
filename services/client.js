import baseRequest from './base-request';

/**
 * Retrives Announcements.
 * @param {*} query
 * @param {*} token
 * @param {*} callback
 * @param {*} errorCallback
 */
function getClients(query, token, callback, errorCallback) {

  const uri = 'client/?' + (query ? baseRequest.toQueryString(query) :'');

  baseRequest.getBaseRequest(token, uri, 'GET', null)
  .then(function(response) {
    if(response.status == 200) {
      return response.data;
    } else {
      errorCallback(response.status);
    }
  })
  .then((responseJson) => {
    console.log(responseJson);
    callback(responseJson);
  })
  .catch(function(err) {
    errorCallback(err);
  });

}

function getClientByCode(code, token, callback, errorCallback) {

  if(!code){
      return errorCallback('Need to supply a client code!');
  }

  const uri = 'client/' + baseRequest.toQueryString(code);

  baseRequest.getBaseRequest(token, uri, 'GET', null)
  .then(function(response) {
    if(response.status == 200) {
      return response.data;
    } else {
      errorCallback(response.status);
    }
  })
  .then((responseJson) => {
    callback(responseJson);
  })
  .catch(function(err) {
    errorCallback(err);
  });

}

module.exports = {
  getClients: getClients,
  getClientByCode:getClientByCode
}
