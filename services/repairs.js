import baseRequest from './base-request';
import constants from '../constants.json';

function getManagerRepairsList(token, areaId, callback, errorCallback) {
  baseRequest.getBaseRequest(token, 'areamanager/storerepair/all?areaId=' + areaId, 'GET', null)
  .then(function(response) {
    console.log(response);
    if(response.status == 200 && response.data) {
      callback(response.data);
    } else {
      errorCallback(response.status);
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(err);
  })
}

function getStoreRepairsList(token, locationId, callback, errorCallback) {
  const uri = 'storerepair/' + locationId;

  baseRequest.getBaseRequest(token, uri, 'GET', null)
  .then(function(response) {
    console.log(response);
    if(response.status == 200) {
      callback(response.data);
    } else errorCallback();
  })
  .catch(function(err) {
    console.log(err);
    errorCallback();
  });
}

function getRepairCategories(token, callback, errorCallback) {

  baseRequest.getBaseRequest(token, 'repair', 'GET', null)
  .then(function(response) {
    console.log(response);
    if(response.status == 200) {
      return response.data;
    } else errorCallback();
  })
  .then((responseJson) => {
    if(responseJson && responseJson != undefined) {
      callback(responseJson);
    } else errorCallback();
  })
  .catch(function(err) {
    console.log(err);
    errorCallback();
  });
}

function searchScannedRepairStickerService(token, sticker, callback, errorCallback) {
  const uri = 'storerepair/qrcode/' + sticker;
  console.log(uri);
  baseRequest.getBaseRequest(token, uri, 'GET', null)
  .then(function(response) {
    console.log(response);
    if(response.status == 200 && response.data && response.data.length > 0) {
      callback(response.data);
    } else errorCallback();
  })
  .catch(function(err) {
    console.log(err);
    errorCallback();
  });
}

function createRepairTicket(token, userId, locationId, sticker, product, repairCategory, imageData, callback, errorCallback) {
  var imageDataArray = [];
  imageDataArray.push(imageData);
  console.log(imageData, imageDataArray);

  const repairJSON = {
    "userId": userId,
    "locationId": locationId,
    "qrcode": sticker,
    "productId": product.productId,
    "productName": product.name,
    "productSku": product.sku,
    "imageData": imageDataArray,
    "repairs": { locationId: locationId, repairId: repairCategory.repairId, userId: userId }
  };


    baseRequest.getBaseRequest(token, 'storerepair', 'POST', repairJSON)
    .then(function(response) {
      console.log(response);
      if(response.status == 200) {
        return response.data;
      } else errorCallback();
    })
    .then((responseJson) => {
      if(responseJson && responseJson != undefined) {
        callback(responseJson);
      } else errorCallback();
    })
    .catch(function(err) {
      console.log(err);
      errorCallback();
    });

  }

  function uploadRepairPhoto(token, imagePath, callback, errorCallback) {

    //first upload the photo to the store walk blob storage
    baseRequest.getPhotoRequest(token, 'storage/repair', 'POST', imagePath)
    .then((response) => {
      console.log('upload photo to blob', response);
      if(response.status == 200) {
        callback(response.data.completeUrl);
      } else errorCallback();
    })
    .catch(function(err) {
      console.log(err);
      errorCallback();
    })
  }

  function updateRepairTicketWithPhoto(token, locationId, storeRepairId, photoObject, callback, errorCallback) {
    const uri = 'storerepair/' + locationId + '/' + storeRepairId;

    baseRequest.getBaseRequest(token, uri, 'PUT', photoObject)
    .then(function(response) {
      console.log(response);
      if(response.status == 200) {
        callback(response.data);
      } else errorCallback();
    })
    .catch(function(err) {
      console.log(err);
      errorCallback();
    });
  }

  function updateRepairTicket(token, storeRepairId, userId, repairId, locationId, imageData, callback, errorCallback) {

    var updateObject = {
      "storeRepairId": storeRepairId,
      "userId": userId,
      "repairId": repairId,
      "locationId": locationId,
      "imageData": imageData
    };

    baseRequest.getBaseRequest(token, 'storerepair/repair', 'POST', updateObject)
    .then(function(response) {
      console.log(response);
      if(response.status == 200) {
        callback(response.data);
      } else errorCallback();
    })
    .catch(function(err) {
      console.log(err);
      errorCallback();
    });
  }

  function removeRepairFromTicket(token, storeRepairId, callback, emptyCallback, errorCallback) {
    const uri = "storerepair/repair/" + storeRepairId;
    baseRequest.getBaseRequest(token, uri, 'DELETE', null)
    .then(function(response) {
      console.log(response);
      if(response.status == 200) {
        return callback(response.data);
      } else if(response.status == 204) {
        emptyCallback();
      } else errorCallback();
    })
    .catch(function(err) {
      console.log(err);
      errorCallback();
    });
  }

  function addRepairToInvoice(token, repairsList, callback, errorCallback) {
    var repairIdsArray = [];
    repairsList.forEach(function(element) {
      repairIdsArray.push(element.id);
    });

    baseRequest.getBaseRequest(token, 'storerepair/repair/complete', 'POST', repairIdsArray)
    .then(function(response) {
      console.log(response);
      if(response.status == 200) {
        return callback();
      } else errorCallback();
    })
    .catch(function(err) {
      console.log(err);
      errorCallback();
    });
  }

module.exports = {
  getManagerRepairsList: getManagerRepairsList,
  getStoreRepairsList: getStoreRepairsList,
  getRepairCategories: getRepairCategories,
  searchScannedRepairStickerService: searchScannedRepairStickerService,
  createRepairTicket: createRepairTicket,
  uploadRepairPhoto: uploadRepairPhoto,
  updateRepairTicketWithPhoto: updateRepairTicketWithPhoto,
  updateRepairTicket: updateRepairTicket,
  removeRepairFromTicket: removeRepairFromTicket,
  addRepairToInvoice: addRepairToInvoice
}
