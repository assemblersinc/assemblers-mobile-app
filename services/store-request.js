import baseRequest from './base-request';
import constants from '../constants.json';

function getManagerStoreRequestList(token, areaId, callback, errorCallback) {
  baseRequest.getBaseRequest(token, 'areamanager/storerequest/all?areaId=' + areaId, 'GET', null)
  .then(function(response) {
    console.log(response);
    if(response.status == 200 && response.data) {
      callback(response.data);
    } else {
      errorCallback(response.status);
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(err);
  })
}

function getStoreRequest(token, locationId, callback, errorCallback) {
  const uri = 'request?locationId=' + locationId + '&requestTypeId=' + constants.requestTypes.storeRequest;

  baseRequest.getBaseRequest(token, uri, 'GET', null)
  .then(function(response) {
    console.log(response);
    if(response.status == 200) {
      return(response.data);
    } else {
      errorCallback(response.status);
    }
  })
  .then((responseJson) => {
    console.log(responseJson);
    if(responseJson && responseJson != undefined && responseJson.length > 0) {
      callback(responseJson);
    } else {
      errorCallback();
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(err);
  })
}

module.exports = {
  getManagerStoreRequestList: getManagerStoreRequestList,
  getStoreRequest: getStoreRequest
}
