import baseRequest from './base-request';
import constants from '../constants.json';

function getManagerInvoiceList(token, areaId, callback, errorCallback) {
	const uri = 'areamanager/invoice/all?areaId=' + areaId;
	baseRequest.getBaseRequest(token, uri, 'GET', null)
	.then((response) => {
		console.log(response);
		if(response.status == 200) {
			callback(response.data);
		} else {
			errorCallback(response.status);
			return;
		}
	})
	.catch(function(err) {
		console.log(err);
		errorCallback(constants.errorMessages.appErrorTryAgain);
	})
}

function getMyInvoiceHistory(query, token, callback, errorCallback) {

	const uri = 'invoice/history?' + (query ? baseRequest.toQueryString(query) : '');

	baseRequest.getBaseRequest(token, uri, 'GET', null)
	.then((response) => {
		console.log(response);
		if(response.status == 200) {
			return response.data;
		} else {
			errorCallback(response.status);
			return;
		}
	})
	.then((responseJson) => {
		callback(responseJson);
	})
	.catch(function(err) {
		console.log(err);
		errorCallback(constants.errorMessages.appErrorTryAgain);
	});
}

function getTodaysInvoices(query, token, callback, errorCallback) {

	const uri = 'invoice/current?' + (query ? baseRequest.toQueryString(query) : '');

	baseRequest.getBaseRequest(token, uri, 'GET', null)
	.then((response) => {
		console.log(response);
		if(response.status == 200) {
			return response.data;
		} else {
			errorCallback(response.status);
			return;
		}
	})
	.then((responseJson) => {
		callback(responseJson);
	})
	.catch(function(err) {
		console.log(err);
		errorCallback(constants.errorMessages.appErrorTryAgain);
	})
}

function updateInvoice(invoice, token, callback, errorCallback) {

	const uri = 'invoice/' + invoice.invoiceId;

	baseRequest.getBaseRequest(token, uri, 'PUT', invoice)
	.then((response) => {
		console.log(response);
		if(response.status == 200) {
			return response.data;
		} else {
			errorCallback(response.status);
			return;
		}
	})
	.then((responseJson) => {
		callback(responseJson);
	})
	.catch(function(err) {
		console.log(err);
		errorCallback(constants.errorMessages.appErrorTryAgain);
	})
}

function updateInvoices(invoices, token, callback, errorCallback) {

	baseRequest.getBaseRequest(token, 'invoice/', 'PUT', invoices)
	.then((response) => {
		console.log(response);
		if(response.status == 200) {
			return response.data;
		} else {
			errorCallback(response.status);
			return;
		}
	})
	.then((responseJson) => {
		callback(responseJson);
	})
	.catch(function(err){
		console.log(err);
		errorCallback(constants.errorMessages.appErrorTryAgain);
	});
}

function saveSurveyWithSignature(data, token, callback, errorCallback) {
	const uri = 'invoice/submit';

	baseRequest.getBaseRequest(token, uri, 'POST', data)
	.then((response) => {
		console.log(response);
		if(response.status == 200) {
			return response.data;
		} else {
			errorCallback(response.status);
			return;
		}
	})
	.then((responseJson) => {
		callback(responseJson);
	})
	.catch(function(err){
		console.log(err);
		errorCallback(constants.errorMessages.appErrorTryAgain);
	})
}

function uploadStampPhoto(token, invoice, imagePath, callback, errorCallback) {

	//first upload the photo to the store walk blob storage
  	baseRequest.getPhotoRequest(token, 'storage/stamp', 'POST', imagePath)
  	.then((response) => {
	    console.log('upload photo to blob', response.data.completeUrl);
	    if(response.status == 200) {

	    invoice.stamp = response.data.completeUrl;

	      //if successfully uploaded to blob storage, add the photo to the store walk object
	      baseRequest.getBaseRequest(token, 'invoice/' + invoice.invoiceId, 'PUT', invoice)
	      .then((response) => {
	        console.log('add stamp to invoice', response)
	        if(response.status == 200) {
	          return response.data;
	        } else {
	          errorCallback(response.status);
	        }
	      })
	      .then((responseJson) => {
	      	callback(responseJson);
	      })
	      .catch(function(err){
	      	console.log(err);
	      	errorCallback(constants.errorMessages.appErrorTryAgain);
	      })

	    } else errorCallback();
	  })
	  .catch(function(err) {
	    console.log(err);
	    errorCallback();
  	})
}

function decreaseInvoiceItemQuantity(data, token, callback, errorCallback) {
	const uri = 'invoice/product/remove';

	baseRequest.getBaseRequest(token, uri, 'POST', data)
	.then((response) => {
		console.log(response);

		if(response.status == 200) {
			return response.data;
		} else {
			errorCallback(response.status);
			return;
		}
	})
	.then((responseJson) => {
		callback(responseJson);
	})
	.catch(function(err){
		console.log(err);
		errorCallback(constants.errorMessages.appErrorTryAgain);
	})
}

module.exports = {
	getManagerInvoiceList: getManagerInvoiceList,
	getMyInvoiceHistory: getMyInvoiceHistory,
	getTodaysInvoices: getTodaysInvoices,
	updateInvoice: updateInvoice,
	updateInvoices: updateInvoices,
	saveSurveyWithSignature: saveSurveyWithSignature,
	uploadStampPhoto: uploadStampPhoto,
	decreaseInvoiceItemQuantity: decreaseInvoiceItemQuantity
}
