import moment from 'moment';
import baseRequest from './base-request';
import constants from '../constants.json';

function submitGrillbarrow(token, userId, locationId,
  onRackCount, holesCount, stagedCount, boxedCount,repairsCount,
  grillCount, wheelbarrowCount, callback, errorCallback) {
  const grillbarrowJSON = {
    "locationId": locationId,
    "bikeOnRackCount": onRackCount,
    "bikeHolesCount": holesCount,
    "bikeStagedCount": stagedCount,
    "bikeBoxedCount": boxedCount,
    "bikeRepairsCount": repairsCount,
    "grillCount": grillCount,
    "wheelBarrowCount": wheelbarrowCount,
    "countedDate": moment().format()
  };

  baseRequest.getBaseRequest(token, 'storeassemblycount', 'POST', grillbarrowJSON)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      callback();
    } else {
      errorCallback();
    }
  })
}

function startStoreWalk(token, userId, locationId, callback, errorCallback) {
    const storeWalkJSON = {
      "locationId": locationId,
      "userId": userId,
      "comments": null,
      "photos": null,
      "scheduleDate": null,
      "createTime": moment().format()
    };

    baseRequest.getBaseRequest(token, 'request/storewalk/start', 'POST', storeWalkJSON)
    .then((response) => {
      console.log(response);
      if(response.status == 200) {
        callback(response.data);
      } else {
        errorCallback(response.status);
      }
    })
    .catch(function(err) {
      console.log(err);
      errorCallback(constants.errorMessages.appErrorTryAgain);
    })
}

function getStoreWalk(token, locationId, callback, errorCallback) {
  var uri = 'request/storewalk?locationId=' + locationId + '&requestTypeId=' + constants.requestTypes.storeWalk;

  baseRequest.getBaseRequest(token, uri, 'GET', null)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      return response.data[0];
    } else if(response.status == 204) {
      callback(null);
    } else {
      errorCallback(constants.errorMessages.appErrorTryAgain);
    }
  })
  .then((responseJson) => {
    console.log(responseJson);
    callback(responseJson);
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}

function addStoreWalkItem(token, userId, requestId, product, qty, location, callback, errorCallback) {
  var itemJSON;
  console.log(location);
  if(product.productId) {
    itemJSON = {
      "requestId": requestId,
      "userId": userId,
      "product": product
    };
  }
  else if(product.serviceId) {
    itemJSON = {
      "requestId": requestId,
      "userId": userId,
      "isService": true,
      "product": product
    };
  }

  itemJSON.product.userGeoLat = location.lat;
  itemJSON.product.userGeoLong = location.lon;
  itemJSON.product.isUserGeoFenced = location.isGeofenced;
  itemJSON.product.qtyTotal = qty;

  baseRequest.getBaseRequest(token, 'request/product', 'POST', itemJSON)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      callback();
    } else {
      errorCallback();
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}

function updateStoreWalkItemQty(token, requestId, product, qty, callback, errorCallback) {
  var uri;
  if(product.requestProductId) {
    uri = "request/product/" + product.requestProductId;
  } else if (product.storeServiceId) {
    uri = "request/service/" + product.storeServiceId;
  }

  const updateJSON = {
    "qtyTotal": qty,
    "requestId": requestId
  }

  baseRequest.getBaseRequest(token, uri, 'PUT', updateJSON)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      callback();
    } else {
      errorCallback();
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}

function uploadStoreWalkPhoto(token, requestId, imagePath, notesPhotosArray, userObject, callback, errorCallback) {

  //first upload the photo to the store walk blob storage
  baseRequest.getPhotoRequest(token, 'storage/storewalk', 'POST', imagePath)
  .then((response) => {
    console.log('upload photo to blob', response);
    if(response.status == 200) {
      var updateArray;
      if(notesPhotosArray) {
          updateArray = notesPhotosArray;
      } else updateArray = [];


      updateArray.push({
        "imageUrl": response.data.completeUrl,
        "comment": null,
        "userGeoLong": userObject.lon,
        "userGeoLat": userObject.lat,
        "isUserGeoFenced": userObject.isGeofenced,
        "firstName": userObject.firstName,
        "lastName": userObject.lastName
      });
      const photoUpdateJSON = {"photos": updateArray};

      //if successfully uploaded to blob storage, add the photo to the store walk object
      baseRequest.getBaseRequest(token, 'request/storewalk/' + requestId, 'PUT', photoUpdateJSON)
      .then((response) => {
        console.log(response)
        if(response.status == 200) {
          callback();
        } else {
          errorCallback();
        }
      })

    } else errorCallback();
  })
  .catch(function(err) {
    console.log(err);
    errorCallback();
  })
}

function addStoreWalkNote(token, requestId, comment, index, notesPhotosArray, callback, errorCallback) {

  var updateArray = notesPhotosArray;
  updateArray[index].comment = comment;
  const photoUpdateJSON = {"photos": updateArray};

  //if successfully uploaded to blob storage, add the photo to the store walk object
  baseRequest.getBaseRequest(token, 'request/storewalk/' + requestId, 'PUT', photoUpdateJSON)
  .then((response) => {
    console.log(response)
    if(response.status == 200) {
      callback();
    } else {
      errorCallback();
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}

function submitStoreWalk(token, userId, requestId, callback, errorCallback) {
  const submitJSON = {
    "userId": userId,
    "requestId": requestId,
    "endTime": moment().format()
  };
  baseRequest.getBaseRequest(token, 'request/storewalk/complete', 'POST', submitJSON)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      callback();
    } else {
      errorCallback();
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}

module.exports = {
  submitGrillbarrow: submitGrillbarrow,
  startStoreWalk: startStoreWalk,
  getStoreWalk: getStoreWalk,
  addStoreWalkItem: addStoreWalkItem,
  updateStoreWalkItemQty: updateStoreWalkItemQty,
  uploadStoreWalkPhoto: uploadStoreWalkPhoto,
  addStoreWalkNote: addStoreWalkNote,
  submitStoreWalk: submitStoreWalk
}
