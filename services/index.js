import announcements from './announcements';
import chat from './chat';
import baseRequest from './base-request';
import client from './client';
import clocking from './clocking.js';
import geolocation from './geolocation';
import invoices from './invoices';
import login from './login.js';
import products from './products.js';
import repairs from './repairs.js';
import reports from './reports.js';
import storeRequest from './store-request';
import storeWalk from './store-walk';
import user from './user';
import workList from './work-list';
import survey from './survey-questions';
import myTechs from './my-techs';

module.exports = {
  announcements: announcements,
  baseRequest: baseRequest,
  chat: chat,
  client: client,
  clocking: clocking,
  invoices:invoices,
  login: login,
  geolocation: geolocation,
  products: products,
  repairs: repairs,
  reports: reports,
  storeRequest: storeRequest,
  storeWalk: storeWalk,
  user:user,
  workList: workList,
  survey: survey,
  myTechs: myTechs
};
