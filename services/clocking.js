import _ from 'lodash';
import moment from 'moment';
import constants from '../constants.json';
import baseRequest from './base-request';

// these are all secure endpoints
// pass time stamps for clocking in and out

function getClockInLocation(clientId, storeNumber, token, callback, errorCallback) {

  var url = 'location/storenumber/' + storeNumber + '/client/' + clientId;

  baseRequest.getBaseRequest(token, url, 'GET', null)
  .then(function(response) {
    console.log(response);
    if(response.status == 200) {
      return response.data;
    } else if(response.status == 204) {
      errorCallback(constants.errorMessages.clockinLocationError);
    }
  })
  .then((responseJson) => {
    console.log(responseJson);
    if(responseJson) {
      callback(responseJson);
    } else {
      errorCallback(constants.errorMessages.clockinLocationError);
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback();
  });
}

function clockInUser(userId, locationId, token, callback, alreadyClockedIn, errorCallback) {

  var clockInJSON = {
    "userId": userId,
    "locationId": locationId,
    "timeIn": moment().format()
  };

  baseRequest.getBaseRequest(token, 'timesheet/clockin', 'POST', clockInJSON)
  .then(function(response) {
    console.log(response);
    if(response.status == 200 || response.status == 201) {
      callback(response.data);
    } else if(response.status == 400) {
      if(response.data && response.data.timesheet) {
        alreadyClockedIn(response.data.timesheet);
      } else errorCallback(constants.errorMessages.clockInError);
    }
    else {
      errorCallback(constants.errorMessages.clockInError);
    }
  })
  .catch(function(err) {
    console.log('clockInUser error ' + err);
    errorCallback(constants.errorMessages.clockInError);
  });

}

function clockOutUser(userId, timeId, token, callback, errorCallback) {

  var clockOutJSON = {
    "userId": userId,
    "timeId": timeId,
    "timeOut": moment().format()
  };

  baseRequest.getBaseRequest(token, 'timesheet/clockout', 'POST', clockOutJSON)
  .then(function(response) {
    console.log(response);
    //if we clocked out the user OR they were already clocked out
    if(response.status == 200 || response.status == 400) {
      callback();
    } else {
      errorCallback(constants.errorMessages.appErrorTryAgain);
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });

}

function getRecentTimesheets(userId, token, callback, errorCallback) {

  const timesheetPath = "timesheet/user/" + userId;

  baseRequest.getBaseRequest(token, timesheetPath, 'GET', null)
  .then(function(response) {
    if(response.status == 200) {
      return response.data;
    } else errorCallback(response.status);
  })
  .then((responseJson) => {
    if(responseJson != undefined && responseJson) {
      callback(responseJson);
    }
  })
  .catch(function(err) {
    errorCallback(err);
  });

}

function travelToAnotherStore(token, timeId, userId, locationId, callback, errorCallback) {
  const uri = 'timesheet/' + timeId + '/user/' + userId;

  const travelingObject = {
    "locationId": locationId,
    "timeOut": moment().format()
  };

  baseRequest.getBaseRequest(token, uri, 'PUT', travelingObject)
  .then(function(response) {
    console.log(response);
    if(response.status == 200) {
      callback(response.data);
    } else errorCallback(response.status);
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(err);
  });

}

function mealOutUser(token, timeId, userId, callback, errorCallback) {
  const mealOutJSON = {
    "timeId": timeId,
    "userId": userId,
    "mealOut": moment().format()
  };
  baseRequest.getBaseRequest(token, 'timesheet/mealout', 'POST', mealOutJSON)
  .then(function(response) {
    console.log(response);
    if(response.status == 200) {
      callback(response.data);
    } else errorCallback(response.status);
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(err);
  });
}

function mealInUser(token, timeId, userId, callback, errorCallback) {
  const mealInJSON = {
    "timeId": timeId,
    "userId": userId,
    "mealIn": moment().format()
  };

  baseRequest.getBaseRequest(token, 'timesheet/mealin', 'POST', mealInJSON)
  .then(function(response) {
    console.log(response);
    if(response.status == 200) {
      callback(response.data);
    } else errorCallback(response.status);
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(err);
  });

}




module.exports = {
  getClockInLocation: getClockInLocation,
  clockInUser: clockInUser,
  clockOutUser: clockOutUser,
  getRecentTimesheets: getRecentTimesheets,
  travelToAnotherStore: travelToAnotherStore,
  mealInUser: mealInUser,
  mealOutUser: mealOutUser
}
