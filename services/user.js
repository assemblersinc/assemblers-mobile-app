import baseRequest from './base-request';

/**
 * Retrives Announcements.
 * @param {*} query
 * @param {*} token
 * @param {*} callback
 * @param {*} errorCallback
 */
function updateNotificationTokenRegistrationService(token, registration, userId, callback, errorCallback) {
  console.log('updateNotificationTokenRegistrationService', registration);
  let notificationJSON = {
    "registration": registration,
    "userId": userId
  };

  const uri = 'user/registerpush';

  baseRequest.getBaseRequest(token, uri, 'POST', notificationJSON)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      callback(response.data);
    } else {
      errorCallback(response.status);
    }
  })
  .catch(function(err) {
    errorCallback(err);
  });

}

function getLastSeenTime(token, userId, callback, errorCallback) {
  console.log('getLastSeenTime', userId);
  const uri = 'user/' + userId + '?noTrack=true';
  baseRequest.getBaseRequest(token, uri, 'GET', null)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      callback(response.data.lastSeen);
    } else errorCallback(response.status);
  })
  .catch(function(err) {
    errorCallback(err);
  });
}

module.exports = {
  updateNotificationTokenRegistrationService: updateNotificationTokenRegistrationService,
  getLastSeenTime: getLastSeenTime
}
