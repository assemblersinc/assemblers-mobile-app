import baseRequest from './base-request';
import constants from '../constants.json';

function logInUser(userId, password, callback, errorCallback) {

  var loginJSON = {
    "username": userId,
    "password": password
  };


  baseRequest.getBaseRequest(null, 'auth/login', 'POST', loginJSON)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      return response.data;
    } else {
      errorCallback(constants.errorMessages.loginError);
    }
  })
  .then((responseJson) => {
    if(responseJson != undefined && responseJson) {
      console.log(responseJson);
      callback(responseJson);
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });

}

function logOutUser(token, userId, callback, errorCallback) {

  var logoutJSON = {
    "userId": userId
  };

  baseRequest.getBaseRequest(token, 'auth/logout', 'POST', logoutJSON)
  .then(function(response) {
    if(response.status == 200) {
      callback();
    } else {
      errorCallback(constants.errorMessages.appError);
    }
  })
  .catch(function(err) {
    errorCallback(err);
  });

}

module.exports = {
  logInUser: logInUser,
  logOutUser: logOutUser,
}
