import baseRequest from './base-request';
import constants from '../constants.json';

function getSurveyQuestions(token, callback, errorCallback) {

	const uri = 'survey/question';

	baseRequest.getBaseRequest(token, uri, 'GET', null)
	.then((response) => {
		if (response.status == 200) {
			return response.data;
		} else {
			errorCallback(response.status);
			return;
		}
	})
	.then((responseJson) => {
		callback(responseJson);
	})
	.catch(function(err) {
		console.log(err);
		errorCallback(constants.errorMessages.appErrorTypeAgain);
	})
}

module.exports = {
	getSurveyQuestions: getSurveyQuestions
}