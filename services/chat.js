import baseRequest from './base-request';

function getChatMessages(token, query, callback, errorCallback) {
  console.log('getChatMessages SERVICE START', query);
  const uri = 'conversation/sender/' + query.senderId +'/receiver/' + query.receiverId;
  baseRequest.getBaseRequest(token, uri, 'POST', { conversationId:query.conversationId, userId: query.userId })
  .then(function(response) {
    console.log('getChatMessages service', response);
    if(response.status == 200) {
      console.log('getchatmessages response', response);
      return response.data;
    } else {
      errorCallback(response.status);
    }
  })
  .then((responseJson) => {
    callback(responseJson);
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(err);
  })
}

function updateReadConversation(token, query, callback, errorCallback) {
  console.log('query' + query);
  baseRequest.getBaseRequest(token, 'conversation/sender/' + query.senderId +'/receiver/' + query.receiverId, 'PUT', {read:1})
  .then(function(response) {
    if(response.status == 200) {
      console.log('updateReadConversation response', response);
      return response.data;
    } else {
      errorCallback(response.status);
    }
  })
  .then((responseJson) => {
    callback(responseJson);
  })
  .catch(function(err) {
    errorCallback(err);
  })
}

function updateReadMessage(token, query, callback, errorCallback) {
  baseRequest.getBaseRequest(token, 'conversation/' + query.conversationId , 'PUT', {read:1, userId: query.userId})
  .then(function(response) {
    if(response.status == 200) {
      console.log('updateReadMessage response', response);
      return response.data;
    } else {
      errorCallback(response.status);
    }
  })
  .then((responseJson) => {
    callback(responseJson);
  })
  .catch(function(err) {
    errorCallback(err);
  })
}

function sendMessage(token, senderId, receiverId, message, locationId, callback, errorCallback) {
  const messageJSON = {
    "senderId": senderId,
    "receiverId": receiverId,
    "message": message,
    "locationId": locationId
  }
  baseRequest.getBaseRequest(token, 'conversation/', 'POST', messageJSON)
  .then(function(response) {
    if(response.status == 200) {
      return response.data;
    } else {
      errorCallback(response.status);
    }
  })
  .then((responseJson) => {
    callback(responseJson);
  })
  .catch(function(err) {
    errorCallback(err);
  })
}


function getLastHistoryMessages(token, userId, callback, errorCallback) {
  console.log('ChatService::getLastHistoryMessages');

  baseRequest.getBaseRequest(token, 'conversation/receiver/' + userId, 'GET', null)
  .then(function(response) {
    if(response.status == 200) {
      return response.data;
    } else {
      errorCallback(response.status);
    }
  })
  .then((responseJson) => {
    callback(responseJson);
  })
  .catch(function(err) {
    errorCallback(err);
  })
}

module.exports = {
  getLastHistoryMessages: getLastHistoryMessages,
  getChatMessages: getChatMessages,
  sendMessage: sendMessage

}
