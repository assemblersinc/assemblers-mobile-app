import baseRequest from './base-request';
import constants from '../constants.json';

function getWorkList(locationId, token, userId, callback, errorCallback) {
  //var url = 'request/?locationId=' + locationId;
  var url = 'request/product/location/' + locationId + "/worklist?userId=" + userId;

  baseRequest.getBaseRequest(token, url, 'GET', null)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      return response.data;
    } else if(response.status == 204) {
      return { "photos": [], "items": [] }
    } else {
      errorCallback(constants.errorMessages.appErrorTryAgain);
    }
  })
  .then((responseJson) => {
    if(responseJson && responseJson != undefined) {
      callback(responseJson);
    }
  })
  .catch(function(err) {
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}

function addItemToInvoice(token, userId, requestProductId,
  clientId, locationId, isService, serialNumber, qty, callback, errorCallback) {

  const uri = 'request/product/' + requestProductId + '/complete';
  let itemJSON = {
    "qtyTotal": qty,
    "clientId": clientId,
    "locationId": locationId,
    "userId": userId,
    "isService": isService,
  };

  if(serialNumber) {
    itemJSON.serialNumber = serialNumber;
  } else {
    itemJSON.serialNumber = "";
  }

  baseRequest.getBaseRequest(token, uri, 'PUT', itemJSON)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      callback();
    } else {
      errorCallback();
    }
  })
  .catch(function(err) {
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}

function addOnDemandItemToInvoice(token, userId, productId, product,
  clientId, locationId, isService, serialNumber, qty, callback, errorCallback) {

    const uri = 'request/product/' + productId + '/complete/ondemand';
    let itemJSON = {
      "clientId": clientId,
      "locationId": locationId,
      "userId": userId,
      "isService": isService,
      "product": product
    };

    if(serialNumber) {
      itemJSON.serialNumber = serialNumber;
    } else {
      itemJSON.serialNumber = "";
    }

    product.qtyTotal = qty;

    baseRequest.getBaseRequest(token, uri, 'PUT', itemJSON)
    .then((response) => {
      console.log(response);
      if(response.status == 200) {
        callback();
      } else {
        errorCallback();
      }
    })
    .catch(function(err) {
      errorCallback(constants.errorMessages.appErrorTryAgain);
    });
}

module.exports = {
  getWorkList: getWorkList,
  addItemToInvoice: addItemToInvoice,
  addOnDemandItemToInvoice: addOnDemandItemToInvoice
}
