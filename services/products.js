import baseRequest from './base-request';
import constants from '../constants.json';


function partialSkuSearch(token, sku, clientId, callback, errorCallback) {

  var uri = 'product/search/' + sku + '/client/' + clientId;
  baseRequest.getBaseRequest(token, uri, 'GET', null)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      return (response.data);
    } else if (response.status == 204) {
      callback([]);
    } else {
      errorCallback(constants.errorMessages.appErrorTryAgain);
    }
  })
  .then((responseJson) => {
    console.log(responseJson);
    callback(responseJson);
  })
  .catch(function(err) {
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}


function findScannedSku(token, sku, clientId, callback, errorCallback) {

  var uri = 'product/' + sku + '/' + clientId;
  baseRequest.getBaseRequest(token, uri, 'GET', null)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      return(response.data);
    } if(response.status == 204) {
      callback(null);
    }
  })
  .then((responseJson) => {
    console.log(responseJson);
    if(responseJson != null) {
      callback(responseJson);
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}


function submitPricingRequest(token, userId, locationId, clientId, product, callback, errorCallback) {
  console.log(product);
  const pricingJSON = {
    "sku": (product.sku) ? product.sku.sku : product.upc,
    "description": "",
    "upc": "",
    "clientId": clientId,
    "locationId": locationId,
    "userId": userId
  };

  baseRequest.getBaseRequest(token, 'pricerequest/', 'POST', pricingJSON)
  .then((response) => {
    console.log(response);

    if(response.status == 200) {
      callback();
    } else {
      errorCallback('Error');
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}

function getServicesList(token, callback, errorCallback) {
  baseRequest.getBaseRequest(token, 'service', 'GET', null)
  .then((response) => {
    if(response.status == 200) {
      return response.data;
    } else {
      errorCallback(constants.errorMessages.appErrorTryAgain);
      return;
    }
  })
  .then((responseJson) => {
    //console.log(responseJson);
    callback(responseJson);
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}

module.exports = {
  findScannedSku: findScannedSku,
  partialSkuSearch: partialSkuSearch,
  submitPricingRequest: submitPricingRequest,
  getServicesList: getServicesList
}
