import baseRequest from './base-request';

/**
 * Retrives Announcements.
 * @param {*} query
 * @param {*} token
 * @param {*} callback
 * @param {*} errorCallback
 */
function getAnnouncements(query, token, callback, errorCallback) {

  const uri = 'announcement?' + (query ? baseRequest.toQueryString(query) : '');

  baseRequest.getBaseRequest(token, uri, 'GET', null)
  .then(function(response) {
    console.log(response);
    if(response.status === 200) {
      return response.data;
    } else if(response.status == 204) {
      return [];
    } else {
      errorCallback(response.status);
    }
  })
  .then((responseJson) => {
    console.log(responseJson);
    callback(responseJson);
  })
  .catch(function(err) {
    errorCallback(err);
  });

}

module.exports = {
  getAnnouncements: getAnnouncements
}
