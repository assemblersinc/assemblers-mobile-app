import baseRequest from './base-request';
import constants from '../constants.json';

function getMyTechs(managerId, token, callback, errorCallback) {
  const uri = 'areamanager/' + managerId + '/mytechs';

  baseRequest.getBaseRequest(token, uri, 'GET', null)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      callback(response.data);
    } else {
      errorCallback(response.status);
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}

function getAllTechs(managerId, token, callback, errorCallback) {
  const uri = 'areamanager/' + managerId + '/unclaimtech';

  baseRequest.getBaseRequest(token, uri, 'GET', null)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      callback(response.data);
    } else {
      errorCallback(response.status);
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}

function claimTech(managerId, techId, token, callback, errorCallback) {
  const uri = 'areamanager/' + managerId + '/claimtech';

  const claimJSON = {
    "managerId": managerId,
    "techId": techId
  };

  baseRequest.getBaseRequest(token, uri, 'POST', claimJSON)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      callback();
    } else {
      errorCallback(response.status);
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}

function dropTech(managerId, techId, token, callback, errorCallback) {
  const uri = 'areamanager/' + managerId + '/claimtech/' + techId;

  baseRequest.getBaseRequest(token, uri, 'DELETE', null)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      callback();
    } else {
      errorCallback(response.status);
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}

function getAreaTechs(token, areaId, callback, errorCallback) {
  const uri = 'user/area/' + areaId + '?roleId=' + constants.roleIds.tech;

  baseRequest.getBaseRequest(token, uri, 'GET', null)
  .then((response) => {
    console.log(response);
    if(response.status == 200) {
      callback(response.data);
    } else {
      errorCallback();
    }
  })
  .catch(function(err) {
    console.log(err);
    errorCallback(constants.errorMessages.appErrorTryAgain);
  });
}

module.exports = {
  getMyTechs: getMyTechs,
  getAllTechs: getAllTechs,
  claimTech: claimTech,
  dropTech: dropTech,
  getAreaTechs: getAreaTechs
}
