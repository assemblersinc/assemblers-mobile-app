import * as baseRequest from "./base-request";

function getReports(token, clientId, startDate, areaId, callback, errorCallback) {

  let query = {startDate: startDate};
  const uri = 'reporting?areaId=' + areaId + '&startDate=' + startDate + (clientId ? '&clientId=' + clientId : '');
  baseRequest.getBaseRequest(token, uri, 'GET', null)
  .then(response => {
    console.log(response);
    if (response.status === 200) {
      callback(response.data);
    } else {
      errorCallback();
    }
  })
  .catch(function (err) {
    console.log("Error fetching reports:" + err);
    errorCallback();
  });
}

module.exports = {
  getReports: getReports
};
