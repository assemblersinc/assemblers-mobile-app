import constants from '../constants.json';
import {
  Alert
}  from 'react-native';
import axios from 'axios';
import Toast from 'react-native-root-toast';
//const BASE_URL = "http://localhost:3000/";
//const BASE_URL = constants.AzureBaseURL;
const BASE_URL = constants.AzureProBaseURL;
//const BASE_URL = constants.MacMiniBaseURL;
axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.headers.common['Content-Type'] = 'application/json';

axios.defaults.timeout = 20000;

function getBaseRequest(token, url, requestType, requestBody) {
  console.log('getBaseRequest ', BASE_URL + url, requestType, requestBody);

  axios.defaults.headers.common['Accept'] = 'application/json';
  axios.defaults.headers.common['Content-Type'] = 'application/json';

  if(token){
    axios.defaults.headers.common['x-access-token'] = token.token;
    axios.defaults.headers.common['x-access-refresh-token'] = token.refreshToken;
  }

  return axios({
    method: requestType,
    url: BASE_URL + url,
    data: requestBody,
    validateStatus: function (status) {
      return status >= 200 && status < 600;
    },
  })
  .then(function(response) {

    if(response.status == 403 && url.indexOf('login') == -1) {

    }

    if(response.status >= 400) {
      let toast = Toast.show('System Error: ' + response.status, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true
      });
    }

    return(response);
  })
}

function getPhotoRequest(token, url, requestType, imagePath) {
  console.log('getPhotoRequest ', BASE_URL + url, requestType, imagePath);

  if(token){
    axios.defaults.headers.common['x-access-token'] = token.token;
    axios.defaults.headers.common['x-access-refresh-token'] = token.refreshToken;
  }

  const imageFile = {
      uri: imagePath,
      name: "image.jpg",
      type: "image/jpg"
  };
  let data = new FormData();
  data.append('image', imageFile);

  console.log(data);

  let options = {
      method: requestType,
      url: BASE_URL + url,
      headers: {
          'Content-Type': `multipart/form-data; boundary=${data._boundary}`
      },
      validateStatus: function (status) {
        return status >= 200 && status < 600;
      },
      data
  };

  return axios(options)
  .then(function(response) {
    return(response);
  })
}

function toQueryString(obj) {
    var parts = [];
    for (var i in obj) {
        if (obj.hasOwnProperty(i)) {
            parts.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
        }
    }
    return parts.join("&");
}

module.exports = {
  getBaseRequest: getBaseRequest,
  getPhotoRequest: getPhotoRequest,
  toQueryString:toQueryString

}
