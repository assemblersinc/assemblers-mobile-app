import React, { Component } from 'react';
import {
  View,
  AppState
} from 'react-native';
import { connect } from 'react-redux';
import { getLastSeenTime, showLeftStoreModal, showBanner, hideBanner } from '../redux/actions';
import { Banner, PushNotifications } from '../components/_common';
import TechDrawerRouter  from '../routers/tech/TechDrawerRouter';
import TechModalOrchestrator from './TechModalOrchestrator';
import utils from '../utils';

class TechApp extends Component {

  state = {
    isVisible: true
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);
    this.compareLocationToTimesheet();
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  render() {
    const { appData, hideBanner } = this.props;

    return(
      <View style={{ flex: 1 }}>
        <TechModalOrchestrator />
        <PushNotifications />
        <Banner
          isVisible={appData.bannerVisible}
          text={appData.bannerText}
          color={appData.bannerColor}
          textColor={appData.bannerTextColor}
          close={hideBanner}
         />
        <TechDrawerRouter />
      </View>
    );
  }

  handleAppStateChange = (state) => {
    const { getLastSeenTime, userData, clockingData } = this.props;

    if(state == 'active' && clockingData.isClockedIn) {
      //get the tech's last seen time to check for inactivity
      getLastSeenTime(userData.token, userData.userId);

      //compare the user's current geolocation to the location saved in their timesheet
      this.compareLocationToTimesheet();
    }
  }

  compareLocationToTimesheet = () => {
    const { showLeftStoreModal, userData } = this.props;
    if(userData.timesheet && userData.timesheet.location && userData.timesheet.location.lat && userData.timesheet.location.lon) {
      //user's clocked in location
      var timesheetLocation = {};
        timesheetLocation.lat = userData.timesheet.location.lat;
        timesheetLocation.lon = userData.timesheet.location.lon;

      navigator.geolocation.getCurrentPosition(
        (position) => {
            const dist = utils.location.distance(position.coords.latitude, position.coords.longitude, timesheetLocation.lat, timesheetLocation.lon);
            if(dist > 1.0) {
              showLeftStoreModal();
            }
        },
        (error) => {
          currentLocation = null;
        },
        {enableHighAccuracy: false, timeout: 5000, maximumAge: 5000}
      );
    }
  }

}

const mapStateToProps = ({ appReducer, userReducer, clockingReducer }) => {
  return { appData: appReducer, userData: userReducer, clockingData: clockingReducer };
};
export default connect(mapStateToProps, { getLastSeenTime, showLeftStoreModal, showBanner, hideBanner })(TechApp);
