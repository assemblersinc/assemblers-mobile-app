import React, { Component } from 'react';
import {
  View,
  AppState,
  NetInfo,
  Platform
} from 'react-native';
import { connect } from 'react-redux';
import { logoutUser, showBanner, hideBanner } from '../redux/actions';
import LoadingModal from '../components/_common/LoadingModal';
import ErrorScreen from '../components/_common/ErrorScreen';
import LoginStackRouter from '../routers/login/LoginStackRouter';
import { TechApp, ManagerApp } from '../roles';
import services from '../services/';
import utils from '../utils';
import styles from '../styles';
import constants from '../constants.json';


class AssemblersApp extends Component {

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);

    NetInfo.isConnected.addEventListener(
      'change',
      this.handleNetworkStatusChange
    );
    this.getNetworkStatus();
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  render() {
    const { userData, logoutUser, appData } = this.props;
    const roles = constants.roleIds;

    if(!userData.isLoggedIn) {
      return <LoginStackRouter.LoginStackNavigator screenProps={this.state}/>;
    }

    switch(userData.roleId) {
      case roles.tech:
        return <TechApp />;
      case roles.subcontractor:
        return <TechApp />;
      case roles.areaManager:
        return <ManagerApp />;
      case roles.contractor:
        return <ManagerApp />;
      case roles.fieldops:
        return <ManagerApp />;
      default:
        return <View style={{ marginTop: 40, flex: 1 }}>
                <LoadingModal isVisible={appData.isLoading} />
                <ErrorScreen
                  errorText={constants.errorMessages.roleError}
                  actionLabel={'Log Out'}
                  action={() => logoutUser(userData.token, userData.userId)}/>
              </View>;
    }
  }

  handleAppStateChange = (state) => {
    if(state == 'active') {
      this.getNetworkStatus();
    }
  }

  getNetworkStatus = () => {
    NetInfo.isConnected.fetch().then(isConnected => {
      if(!isConnected) { this.showConnectivityBanner(); } else this.hideConnectivityBanner();
    });
  }

  handleNetworkStatusChange = (isConnected) => {
    if(!isConnected) { this.showConnectivityBanner(); } else this.hideConnectivityBanner();
  }

  showConnectivityBanner = () => {
    const { showBanner } = this.props;
    showBanner('No Internet connection.\nPlease connect to WiFi.', styles.palette.red, styles.fonts.whiteText);
  }

  hideConnectivityBanner = () => {
    const { hideBanner } = this.props;
    hideBanner();
  }
}

const mapStateToProps = ({ userReducer, appReducer }) => {
  return { userData: userReducer, appData: appReducer };
};

export default connect(mapStateToProps, { logoutUser, showBanner, hideBanner })(AssemblersApp);
