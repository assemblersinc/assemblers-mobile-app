import React, { Component } from 'react';
import {
  Modal,
  View,
  Text,
  Animated,
  Easing,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { hideAreaManagerLocationModal, checkInSubcontractor } from '../redux/actions';

import LoadingModal from '../components/_common/LoadingModal';
import { AreaSelectionModal } from '../components/fieldops';
import ManagerLocationChangePrompt from '../components/tech/clocking/ManagerLocationChangePrompt';
import ErrorModal from '../components/_common/ErrorModal';
import constants from '../constants.json';

class ManagerModalOrchestrator extends Component {

  render() {
    const { appData, userData, hideAreaManagerLocationModal, checkInSubcontractor } = this.props;
    console.log(appData);

    if(appData.isLoading) {
      return <LoadingModal isLoading={appData.isLoading} />
    } else if(appData.fieldOpsModalVisible) {
      return <AreaSelectionModal />
    } else if(appData.areaManagerLocationModalVisible && appData.foundLocation) {
      return <ManagerLocationChangePrompt
                store={appData.foundLocation}
                closeModal={hideAreaManagerLocationModal}
                checkIn={this.checkInManager} />
    } else return null;
  }

  checkInManager = () => {
    const { appData, hideAreaManagerLocationModal, checkInSubcontractor } = this.props;
    checkInSubcontractor(appData.foundLocation);
    hideAreaManagerLocationModal();
  }

}
const mapStateToProps = ({ appReducer, userReducer }) => {
  return { appData: appReducer, userData: userReducer };
};
export default connect(mapStateToProps, { hideAreaManagerLocationModal, checkInSubcontractor })(ManagerModalOrchestrator);
