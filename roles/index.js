import ManagerApp from './ManagerApp';
import ManagerModalOrchestrator from './ManagerModalOrchestrator';
import TechApp from './TechApp';
import TechModalOrchestrator from './TechModalOrchestrator';

module.exports = {
  ManagerApp: ManagerApp,
  ManagerModalOrchestrator: ManagerModalOrchestrator,
  TechApp: TechApp,
  TechModalOrchestrator: TechModalOrchestrator,
}
