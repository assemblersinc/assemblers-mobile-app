import React, { Component } from 'react';
import {
  View,
  AppState
} from 'react-native';
import { connect } from 'react-redux';
import { findManagerStore, checkAreaManagerLocation, showBanner, hideBanner } from '../redux/actions';
import { Banner, PushNotifications } from '../components/_common';
import { AreaManagerDrawerRouter, FieldOpsDrawerRouter } from '../routers/manager-common';
import ManagerModalOrchestrator from './ManagerModalOrchestrator';
import constants from '../constants.json';

class ManagerApp extends Component {

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);
    this.checkManagerStore();
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  render() {
    const { appData, hideBanner, userData } = this.props;
    return(
      <View style={{ flex: 1 }}>
        <ManagerModalOrchestrator />
        <PushNotifications />
        <Banner
          isVisible={appData.bannerVisible}
          text={appData.bannerText}
          color={appData.bannerColor}
          textColor={appData.bannerTextColor}
          close={hideBanner}
         />
         {(userData.roleId === constants.roleIds.fieldops)
           ? <FieldOpsDrawerRouter />
           : <AreaManagerDrawerRouter />
         }

      </View>
    );
  }

  handleAppStateChange = (state) => {
    if(state == 'active') {
      //find out if the manager is currently in a store, if so set their timesheet with that store
      this.checkManagerStore();
    }
  }

  checkManagerStore = () => {
    const { userData, findManagerStore, checkAreaManagerLocation } = this.props;
    console.log('checkManagerStore');
    //for fieldops users or AMs/contractors with no current location
    if(userData.roleId == constants.roleIds.fieldops
      || !userData.timesheet || !userData.timesheet.locationId) {
      findManagerStore(userData.token, null, null);
    } else {
      checkAreaManagerLocation(userData.token, userData.timesheet.locationId);
    }

  }

}

const mapStateToProps = ({ appReducer, userReducer }) => {
  return { appData: appReducer, userData: userReducer };
};
export default connect(mapStateToProps, { findManagerStore, checkAreaManagerLocation, showBanner, hideBanner })(ManagerApp);
