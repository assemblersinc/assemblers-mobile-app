import React, { Component } from 'react';
import {
  Modal,
  View,
  Text,
  Animated,
  Easing,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { closeClockInPromptModal, closeClockOutPromptModal, clockInUser, clockOutUser, checkOutSubcontractor, hideClockInLeftStoreModal, hideInactivityModal } from '../redux/actions';

import { ClockInPromptModal, ClockOutPromptModal, ClockedOutLeftStoreModal, ClockOutInactivityModal } from '../components/tech/clocking';
import { ErrorModal, LoadingModal } from '../components/_common';
import constants from '../constants.json';

class TechModalOrchestrator extends Component {

  render() {
    const { appData, userData, clockingData, closeClockInPromptModal, clockInUser, closeClockOutPromptModal, closeInactivityModal, hideClockInLeftStoreModal, clockOutUser, checkOutSubcontractor, hideInactivityModal } = this.props;

    if(appData.isLoading) {
      return <LoadingModal isLoading={appData.isLoading} />
    } else if(appData.clockOutPromptModalVisible && clockingData.isClockedIn) {
        return(
          <ClockOutPromptModal
            isVisible={appData.clockOutPromptModalVisible}
            isLoading={appData.isLoading}
            closeModal={closeClockOutPromptModal}
            roleId={userData.roleId}
            clockOut={
              () => {
                if(userData.roleId == constants.roleIds.tech) {
                  clockOutUser(userData.userId, userData.timesheet.timeId, userData.token, null)
                } else {
                  checkOutSubcontractor();
                }
              }
            } />
        );
    } else if(appData.leftStoreModalVisible && userData.timesheet && clockingData.isClockedIn) {
      return (
        <ClockedOutLeftStoreModal
          isVisible={appData.leftStoreModalVisible}
          store={userData.timesheet.location.Location}
          clockOutUser={() => clockOutUser(userData.userId, userData.timesheet.timeId, userData.token, null)}
          closeModal={hideClockInLeftStoreModal} />
      );
    } else if (appData.inactivityModalVisible && userData.timesheet && clockingData.isClockedIn) {
      return(
        <ClockOutInactivityModal
          isVisible={appData.inactivityModalVisible}
          clockOutUser={() => clockOutUser(userData.userId, userData.timesheet.timeId, userData.token, null)}
          closeModal={hideInactivityModal} />
      );
    } else if(appData.errorVisible) {
        return(
          <ErrorModal />
        );
    } else return null;
  }

}
const mapStateToProps = ({ appReducer, userReducer, clockingReducer }) => {
  return { appData: appReducer, userData: userReducer, clockingData: clockingReducer };
};
export default connect(mapStateToProps, {closeClockInPromptModal, closeClockOutPromptModal, clockInUser, clockOutUser, checkOutSubcontractor, hideClockInLeftStoreModal, hideInactivityModal })(TechModalOrchestrator);
