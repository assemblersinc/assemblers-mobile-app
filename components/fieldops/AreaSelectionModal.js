import React, { Component } from 'react';
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  ListView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { getAreasRegions, hideFieldopsSelectionModal, fieldOpsSelectAreaRegion } from '../../redux/actions';
import Button from '../_common/Button';
import styles from '../../styles';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class AreaSelectionModal extends Component {

  state = {
    selectedRegion: null,
    selectedArea: null
  }

  componentWillMount() {
    const { getAreasRegions, userData } = this.props;
    getAreasRegions(userData.token);
  }

  render() {

    const { loginData, userData, hideFieldopsSelectionModal } = this.props;
    //if they have previously selected an area, they can close the modal. if their current areaId is null they MUST make a selection.
    //list of areas is in loginData.fieldOpsAreas

    return(
      <Modal
        animationType={"fade"}
        transparent={true}>
        <View style={styles.modal.container}>
          <View style={{ flex: 1, backgroundColor: styles.palette.white, padding: 20, marginTop: 20, marginBottom: 20 }}>

            <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'flex-end', marginBottom: 20 }}>
              {(userData.areaId && userData.region)
                ? <TouchableOpacity onPress={hideFieldopsSelectionModal}><Text style={styles.fonts.body1}>Cancel</Text></TouchableOpacity>
                : null
              }
            </View>

            <Text style={[styles.fonts.headerBlueGrey, { textAlign: 'left', marginBottom: 20 }]}>Region & Area Selection</Text>

            <ListView
              dataSource={ds.cloneWithRows(loginData.fieldOpsAreas)}
              renderRow={this.renderRegionRow}
            />


          </View>
        </View>
      </Modal>
    );
  }

  renderRegionRow = (region) => {
    console.log(region);
    if(region.geoareas && region.geoareas.length > 0) {
      return(
        <View style={{ marginTop: 10, marginBottom: 10 }}>
          <TouchableOpacity
            style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}
            onPress={() => this.onRegionSelection(region)}>
            <Text style={styles.fonts.body3Bold}>{region.name}</Text>
            {(region == this.state.selectedRegion)
              ? <Icon name={'minus'} size={30} color={styles.palette.black} />
              : <Icon name={'plus'} size={30} color={styles.palette.black} />
            }

          </TouchableOpacity>
          <ListView
            style={this.getInnerListStyle(region.regionId)}
            renderRow={this.renderAreaRow}
            dataSource={ds.cloneWithRows(region.geoareas)} />
        </View>
      );
    } else return null;

  }

  renderAreaRow = (area) => {
    return(
      <TouchableOpacity onPress={() => this.onAreaSelection(area)}>
        <Text style={[styles.fonts.body1, { margin: 5 }]}>{area.name}</Text>
      </TouchableOpacity>
    );
  }

  getInnerListStyle = (regionId) => {
    if(!this.state.selectedRegion || regionId != this.state.selectedRegion.regionId) {
      return (
        { height: 0 }
      );
    } else {
      return (
        { paddingLeft: 20, marginTop: 10 }
      );
    }
  }

  onRegionSelection = (region) => {
    console.log(region);

    if(region == this.state.selectedRegion) {
      this.setState({ selectedRegion: null })
    } else {
      this.setState({ selectedRegion: region });
    }

  }

  onAreaSelection = (area) => {
    console.log(area);
    this.setState({ selectedArea: area.areaId });
    this.props.fieldOpsSelectAreaRegion(area, this.state.selectedRegion);
    this.props.hideFieldopsSelectionModal();
  }
}

const mapStateToProps = ({ loginReducer, userReducer }) => {
  return { loginData: loginReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getAreasRegions, hideFieldopsSelectionModal, fieldOpsSelectAreaRegion })(AreaSelectionModal);
