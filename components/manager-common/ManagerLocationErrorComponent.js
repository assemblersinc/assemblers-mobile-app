import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Linking
} from 'react-native';

import Button from '../_common/Button';
import styles from '../../styles';

const ManagerLocationError = ({ label, retry }) => {
  return(
    <View style={{ flex: 1, backgroundColor: styles.palette.white, padding: 30, paddingTop: 80, paddingBottom: 80, justifyContent: 'space-between', alignItems: 'center' }}>
      <Text style={styles.fonts.h2}>You must be at a store to access {label}.</Text>

      <View style={{ width: '100%' }}>
        <Text style={[{ margin: 10, marginBottom: 20, textAlign: 'center' }, styles.fonts.body1]}>Are you at the store?</Text>
        <View style={{ flexDirection: 'row' }}>
          <Button label={'REFRESH'} onPress={retry} color={styles.palette.mediumBlue} />
        </View>
      </View>

      <View>
        <Text style={[{ margin: 10, marginBottom: 20, textAlign: 'center' }, styles.fonts.body1]}>If you are having trouble, make sure your location services are turned on in your phone settings.</Text>
        <View style={{ flexDirection: 'row' }}>
          <Button label={'SETTINGS'} onPress={() => Linking.openURL('app-settings:')} color={styles.palette.mediumBlue} />
        </View>
      </View>
    </View>
  );
}

export default ManagerLocationError;
