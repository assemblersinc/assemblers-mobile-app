import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  ScrollView
} from 'react-native';
import numeral from 'numeral';
import Communications from 'react-native-communications';
import { connect } from 'react-redux';
import { getReports } from '../../redux/actions';
import DashboardLineItem from './DashboardLineItem';
import styles from '../../styles';
import constants from '../../constants.json';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class DashboardListView extends Component {

  render() {

    const { reportingData } = this.props;

    let keys = Object.keys(reportingData.totals);
    let index = keys.indexOf('name');
    if (index > -1) {
      keys.splice(index, 1);
    }

    let data = [];
    if (this.props.selectedClient != null) {
      data = keys.map((key) => {
        return {title: key, totals: reportingData.totals[key], client: this.props.selectedClient[key]};
      });
    } else {
      data = keys.map((key) => {
        return {title: key, totals: reportingData.totals[key]};
      });
    }

    return (
      <ScrollView style={{backgroundColor: styles.palette.white, flex: 1 }}>
        <ListView
          style={{backgroundColor: styles.palette.white}}
          dataSource={ds.cloneWithRows(data)}
          renderRow={this.renderRow}
          renderHeader={this.renderHeader}
        />
        <View style={{ backgroundColor: styles.palette.white, padding: 20 }}>
          <TouchableOpacity
            style={{ padding: 10, alignItems: 'center' }}
            onPress={() => Communications.web(constants.urls.compliance)}>
            <Text style={styles.fonts.smallLink}>View compliance and coverage report</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }

  renderHeader = () => {
    let clientTitle = '';
    if (this.props.selectedClient != null) {
      clientTitle = this.props.selectedClient.name;
    }

    return <DashboardLineItem title={''} totals={'TOTAL'} client={clientTitle} isHeader={true}/>
  };

  renderRow = (item) => {
    return (
      <DashboardLineItem
        title={item.title}
        totals={(item.title && item.title.indexOf('revenue') > -1) ? numeral(item.totals).format('$0.00') : item.totals}
        client={(item.client != undefined && item.title && item.title.indexOf('revenue') > -1) ? numeral(item.client).format('$0.00') : item.client}
      />
    )
  }

}

const mapStateToProps = ({reportingReducer, userReducer}) => {
  return {reportingData: reportingReducer, userData: userReducer};
};

export default connect(mapStateToProps, { getReports })(DashboardListView);
