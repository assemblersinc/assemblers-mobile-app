import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native'
import styles from "../../styles";

const DISPLAY_NAMES = {
  name: ' ',
  serviceDays: 'Service days scheduled',
  servicedOnDay: 'Serviced on scheduled day',
  servicedOnOther: 'Serviced on non-scheduled day',
  revenue: 'Revenue',
  requestedItems: 'Requested items',
  assembledItems: 'Assembled items'
};

const DashboardLineItem = ({title, totals, client, isHeader}) => {

  //console.log("Title: " + title + " totals: " + totals + " client: " + client);

  let clientText = '';

  if (client != null) {
    clientText = client
  }

  let rowBackground = styles.palette.white;

  if (isHeader != null && isHeader == true) {
    rowBackground = styles.palette.paleGray;
  }

  const valueStyle = StyleSheet.flatten(...styles.fonts.body1, {padding: 10});
  return (
    <View style={{flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray}}>
      <View style={{ flex: 2, justifyContent: 'center', alignItems: 'flex-start', padding: 10, backgroundColor: styles.palette.paleGray }}>
        <Text style={styles.fonts.body1}>{(DISPLAY_NAMES[title] || title) + ''}</Text>
      </View>
      <View style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        paddingLeft: 0,
        borderBottomColor: styles.palette.borderGray,
        borderRightWidth: 1,
        borderRightColor: isHeader ? styles.palette.completedGray : styles.palette.darkGray,
        backgroundColor: rowBackground
      }}>
        <Text style={valueStyle}>{String(totals)}</Text>
      </View>
      <View style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        paddingLeft: 0,
        borderBottomColor: styles.palette.borderGray,
        backgroundColor: rowBackground
      }}>
        <Text style={valueStyle}>{clientText}</Text></View>
    </View>
  );
};

export default DashboardLineItem;
