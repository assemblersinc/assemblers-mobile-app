import AreaTechsView from './AreaTechsView';
import MyTechsView from './MyTechsView';
import ManageTechsView from './ManageTechsView';
import ReadOnlyWorkListView from './ReadOnlyWorkListView';
import SearchTechsView from './SearchTechsView';
import TechDetailView from './TechDetailView';

module.exports = {
  AreaTechsView: AreaTechsView,
  MyTechsView: MyTechsView,
  ManageTechsView: ManageTechsView,
  ReadOnlyWorkListView: ReadOnlyWorkListView,
  SearchTechsView: SearchTechsView,
  TechDetailView: TechDetailView
}
