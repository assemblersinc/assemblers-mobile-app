import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';
import styles from '../../../styles';
import { MinusTech } from '../../../assets';

const MyTechLineItem = ({ onPress, firstName, lastName, techId, removeTech }) => {
  return(
    <View style={{ flexDirection: 'row', alignItems: 'flex-end', paddingTop: 20, paddingBottom: 20, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>

      <View style={{ flex: 3 }}>
        <TouchableOpacity
          onPress={onPress}>
          <Text style={styles.fonts.mediumLink}>{firstName} {lastName}</Text>
        </TouchableOpacity>
        <View style={{ marginTop: 5 }}>
          <Text style={styles.fonts.body1}>{techId}</Text>
        </View>
      </View>

      <TouchableOpacity
        onPress={removeTech}
        style={{ flex: 1, alignItems: 'flex-end' }}>
        <Image style={{ width: 50, height: 50 }} source={MinusTech} resizeMode={'contain'} />
      </TouchableOpacity>

    </View>
  );
};

export default MyTechLineItem;
