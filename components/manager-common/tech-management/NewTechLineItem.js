import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';
import styles from '../../../styles';
import { AddTechBlue } from '../../../assets';

const NewTechLineItem = ({ onPress, firstName, lastName, techId, addTech }) => {
  return(
    <View style={{ flexDirection: 'row', alignItems: 'flex-end', paddingTop: 20, paddingBottom: 20, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>

      <View style={{ flex: 3 }}>
        <TouchableOpacity
          onPress={onPress}>
          <Text style={styles.fonts.mediumLink}>{firstName} {lastName}</Text>
        </TouchableOpacity>
        <View style={{ marginTop: 5 }}>
          <Text style={styles.fonts.body1}>{techId}</Text>
        </View>
      </View>

      <TouchableOpacity
        onPress={addTech}
        style={{ flex: 1, alignItems: 'flex-end' }}>
        <Image style={{ width: 35, height: 35 }} resizeMode={'contain'} source={AddTechBlue} />
      </TouchableOpacity>

    </View>
  );
};

export default NewTechLineItem;
