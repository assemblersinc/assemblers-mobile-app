import React, { Component } from 'react';
import {
  View,
  Text,
  ListView,
} from 'react-native';
import Toast from 'react-native-root-toast';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { getMyTechs, getAllTechs, claimTech, deleteTech } from '../../../redux/actions'
import MyTechLineItem from './MyTechLineItem';
import NewTechLineItem from './NewTechLineItem';
import { PredictiveSearchComponent } from '../../_common';
import styles from '../../../styles';
import constants from '../../../constants.json';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class SearchTechsView extends Component {

  state = {
    text: "",
    masterList: [],
    filteredList: [],
    label: "",
    type: ""
  }

  componentDidMount() {
    const { navigation, techData } = this.props;
    const { params } = navigation.state;

    if(params.listType == 'my-techs' && techData.myTechs) {
      const master = [];
      techData.myTechs.clockedin.forEach(function(element) {
        master.push(element);
      });
      techData.myTechs.clockedout.forEach(function(element) {
        master.push(element);
      })
      this.setState({ masterList: master, filteredList: master, type: params.listType, label: "My Techs" });
    } else if(techData.allTechs) {
      this.setState({ masterList: techData.allTechs, filteredList: techData.allTechs, type: params.listType, label: "All Techs" });
    }
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'space-between', backgroundColor: styles.palette.white }}>
        <PredictiveSearchComponent
          goBack={this.props.navigation.goBack}
          text={this.state.text}
          onChangeText={this.searchTechs}
          keyboardType={'default'}
          label={this.state.label}
        />
        {(this.state.type == 'my-techs')
          ? <ListView
            style={{ padding: 20 }}
            dataSource={ds.cloneWithRows(this.state.filteredList)}
            renderRow={this.renderMyTechsRow} />
          : <ListView
            style={{ padding: 20 }}
            dataSource={ds.cloneWithRows(this.state.filteredList)}
            renderRow={this.renderAllTechsRow} />
        }
      </View>
    );
  }

  searchTechs = (text) => {
    this.setState({ text: text });
    var matches = [];
    const masterList = (this.state.masterList) ? this.state.masterList : [];
    var that = this;
    masterList.forEach(function(element) {
      var fullName;
      if(that.state.type == 'my-techs') {
        fullName = element.tech.firstName + " " + element.tech.lastName;
      } else {
        fullName = element.firstName + " " + element.lastName;
      }

      if(fullName.toLowerCase().indexOf(text.toLowerCase()) > -1) {
        matches.push(element);
      }
    })
    this.setState({ filteredList: matches });
  }

  renderMyTechsRow = (item) => {
    const { userData, deleteTech, navigation } = this.props;
    return(
      <MyTechLineItem
        onPress={ () => navigation.navigate('TechDetail', { tech: item }) }
        firstName={item.tech.firstName}
        lastName={item.tech.lastName}
        techId={item.tech.techId}
        removeTech={ () => deleteTech(userData.userId, item.tech.userId, userData.token, this.showTechRemovedToast, () => this.showTechErrorToast('removing')) }
      />
    );
  }

  renderAllTechsRow = (item) => {
    const { userData, claimTech, navigation } = this.props;
    return(
      <NewTechLineItem
        onPress={ () => navigation.navigate('TechDetail', { tech: item }) }
        firstName={item.firstName}
        lastName={item.lastName}
        techId={item.techId}
        addTech={ () => claimTech(userData.userId, item.userId, userData.token, this.showTechAddedToast, () => this.showTechErrorToast('adding')) }
      />
    );
  }

  getAllTechs = () => {
    const { userData, getMyTechs, getAllTechs } = this.props;
    getMyTechs(userData.userId, userData.token, getAllTechs);
    this.props.navigation.goBack();
  }

  showTechAddedToast = () => {
    let toast = Toast.show('Tech successfully added to your list', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
    this.getAllTechs();
  }

  showTechRemovedToast = () => {
    let toast = Toast.show('Tech successfully removed from your list', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
    this.getAllTechs();
  }

  showTechErrorToast = (type) => {
    let toast = Toast.show('Error ' + type + ' tech', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }
}

const mapStateToProps = ({ userReducer, myTechsReducer }) => {
  return { userData: userReducer, techData: myTechsReducer };
};

export default connect(mapStateToProps, { getMyTechs, getAllTechs, claimTech, deleteTech })(SearchTechsView);
