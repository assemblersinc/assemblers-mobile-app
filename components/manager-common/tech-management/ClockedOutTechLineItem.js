import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from '../../../styles';

const ClockedOutTechLineItem = ({ onPress, firstName, lastName, lastClockOutStore, lastClockOutDate, lastClockOutTime }) => {
  return(
    <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 20, paddingBottom: 20, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>

      <View style={{ flex: 1 }}>
        <TouchableOpacity
          style={{ flexDirection: 'row', alignItems: 'center' }}
          onPress={onPress}>
          <Icon name="circle" size={10} color={styles.palette.black} style={{ marginRight: 5 }} />
          <Text style={styles.fonts.mediumLink}>{firstName} {lastName}</Text>
        </TouchableOpacity>

        <View style={{ marginTop: 5, marginLeft: 15 }}>
           <Text style={styles.fonts.body1}>Last clocked out:</Text>
           {(lastClockOutStore)
             ? <Text style={styles.fonts.body1}>{lastClockOutStore}</Text>
             : <Text style={styles.fonts.body1}>Tech has not clocked in/out</Text>
           }
        </View>

      </View>

      {(lastClockOutDate && lastClockOutTime)
        ? <View style={{ flex: 1 }}>
            <Text style={styles.fonts.body1}>{lastClockOutDate}, {lastClockOutTime}</Text>
          </View>
        : null
      }

    </View>
  );
};

export default ClockedOutTechLineItem;
