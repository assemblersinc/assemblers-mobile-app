import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView
} from 'react-native';
import { connect } from 'react-redux';
import { getMyTechs, getAllTechs } from '../../../redux/actions';
import { DrawerPageHeader } from '../../_common/drawer-nav';
import ClockedInTechLineItem from './ClockedInTechLineItem';
import ClockedOutTechLineItem from './ClockedOutTechLineItem';
import styles from '../../../styles';
import utils from '../../../utils';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class MyTechsView extends Component {

  state = {
    activeButton: 'clock-in'
  }

  componentWillMount() {
    const { userData, getMyTechs, getAllTechs } = this.props;
    getMyTechs(userData.userId, userData.token, getAllTechs);
  }

  render() {
    const { techData } = this.props;
    const rightButton =  <TouchableOpacity
                            style={{ justifyContent: 'flex-end', flexDirection: 'row' }}
                            onPress={() => this.props.navigation.navigate('ManageTechs')}>
                            <Text style={styles.fonts.mediumLink}>Manage Techs</Text>
                          </TouchableOpacity>;

    return(
      <View style= {{ flex: 1, backgroundColor: styles.palette.white }}>
          <DrawerPageHeader rightButton={rightButton} navigation={this.props.navigation} />
          <Text style={[ styles.fonts.headerBlueGrey, { margin: 20 }]}>My Techs</Text>

          <View style={{ flexDirection: 'row', margin: 20, marginBottom: 0, padding: 5, alignItems: 'center', justifyContent: 'space-around', backgroundColor: styles.palette.paleGray }}>
            <TouchableOpacity
              style={[this.getToggleStyle('clock-in'), {alignItems: 'center', padding: 10, flex: 1}]}
              onPress={this.showClockedInTechs}>
              <Text style={styles.fonts.body3Bold}>Clocked in</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[this.getToggleStyle('clock-out'), {alignItems: 'center', padding: 10, flex: 1}]}
              onPress={this.showClockedOutTechs}>
              <Text style={styles.fonts.body3Bold}>Clocked out</Text>
            </TouchableOpacity>
          </View>

          <View style={{ flex: 1 }}>
            {(this.state.activeButton == 'clock-in')
            ? (techData.myTechs && techData.myTechs.clockedin && techData.myTechs.clockedin.length > 0)
              ? <ListView
                  style={{ padding: 20 }}
                  renderRow={this.renderClockedInRow}
                  dataSource={ds.cloneWithRows(techData.myTechs.clockedin)}
                 />
              : <Text style={[styles.fonts.body1], { padding: 20 }}>Go to Manage Techs to add techs to your app</Text>
            : (techData.myTechs && techData.myTechs.clockedout && techData.myTechs.clockedout.length > 0)
              ? <ListView
                  style={{ padding: 20 }}
                  renderRow={this.renderClockedOutRow}
                  dataSource={ds.cloneWithRows(techData.myTechs.clockedout)}
                />
              : <Text style={[styles.fonts.body1], { padding: 20 }}>Go to Manage Techs to add techs to your app</Text>
            }
          </View>

      </View>
    );
  }

  getToggleStyle = (buttonName) => {
    if(buttonName == this.state.activeButton) {
      return { backgroundColor: styles.palette.white };
    } else {
      return { backgroundColor: styles.palette.paleGray };
    }
  }

  showClockedInTechs = () => {
    this.setState({ activeButton: 'clock-in' });
  }

  showClockedOutTechs = () => {
    this.setState({ activeButton: 'clock-out' });
  }

  renderClockedInRow = (item) => {
    return(
      <ClockedInTechLineItem
        onPress={() => this.props.navigation.navigate('TechDetail', { tech: item })}
        firstName={item.tech.firstName}
        lastName={item.tech.lastName}
        clockInStore={(item.tech.timesheet && item.tech.timesheet.location) ? item.tech.timesheet.location.Location : null}
        clockInTime={(item.tech.timesheet) ? utils.dateTime.getFormattedTimestamp(item.tech.timesheet.timeIn) : null}
        />
    );
  }

  renderClockedOutRow = (item) => {
    return(
      <ClockedOutTechLineItem
        onPress={() => this.props.navigation.navigate('TechDetail', { tech: item })}
        firstName={item.tech.firstName}
        lastName={item.tech.lastName}
        lastClockOutStore={(item.tech.timesheet) ? item.tech.timesheet.location.Location : null }
        lastClockOutDate={(item.tech.timesheet) ? utils.dateTime.getFormattedDate(item.tech.timesheet.timeOut) : null }
        lastClockOutTime={(item.tech.timesheet) ? utils.dateTime.getFormattedTimestamp(item.tech.timesheet.timeOut) : null }
        />
    );
  }

}
const mapStateToProps = ({ myTechsReducer, userReducer }) => {
  return { techData: myTechsReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getMyTechs, getAllTechs })(MyTechsView);
