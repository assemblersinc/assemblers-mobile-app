import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { BasicPageHeader } from '../../_common/drawer-nav';
import { ChatIcon, Clock, RepairWrench, Wrench } from '../../../assets';
import { setTechChat } from '../../../redux/actions';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Card from '../../_common/CardComponent';
import styles from '../../../styles';
import utils from '../../../utils';
import constants from '../../../constants.json';

class TechDetailView extends Component {

  state = {
    currentTech: null,
    repairCount: 0,
    assemblyCount: 0
  }

  componentDidMount() {
    const { params } = this.props.navigation.state;
    var currentTech;
    if(params.tech) {
      console.log(params.tech);
      if(params.tech.tech  && typeof params.tech.tech === 'object') {
        currentTech = params.tech.tech;
      } else {
        currentTech = params.tech;
      }
    } else {
      //TODO: error
    }

    var repairCount = 0;
    var assemblyCount = 0;
    if(currentTech && currentTech.invoices && currentTech.invoices.length > 0) {
      currentTech.invoices.forEach(function(invoice) {
        if(invoice && invoice.repairs && invoice.repairs.length > 0) {
          repairCount += invoice.repairs.length;
        }
        if(invoice && invoice.products && invoice.products.length) {
          assemblyCount += invoice.products.length;
        }
      })
    }

    this.setState({ currentTech: currentTech, repairCount: repairCount, assemblyCount: assemblyCount });
  }

  render() {
    if(!this.state.currentTech) {
      return (
        <View>
          <BasicPageHeader navigation={this.props.navigation} />
          <Text style={[styles.fonts.body1, { textAlign: 'center', padding: 20 }]}>Error: No tech info to show</Text>
        </View>
      )
    }

    const rightButton = <TouchableOpacity onPress={this.chatWithTech}>
                          <Image source={ChatIcon} style={{ width: 20, height: 20, marginRight: 20 }} />
                        </TouchableOpacity>;
    return(
      <View style={{ flex: 1, backgroundColor: styles.palette.white, justifyContent: 'space-between'}}>
        {(this.props.userData.roleId == constants.roleIds.fieldops)
          ? <BasicPageHeader navigation={this.props.navigation} />
          : <BasicPageHeader navigation={this.props.navigation} rightButton={rightButton} />
        }
        <View style={{ padding: 20, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>
          <Text style={styles.fonts.h2}>{this.state.currentTech.firstName} {this.state.currentTech.lastName}</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 10 }}>
              {(this.state.currentTech.clockStatus == 'clock-in')
                ? <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon name="circle" size={10} color={styles.palette.green} />
                    <Text style={[styles.fonts.body1, { marginLeft: 10 }]}>Clocked in</Text>
                  </View>
                : <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon name="circle" size={10} color={styles.palette.black} />
                    <Text style={[styles.fonts.body1, { marginLeft: 10 }]}>Clocked out</Text>
                  </View>
              }
              {(this.state.currentTech.timesheet && this.state.currentTech.timesheet.location)
                ? <Text style={styles.fonts.body1}>{this.state.currentTech.timesheet.location.Location}</Text>
                : null
              }

            </View>
        </View>

        {(this.state.currentTech.clockStatus == 'clock-in')
          ? <ScrollView style={{ flex: 1, paddingBottom: 40 }}>
              <View style={{ flexDirection: 'row', alignItems: 'center', margin: 20 }}>
                <Image source={Clock} style={{ width: 30, height: 30, marginRight: 10 }} resizeMode={'contain'} />
                <Text>{(this.state.currentTech.timesheet) ? utils.dateTime.getFormattedTimestamp(this.state.currentTech.timesheet.timeIn) : null}</Text>
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'center', margin: 20 }}>
                <Image source={Wrench} style={{ width: 30, height: 30, marginRight: 10 }} resizeMode={'contain'} />
                <Text>{this.state.assemblyCount} item(s) assembled</Text>
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'center', margin: 20 }}>
                <Image source={RepairWrench} style={{ width: 30, height: 30, marginRight: 10 }} resizeMode={'contain'} />
                <Text>{this.state.repairCount} item(s) repaired</Text>
              </View>
            </ScrollView>
          : <View style={{ flex: 1 }}></View>
        }


        <Card direction={'top'}>

          {(this.state.currentTech.clockStatus == 'clock-in')
            ? <View>
                <View style={{ borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('TechWorkList', { tech: this.state.currentTech})}
                    style={{ padding: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={styles.fonts.boldItemName}>Store work list</Text>
                    <Icon name="chevron-right" size={30} color={styles.palette.borderGray} />
                  </TouchableOpacity>
                </View>

                <View style={{ borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Timesheet', { tech: this.state.currentTech})}
                    style={{ padding: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={styles.fonts.boldItemName}>Time sheets</Text>
                    <Icon name="chevron-right" size={30} color={styles.palette.borderGray} />
                  </TouchableOpacity>
                </View>
              </View>
            : null
          }

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('TechInvoices', { tech: this.state.currentTech})}
            style={{ padding: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
            <Text style={styles.fonts.boldItemName}>Invoices</Text>
            <Icon name="chevron-right" size={30} color={styles.palette.borderGray} />
          </TouchableOpacity>

        </Card>
      </View>
    );
  }

  productNotPulled = () => {
    const { navigation, userData, sendGeneratedMessage } = this.props;
    sendGeneratedMessage(constants.generatedMessages.productNotPulled, userData.timesheet.location.Location);
    this.props.navigation.navigate('Chat');
  }

  chatWithTech = () => {
    const { navigation, userData, setTechChat } = this.props;
    setTechChat({tech: this.state.currentTech});
    navigation.navigate('Chat', { origin: "tech-detail", tech: this.state.currentTech });
  }
}

const mapStateToProps = ({ userReducer }) => {
  return { userData: userReducer };
};

export default connect(mapStateToProps, { setTechChat })(TechDetailView);
