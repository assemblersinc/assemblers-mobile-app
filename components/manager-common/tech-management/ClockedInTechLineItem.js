import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from '../../../styles';

const ClockedInTechLineItem = ({ onPress, firstName, lastName, clockInStore, clockInTime }) => {
  return(
    <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 20, paddingBottom: 20, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>

      <View style={{ flex: 3, marginRight: 10 }}>
        <TouchableOpacity
          style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}
          onPress={onPress}>
          <Icon name="circle" size={10} color={styles.palette.green} style={{ marginRight: 5 }}/>
          <Text style={styles.fonts.mediumLink}>{firstName} {lastName}</Text>
        </TouchableOpacity>
        <View style={{ marginLeft: 15 }}>
          <Text style={[styles.fonts.body2], { marginBottom: 5 }}>Clocked In:</Text>
          <Text style={styles.fonts.body2}>{clockInStore}</Text>
        </View>
      </View>

      <View style={{ flex: 1, alignItems: 'center' }}>
        <Text style={styles.fonts.body1}>{clockInTime}</Text>
      </View>

    </View>
  );
};

export default ClockedInTechLineItem;
