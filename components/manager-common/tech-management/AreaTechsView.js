import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView
} from 'react-native';
import { connect } from 'react-redux';
import { getAreaTechs } from '../../../redux/actions';
import { DrawerPageHeader } from '../../_common/drawer-nav';
import ClockedInTechLineItem from './ClockedInTechLineItem';
import ClockedOutTechLineItem from './ClockedOutTechLineItem';
import styles from '../../../styles';
import utils from '../../../utils';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class AreaTechsView extends Component {

  state = {
    activeButton: 'clock-in'
  }

  componentWillMount() {
    const { userData, getAreaTechs } = this.props;
    getAreaTechs(userData.token, userData.areaId);
  }

  render() {

    const { techData, userData } = this.props;
    return(
      <View style= {{ flex: 1, backgroundColor: styles.palette.white }}>
          <Text style={[ styles.fonts.headerBlueGrey, { margin: 20 }]}>Area Techs in {userData.geoarea.name}</Text>

          <View style={{ flexDirection: 'row', margin: 20, marginBottom: 0, padding: 5, alignItems: 'center', justifyContent: 'space-around', backgroundColor: styles.palette.paleGray }}>
            <TouchableOpacity
              style={[this.getToggleStyle('clock-in'), {alignItems: 'center', padding: 10, flex: 1}]}
              onPress={this.showClockedInTechs}>
              <Text style={styles.fonts.body3Bold}>Clocked in</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[this.getToggleStyle('clock-out'), {alignItems: 'center', padding: 10, flex: 1}]}
              onPress={this.showClockedOutTechs}>
              <Text style={styles.fonts.body3Bold}>Clocked out</Text>
            </TouchableOpacity>
          </View>

          <View style={{ flex: 1 }}>
            {(this.state.activeButton == 'clock-in')
            ? (techData.areaTechs && techData.areaTechs.clockedin && techData.areaTechs.clockedin.length > 0)
              ? <ListView
                  style={{ padding: 20 }}
                  renderRow={this.renderClockedInRow}
                  dataSource={ds.cloneWithRows(techData.areaTechs.clockedin)}
                 />
              : <Text style={[styles.fonts.body1], { padding: 20 }}>There are no clocked in techs in your area.</Text>
            : (techData.areaTechs && techData.areaTechs.clockedout && techData.areaTechs.clockedout.length > 0)
              ? <ListView
                  style={{ padding: 20 }}
                  renderRow={this.renderClockedOutRow}
                  dataSource={ds.cloneWithRows(techData.areaTechs.clockedout)}
                />
              : <Text style={[styles.fonts.body1], { padding: 20 }}>There are no clocked out techs in your area.</Text>
            }
          </View>

      </View>
    );
  }

  getToggleStyle = (buttonName) => {
    if(buttonName == this.state.activeButton) {
      return { backgroundColor: styles.palette.white };
    } else {
      return { backgroundColor: styles.palette.paleGray };
    }
  }

  showClockedInTechs = () => {
    this.setState({ activeButton: 'clock-in' });
  }

  showClockedOutTechs = () => {
    this.setState({ activeButton: 'clock-out' });
  }

  renderClockedInRow = (tech) => {
    return(
      <ClockedInTechLineItem
        onPress={() => this.props.navigation.navigate('TechDetail', { tech: tech })}
        firstName={tech.firstName}
        lastName={tech.lastName}
        clockInStore={(tech.timesheet) ? tech.timesheet.location.Location : null}
        clockInTime={(tech.timesheet) ? utils.dateTime.getFormattedTimestamp(tech.timesheet.timeIn) : null}
        />
    );
  }

  renderClockedOutRow = (tech) => {
    return(
      <ClockedOutTechLineItem
        onPress={() => this.props.navigation.navigate('TechDetail', { tech: tech })}
        firstName={tech.firstName}
        lastName={tech.lastName}
        lastClockOutStore={(tech.timesheet && tech.timesheet.location) ? tech.timesheet.location.Location : null }
        lastClockOutDate={(tech.timesheet) ? utils.dateTime.getFormattedDate(tech.timesheet.timeOut) : null }
        lastClockOutTime={(tech.timesheet) ? utils.dateTime.getFormattedTimestamp(tech.timesheet.timeOut) : null }
        />
    );
  }

}
const mapStateToProps = ({ myTechsReducer, userReducer }) => {
  return { techData: myTechsReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getAreaTechs })(AreaTechsView);
