import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  ListView,
  Image,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import { getWorkList } from '../../../redux/actions';

import WorkListLineItem from '../../_common/work-list/WorkListLineItem';
import { Card, ErrorScreen } from '../../_common';
import { BannerBackground, ScanButton } from '../../../assets';
import styles from '../../../styles';
import constants from '../../../constants.json';

const dimensions = Dimensions.get('window');
const bannerWidth = dimensions.width - 20;
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class ReadOnlyWorkListView extends Component {

  componentDidMount() {
    const { getWorkList, userData } = this.props;
    const { params } = this.props.navigation.state;
    if(params && params.tech) {
      getWorkList(params.tech.timesheet.locationId, userData.token, params.tech.userId);
    }
  }

  render() {

    const { userData, workListData, getWorkList } = this.props;
    const { params } = this.props.navigation.state;

    if(workListData.error) {
      return (
        <View style={{ flex: 1 }}>
          <ErrorScreen
              errorText={constants.errorMessages.workListError}
              actionLabel={'REFRESH'}
              action={this.getWorkList}
            />
        </View>
      )
    }

    return(
      <View style= {{ flex: 1, alignItems: 'flex-start', backgroundColor: styles.palette.white }}>

          {(workListData.workList.length > 0)
            ? <ScrollView>
                <Text style={[{ margin: 20 }, styles.fonts.headerBlueGrey]}>Today{"\'"}s Work List</Text>
                {(params.tech && params.tech.timesheet)
                  ? <Text style={[{ margin: 20, marginTop: 0 }, styles.fonts.h2]}>{params.tech.timesheet.location.Location}</Text>
                  : null
                }
                <Card direction={'bottom'} style={{ flex: 1, margin: 10}}>
                  <Image
                    resizeMode='cover'
                    source={BannerBackground}
                    style={{ flexDirection: 'row', padding: 10, paddingLeft: 20, marginBottom: 0, height: 85, width: bannerWidth }}>
                    <Text style={[{ flex: 1, marginTop: 15, backgroundColor: 'transparent' }, styles.fonts.whiteText]}>Work List</Text>
                  </Image>

                  <ListView
                    style={{ backgroundColor: 'white', flex: 1 }}
                    dataSource={ds.cloneWithRows(workListData.workList)}
                    renderRow={this.renderRow}
                    removeClippedSubviews={false}
                    showsVerticalScrollIndicator={false}
                  />
                </Card>
              </ScrollView>
            : <ErrorScreen
                errorText={constants.errorMessages.noWorkList}
                actionLabel={'REFRESH'}
                action={() => getWorkList(userData.timesheet.locationId, userData.token, userData.userId)} />
          }

      </View>
    );
  }

  renderRow = (item) => {
    return(
        <WorkListLineItem
          name={item.productName}
          sku={(item.sku && item.sku.sku) ? item.sku.sku : null}
          upc={item.upc}
          assembled={item.qtyAssembled}
          requested={item.qtyTotal}
          requestType={item.requestType}/>
    );
  }

}

const mapStateToProps = ({ workListReducer, userReducer }) => {
  return { workListData: workListReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getWorkList })(ReadOnlyWorkListView);
