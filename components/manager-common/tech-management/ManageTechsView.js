import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Toast from 'react-native-root-toast';
import { connect } from 'react-redux';
import { getMyTechs, getAllTechs, claimTech, deleteTech } from '../../../redux/actions'
import { BasicPageHeader } from '../../_common/drawer-nav';
import MyTechLineItem from './MyTechLineItem';
import NewTechLineItem from './NewTechLineItem';
import styles from '../../../styles';
import utils from '../../../utils';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class ManageTechsView extends Component {

  state = {
    activeButton: 'my-techs'
  }

  componentWillMount() {
    this.getAllTechs();
  }

  render() {
    const rightButton = <TouchableOpacity onPress={() => this.props.navigation.navigate('SearchTechs', { listType: this.state.activeButton })}>
                          <Icon name="magnify" size={30}  color={styles.palette.mediumBlue} style={{ paddingRight: 10 }} />
                        </TouchableOpacity>;
    const { techData } = this.props;
    const myTechs = [];

    if(techData.myTechs) {
      techData.myTechs.clockedin.forEach(function(element) {
        myTechs.push(element);
      });
      techData.myTechs.clockedout.forEach(function(element) {
        myTechs.push(element);
      })
    }

    return(
      <View style= {{ flex: 1, backgroundColor: styles.palette.white }}>
      
          <BasicPageHeader navigation={this.props.navigation} rightButton={rightButton} />

          <Text style={[ styles.fonts.headerBlueGrey, { margin: 20 }]}>My Techs</Text>

          <View style={{ flexDirection: 'row', margin: 20, marginBottom: 0, padding: 5, alignItems: 'center', justifyContent: 'space-around', backgroundColor: styles.palette.paleGray }}>
            <TouchableOpacity
              style={[this.getToggleStyle('my-techs'), {alignItems: 'center', padding: 10, flex: 1}]}
              onPress={this.showMyTechs}>
              <Text style={styles.fonts.body3Bold}>My techs</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[this.getToggleStyle('new-techs'), {alignItems: 'center', padding: 10, flex: 1}]}
              onPress={this.showNewTechs}>
              <Text style={styles.fonts.body3Bold}>Add techs</Text>
            </TouchableOpacity>
          </View>

          <View style={{ flex: 1 }}>
            {(this.state.activeButton == 'my-techs')
            ? (myTechs.length > 0)
              ? <ListView
                  style={{ padding: 20 }}
                  renderRow={this.renderMyTechsRow}
                  dataSource={ds.cloneWithRows(myTechs)}
                 />
              : <Text style={[styles.fonts.body1, { padding: 20 }]}>You have not claimed any techs</Text>
            : (techData.allTechs && techData.allTechs.length > 0)
              ? <ListView
                  style={{ padding: 20 }}
                  renderRow={this.renderNewTechsRow}
                  dataSource={ds.cloneWithRows(techData.allTechs)}
                />
              : <Text style={[styles.fonts.body1, { padding: 20 }]}>No unclaimed techs in your region</Text>
            }
          </View>

      </View>

    );
  }

  getToggleStyle = (buttonName) => {
    if(buttonName == this.state.activeButton) {
      return { backgroundColor: styles.palette.white };
    } else {
      return { backgroundColor: styles.palette.paleGray };
    }
  }

  showMyTechs = () => {
    this.setState({ activeButton: 'my-techs' });
  }

  showNewTechs = () => {
    this.setState({ activeButton: 'new-techs' });
  }

  getAllTechs = () => {
    const { userData, getMyTechs, getAllTechs } = this.props;
    getMyTechs(userData.userId, userData.token, getAllTechs);
  }

  renderMyTechsRow = (item) => {
    const { userData, deleteTech } = this.props;
    return(
      <MyTechLineItem
        onPress={ () => this.props.navigation.navigate('TechDetail', { tech: item }) }
        firstName={item.tech.firstName}
        lastName={item.tech.lastName}
        techId={item.tech.techId}
        removeTech={ () => deleteTech(userData.userId, item.tech.userId, userData.token, this.showTechRemovedToast, () => this.showTechErrorToast('removing')) }
      />
    );
  }

  renderNewTechsRow = (item) => {
    const { userData, claimTech } = this.props;
    return(
      <NewTechLineItem
        onPress={ () => this.props.navigation.navigate('TechDetail', { tech: item }) }
        firstName={item.firstName}
        lastName={item.lastName}
        techId={item.techId}
        addTech={ () => claimTech(userData.userId, item.userId, userData.token, this.showTechAddedToast, () => this.showTechErrorToast('adding')) }
      />
    );
  }

  showTechAddedToast = () => {
    let toast = Toast.show('Tech successfully added to your list', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
    this.getAllTechs();
  }

  showTechRemovedToast = () => {
    let toast = Toast.show('Tech successfully removed from your list', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
    this.getAllTechs();
  }

  showTechErrorToast = (type) => {
    let toast = Toast.show('Error ' + type + ' tech', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }

}
const mapStateToProps = ({ myTechsReducer, userReducer }) => {
  return { techData: myTechsReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getMyTechs, getAllTechs, claimTech, deleteTech })(ManageTechsView);
