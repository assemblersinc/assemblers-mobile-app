import React, {Component} from 'react';
import {
  Image,
  Picker,
  Platform,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import moment from "moment";
import Toast from 'react-native-root-toast';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { getReports } from '../../redux/actions';
import DashboardListView from './DashboardListView';
import { getFormattedWeekOf, getMonday, getWeeks } from "../../utils/date-time";
import { Calendar, Client } from "../../assets/index";
import styles from "../../styles";

const allClients = "All Clients";
const thisMonday = getMonday(new Date());
const isAndroid = Platform.OS == 'android';
const pickerColor = Platform.OS == 'ios' ? styles.palette.white : styles.palette.black;
const PickerItem = Picker.Item;
const iconStyle = {height: 25, width: 25, marginTop: 0, marginBottom: 0, marginRight: 20};
const searchOptionStyle = {
  height: 50,
  padding: 10,
  margin: 10,
  borderWidth: 0,
  flexDirection: 'row',
  backgroundColor: styles.palette.paleBlue,
  alignItems: 'center'
};

class DashboardView extends Component {

  state = {
    selectedClientIndex: 0,
    selectedClientName: allClients,
    selectedClient: null,
    showingPicker: false,
    showingCalendar: false,
    weeks: [],
    selectedWeek: {
      monday: thisMonday,
      description: getFormattedWeekOf(thisMonday),
      formattedForApi: moment(thisMonday).format('YYYY-MM-DD')
    }
  };

  componentWillMount() {
    this.getReports();
    let weeks = getWeeks(60);
    this.setState({weeks: weeks, selectedWeek: weeks[0]})
  }

  render() {
    const {reportingData, navigation} = this.props;

    let items = [<PickerItem key={0} label={allClients} value={allClients}
                             color={pickerColor}/>];

    for (let i = 1; i < reportingData.clientTotals.length + 1; i++) {
      let value = reportingData.clientTotals[i - 1];
      items.push(<PickerItem key={"" + i} label={value.name} value={value} color={pickerColor}/>);
    }

    let clientPicker = null;
    let weekPicker = null;

    if (isAndroid) {
      clientPicker = (<Picker mode={Picker.MODE_DROPDOWN}
                              selectedValue={this.state.selectedClientName}
                              prompt={this.state.selectedClientName}
                              onValueChange={this.onClientSelected.bind(this)}
                              style={{flex: 1, color: styles.palette.white}}>
        {items}
      </Picker>);


      let displayedWeek = (this.state && this.state.selectedWeek) || (this.state && this.state.weeks[0]);
      weekPicker = <Picker mode={Picker.MODE_DIALOG}
                           selectedValue={displayedWeek}
                           prompt={displayedWeek.description}
                           style={{flex: 1, color: styles.palette.white}}
                           onValueChange={this.onWeekSelected.bind(this)}>
        {this.state.weeks.map((week, index) => {
          return <PickerItem key={"" + index} label={week.description} value={week}
                             color={pickerColor}/>
        })}
      </Picker>;
    }

    return (
      <View style={{flex: 1, backgroundColor: styles.palette.white}}>
        <View style={{
          height: 70,
          backgroundColor: styles.palette.mediumBlue,
          flexDirection: 'row',
          alignItems: 'flex-end',
          paddingLeft: 20,
          paddingRight: 20,
          paddingTop: 20
        }}>
          <TouchableOpacity onPress={() => navigation.navigate('DrawerOpen')}>
            <Icon name="menu" size={30} color={styles.palette.white}/>
          </TouchableOpacity>
        </View>
        <View style={{backgroundColor: styles.palette.mediumBlue, padding: 10}}>
          <View>
            {isAndroid ? (
              <View style={searchOptionStyle}>
                <Image source={Client} resizeMode='contain' style={iconStyle}/>
                {clientPicker}
              </View>
            ) : (
              <View>
                <TouchableOpacity
                  style={searchOptionStyle}
                  onPress={() => this.setState({
                    showingPicker: !this.state.showingPicker,
                    showingCalendar: false })}>
                  <Image source={Client} resizeMode='contain' style={iconStyle}/>
                  <Text style={styles.fonts.whiteTextLight}>{'Clients: ' + this.state.selectedClientName}</Text>
                </TouchableOpacity>
                {this.state.showingPicker &&
                  <Picker
                    mode={Picker.MODE_DROPDOWN}
                    selectedValue={this.state.selectedClientName}
                    onValueChange={this.onClientSelected.bind(this)}>
                    <PickerItem
                      label={allClients}
                      value={allClients}
                      color={pickerColor}/>
                      {reportingData && reportingData.clientTotals && reportingData.clientTotals.map((client, index) => {
                        return <PickerItem key={String(index)} label={client.name} value={client} color={pickerColor}/>
                      })}
                  </Picker>
                }
              </View>
            )}

          </View>

          {isAndroid ? (
            <View style={searchOptionStyle}>
              <Image source={Calendar} resizeMode='contain' style={iconStyle}/>
              {weekPicker}
            </View>
          ) : (
            <View>
              <TouchableOpacity
                style={searchOptionStyle}
                onPress={ () => this.setState({
                          showingPicker: false,
                          showingCalendar: !this.state.showingCalendar
                        })
                }>
                <Image source={Calendar} resizeMode='contain' style={iconStyle}/>
                <Text style={styles.fonts.whiteTextLight}>{this.state.selectedWeek.description}</Text>
              </TouchableOpacity>
              {this.state.showingCalendar
              && <Picker mode={Picker.MODE_DROPDOWN}
                         selectedValue={(this.state && this.state.selectedWeek) || (this.state && this.state.weeks[0])}
                         onValueChange={this.onWeekSelected.bind(this)}>
                {this.state.weeks.map((week, index) => {
                  return <PickerItem
                            key={String(index)}
                            label={week.description}
                            value={week}
                            color={styles.palette.white} /> })
                }
              </Picker>}
            </View>
          )}
        </View>

        <DashboardListView selectedClient={this.state.selectedClient}/>

      </View>
    )
  }

  onClientSelected = (client, itemIndex) => {
    if(typeof client === "string" && client.indexOf('All Clients') > -1) {
      this.setState({
        selectedClient: null,
        selectedClientName: client,
        selectedClientIndex: itemIndex,
        showingPicker: false
      }, () => this.getReports());
    } else {
      this.setState({
        selectedClient: client,
        selectedClientName: client.name,
        selectedClientIndex: itemIndex,
        showingPicker: false
      }, () => this.getReports());
    }
  };

  onWeekSelected = (week) => {
    this.setState({
      selectedWeek: week,
      showingCalendar: false
    }, () => this.getReports());

  };

  getReports = () => {
    const {reportingData, userData, getReports} = this.props;
    if(!this.state.selectedClient) {
      getReports(userData.token, null, this.state.selectedWeek.formattedForApi, userData.areaId, this.viewErrorCallback);
    } else {
      getReports(userData.token, this.state.selectedClient.clientId, this.state.selectedWeek.formattedForApi, userData.areaId, this.viewErrorCallback);
    }
  };

  viewErrorCallback = () => {
    let toast = Toast.show('Unable to get lastest dashboard data.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }
}

const mapStateToProps = ({reportingReducer, userReducer}) => {
  return {reportingData: reportingReducer, userData: userReducer};
};

export default connect(mapStateToProps, { getReports })(DashboardView);
