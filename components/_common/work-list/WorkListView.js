import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
  ListView,
  Image,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import { findManagerStore, getWorkList, selectWorkListItem, searchScannedBarcode } from '../../../redux/actions';

import WorkListLineItem from './WorkListLineItem';
import ServiceSKUBottomBar from '../ServiceSKUBottomBarComponent';
import { Card, ErrorScreen, PhotoNoteLineItem } from '../';
import { BannerBackground, ScanButton } from '../../../assets';
import { ManagerLocationErrorComponent } from '../../manager-common';
import styles from '../../../styles';
import services from '../../../services';
import constants from '../../../constants.json';

const dimensions = Dimensions.get('window');
const bannerWidth = dimensions.width - 20;
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

const sampleItems = [{
      "id": 123,
      "firstName": "Bob",
      "lastName": "Builder",
      "imageUrl": "https://d2q79iu7y748jz.cloudfront.net/s/_logo/9d2f62ee3bc39480c738d6023ffeda4a.png"
    }];
class WorkListView extends Component {

  componentDidMount() {
    const { userData } = this.props;
    if(userData.timesheet) {
        this.getWorkList();
    }
  }

  getWorkList = () => {
    const { userData, getWorkList } = this.props;
    getWorkList(userData.timesheet.locationId, userData.token, userData.userId);
  }

  render() {

    const { userData, workListData, getWorkList, findManagerStore } = this.props;

    if((userData.roleId == constants.roleIds.areaManager || userData.roleId == constants.roleIds.fieldops
      || userData.roleId == constants.roleIds.contractor) && userData.timesheet == null) {
      return(
        <ManagerLocationErrorComponent label={'Work List'} retry={() => findManagerStore(userData.token, this.getWorkList)} />
      )
    }

    if(workListData.error) {
      return (
        <View style={{ flex: 1 }}>
          <ErrorScreen
              errorText={constants.errorMessages.workListError}
              actionLabel={'REFRESH'}
              action={this.getWorkList}
            />
        </View>
      )
    }

    return(
      <ScrollView style= {{ flex: 1, backgroundColor: styles.palette.white }}>
        <Text style={[{ margin: 20 }, styles.fonts.headerBlueGrey]}>Work List</Text>

         <View>
              <Card direction={'bottom'} style={{ flex: 1, margin: 10}}>
                <Image
                  resizeMode='cover'
                  source={BannerBackground}
                  style={{ flexDirection: 'row', padding: 10, paddingLeft: 20, marginBottom: 0, height: 85, width: bannerWidth }}>
                  <Text style={[{ flex: 1, marginTop: 15, backgroundColor: 'transparent' }, styles.fonts.whiteText]}>This week{"\'"}s photos & notes</Text>
                </Image>

                {(workListData.photos && workListData.photos.length > 0)
                  ? <ScrollView>
                      <ListView
                        style={{ backgroundColor: 'white', flex: 1 }}
                        dataSource={ds.cloneWithRows(workListData.photos)}
                        renderRow={this.renderPhotoRow}
                        removeClippedSubviews={false}
                        showsVerticalScrollIndicator={false} />
                    </ScrollView>
                  : <Text style={[styles.fonts.body1, { padding: 20 }]}>There are no current photos at your location.</Text>
                }
              </Card>
            </View>

           <View>
              <Card direction={'bottom'} style={{ flex: 1, margin: 10}}>
                <Image
                  resizeMode='cover'
                  source={BannerBackground}
                  style={{ flexDirection: 'row', padding: 10, paddingLeft: 20, marginBottom: 0, height: 85, width: bannerWidth }}>
                  <Text style={[{ flex: 1, marginTop: 15, backgroundColor: 'transparent' }, styles.fonts.whiteText]}>Scan to assemble</Text>
                  <TouchableOpacity onPress={this.scanToInvoice} style={{ width: 70, height: 70, marginLeft: 30, backgroundColor: 'transparent'}}>
                    <Image
                      source={ScanButton}
                      style={{ width: 70, height: 70 }}
                      shadowColor={'black'}
                      shadowOpacity={0.3} />
                  </TouchableOpacity>
                </Image>

                {(workListData.workList && workListData.workList.length > 0)
                  ? <ScrollView>
                      <ListView
                        style={{ backgroundColor: 'white', flex: 1 }}
                        dataSource={ds.cloneWithRows(workListData.workList)}
                        renderRow={this.renderRow}
                        removeClippedSubviews={false}
                        showsVerticalScrollIndicator={false} />
                    </ScrollView>
                  : <Text style={[styles.fonts.body1, { padding: 20 }]}>There are no work list items at your location.</Text>
                }

              </Card>
            </View>

      </ScrollView>
    );
  }

  renderRow = (item) => {
    return(
        <WorkListLineItem
          name={item.productName}
          sku={(item.sku && item.sku.sku) ? item.sku.sku : null}
          upc={item.upc}
          assembled={item.qtyAssembled}
          requested={item.qtyTotal}
          requestType={item.requestType}
          onPress={() => this.toItemDetail(item)}/>
    );
  }

  renderPhotoRow = (item) => {
    return(
      <PhotoNoteLineItem item={item} />
    );
  }

  enterSku = () => {
    const { navigation, workListData } = this.props;
    navigation.navigate('WorkListSkuView', { origin: constants.origins.WorkList, workList: workListData });
  }
  enterService = () => {
    const { navigation, workListData } = this.props;
    navigation.navigate('WorkListServicesView', { origin: constants.origins.WorkList, workList: workListData });
  }

  scanToInvoice = () => {
    const bottomBarComponent = <ServiceSKUBottomBar enterService={this.enterService} enterSku={this.enterSku} />
    this.props.navigation.navigate('Camera', { cameraType: constants.cameraTypes.barcode, onBarcodeFound: this.barcodeCallback, bottomBarComponent: bottomBarComponent });
  }

  barcodeCallback = (sku) => {
    const { userData, workListData, selectWorkListItem, searchScannedBarcode } = this.props;
    let isInWorklist = false;
    let product = null;

    for (let element of workListData.workList){
      if(
        (element.sku && element.sku.sku && element.sku.sku.indexOf(sku) > -1)
        || (element.upc && element.upc.indexOf(sku) > -1)
      ){
        isInWorklist = true;
        product = element;
        break;
      }
    };

    if(!product) {
      //strip the leading zeroes on the scanned sku
      let strippedSku = sku.replace(/\b0+/g, '');

      for (let element of workListData.workList){
        if(
          (element.sku && element.sku.sku && element.sku.sku.indexOf(strippedSku) > -1)
          || (element.upc && element.upc.indexOf(strippedSku) > -1)
        ){
          isInWorklist = true;
          product = element;
          break;
        }
      };
    }

    if(isInWorklist) {
      console.log('scanned product in worklist');
      product.isService = false;
      selectWorkListItem(product);
      this.props.navigation.navigate('Confirmation', { product: product, productType: constants.productTypes.product});
    } else {
      console.log('scanned product not in worklist');
      searchScannedBarcode(userData.token, sku, userData.timesheet.location.clientId, this.toItemDetail, this.barcodeNotFound);
    }
  }

  barcodeNotFound = () => {
    const { workListData } = this.props;
    this.props.navigation.navigate('WorkListSkuView', { label: 'We could not find that item.\nGo back to re-scan or enter the SKU to search.', workList: workListData });
  }

  toItemDetail = (item) => {
    const { selectWorkListItem, navigation } = this.props;
    const { params } = this.props.navigation.state;
    console.log(item);
    if(item.pricing || item.price) {
      selectWorkListItem(item);
      navigation.navigate('Confirmation', { origin: 'WorkList' });
    } else {
      this.props.navigation.navigate('SubmitPriceRequest', { product: item, origin: 'WorkList' });
    }

  }
}

const mapStateToProps = ({ workListReducer, userReducer, clockingReducer }) => {
  return { workListData: workListReducer, userData: userReducer, clockingData: clockingReducer };
};

export default connect(mapStateToProps, { findManagerStore, getWorkList, selectWorkListItem, searchScannedBarcode })(WorkListView);
