import React, { Component } from 'react';
import {
  Modal,
  Text,
  TouchableOpacity,
  View,
  Image,
  Alert,
  ScrollView
} from 'react-native';
import {
  MKTextField,
  mdl,
} from 'react-native-material-kit';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Toast from 'react-native-root-toast';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { getWorkList, addItemToInvoice, addOnDemandItemToInvoice, clearProductResults } from '../../../redux/actions';
import Button from '../Button';
import Card from '../CardComponent';
import { AddQtyIcon, MinusQtyIcon } from '../../../assets';
import styles from '../../../styles';
import constants from '../../../constants.json';

class ItemDetailView extends Component {

  state = {
    qty: 1,
    serialNumber: ''
  }

  componentDidMount() {
    this.setState({ qty: 1, serialNumber: '' });
  }

  render() {
    const { workListData } = this.props;
    const product = workListData.selectedItem;

    return (
      <View style= {{ flex: 1, justifyContent: 'space-between', backgroundColor: 'white' }}>

      <KeyboardAwareScrollView
        scrollEnabled={true}
        extraHeight={40}>
          {(!product.isService)
            ? <View style={{ marginTop: 20 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', padding: 20, paddingTop: 30, paddingBottom: 30, backgroundColor: styles.palette.paleGray}}>
                  <Text style={[{ flex: 1, textAlign: 'left' }, styles.fonts.body1]}>If item needs repair:</Text>
                  <TouchableOpacity
                    style={{ flex: 1 }}
                    onPress={() => this.cantAssemble()}>
                    <Text style={[{ textAlign: 'left' }, styles.fonts.smallLink]}>Create a repair ticket</Text>
                  </TouchableOpacity>
                </View>
              </View>
            : null
          }


          <View style={{ flex: 1, paddingTop: 50, paddingLeft: 20, paddingRight: 20, justifyContent: 'flex-start' }}>

            <Text style={[styles.fonts.bold28, {marginBottom: 10}]}>{(product.productName) ? product.productName : product.name}</Text>

            {(product.requestType)
              ? <Text style={[styles.fonts.body2, { marginBottom: 20 }]}>Request Type: {product.requestType}</Text>
              : <Text style={[styles.fonts.body2, { marginBottom: 20 }]}>Request Type: On Demand</Text>
            }

            {(product.sku && product.sku.sku && product.sku.sku.length > 0)
              ? <Text style={[styles.fonts.qty, { marginBottom: 5 }]}>SKU {product.sku.sku}</Text>
              : (product.upc)
                ? <Text style={styles.fonts.qty}>UPC {product.upc}</Text>
                : <Text style={styles.fonts.qty}>No SKU/UPC</Text>
            }


            {(this.isBicycle(product.categoryId))
              ? <MKTextField
                floatingLabelEnabled={true}
                tintColor={styles.palette.black}
                textInputStyle={[styles.fonts.qty, { height: 70, flex: 1 }]}
                placeholder={'Serial number (required)'}
                placeholderTextColor={styles.palette.black}
                highlightColor={styles.palette.black}
                style={{ height: 55, marginTop: 40, marginBottom: 60 }}
                value={this.state.serialNumber}
                onTextChange={(input) => this.setState({ serialNumber: input })}
                keyboardType={'default'}
              />
              : null
            }

            {(product.isService)
              ? <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10, marginTop: 60 }}>
                  <Text style={[styles.fonts.qty, { flex: 2 }]}>Quantity</Text>
                  <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', flex: 1 }}>
                    <View style={{ flex: 1 }}>
                      {(this.state.qty > 1)
                        ? <TouchableOpacity
                            onPress={this.decreaseServiceQyy}>
                            <Image source={MinusQtyIcon} style={{ width: 30, height: 30 }} resizeMode={'contain'} />
                          </TouchableOpacity>
                        : <View></View>
                      }
                    </View>
                    <Text style={[{ marginLeft: 15, marginRight: 15, flex: 1, textAlign: 'center' }, styles.fonts.qty]}>{this.state.qty}</Text>
                    <View style={{ flex: 1 }}>
                      {(this.canIncreaseQty())
                        ? <TouchableOpacity onPress={this.increaseServiceQty}>
                            <Image source={AddQtyIcon} style={{ width: 30, height: 30 }} resizeMode={'contain'} />
                          </TouchableOpacity>
                        : <View></View>
                      }
                    </View>
                  </View>
                </View>
              : <Text style={[{ marginBottom: 40, marginTop: 10 }, styles.fonts.qty]}>Quantity: 1</Text>
            }

          </View>
        </KeyboardAwareScrollView>
        <Card direction={'top'}>
          <View
            style={{ flexDirection: 'row', alignItems: 'center', height: 80, backgroundColor: 'white', margin: 10 }}>
            <Text style={[{ flex: 1, textAlign: 'center' }, styles.fonts.body3Bold]}>Add to invoice?</Text>
            <Button label={'ADD'} color={styles.palette.mediumBlue} onPress={this.addToInvoice} />
          </View>
        </Card>

       </View>
    );
  }

  decreaseServiceQyy = () => {
    if(this.state.qty > 1) {
      this.setState({ qty: --this.state.qty})
    }
  }

  increaseServiceQty = () => {
    if(this.canIncreaseQty()) {
      this.setState({ qty: ++this.state.qty});
    }
  }

  canIncreaseQty = () => {
    const { workListData } = this.props;
    const product = workListData.selectedItem;
    console.log(product.isWorkList, product.qtyTotal);
    return (product.isWorkList && Number(this.state.qty) < (product.qtyTotal - product.qtyAssembled)) || !product.isWorkList;
  }

  isBicycle = (category) => {
    if(constants.productCategories.bicycles.indexOf(category) > -1) {
      return true;
    } else return false;
  }

  cantAssemble = () => {
    this.props.navigation.navigate('RepairProductDetails', { origin: 'WorkList' });
  }

  addToInvoice = () => {
    const { addItemToInvoice, addOnDemandItemToInvoice, userData, workListData } = this.props;
    const product = workListData.selectedItem;

    if((!this.isBicycle(product.categoryId)) ||
    (this.isBicycle(product.categoryId) && this.state.serialNumber.length > 0)) {

      if(product.isWorkList) {
        addItemToInvoice(userData.token, userData.userId, product.requestProductId,
          userData.timesheet.location.clientId, userData.timesheet.locationId, product.isService,
          this.state.serialNumber, this.state.qty, this.invoiceSuccessCallback, this.invoiceFailCallback);
      } else {
        let itemId = (product.productId) ? product.productId : product.serviceId;
        addOnDemandItemToInvoice(userData.token, userData.userId, itemId, product,
          userData.timesheet.location.clientId, userData.timesheet.locationId, product.isService,
          this.state.serialNumber, this.state.qty, this.invoiceSuccessCallback, this.invoiceFailCallback);
      }
    }
  }

  invoiceSuccessCallback = () => {
    let toast = Toast.show('Item added to your invoice', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
    this.props.clearProductResults();
    this.backToWorklist();
  }

  invoiceFailCallback = () => {
    let toast = Toast.show('Error adding item to your invoice', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }

  backToWorklist = () => {
    //go back to worklist and refresh it
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'WorkList'}),
      ]
    });
    const { navigation, getWorkList, userData } = this.props;
    navigation.dispatch(resetAction);
    getWorkList(userData.timesheet.locationId, userData.token);
  }

}

const mapStateToProps = ({ workListReducer, userReducer, clockingReducer }) => {
  return { workListData: workListReducer, userData: userReducer, clockingData: clockingReducer };
};

export default connect(mapStateToProps, { getWorkList, addItemToInvoice, addOnDemandItemToInvoice, clearProductResults })(ItemDetailView);
