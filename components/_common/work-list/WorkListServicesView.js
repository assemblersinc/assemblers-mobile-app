import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView
} from 'react-native';
import numeral from 'numeral';
import { connect } from 'react-redux';
import { selectWorkListItem, getServicesList, searchServices } from '../../../redux/actions';
import PredictiveSearchComponent from '../PredictiveSearchComponent.js';
import styles from '../../../styles';
import constants from '../../../constants.json';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class WorkListServicesView extends Component {

  state = {
    masterList: [],
    results: [],
    searchText: ''
  }

  componentWillMount() {
    const { params } = this.props.navigation.state;

    //get all service items in the work list
    var workListServices = [];
    for(var i=0; i< params.workList.workList.length; i++) {
      if (params.workList.workList[i].isService == 1) {
        workListServices.push(params.workList.workList[i]);
      }
    }
    this.setState({ masterList: workListServices, results: workListServices });

    //get the list of all available services
    const { userData, getServicesList } = this.props;
    getServicesList(userData.token);
  }

  render() {
    const { userData, servicesData } = this.props;
    const { params } = this.props.navigation.state;
    console.log(params.workList);

    return(
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style= {{ flex: 1, backgroundColor: 'white' }}>
          <PredictiveSearchComponent
            goBack={this.props.navigation.goBack}
            text={this.state.searchText}
            onChangeText={(text) => this.searchServices(text)}
            label={'Add requested service to invoice'}
            keyboardType={'default'}/>

          <ScrollView>
            {(params.workList && params.workList.workList && params.workList.workList.length > 0)
              ? (this.state.results.length > 0)
                ? <ListView
                    dataSource={ds.cloneWithRows(this.state.results)}
                    style={{ margin: 10, marginTop: 0 }}
                    renderRow={this.renderRow}
                    />
                : <View style={{ padding: 20, marginTop: 20, alignItems: 'center' }}>
                    <Text style={styles.fonts.body1}>No services on the work list match your search.</Text>
                  </View>
              : <View style={{ padding: 20, marginTop: 20, alignItems: 'center' }}>
                  <Text style={styles.fonts.body1}>No services are on the work list.</Text>
                </View>
            }

            {(servicesData.searchResults && servicesData.searchResults.length > 0)
              ? <ListView
                  dataSource={ds.cloneWithRows(servicesData.searchResults)}
                  style={{ margin: 10, marginTop: 0 }}
                  renderRow={this.renderRow}
                  />
              : null
            }
          </ScrollView>

        </View>
      </TouchableWithoutFeedback>
    );
  }

  searchServices = (text) => {
    var matches = [];
    const masterList = (this.state.masterList) ? this.state.masterList : [];
    masterList.forEach(function(element) {
      if(element.productName.toLowerCase().indexOf(text.toLowerCase()) > -1) matches.push(element);
    });

    this.setState({ searchText: text, results: matches });

    this.props.searchServices(text);
  }

  renderRow = (item) => {
    return(
      <ServiceLineItem
        name={(item.productName) ? item.productName : item.name}
        price={item.price}
        processServiceSelection={this.processServiceSelection}
        serviceObject={item}
        isWorkList={item.isWorkList} />
    );
  }

  processServiceSelection = (service) => {
    const { selectWorkListItem } = this.props;
    const { params } = this.props.navigation.state;

    if(service.pricing || service.price ) {
      service.isService = true;
      if(!service.isWorkList) service.isWorkList = false;
      selectWorkListItem(service);
      this.setState({ text: '' });
      this.props.navigation.navigate('Confirmation', { origin: params.origin });
    } else {
      this.sendPricingRequest(service);
    }
  }

  sendPricingRequest = (product) => {
    this.props.navigation.navigate('SubmitPriceRequest', { product: product, origin: 'StoreWalk' });
  }

  onBack = () => {

  }

}

const ServiceLineItem = ({ name, price, processServiceSelection, serviceObject, isWorkList }) => {
  return(
    <TouchableOpacity
      style={{ flexDirection: 'row', alignItems: 'flex-start', padding: 10, paddingRight: 20, paddingTop: 20 }}
      onPress={() => processServiceSelection(serviceObject)}>
      <View style={{ flex: 1 }}>
        <Text
          numberOfLines={2}
          ellipseMode={'tail'}
          style={[styles.fonts.body1, { marginBottom: 5 }]}>{name}</Text>
        {(isWorkList)
          ? <Text style={styles.fonts.smallLink}>Work List Item</Text>
          : null
        }
      </View>
      <Text style={[{ width: 100, textAlign: 'right' }, styles.fonts.body1]}>{numeral(price).format('$0.00')}</Text>
    </TouchableOpacity>
  );
}

const mapStateToProps = ({ userReducer, servicesReducer }) => {
  return { userData: userReducer, servicesData: servicesReducer };
};

export default connect(mapStateToProps, { selectWorkListItem, getServicesList, searchServices })(WorkListServicesView);
