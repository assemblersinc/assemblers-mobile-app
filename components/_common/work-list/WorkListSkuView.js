import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { selectWorkListItem, skuTextChanged, clearProductResults } from '../../../redux/actions';
import { NavigationActions } from 'react-navigation';

import PredictiveSearchComponent from '../PredictiveSearchComponent';
import Button from '../Button';
import styles from '../../../styles';
import constants from '../../../constants.json';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class WorkListSkuView extends Component {

  state = {
    masterList: [],
    results: [],
    searchText: '',
    hasSearchHappened: false
  }

  componentWillMount() {
    const { params } = this.props.navigation.state;

    var workListSkus = [];
    for(var i=0; i< params.workList.workList.length; i++) {
      if (params.workList.workList[i].isService == 0) {
        workListSkus.push(params.workList.workList[i]);
      }
    }
    this.setState({ masterList: workListSkus, results: workListSkus, hasSearchHappened: false });

    if(params.label) {
      this.label = params.label;
    } else {
      this.label = 'Add SKU to invoice';
    }
  }

  render() {

    const { userData, productsData } = this.props;
    const { params } = this.props.navigation.state;

    console.log(productsData);

    return(
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style= {{ flex: 1, backgroundColor: 'white', justifyContent: 'flex-start' }}>
          <PredictiveSearchComponent
            goBack={this.onBack}
            text={this.state.searchText}
            onChangeText={(text) => this.searchSkus(text)}
            label={this.label}
            keyboardType={'numeric'} />

          <ScrollView>
          {(params.workList && params.workList.workList && params.workList.workList.length > 0)
            ? (this.state.results.length > 0)
              ? <ListView
                  dataSource={ds.cloneWithRows(this.state.results)}
                  style={{ margin: 10, marginTop: 0 }}
                  renderRow={this.renderRow}
                  />
              : <View style={{ padding: 20, marginTop: 20, alignItems: 'center' }}>
                  <Text style={styles.fonts.body1}>No SKUs on the work list match your search.</Text>
                </View>
            : <View style={{ padding: 20, marginTop: 20, alignItems: 'center' }}>
                <Text style={styles.fonts.body1}>No SKUs are on the work list.</Text>
              </View>
          }

          {(productsData.results && productsData.results.length > 0)
            ? <ListView
                dataSource={ds.cloneWithRows(productsData.results)}
                style={{ margin: 10, marginTop: 0 }}
                renderRow={this.renderRow}
                />
            : (this.state.hasSearchHappened)
              ? <View style={{ padding: 20, marginTop: 20, alignItems: 'center' }}>
                  <Text style={[styles.fonts.body1, { marginTop: 20, marginBottom: 20 }]}>No results found.</Text>
                  <Text style={styles.fonts.body1}>This SKU doesn’t match any items in our system. You can request the item to be added so that it can be priced.</Text>
                  <View style={{ flexDirection: 'row', marginTop: 40 }}>
                    <Button label={'SEND PRICING REQUEST'} color={styles.palette.mediumBlue} onPress={() => this.sendPricingRequest({ sku: { sku: this.state.searchText}})}/>
                  </View>
                </View>
              : null
          }
          </ScrollView>

        </View>
      </TouchableWithoutFeedback>
    );
  }

  searchSkus = (text) => {
    //find matching work list elements
    var matches = [];
    const masterList = (this.state.masterList) ? this.state.masterList : [];
    masterList.forEach(function(element) {
      if((element.sku && element.sku.sku && element.sku.sku != undefined && element.sku.sku.indexOf(text) > -1)
      || (element.upc && element.upc != undefined && element.upc.indexOf(text) > -1)) {
        matches.push(element);
      }
    });

    this.setState({ searchText: text, results: matches });

    //find matching products for the client
    const { skuTextChanged, userData } = this.props;
    skuTextChanged(userData.token, text, userData.timesheet.location.clientId);

    if(text.length > 2) { this.setState({ hasSearchHappened: true }); }
  }

  renderRow = (item) => {
    console.log(item);
    return(
      <SkuLineItem
        name={(item.productName) ? item.productName : item.name }
        sku={item.skus}
        upc={item.upc}
        processSkuSelection={this.processSkuSelection}
        productObject={item}
        isWorkList={item.isWorkList} />
    );
  }

  processSkuSelection = (product) => {
    const { selectWorkListItem } = this.props;
    const { params } = this.props.navigation.state;

    if(product.pricing || product.price) {
      product.isService = false;
      if(!product.isWorkList) product.isWorkList = false;
      selectWorkListItem(product);
      this.setState({ text: '' });
      this.props.navigation.navigate('Confirmation', { origin: params.origin });
    } else {
      this.sendPricingRequest(product);
    }

  }

  sendPricingRequest = (product) => {
    this.props.navigation.navigate('SubmitPriceRequest', { product: product, origin: 'WorkList' });
  }

  onBack = () => {
    const { navigation, clearProductResults } = this.props;
    navigation.goBack();
    clearProductResults();
  }

}

const SkuLineItem = ({ sku, upc, name, processSkuSelection, productObject, isWorkList }) => {
  return(
      <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: 20, marginBottom: 20 }} onPress={() => processSkuSelection(productObject)}>
        <View style={{ flex: 2 }}>
          {(sku && sku.length > 0)
            ? <Text numberOfLines={1} style={styles.fonts.body1}>SKU {sku}</Text>
            : (upc)
              ? <Text numberOfLines={1} style={styles.fonts.body1}>UPC {upc}</Text>
              : <Text style={styles.fonts.body1}>No SKU/UPC</Text>
          }
        </View>
        <View style={{ flex: 3, justifyContent: 'flex-end' }}>
          <Text
            numberOfLines={1}
            ellipseMode={'tail'}
            style={[styles.fonts.body1, { textAlign: 'right' }]}>{name}</Text>
          {(isWorkList)
            ? <Text style={[styles.fonts.smallLink, { marginTop: 5, textAlign: 'right' }]}>Work List Item</Text>
            : null
          }
        </View>
      </TouchableOpacity>
  );
};

const mapStateToProps = ({ userReducer, productsReducer }) => {
  return { userData: userReducer, productsData: productsReducer };
};

export default connect(mapStateToProps, { selectWorkListItem, skuTextChanged, clearProductResults })(WorkListSkuView);
