import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';

import styles from '../../../styles';

const WorkListLineItem = ({ name, assembled, requested, sku, upc, requestType, onPress }) => {
    return(
      <View style={{ borderBottomWidth: 1, borderBottomColor: styles.palette.paleGray, padding: 10, paddingTop: 20, paddingBottom: 20, marginLeft: 10, marginRight: 10 }}>
        <TouchableOpacity onPress={onPress}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ paddingRight: 10, flex: 2 }}>
              <Text style={styles.fonts.boldItemName} numberOfLines={4} ellipseMode={'tail'}>{name? name : ''}</Text>
            </View>
            <Text style={[{ flex: 1, textAlign: 'right' }, styles.fonts.qty]}>{assembled ? assembled : '0' } / {requested ? requested : '0'}</Text>
          </View>

          <View style={{ flexDirection: 'row', marginTop: 20 }}>
          {(sku)
            ? <Text style={[{ flex: 1, textAlign: 'left' }, styles.fonts.body1]}>SKU {sku}</Text>
            : (upc)
              ? <Text style={[{ flex: 1, textAlign: 'left' }, styles.fonts.body1]}>UPC {upc}</Text>
              : <Text style={[{ flex: 1, textAlign: 'left' }, styles.fonts.body1]}>No SKU/UPC</Text>
          }
            <Text style={[{ flex: 1, textAlign: 'right' }, styles.fonts.body1]}>{requestType ? requestType : 'n/a'}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
}

export default WorkListLineItem;
