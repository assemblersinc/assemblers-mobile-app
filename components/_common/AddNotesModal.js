import React, { Component } from 'react';
import {
  View,
  Modal,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Keyboard,
  Text
} from 'react-native';
import {
  MKTextField,
  mdl
} from 'react-native-material-kit';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Button from './Button';
import styles from '../../styles';

export default class AddNotesModal extends Component {

  state = {
    noteText: ''
  }

  render() {
    return(
      <Modal
        animationType={"fade"}
        transparent={true}
        visible={this.props.isVisible}
        onRequestClose={() => console.log('onRequestClose')}>
        <View style={styles.modal.container}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.modal.modal}>

              <View style={{ height: 50, backgroundColor: 'transparent', flexDirection: 'row', alignItems: 'flex-start', alignSelf: 'flex-end' }}>
                <TouchableOpacity onPress={this.close}>
                  <Icon name="close" size={30}  color={styles.palette.darkGray} />
                </TouchableOpacity>
              </View>

              <MKTextField
                floatingLabelEnabled={true}
                tintColor={styles.palette.darkGray}
                textInputStyle={[{ flex: 1 }, styles.fonts.formInput]}
                placeholder='Notes'
                placeholderTextColor={styles.palette.darkGray}
                highlightColor={styles.palette.darkGray}
                style={{ height: 55, width: '100%', marginTop: 40 }}
                value={this.state.noteText}
                onTextChange={ (noteText) =>  this.setState({ noteText: noteText }) }
                underlineColorAndroid='transparent'
              />

              <View style={{ marginTop: 40, flexDirection: 'row'}}>
                <Button label="ADD NOTE" onPress={this.submit} color={styles.palette.mediumBlue} />
              </View>

            </View>
          </TouchableWithoutFeedback>
        </View>
      </Modal>
    );
  }

  close = () => {
    this.setState({ noteText: "" });
    this.props.hideModal();
  }

  submit = () => {
    this.setState({ noteText: "" });
    this.props.saveNote(this.state.noteText);
  }
}
