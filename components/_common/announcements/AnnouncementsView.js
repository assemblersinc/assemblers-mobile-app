import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  ScrollView,
  Linking,
  Alert
} from 'react-native';
import Communications from 'react-native-communications';
import { connect } from 'react-redux';
import { getAnnouncements } from '../../../redux/actions';
import AnnouncementsLineItem from './AnnouncementsLineItem';
import { Button, ErrorScreen } from '../';
import styles from '../../../styles';
import constants from '../../../constants.json';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
const query = { offset: 0, limit: 15 };

class AnnouncementsView extends Component {

  componentWillMount() {
    this.getAnnouncements();
  }

  render() {
    const { announcementsData } = this.props;
    console.log(announcementsData);

    if(announcementsData.error) {
      return(
        <ErrorScreen
            errorText={constants.errorMessages.announcementsError}
            actionLabel={'TRY AGAIN'}
            action={this.getAnnouncements}/>
      );
    }
    return(
      <View style={{ flex: 1, backgroundColor: styles.palette.white }}>

        {(announcementsData.announcements && announcementsData.announcements.length > 0)
          ? <View style={{ padding: 20 }}>
              <Text style={styles.fonts.headerBlueGrey}>Announcements</Text>
              <ScrollView >
                <ListView
                  style={{ marginTop: 20 }}
                  dataSource={ds.cloneWithRows(announcementsData.announcements)}
                  renderRow={this.renderRow} />
              </ScrollView>
            </View>
          : <ErrorScreen
              errorText={constants.errorMessages.noAnnouncementsError}
              actionLabel={'REFRESH'}
              action={this.getAnnouncements}/>
        }

      </View>
    );
  }

  getAnnouncements = () => {
    const { userData, getAnnouncements } = this.props;
    getAnnouncements(query, userData.token, this.errorAlertCallback);
  }

  renderRow = (item) => {
    if(item.author == null) {
      return null;
    }
    return(
      <AnnouncementsLineItem
        text={item.text}
        firstName={item.author.firstName}
        lastName={item.author.lastName}
        timeStamp={item.createdAt}
        linkText={item.url}
        linkAction={() => Communications.web(item.url)} />
    );
  }

  errorAlertCallback = () => {
    console.log('AnnouncementsView errorAlertCallback');
  }

}


const mapStateToProps = ({ announcementsReducer, userReducer }) => {
  return { announcementsData: announcementsReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getAnnouncements })(AnnouncementsView);
