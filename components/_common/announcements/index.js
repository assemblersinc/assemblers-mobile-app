import AnnouncementsLineItem from './AnnouncementsLineItem';
import AnnouncementsView from './AnnouncementsView';

module.exports = {
  AnnouncementsLineItem: AnnouncementsLineItem,
  AnnouncementsView: AnnouncementsView
}
