import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity
} from 'react-native';

import utils from '../../../utils';
import styles from '../../../styles';

const AnnouncementsLineItem = ({ text, linkAction, linkText, firstName, lastName, timeStamp }) => {
  return(
    <View style={{ paddingTop: 20, paddingBottom: 20, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray}}>
      <Text style={[styles.fonts.body3Bold, { marginBottom: 20 }]}>{text}</Text>
      {(linkAction && linkText)
        ? <TouchableOpacity
            style={{ marginBottom: 10 }}
            onPress={linkAction}>
            <Text style={styles.fonts.smallLink}>{linkText}</Text>
          </TouchableOpacity>
        : null
      }
      {(firstName && lastName)
        ? <Text style={styles.fonts.body1}>by {firstName} {lastName.slice(0,1)}, {utils.dateTime.getFormattedDate(timeStamp)}</Text>
        : null
      }
    </View>
  );
}
export default AnnouncementsLineItem;
