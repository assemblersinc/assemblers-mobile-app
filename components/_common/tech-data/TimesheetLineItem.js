import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';

import styles from '../../../styles';

const TimesheetLineItem = ({ day, date, startTime, endTime, storeName, address1, city, state, zip, hoursWorked }) => {
  return(
    <View style={{ flexDirection: 'row', margin: 20, marginBottom: 0, paddingBottom: 20, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>
      <View style={{ flex: 4 }}>
        <Text style={[styles.fonts.boldItemName, {marginBottom: 5}]}>{day} {date}</Text>
        <Text style={styles.fonts.body1}>{startTime} - {endTime}</Text>
        <Text style={styles.fonts.body1}>{storeName}</Text>
        <Text style={styles.fonts.body1}>{address1}</Text>
        <Text style={styles.fonts.body1}>{city}, {state}  {zip}</Text>
      </View>
      <Text style={[{ flex: 1, textAlign: 'right', marginLeft: 10 }, styles.fonts.body3Bold]}>{hoursWorked} hrs</Text>
    </View>
  );
}

export default TimesheetLineItem;
