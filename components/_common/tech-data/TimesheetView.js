import React, { Component } from 'react';
import {
  View,
  Text,
  ListView,
  ScrollView
} from 'react-native';
import _ from 'lodash';
import moment from 'moment';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { getRecentTimesheets } from '../../../redux/actions';
import TimesheetLineItem from './TimesheetLineItem';
import { SectionHeader, Button, ErrorScreen } from '../';
import styles from '../../../styles';
import utils from '../../../utils';
import constants from '../../../constants.json';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class TimesheetView extends Component {

  state = {
    tech: {}
  }

  componentDidMount() {
    const { userData, timesheetData, getRecentTimesheets } = this.props;
    const { params } = this.props.navigation.state;

    if(params && params.tech) {
      getRecentTimesheets(params.tech.userId, userData.token);
      this.setState({ tech: params.tech });
    } else {
      getRecentTimesheets(userData.userId, userData.token);
    }
  }

  render() {
    const { appData, userData, timesheetData, getRecentTimesheets } = this.props;
    const { params } = this.props.navigation.state;

    return(
      <View style= {{ flex: 1, backgroundColor: styles.palette.white }} >

          {(timesheetData.length > 0)
            ?<View>
              <View style={{ margin: 20 }}>
                  {(userData.roleId == constants.roleIds.tech || userData.roleId == constants.roleIds.subcontractor)
                    ? <Text style={styles.fonts.headerBlueGrey}>My Hours</Text>
                    : <View>
                        <Text style={[styles.fonts.h2, { marginBottom: 5 }]}>{this.state.tech.firstName} {this.state.tech.lastName}</Text>
                        <Text style={styles.fonts.headerBlueGrey}>Timesheet</Text>
                      </View>
                  }
              </View>
              <ScrollView>
                <ListView
                dataSource={ds.cloneWithRows(timesheetData)}
                renderRow={this.renderRow} />
              </ScrollView>
            </View>
            : (userData.roleId == constants.roleIds.tech || userData.roleId == constants.roleIds.subcontractor)
              ? <ErrorScreen
                  errorText={constants.errorMessages.techNoTimesheet}
                  actionLabel={'REFRESH'}
                  action={() => getRecentTimesheets(userData.userId, userData.token)}
                />
              : <ErrorScreen
                  errorText={constants.errorMessages.managerNoTimesheet + this.state.tech.firstName + ' ' + this.state.tech.lastName }
                  actionLabel={'REFRESH'}
                  action={() => getRecentTimesheets(userData.userId, userData.token)}
                />
          }

      </View>
    );
  }

  renderRow = (item) => {
    const daysDataSource = ds.cloneWithRows(item.days);
    return(
      <View>
        <SectionHeader header={item.weekdate} />
        <ListView
          renderRow={this.renderInnerRow}
          dataSource={daysDataSource}
        />
      </View>
    );
  }

  renderInnerRow = (item) => {
    return(
      <TimesheetLineItem
        day={utils.dateTime.getDayOfWeek(item.timeIn)}
        date={utils.dateTime.getFormattedDate(item.timeIn)}
        startTime={utils.dateTime.getFormattedTimestamp(item.timeIn)}
        endTime={this.getEndLabel(item.timeOut)}
        storeName={item.location}
        address1={item.address}
        city={item.city}
        state={item.state}
        zip={item.zip}
        hoursWorked={this.getHoursLabel(item.timeIn, item.timeOut)}
      />
    );
  }

  getEndLabel = (timeOut) => {
    if(_.isNil(timeOut)) return 'Now';
    else return utils.dateTime.getFormattedTimestamp(timeOut);
  }

  getHoursLabel = (timeIn, timeOut) => {
    if(_.isNil(timeOut)) return utils.dateTime.calculateHours(timeIn, moment().format());
    else return utils.dateTime.calculateHours(timeIn, timeOut);
  }

}

const mapStateToProps = ({ userReducer, timesheetReducer, appReducer }) => {
  return { userData: userReducer, timesheetData: timesheetReducer, appData: appReducer };
};
export default connect(mapStateToProps, { getRecentTimesheets })(TimesheetView);
