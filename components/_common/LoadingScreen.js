import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal
} from 'react-native';
import {
  MKProgress,
  MKSpinner,
} from 'react-native-material-kit';
import styles from '../../styles';

export default class LoadingScreen extends Component {

  render() {
    return(
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <MKSpinner />
      </View>
    );
  }
}
