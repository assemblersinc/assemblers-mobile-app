import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet
} from 'react-native';
import utils from '../../../utils';
import styles from '../../../styles';
import increaseIcon from '../../../assets/addQty@3x.png';
import decreaseIcon from '../../../assets/minusQty@3x.png';

const StoreWalkLineItem = ({ name, sku, upc, addedBy, timeStamp, quantity, increaseItemQuantity, decreaseItemQuantity }) => {
    return(
      <View style={itemStyles.container}>
        <View style={{ flexDirection: 'row' }}>
          <View style={itemStyles.itemSection}>
            <Text numberOfLines={3} ellipseMode={'tail'} style={styles.fonts.boldItemName}>{name}</Text>
            {(sku && sku.sku)
              ? <Text style={[{ flex: 1, textAlign: 'left', marginTop: 5 }, styles.fonts.body1]}>{sku.sku}</Text>
              : (upc)
                ? <Text style={[{ flex: 1, textAlign: 'left', marginTop: 5 }, styles.fonts.body1]}>{upc}</Text>
                : <Text style={[{ flex: 1, textAlign: 'left', marginTop: 5 }, styles.fonts.body1]}>No SKU/UPC</Text>
            }

            <View style={{ marginTop: 5}}>
              {(addedBy && addedBy.firstName && addedBy.lastName)
                ? <Text numberOfLines={2} style={styles.fonts.body1}>Added by {addedBy.firstName} {addedBy.lastName.slice(0, 1)}</Text>
                : <Text numberOfLines={2} style={styles.fonts.body1}>Added by Unknown User</Text>
              }
              <Text style={styles.fonts.body1}>{utils.dateTime.getFormattedDate(timeStamp)} at {utils.dateTime.getFormattedTimestamp(timeStamp)}</Text>
            </View>
          </View>
          <View style={itemStyles.quantitySection}>
            <TouchableOpacity onPress={decreaseItemQuantity}>
              <Image source={decreaseIcon} style={itemStyles.icon}/>
            </TouchableOpacity>
            <Text style={[{ marginLeft: 10, marginRight: 10 }, styles.fonts.qty]}>{quantity}</Text>
            <TouchableOpacity onPress={increaseItemQuantity}>
              <Image source={increaseIcon} style={itemStyles.icon}/>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
}

const itemStyles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderBottomColor: styles.palette.paleGray,
    padding: 10,
    paddingTop: 20,
    paddingBottom: 20,
    marginLeft: 10,
    marginRight: 10,
  },
  itemSection: {
    flex: 2,
    paddingRight: 5
  },
  quantitySection: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  icon: {
    width: 30,
    height: 30
  }
});

export default StoreWalkLineItem;
