import AddSKUView from './AddSKUView';
import AddServiceView from './AddServiceView';
import StoreWalkItemDetailView from './StoreWalkItemDetailView';
import StoreWalkLineItem from './StoreWalkLineItem';
import StoreWalkView from './StoreWalkView';
import SubmitPriceRequestView from './SubmitPriceRequestView';

module.exports = {
  AddSKUView: AddSKUView,
  AddServiceView: AddServiceView,
  StoreWalkItemDetailView: StoreWalkItemDetailView,
  StoreWalkLineItem: StoreWalkLineItem,
  StoreWalkView: StoreWalkView,
  SubmitPriceRequestView: SubmitPriceRequestView
}
