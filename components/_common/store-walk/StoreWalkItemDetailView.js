import React, { Component } from 'react';
import {
  Modal,
  Text,
  TouchableOpacity,
  View,
  Image,
  Alert
} from 'react-native';
import Toast from 'react-native-root-toast';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { addStoreWalkItem, clearProductResults } from '../../../redux/actions';
import { Button, Card } from '../';
import { AddQtyIcon, MinusQtyIcon } from '../../../assets';
import styles from '../../../styles';
import utils from '../../../utils';
import constants from '../../../constants.json';

class StoreWalkItemDetailView extends Component {

  state = {
    quantity: 1,
    isGeofenced: null,
    userLat: null,
    userLong: null
  }

  componentDidMount() {
    const { userData } = this.props;
    //check if they are actually at the store
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log(position);
          const dist = utils.location.distance(position.coords.latitude, position.coords.longitude, userData.timesheet.location.lat, userData.timesheet.location.lon);
          let isGeofenced;
          if(dist > constants.storeRadius) {
            isGeofenced = false;
          } else isGeofenced = true;

          this.setState({ isGeofenced: isGeofenced, userLat: position.coords.latitude, userLong: position.coords.longitude });
      },
      (error) => {
        this.setState({ locationType: "unknown" });
      },
      {enableHighAccuracy: false, timeout: 5000, maximumAge: 5000}
    );
  }

  render() {
    const { params } = this.props.navigation.state;
    const product = params.product;


    return (
      <View style= {{ flex: 1, justifyContent: 'space-between', backgroundColor: 'white' }}>

        {(params.productType == constants.productTypes.product)
          ? <View style={{ marginTop: 20 }}>
              <View style={{ flexDirection: 'row', alignItems: 'center', padding: 20, paddingTop: 30, paddingBottom: 30, backgroundColor: styles.palette.paleGray}}>
                <Text style={[{ flex: 1, textAlign: 'left' }, styles.fonts.body1]}>If item needs repair:</Text>
                <TouchableOpacity
                  style={{ flex: 1 }}
                  onPress={() => this.toRepairFlow(product)}>
                  <Text style={[{ textAlign: 'left' }, styles.fonts.smallLink]}>Create a repair ticket</Text>
                </TouchableOpacity>
              </View>
            </View>
          : null
        }

        <View style={{ flex: 1, justifyContent: 'center', margin: 20 }}>

          <View>
            <Text style={[styles.fonts.bold28, { marginBottom: 20 }]}>{product.name}</Text>
            {(product.sku && product.sku.sku)
              ? <Text style={styles.fonts.qty}>SKU {product.sku.sku}</Text>
              : (product.upc)
                ? <Text style={styles.fonts.qty}>UPC {product.upc}</Text>
                : <Text style={styles.fonts.qty}>No SKU/UPC Information</Text>
            }
          </View>

          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 60 }}>
            <Text style={styles.fonts.qty}>Quantity</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <TouchableOpacity onPress={this.decreaseItemQuantity}>
                <Image source={MinusQtyIcon} style={{ width: 30, height: 30 }}/>
              </TouchableOpacity>
              <Text style={[{ marginLeft: 15, marginRight: 15 }, styles.fonts.qty]}>{this.state.quantity}</Text>
              <TouchableOpacity onPress={this.increaseItemQuantity}>
                <Image source={AddQtyIcon} style={{ width: 30, height: 30 }}/>
              </TouchableOpacity>
            </View>
          </View>

        </View>

        <Card direction={'top'}>
          <View style={{ flexDirection: 'row', alignItems: 'center', height: 80, backgroundColor: styles.palette.white }}>
            <Text style={[{ flex: 1, textAlign: 'center' }, styles.fonts.body3Bold]}>Add to work list</Text>
            <Button color={styles.palette.mediumBlue} label={'ADD'} onPress={() => this.addToWorkList(product)} />
          </View>
        </Card>

       </View>
    );
  }

  decreaseItemQuantity = () => {
    if(this.state.quantity > 1) {
      this.setState({ quantity: this.state.quantity - 1 })
    }
  }

  increaseItemQuantity = () => {
    this.setState({ quantity: this.state.quantity + 1 })
  }

  addToWorkList = (product) => {
    const { userData, addStoreWalkItem, storeWalkData } = this.props;
    let userObject = {"lat": this.state.userLat, "lon": this.state.userLong, "isGeofenced": this.state.isGeofenced};
    addStoreWalkItem(userData.token, userData.userId, storeWalkData.storeWalk.requestId, product, this.state.quantity,
      userObject, this.successCallback, this.failCallback);
  }

  successCallback = () => {
    this.showConfirmationToast();
    this.backToStoreWalk();
    this.props.clearProductResults();
  }

  failCallback = () => {
    let toast = Toast.show('Unable to add item to store walk. Please try again.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }

  toRepairFlow = (product) => {
    //transform the data to look like the products in work list
    const skuObject = {...product.sku};
    if(skuObject && skuObject.sku) {
      product.sku = skuObject.sku;
    }
    product.productName = product.name;
    this.props.navigation.navigate('RepairProductDetails', { origin: 'StoreWalk', product: product });
  }

  showConfirmationToast = () => {
    let toast = Toast.show('Item added to store walk.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }

  backToStoreWalk = () => {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'StoreWalk'}),
      ]
    })
    this.props.navigation.dispatch(resetAction);
  }

}

const mapStateToProps = ({ userReducer, storeWalk }) => {
  return { userData: userReducer, storeWalkData: storeWalk };
};

export default connect(mapStateToProps, { addStoreWalkItem, clearProductResults })(StoreWalkItemDetailView);
