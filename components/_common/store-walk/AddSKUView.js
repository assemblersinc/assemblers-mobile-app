import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import { skuTextChanged, clearProductResults } from '../../../redux/actions';
import { NavigationActions } from 'react-navigation';

import PredictiveSearchComponent from '../PredictiveSearchComponent';
import Button from '../Button';
import styles from '../../../styles';
import constants from '../../../constants.json';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class AddSKUView extends Component {

  render() {
    const { userData, storeWalkData, productsData, skuTextChanged } = this.props;
    const { params } = this.props.navigation.state;

    return(
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style= {{ flex: 1, backgroundColor: 'white', justifyContent: 'flex-start' }}>
          <PredictiveSearchComponent
            goBack={this.goBack}
            text={productsData.searchText}
            onChangeText={(text) => skuTextChanged(userData.token, text, userData.timesheet.location.clientId, params.origin)}
            label={(params.label) ? params.label : 'Add products to the work list'}
            keyboardType={'numeric'} />

          {(productsData.searchText.length < 3)
            ? <Text style={[styles.fonts.body1, { margin: 10 }]}>Begin typing SKU to search for products.</Text>
            : (productsData.results.length > 0)
              ? <ListView
                  dataSource={ds.cloneWithRows(productsData.results)}
                  style={{ margin: 10, marginTop: 0 }}
                  renderRow={this.renderRow}
                  />
              : <View style={{ padding: 20, alignItems: 'center' }}>
                  <Text style={[styles.fonts.body1, { marginTop: 20, marginBottom: 20 }]}>No results found.</Text>
                  <Text style={styles.fonts.body1}>This SKU doesn’t match any items in our system. You can request the item to be added so that it can be priced.</Text>
                  <View style={{ flexDirection: 'row', marginTop: 40 }}>
                    <Button label={'SEND PRICING REQUEST'} color={styles.palette.mediumBlue} onPress={() => this.sendPricingRequest({ sku: { sku: productsData.searchText}})}/>
                  </View>
                </View>
          }

        </View>
      </TouchableWithoutFeedback>
    );
  }

  goBack = () => {
    const { navigation, clearProductResults } = this.props;
    clearProductResults();
    navigation.goBack();
  }

  renderRow = (item) => {
    return(
      <SkuLineItem
        name={item.name}
        sku={item.sku}
        upc={item.upc}
        processSkuSelection={() => this.processSkuSelection(item)}
        productObject={item} />
    );
  }

  processSkuSelection = (product) => {
    if(product.pricing) {
      this.props.navigation.navigate('Confirmation', { product: product, productType: constants.productTypes.product, origin: 'StoreWalk' });
    } else {
      this.sendPricingRequest(product);
    }
  }

  sendPricingRequest = (product) => {
    this.props.navigation.navigate('SubmitPriceRequest', { product: product, origin: 'StoreWalk' });
  }

}

const SkuLineItem = ({ sku, upc, name, processSkuSelection, productObject }) => {
  return(
    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', height: 50 }} onPress={() => processSkuSelection(productObject)}>
      <View style={{ flex: 1 }}>
        {(sku != null && sku != undefined)
          ? <Text style={styles.fonts.body1}>SKU {sku.sku}</Text>
          : (upc != null && upc != undefined)
            ? <Text style={styles.fonts.body1}>UPC {upc}</Text>
            : <Text style={styles.fonts.body1}>No SKU/UPC</Text>
        }
      </View>
      <Text
        numberOfLines={1}
        ellipseMode={'tail'}
        style={[styles.fonts.body1, { flex: 1 }]}>{name}</Text>
    </TouchableOpacity>
  );
};

const mapStateToProps = ({ userReducer, productsReducer, storeWalk }) => {
  return { userData: userReducer, productsData: productsReducer, storeWalkData: storeWalk };
};

export default connect(mapStateToProps, { skuTextChanged, clearProductResults })(AddSKUView);
