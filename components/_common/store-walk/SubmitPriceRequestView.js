import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';
import Toast from 'react-native-root-toast';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { submitPricingRequest, clearProductResults } from '../../../redux/actions';
import styles from '../../../styles';
import Button from '../Button';

class SubmitPricingRequestView extends Component {
  render() {
    const { params } = this.props.navigation.state;
    const product = params.product;

    return(
      <View style={{ flex: 1, backgroundColor: styles.palette.white, padding: 40, justifyContent: 'space-around' }}>
        <Text style={styles.fonts.h2}>This item has not been priced by Assemblers, Inc.</Text>
        {(product.sku)
          ? <Text style={styles.fonts.h2}>SKU {product.sku.sku}</Text>
          : (product.upc)
            ? <Text style={styles.fonts.h2}>UPC {product.upc}</Text>
            : null
        }

        <Text style={styles.fonts.body2}>If this is the SKU/UPC for your item, submit a request for this item to be priced and check back in 1-2 hours.</Text>

        <View style={{ flexDirection: 'row'}}>
            <Button label={'SUBMIT'} color={styles.palette.mediumBlue} onPress={() => this.submitPricingRequest(product)} />
          </View>
      </View>
    );
  }

  submitPricingRequest = (product) => {
    const { userData, submitPricingRequest } = this.props;
    submitPricingRequest(userData.token, userData.userId,
      userData.timesheet.location.locationId, userData.timesheet.location.clientId, product,
      this.pricingRequestSuccessCallback, () => this.pricingRequestFailCallback(product));
  }

  pricingRequestSuccessCallback = () => {
    this.showConfirmationToast();
    let that = this;
    setTimeout(function() {
      that.backToOrigin();
    }, 500);

  }

  pricingRequestFailCallback = (product) => {
    let toast = Toast.show('Error submitting price request. Please try again.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }

  showConfirmationToast = () => {
    let toast = Toast.show('Pricing request has been submitted.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });

    this.props.clearProductResults();
  }

  backToOrigin = () => {
    const { params } = this.props.navigation.state;
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: params.origin }),
      ]
    })
    this.props.navigation.dispatch(resetAction);
  }
}

const mapStateToProps = ({ userReducer }) => {
  return { userData: userReducer };
};

export default connect(mapStateToProps, { submitPricingRequest, clearProductResults })(SubmitPricingRequestView);
