import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native';
import numeral from 'numeral';
import { connect } from 'react-redux';
import { getServicesList, searchServices } from '../../../redux/actions';
import PredictiveSearchComponent from '../PredictiveSearchComponent.js';
import styles from '../../../styles';
import constants from '../../../constants.json';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class AddServiceView extends Component {

  componentWillMount() {
    const { userData, getServicesList } = this.props;
    getServicesList(userData.token);
  }

  render() {
    const { userData, servicesData, getServicesList, searchServices } = this.props;
    return(
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style= {{ flex: 1, backgroundColor: 'white' }}>
          <PredictiveSearchComponent
            goBack={this.props.navigation.goBack}
            text={servicesData.searchText}
            onChangeText={searchServices}
            label={'Add other service to work list'}
            keyboardType={'default'}/>

          {(servicesData.searchResults.length > 0)
            ? <ListView
              dataSource={ds.cloneWithRows(servicesData.searchResults)}
              style={{ marginLeft: 10 }}
              renderRow={this.renderRow} />
            : <Text style={[styles.fonts.body1, { margin: 10 }]}>Your search doesn’t match any items in our system.</Text>
          }

        </View>
      </TouchableWithoutFeedback>
    );
  }

  renderRow = (item) => {
    return(
      <ServiceLineItem
        name={item.name}
        price={item.price}
        navigation={this.props.navigation}
        serviceObject={item} />
    );
  }

}

const ServiceLineItem = ({ name, price, navigation, serviceObject }) => {
  return(
    <TouchableOpacity
      style={{ flexDirection: 'row', alignItems: 'center', padding: 10, paddingRight: 20, paddingTop: 20 }}
      onPress={() => navigation.navigate('Confirmation', { product: serviceObject, productType: constants.productTypes.service })}>
      <Text
        numberOfLines={2}
        ellipseMode={'tail'}
        style={[styles.fonts.body1, { flex: 1 }]}>{name}</Text>
      <Text style={[{ width: 100, textAlign: 'right' }, styles.fonts.body1]}>{numeral(price).format('$0.00')}</Text>
    </TouchableOpacity>
  );
}

const mapStateToProps = ({ userReducer, servicesReducer }) => {
  return { userData: userReducer, servicesData: servicesReducer };
};

export default connect(mapStateToProps, { getServicesList, searchServices })(AddServiceView);
