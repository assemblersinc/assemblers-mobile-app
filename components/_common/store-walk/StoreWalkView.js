import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  ListView,
  Image,
  ScrollView,
  Navigator,
  Alert
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { findManagerStore, startStoreWalk, getStoreWalk, updateStoreWalkItemQty, uploadStoreWalkPhoto,
  addStoreWalkComment, submitStoreWalk, searchScannedBarcode, sendGeneratedMessage } from '../../../redux/actions';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import StoreWalkLineItem from './StoreWalkLineItem';
import { ServiceSKUBottomBarComponent, Button, Card, AddNotesModal, PhotoNoteLineItem, ErrorScreen } from '../';
import { ManagerLocationErrorComponent } from '../../manager-common';
import { DrawerPageHeader } from '../../_common/drawer-nav';

import bannerBackround from '../../../assets/add-item-background.png';
import addButton from '../../../assets/worklistAddButton@3x.png';
import photoButton from '../../../assets/cameraIcon@3x.png';
import chatIcon from '../../../assets/chatIcon@3x.png';

import styles from '../../../styles';
import services from '../../../services';
import constants from '../../../constants.json';
import utils from '../../../utils';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

const dimensions = Dimensions.get('window');
const bannerWidth = dimensions.width - 20;

class StoreWalkView extends Component {

  state = {
    notesModalVisible: false,
    managerLocationError: false,
    quantity: 1,
    isGeofenced: null,
    userLat: null,
    userLong: null
  }

  componentWillMount() {
    const { userData } = this.props;
    if(userData.timesheet) {
      this.getStoreWalk();
    }

    //check if they are actually at the store
    navigator.geolocation.getCurrentPosition(
      (position) => {
          const dist = utils.location.distance(position.coords.latitude, position.coords.longitude, userData.timesheet ? userData.timesheet.location.lat : 0, userData.timesheet ? userData.timesheet.location.lon : 0);
          let isGeofenced;
          if(dist > constants.storeRadius) {
            isGeofenced = false;
          } else isGeofenced = true;

          this.setState({ isGeofenced: isGeofenced, userLat: position.coords.latitude, userLong: position.coords.longitude });
      },
      (error) => {
        this.setState({ locationType: "unknown" });
      },
      {enableHighAccuracy: false, timeout: 5000, maximumAge: 5000}
    );
  }

  getStoreWalk = () => {
    const { userData, getStoreWalk } = this.props;
    getStoreWalk(userData.token, userData.timesheet.locationId);
  }

  render() {
    const { userData, storeWalkData, navigation, getStoreWalk, startStoreWalk, submitStoreWalk, findManagerStore } = this.props;
    console.log(userData);

    if((userData.roleId == constants.roleIds.areaManager || userData.roleId == constants.roleIds.fieldops
      || userData.roleId == constants.roleIds.contractor) && userData.timesheet == null) {
      return(
        <View style={{ flex: 1 }}>
          <DrawerPageHeader navigation={this.props.navigation} />
          <ManagerLocationErrorComponent label={'Store Walk'} retry={() => findManagerStore(userData.token, this.getStoreWalk)} />
        </View>
      )
    }

    if(storeWalkData.error) {
      return (
        <View style={{ flex: 1 }}>
          <DrawerPageHeader navigation={this.props.navigation} />
          <ErrorScreen
              errorText={constants.errorMessages.storeWalkError}
              actionLabel={'REFRESH'}
              action={this.getStoreWalk}
            />
        </View>
      )
    }

    return(
      <View style= {{ flex: 1, backgroundColor: styles.palette.white }}>

        {(userData.roleId == constants.roleIds.tech || userData.roleId == constants.roleIds.subcontractor)
          ? <View style={{height: 70, backgroundColor: styles.palette.white, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between', paddingLeft: 20, paddingRight: 20, paddingTop: 20 }}>
                <TouchableOpacity onPress={ () => navigation.navigate('DrawerOpen') }>
                  <Icon name="menu" size={30} color={styles.palette.mediumBlue} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flexDirection: 'row', alignItems: 'center', paddingBottom: 10 }}
                  onPress={this.productNotPulled}>
                  <Text style={[{ marginRight: 10 }, styles.fonts.smallLink]}>Product not pulled</Text>
                  <Image source={chatIcon} style={{ width: 20, height: 20 }} />
                </TouchableOpacity>
              </View>
          : <DrawerPageHeader navigation={this.props.navigation} />
        }

        {(!storeWalkData.storeWalk || Object.keys(storeWalkData.storeWalk).length === 0)
          ? <ErrorScreen
              errorText={constants.errorMessages.noStoreWalk}
              actionLabel={'START'}
              action={() => startStoreWalk(userData.token, userData.userId, userData.timesheet.locationId)}
            />
          : <View style={{ flex: 1 }}>
              <AddNotesModal isVisible={this.state.notesModalVisible} hideModal={this.hideNotesModal} saveNote={this.saveNote}/>

              <ScrollView>

                <Text style={[{ margin: 20, marginBottom: 30 }, styles.fonts.headerBlueGrey]}>Store Walk</Text>

                <Card direction={'bottom'}>
                  <Image
                      resizeMode='cover'
                      source={bannerBackround}
                      style={{ flexDirection: 'row', padding: 10, marginBottom: 0, height: 85, width: bannerWidth }}>
                      <Text style={[{ flex: 1, marginTop: 5, backgroundColor: 'transparent' }, styles.fonts.whiteText]}>Scan to add to today{"\'"}s{"\n"}work list</Text>
                      <TouchableOpacity onPress={this.scanToWorkList} style={{ width: 70, height: 70, marginLeft: 30, backgroundColor: 'transparent'}}>
                        <Image
                          source={addButton}
                          style={{ width: 70, height: 70 }}
                          shadowColor={'black'}
                          shadowOpacity={0.3}
                          />
                      </TouchableOpacity>
                    </Image>
                    {(storeWalkData.storeWalk.productsServices && storeWalkData.storeWalk.productsServices.length > 0)
                      ? <ListView
                          style={{ flex: 1, backgroundColor: 'white' }}
                          dataSource={ds.cloneWithRows(storeWalkData.storeWalk.productsServices)}
                          renderRow={this.renderRow}
                          removeClippedSubviews={false}
                          showsVerticalScrollIndicator={false}
                        />
                      : <Text style={[{ margin: 20, marginBottom: 30 }, styles.fonts.body2]}>Add items for the work list.</Text>
                    }

                </Card>

                <Card direction={'bottom'}>
                    <Image
                      resizeMode='cover'
                      source={bannerBackround}
                      style={{ flexDirection: 'row', padding: 10, marginBottom: 0, height: 85, width: bannerWidth }}>
                      <Text style={[{ flex: 1, marginTop: 15, backgroundColor: 'transparent' }, styles.fonts.whiteText]}>Photos & notes</Text>
                      <TouchableOpacity
                        onPress={this.addPhotosNotes}
                        style={{ width: 70, height: 70, marginLeft: 30,  backgroundColor: 'transparent'}}>
                        <Image
                          source={photoButton}
                          style={{ width: 70, height: 70 }}
                          shadowColor={'black'}
                          shadowOpacity={0.3}
                          />
                      </TouchableOpacity>
                    </Image>

                    {(storeWalkData.storeWalk.photos && storeWalkData.storeWalk.photos.length > 0)
                      ? <ListView
                          style={{ flex: 1, backgroundColor: 'white' }}
                          dataSource={ds.cloneWithRows(storeWalkData.storeWalk.photos)}
                          renderRow={this.renderPhoto}
                          showsVerticalScrollIndicator={false}
                        />
                      : <Text style={[{ margin: 20, marginBottom: 30 }, styles.fonts.body2]}>You can add photos & notes to your store walk report</Text>
                    }

                  </Card>

                </ScrollView>

                <Card direction={'top'}>
                  <View
                    style={{ width: '100%', padding: 20, alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row' }}>
                    <Text style={[{ marginRight: 20 }, styles.fonts.body1]}>Done with the store walk?</Text>
                    <Button label={'SUBMIT'} onPress={() => submitStoreWalk(userData.token, userData.userId, storeWalkData.storeWalk.requestId, this.modifyStoreWalkSuccessCallback)} color={styles.palette.mediumBlue} />
                  </View>
                </Card>
              </View>
        }
      </View>

    );
  }

  productNotPulled = () => {
    const { navigation, chatData, userData, sendGeneratedMessage } = this.props;
    sendGeneratedMessage(constants.generatedMessages.productNotPulled, userData.timesheet.location.Location);
    this.props.navigation.navigate('Chat');
  }

  modifyStoreWalkSuccessCallback = () => {
    const { userData, storeWalkData, getStoreWalk } = this.props;
    getStoreWalk(userData.token, userData.timesheet.locationId);
  }

  scanToWorkList = () => {
    const bottomBarComponent = <ServiceSKUBottomBarComponent enterService={this.enterService} enterSku={this.enterSku} />
    this.props.navigation.navigate('Camera', { cameraType: constants.cameraTypes.barcode, onBarcodeFound: this.barcodeCallback, bottomBarComponent: bottomBarComponent });
  }

  enterSku = () => {
    this.props.navigation.navigate('AddSKU', { origin: 'StoreWalk' });
  }

  enterService = () => {
    this.props.navigation.navigate('AddService', { origin: 'StoreWalk' });
  }

  barcodeCallback = (sku) => {
    const { userData, storeWalkData, searchScannedBarcode } = this.props;
    searchScannedBarcode(userData.token, sku, userData.timesheet.location.clientId, this.barcodeSearchCallback, null);
  }

  barcodeSearchCallback = (item) => {
    console.log(item);

    if(!item || item == undefined) {
      this.props.navigation.navigate('AddSKU', { label: 'We could not find that SKU.\nGo back to re-scan or enter the SKU to search.', origin: 'StoreWalk' });
    }
    else if(item.pricing) {
      item.isService = false;
      this.props.navigation.navigate('Confirmation', { product: item, productType: constants.productTypes.product, origin: 'StoreWalk'});
    }
    else if(!item.pricing) {
      this.props.navigation.navigate('SubmitPriceRequest', { product: item, origin: 'StoreWalk' });
    }
    else {
      this.props.navigation.navigate('AddSKU', { label: 'We could not find that SKU.\nGo back to re-scan or enter the SKU to search.', origin: 'StoreWalk' });
    }

  }

  decreaseItemQuantity = (item) => {
    const { userData, storeWalkData, updateStoreWalkItemQty } = this.props;
    updateStoreWalkItemQty(userData.token, storeWalkData.storeWalk.requestId, item, item.qtyTotal - 1, this.modifyStoreWalkSuccessCallback);
  }

  increaseItemQuantity = (item) => {
    const { userData, storeWalkData, updateStoreWalkItemQty } = this.props;
    updateStoreWalkItemQty(userData.token, storeWalkData.storeWalk.requestId, item, item.qtyTotal + 1, this.modifyStoreWalkSuccessCallback);
  }


  addPhotosNotes = () => {
    this.props.navigation.navigate('Camera', { cameraType: constants.cameraTypes.photo, photoCallback: this.photoUploadCallback });
  }

  photoUploadCallback = (photoPath) => {
    const { userData, storeWalkData, uploadStoreWalkPhoto } = this.props;
    let userObject = {"lat": this.state.userLat, "lon": this.state.userLong, "isGeofenced": this.state.isGeofenced, "firstName": userData.firstName, "lastName": userData.lastName};
    uploadStoreWalkPhoto(userData.token, storeWalkData.storeWalk.requestId, photoPath, storeWalkData.storeWalk.photos, userObject, this.resetToStoreWalk);
  }

  resetToStoreWalk = () => {
    this.modifyStoreWalkSuccessCallback();
    const { params } = this.props.navigation.state;

    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'StoreWalk'}),
      ]
    })
    this.props.navigation.dispatch(resetAction);
  }

  showNotesModal = (imageUrl) => {
    var noteIndex;
    this.props.storeWalkData.storeWalk.photos.forEach(function(element, index, array) {
      if(element.imageUrl == imageUrl) noteIndex = index;
    })
    this.setState({ notesModalVisible: true, currentNoteId: noteIndex });
  }

  hideNotesModal = () => {
    this.setState({ notesModalVisible: false, currentNoteId: null });
  }

  saveNote = (noteText) => {
    const { addStoreWalkComment, userData, storeWalkData } = this.props;
    this.setState({ notesModalVisible: false}, () => {
      addStoreWalkComment(userData.token, storeWalkData.storeWalk.requestId, noteText, this.state.currentNoteId, storeWalkData.storeWalk.photos, this.modifyStoreWalkSuccessCallback);
      this.hideNotesModal();
    }
    );
  }

  submitStoreWalkErrorCallback = () => {
    const { userData, storeWalkData, submitStoreWalk } = this.props;
  }

  renderRow = (item) => {
    if(item.serviceId) {
      return(
        <StoreWalkLineItem
          name={item.service.name}
          sku={item.service.sku}
          upc={item.service.upc}
          addedBy={item.user}
          timeStamp={item.updatedAt}
          quantity={item.qtyTotal}
          increaseItemQuantity={() => this.increaseItemQuantity(item)}
          decreaseItemQuantity={() => this.decreaseItemQuantity(item)}/>
      );
    } else {
      return(
        <StoreWalkLineItem
          name={item.product.name}
          sku={item.product.sku}
          upc={item.product.upc}
          addedBy={item.user}
          timeStamp={item.updatedAt}
          quantity={item.qtyTotal}
          increaseItemQuantity={() => this.increaseItemQuantity(item)}
          decreaseItemQuantity={() => this.decreaseItemQuantity(item)}/>
      );
    }

  }

  renderPhoto = (item) => {
    return(
      <PhotoNoteLineItem
        item={item}
        showNotesModal={() => this.showNotesModal(item.imageUrl)}
        submitNoteFunction={this.submitNotes}
      />
    )
  }

}

const mapStateToProps = ({ storeWalk, userReducer, chatReducer }) => {
  return { storeWalkData: storeWalk, userData: userReducer, chatData: chatReducer };
};

export default connect(mapStateToProps, { findManagerStore, getStoreWalk, startStoreWalk, searchScannedBarcode, updateStoreWalkItemQty, uploadStoreWalkPhoto, addStoreWalkComment, submitStoreWalk, sendGeneratedMessage })(StoreWalkView);
