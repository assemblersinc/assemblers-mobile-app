import React, { Component } from 'react';
import {
  View,
  Text,
  ListView,
  ScrollView
} from 'react-native';
import {
  MKRadioButton,
} from 'react-native-material-kit';
import { connect } from 'react-redux';
import { getClients } from '../../redux/actions';
import RadioButtonLineItem from './RadioButtonLineItem';
import FloatingActionButton from './FloatingActionButton';
import styles from '../../styles';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class ClientSelectionListView extends Component {

  state = {
    checked: null
  }

  componentWillMount() {
    this.radioGroup = new MKRadioButton.Group();

    const { userData, getClients } = this.props;
    const query = {};
    getClients(query, userData.token);
  }

  render() {
    const { clientData } = this.props;
    return(
      <View style= {{ flex: 1 , paddingTop: 20, backgroundColor: styles.palette.white }}>
        {(clientData && clientData.clients && clientData.clients.length > 0)
          ? <ScrollView style={{ padding: 10 }}>
              <RadioButtonLineItem
                radioGroup={this.radioGroup}
                onCheckedChange={(e) => this.onCheckedChanged(e, 'all')}
                label={'Show All Clients'}
              />
              <ListView
                dataSource={ds.cloneWithRows(clientData.clients)}
                renderRow={this.renderRow} />
            </ScrollView>
          : <Text style={[styles.fonts.body2, { padding: 20 }]}>Could not get clients list.</Text>
        }

        {(this.state.checked)
          ? <View style={{ margin: 20, marginTop: 0 }}>
              <FloatingActionButton color={styles.palette.white} backgroundColor={styles.palette.mediumBlue} onPress={this.selectClient} />
            </View>
          : null
        }


      </View>
    );
  }

  renderRow = (item) => {
    return(
      <RadioButtonLineItem
        radioGroup={this.radioGroup}
        onCheckedChange={(e) => this.onCheckedChanged(e, item)}
        label={item.name}
      />
    );
  }

  onCheckedChanged = (e, location) => {
    if(e.checked) {
      console.log(location);
      this.setState({ checked: location });
    }
  }

  selectClient = () => {
    const { navigation } = this.props;
    const { params } = navigation.state;
    console.log(this.state.checked);
    params.onPressAction(this.state.checked);
    navigation.goBack();
  }
}

const mapStateToProps = ({ clientReducer, userReducer }) => {
  return { clientData: clientReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getClients })(ClientSelectionListView);
