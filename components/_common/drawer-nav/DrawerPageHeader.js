import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import palette from '../../../styles/palette.js';

const DrawerPageHeader = ({ navigation, rightButton, color }) => {

  if(rightButton) {
    return (
      <View style={{height: 70, backgroundColor: palette.white, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end', paddingLeft: 20, paddingRight: 20, paddingTop: 20 }}>
        <TouchableOpacity onPress={ () => navigation.navigate('DrawerOpen') }>
          <Icon name="menu" size={30} color={palette.mediumBlue} />
        </TouchableOpacity>
        {rightButton}
      </View>
    );
  } else {
    return(
      <View style={{height: 70, backgroundColor: palette.white, flexDirection: 'row', alignItems: 'flex-end', paddingLeft: 20, paddingRight: 20, paddingTop: 20 }}>
        <TouchableOpacity onPress={ () => navigation.navigate('DrawerOpen') }>
          <Icon name="menu" size={30} color={palette.mediumBlue} />
        </TouchableOpacity>
      </View>
    );
  }

}

export default DrawerPageHeader;
