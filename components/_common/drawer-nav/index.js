import BasicPageHeader from './BasicPageHeader';
import CallSupportButton from './CallSupportButton';
import ClockingButton from './ClockingButton';
import DrawerPageHeader from './DrawerPageHeader';
import Header from './Header';
import ManagerContentComponent from './ManagerContentComponent';
import NavListItem from './NavListItem';
import TechContentComponent from './TechContentComponent';

module.exports = {
  BasicPageHeader: BasicPageHeader,
  CallSupportButton: CallSupportButton,
  ClockingButton: ClockingButton,
  DrawerPageHeader: DrawerPageHeader,
  Header: Header,
  ManagerContentComponent: ManagerContentComponent,
  NavListItem: NavListItem,
  TechContentComponent: TechContentComponent
}
