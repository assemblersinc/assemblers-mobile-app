import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import styles from '../../../styles';
import constants from '../../../constants.json';

const NavHeader = ({ user, firstName, lastName, roleId, area, region, onLogoutPress, onAreaPress, onStorePress }) => {
  return(
    <View style={{ marginLeft: 30, borderBottomWidth: 1, borderBottomColor: styles.palette.lightGray}}>
      <Text style={[{ marginTop: 40, marginBottom: 5 }, styles.fonts.sectionHeader]}>{firstName} {lastName}</Text>

      {(roleId == constants.roleIds.areaManager || roleId == constants.roleIds.contractor)
          ? <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 5 }}>
              <Text style={styles.fonts.body3Semi}>My Area: </Text>
              <Text style={styles.fonts.body1}> {area}</Text>
            </View>
          : null
      }

      {(roleId == constants.roleIds.fieldops)
        ? <TouchableOpacity
            style={{ marginTop: 10, marginBottom: 5 }}
            onPress={onAreaPress}>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
              <Text style={styles.fonts.smallLink}>Region: </Text>
              <Text style={styles.fonts.body1Blue}> {region}</Text>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={styles.fonts.smallLink}>Area: </Text>
              <Text style={styles.fonts.body1Blue}> {area}</Text>
            </View>
          </TouchableOpacity>
        : null
      }

      {(user.timesheet && user.timesheet.location)
          ? (user.roleId == constants.roleIds.tech)
            ? <Text style={styles.fonts.body1}>Clocked in at {user.timesheet.location.Location}</Text>
            : <Text style={styles.fonts.body1}>Checked in at {user.timesheet.location.Location}</Text>
          : null
      }


      {(user.roleId == constants.roleIds.areaManager || user.roleId == constants.roleIds.contractor)
          ? <TouchableOpacity onPress={onStorePress}>
              <Text style={[styles.fonts.smallLink, { marginTop: 20 }]}>Change Store</Text>
            </TouchableOpacity>
          : null
        }


      <TouchableOpacity
        style={{
          paddingTop: 10,
          paddingBottom: 10,
          alignItems: 'flex-start'
        }}
        onPress={onLogoutPress}>
        <Text style={[ { marginBottom: 10 }, styles.fonts.smallLink ]}>Log Out</Text>
      </TouchableOpacity>
    </View>
  );
}

export default NavHeader;
