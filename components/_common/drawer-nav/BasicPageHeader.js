import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import palette from '../../../styles/palette.js';
import styles from '../../../styles';

const BasicPageHeader = ({ navigation, onBackFunction, version, rightButton, title }) => {
  if(version == 'blue') {
    return(
      <View style={{ height: 70, backgroundColor: palette.mediumBlue, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingTop: 30 }}>

        <TouchableOpacity>
          <Icon name="chevron-left" size={35}  color={palette.white} style={{ paddingLeft: 10 }} onPress={
            () => {
              if(onBackFunction) { onBackFunction(); }
              navigation.goBack();
            }
          } />
        </TouchableOpacity>

        <Text style={[{ flex: 1, textAlign: 'center' }, styles.fonts.h2White]}>{title}</Text>

        <View style={{ width: 40 }}>
          {rightButton}
        </View>

      </View>
    );
  }
  return(
    <View style={{ height: 70, backgroundColor: palette.white, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingTop: 30 }}>

      <TouchableOpacity>
        <Icon name="chevron-left" size={35}  color={palette.mediumBlue} style={{ paddingLeft: 10 }} onPress={
          () => {
            if(onBackFunction) { onBackFunction(); }
            navigation.goBack();
          }
        } />
      </TouchableOpacity>

      <Text style={[{ flex: 1, textAlign: 'center' }, styles.fonts.h2]}>{title}</Text>

      <View style={{ width: 40 }}>
        {rightButton}
      </View>

    </View>
  );
}
export default BasicPageHeader;
