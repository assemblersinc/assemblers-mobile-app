import React, { Component } from 'react';
import Communications from 'react-native-communications';
import {
  Text,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from '../../../styles';
import config from '../../../app/config';

const CallSupportButton = () => {
  return(
    <TouchableOpacity
      onPress={() =>  Communications.phonecall(config.app.customerServicePhoneNumber, true)}
      style={{ flexDirection: 'row', marginLeft: 30, marginTop: 20, marginBottom: 50, alignItems: 'center' }}>
      <Icon name="phone-outgoing" color={styles.palette.mediumBlue} size={30} style={{ marginRight: 10 }}/>
      <Text style={styles.fonts.nav}>Call help desk</Text>
    </TouchableOpacity>
  )
};

export default CallSupportButton;
