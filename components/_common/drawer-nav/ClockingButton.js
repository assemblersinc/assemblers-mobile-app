import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import styles from '../../../styles';
import constants from '../../../constants';

const ClockingButton = ({ isClockedIn, isOnMeal, hasTakenMeal, roleId, onClockInPress, onClockOutPress, onMealInPress, onMealOutPress }) => {

  if(roleId == constants.roleIds.tech && isClockedIn && !isOnMeal && !hasTakenMeal) {
    return (
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity
          onPress={onClockOutPress}
          style={{ flex: 1, backgroundColor: styles.palette.mediumGray, paddingTop: 16, paddingBottom: 16 }}>
          <Text style={[styles.fonts.whiteTextMedium, { textAlign: 'center' }]}>Clock Out</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={onMealOutPress}
          style={{ flex: 1, backgroundColor: styles.palette.mediumBlue, paddingTop: 16, paddingBottom: 16 }}>
          <Text style={[styles.fonts.whiteTextMedium, { textAlign: 'center' }]}>Meal Out</Text>
        </TouchableOpacity>
      </View>
    );
  }
  if(isClockedIn) {
    return (
      <TouchableOpacity
          onPress={(isOnMeal) ? onMealInPress: onClockOutPress}
          style={{
              height: 60,
              paddingTop: 16,
              paddingBottom: 16,
              width: '100%',
              backgroundColor: styles.palette.mediumGray
            }}>
            {(roleId == constants.roleIds.tech)
              ? (isOnMeal)
                ? <Text style={[{ marginLeft: 30 }, styles.fonts.whiteTextMedium]}>Meal In</Text>
                : <Text style={[{ marginLeft: 30 }, styles.fonts.whiteTextMedium]}>Clock Out</Text>
              : <Text style={[{ marginLeft: 30 }, styles.fonts.whiteTextMedium]}>Check Out</Text>
            }
        </TouchableOpacity>
    );
  } else {
    return (
      <TouchableOpacity
          onPress={onClockInPress}
          style={{
            height: 60,
            paddingTop: 16,
            paddingBottom: 16,
            paddingLeft: 30,
            width: '100%',
            backgroundColor: styles.palette.mediumBlue}}>
            {(roleId == constants.roleIds.tech)
              ? <Text style={styles.fonts.whiteTextMedium}>Clock In</Text>
              : <Text style={styles.fonts.whiteTextMedium}>Check In</Text>
            }
        </TouchableOpacity>
    );
  }
}

export default ClockingButton;
