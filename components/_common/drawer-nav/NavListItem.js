import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import styles from '../../../styles';
import constants from '../../../constants.json';

class NavListItem extends Component {

  render() {

    const { navigation, route, label, activeItemKey, roleId, isClockedIn, isOnMeal, appData } = this.props;

    return(
      <View>
        {(route.routeName == 'Chat')
        ? <View style={navStyles.standardNavButton}>
            <TouchableOpacity
              style={{ flexDirection: 'row', alignItems: 'center' }}
              onPress={() => {
                navigation.navigate('DrawerClose')
                navigation.navigate(route.routeName)
              }}>
                <Text style={[this.getLabelStyle(route.key, activeItemKey), { flex: 1 }]}>
                  {label}
                </Text>

                {(appData.chatMessageCount > 0)
                  ? <View style={{ marginRight: 20, backgroundColor: styles.palette.mediumBlue, width: 30, height: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center'}}>
                      <Text style={[{ backgroundColor: 'transparent', textAlign: 'center' }, styles.fonts.whiteText]}>{appData.chatMessageCount}</Text>
                    </View>
                  : null
                }

              </TouchableOpacity>
            </View>
        : (roleId == constants.roleIds.tech || roleId == constants.roleIds.subcontractor)
            && (route.routeName == 'StoreWalk' || route.routeName == 'WorkList' || route.routeName == 'TodaysInvoice'
              || route.routeName == 'StoreRequest' || route.routeName == 'StoreRepairs' )

          ? (!isClockedIn || isOnMeal)
              ? <View style={navStyles.standardNavButton}>
                  <Text style={styles.fonts.navInactive}>
                    {label}
                  </Text>
                </View>

              : <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('DrawerClose')
                    navigation.navigate(route.routeName)
                  }}>
                  <View style={navStyles.standardNavButton}>
                    <Text style={this.getLabelStyle(route.key, activeItemKey)}>
                      {label}
                    </Text>
                  </View>
                </TouchableOpacity>

          : ((roleId == constants.roleIds.subcontractor && route.routeName == 'MyHours')
              || (roleId == constants.roleIds.contractor && route.routeName == 'MyTechs')
              || (route.routeName == 'ClockIn') || (route.routeName == 'CheckIn'))
            ? null
            : <TouchableOpacity
                onPress={() => {
                  navigation.navigate('DrawerClose')
                  navigation.navigate(route.routeName)
                }}>
                <View style={navStyles.standardNavButton}>
                  <Text style={this.getLabelStyle(route.key, activeItemKey)}>
                    {label}
                  </Text>
                </View>
              </TouchableOpacity>
        }
      </View>
    );
  }

  getLabelStyle = (route, current) => {
    if(route == current) return styles.fonts.navCurrent;
    else return styles.fonts.nav;
  }
}

const navStyles = StyleSheet.create({
  standardNavButton: {
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 20,
    marginLeft: 30,
    height: 40
  },
})

const mapStateToProps = ({ appReducer }) => {
  return { appData: appReducer };
};

export default connect(mapStateToProps)(NavListItem);
