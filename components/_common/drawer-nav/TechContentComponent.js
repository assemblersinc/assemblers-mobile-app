import React, { Component } from 'react';
import {
  View,
  ScrollView,
} from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import { logoutUser, openClockOutPromptModal, clearNeedsUpdate, mealInUser, mealOutUser, showBanner } from '../../../redux/actions';
import Header from './Header';
import NavListItem from './NavListItem';
import ClockingButton from './ClockingButton';
import CallSupportButton from './CallSupportButton';
import styles from '../../../styles';

class TechContentComponent extends Component {

  render() {
    const { userData, clockingData, navProps, openClockOutPromptModal, mealInUser, mealOutUser } = this.props;
    this.navFromClocking();

    return(
      <View style={{ flex: 1, justifyContent: 'flex-end' }}>

        <View style= {{ flex: 1, justifyContent: 'space-between' }}>
          <Header
            user={userData}
            firstName={userData.firstName}
            lastName={userData.lastName}
            roleId={userData.roleId}
            areaId={null}
            onLogoutPress={() => this.props.logoutUser(userData.token, userData.userId)} />
          <ScrollView style={{ paddingTop: 20, paddingBottom: 200, flex: 1  }}>
            {navProps.navigation.state.routes.map((route: *, index: number) => {
              return (
                <NavListItem
                  key={route.key}
                  route={route}
                  navigation={navProps.navigation}
                  label={navProps.getLabel({ route, index })}
                  activeItemKey={navProps.activeItemKey}
                  roleId={userData.roleId}
                  isClockedIn={clockingData.isClockedIn}
                  isOnMeal={clockingData.isOnMeal} />
                );
            })}
            <CallSupportButton />
          </ScrollView>
          <ClockingButton
            isClockedIn={clockingData.isClockedIn}
            roleId={userData.roleId}
            onClockInPress={this.enterClockInFlow}
            isOnMeal={clockingData.isOnMeal}
            hasTakenMeal={(userData.timesheet && userData.timesheet.mealIn) ? true : false}
            onMealInPress={() => mealInUser(userData.token, userData.timesheet.timeId, userData.userId, this.showMealInBanner)}
            onMealOutPress={() => mealOutUser(userData.token, userData.timesheet.timeId, userData.userId, this.showMealOutBanner)}
            onClockOutPress={openClockOutPromptModal}/>
        </View>
      </View>
    );
  }

  enterClockInFlow = () => {
    const { navProps } = this.props;
    navProps.navigation.navigate('ClockIn');
  }

  navFromClocking = () => {
    const { clockingData, navProps, clearNeedsUpdate } = this.props;
    if(clockingData.needsUpdate) {
      if(clockingData.isClockedIn && !clockingData.isOnMeal) {
        navProps.navigation.navigate('WorkList');
      } else {
        navProps.navigation.navigate('Announcements');
      }
      clearNeedsUpdate();
    }
  }

  showMealInBanner = () => {
    this.props.showBanner('Meal in time has been recorded.\nRefer to Optimum to review meal times.', styles.palette.mediumBlue, styles.fonts.whiteText);
  }

  showMealOutBanner = () => {
    this.props.showBanner('Meal out time has been recorded.\nMake sure to meal in after your break', styles.palette.mediumBlue, styles.fonts.whiteText);
  }

}

const mapStateToProps = ({ userReducer, clockingReducer }) => {
  return { userData: userReducer, clockingData: clockingReducer };
};
export default connect(mapStateToProps, { logoutUser, openClockOutPromptModal, clearNeedsUpdate, mealInUser, mealOutUser, showBanner })(TechContentComponent);
