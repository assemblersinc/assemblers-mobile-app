import React, { Component } from 'react';
import {
  View,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import { logoutUser, showFieldopsSelectionModal, clearNeedsUpdate } from '../../../redux/actions';
import Header from './Header';
import NavListItem from './NavListItem';
import CallSupportButton from './CallSupportButton';

class ManagerContentComponent extends Component {
  render() {
    const { userData, navProps, showFieldopsSelectionModal } = this.props;
    this.navFromCheckIn();

    return(
      <View style={{ flex: 1, justifyContent: 'flex-end' }}>

        <View style= {{ flex: 1, justifyContent: 'space-between' }}>
          <Header
            user={userData}
            firstName={userData.firstName}
            lastName={userData.lastName}
            roleId={userData.roleId}
            area={(userData.geoarea) ? userData.geoarea.name : 'None'}
            region={(userData.region) ? userData.region.name : 'None'}
            onAreaPress={showFieldopsSelectionModal}
            onStorePress={() => navProps.navigation.navigate('CheckIn')}
            onLogoutPress={() => this.props.logoutUser(userData.token, userData.userId)} />
          <ScrollView style={{ paddingTop: 20, paddingBottom: 200, flex: 1  }}>
            {navProps.navigation.state.routes.map((route: *, index: number) => {
              return (
                <NavListItem
                  key={route.key}
                  route={route}
                  navigation={navProps.navigation}
                  label={navProps.getLabel({ route, index })}
                  activeItemKey={navProps.activeItemKey}
                  roleId={userData.roleId}
                  isClockedIn={null} />
                );
            })}

            <CallSupportButton />
          </ScrollView>
        </View>
      </View>
    );
  }

  navFromCheckIn = () => {
    const { clockingData, navProps, clearNeedsUpdate } = this.props;
    if(clockingData.needsUpdate) {

      if(clockingData.isClockedIn && !clockingData.isOnMeal) {
        navProps.navigation.navigate('StoreWalk');
      } else {
        navProps.navigation.navigate('Announcements');
      }
      clearNeedsUpdate();
    }
  }


}

const mapStateToProps = ({ userReducer, clockingReducer }) => {
  return { userData: userReducer, clockingData: clockingReducer };
};
export default connect(mapStateToProps, { logoutUser, showFieldopsSelectionModal, clearNeedsUpdate })(ManagerContentComponent);
