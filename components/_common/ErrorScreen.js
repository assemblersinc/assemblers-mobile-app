import React, { Component } from 'react';
import {
  Text,
  View
} from 'react-native';

import Button from './Button';
import styles from '../../styles';

const ErrorScreen = ({ errorText, actionLabel, action }) => {
    return(
      <View style= {{ flex: 1, justifyContent: 'space-between', alignItems: 'center', backgroundColor: styles.palette.white, padding: 20 }}>
          <Text style={styles.fonts.h2}>{errorText}</Text>
          <View style={{ flexDirection: 'row'}}>
            <Button label={actionLabel} onPress={action} color={styles.palette.mediumBlue} />
          </View>
      </View>
    );
}

export default ErrorScreen;
