import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from '../../styles';

const SectionHeader = ({ header, badgeCount }) => {
    return(
      <View style={{ flexDirection: 'row', height: 60, backgroundColor: styles.palette.paleGray, paddingLeft: 20, paddingBottom: 10, alignItems: 'flex-end', justifyContent: 'space-between' }}>
        <Text style={styles.fonts.sectionHeader}>{header}</Text>
        {(badgeCount != null && badgeCount > 0)
          ? <View style={{ flexDirection: 'row', alignItems: 'center',  marginRight: 20 }}>
           	  <Icon style={{ width: 30 }} name={'circle'} color={styles.palette.orange} backgroundColor="#FFF" size={30} />
              <Text style={{ backgroundColor: 'transparent', marginLeft: -30, paddingLeft: 0, width: 30, textAlign: 'center', color: styles.palette.white}}>{badgeCount}</Text>
           </View>
           : null
    	}
      </View>
    );
}

export default SectionHeader;
