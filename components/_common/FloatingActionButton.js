import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import {
  MKButton,
  mdl,
} from 'react-native-material-kit';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from '../../styles';

const FloatingActionButton = ({ backgroundColor, color, onPress }) =>{
  return(
    <View style={{ alignItems: 'flex-end', marginTop: 20, marginBottom: 20 }}>
      <MKButton
        style={{ justifyContent: 'center', alignItems: 'center', width: 50, height: 50, borderRadius: 25, backgroundColor: backgroundColor }}
        fab={true}
        rippleColor={styles.palette.mediumBlue30}
        rippleLocation="center"
        onPress={onPress} >
        <Icon name="chevron-right" size={45} color={color}  style={{ backgroundColor: 'transparent'}}/>
      </MKButton>
    </View>
  );
}

export default FloatingActionButton;
