import React, { Component } from 'react';
import {
  TouchableOpacity,
  Text,
  View
} from 'react-native';

import styles from '../../styles';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Generic button component for use throughout the app.
//REQUIRED PROPS: buttonText and buttonAction

export default class Button extends Component {

  render() {
    return(
      <TouchableOpacity onPress={this.props.onPress} style={{ backgroundColor: 'white', flex: 1, height: 50, justifyContent: 'center', alignItems: 'center', marginLeft: 10, marginRight: 10, borderColor: this.props.color, borderWidth: 1, borderRadius: 3 }}>
        <Text style={[ styles.fonts.body2, { textAlign: 'center', color: this.props.color }]}>{this.props.label}</Text>
      </TouchableOpacity>
    );
  }

}
