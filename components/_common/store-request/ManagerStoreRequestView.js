import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView
} from 'react-native';
import { connect } from 'react-redux';
import { getManagerStoreRequestList, selectRequest } from '../../../redux/actions';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from '../../../styles';
import utils from '../../../utils';
import constants from '../../../constants.json';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class ManagerStoreRequestView extends Component {

  state = {
    "update": false,
    "filteredList": [],
    "masterList": [],
    "selectedClient": "All Clients"
  }

  componentDidMount() {
    const { userData, storeRequestData, getManagerStoreRequestList } = this.props;
    getManagerStoreRequestList(userData.token, userData.areaId,
    () => this.setState({ "filteredList": storeRequestData.managerList, "masterList": storeRequestData.managerList, "update": true }) );
  }

  render() {

    const { navigation, storeRequestData } = this.props;

    return(
      <View style={{ flex: 1 }} key={this.state.update}>
        <View style={{ backgroundColor: styles.palette.paleGray, padding: 20, paddingTop: 30 }}>
          <View style={{height: 70, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
            <TouchableOpacity onPress={ () => navigation.navigate('DrawerOpen') }>
              <Icon name="menu" size={30} color={styles.palette.mediumBlue} />
            </TouchableOpacity>
            <Text style={[styles.fonts.headerBlueGrey, { marginLeft: 20 }]}>Store request list</Text>
          </View>

          <TouchableOpacity
            onPress={this.toClientList}
            style={{ backgroundColor: styles.palette.white, marginTop: 20, marginBottom: 10, padding: 10, flexDirection: 'row', alignItems: 'center' }}>
            <Icon name="account-multiple" size={30} color={styles.palette.mediumGray} />
            <Text style={[styles.fonts.formInput, { marginLeft: 10 }]}>{this.state.selectedClient}</Text>
          </TouchableOpacity>
        </View>
        <View style={{ backgroundColor: styles.palette.white, flex: 1 }}>
          {(!this.state.masterList || this.state.masterList.length == 0)
            ? <Text style={[styles.fonts.body1, { padding: 20 }]}>Could not get store request information.</Text>
            : (this.state.filteredList && this.state.filteredList.length > 0)
              ? <ListView
                style={{ padding: 20 }}
                renderRow={this.renderRow}
                dataSource={ds.cloneWithRows(this.state.filteredList)} />
              : <Text style={[styles.fonts.body1, { padding: 20 }]}>No current store requests to show for {this.state.selectedClient}.</Text>
          }

        </View>
      </View>
    );
  }

  toClientList = () => {
    this.props.navigation.navigate('ClientSelection', { onPressAction: this.filterList });
  }

  renderRow = (item) => {
    return(
      <RequestListItem
        store={item.location.Location}
        requestedTimestamp={item.createTime}
        forTimestamp={item.scheduleData}
        productCount={item.requestproduct.length + item.requestservice.length}
        onPress={() => this.toStoreRequestDetail(item)} />
    );
  }

  filterList = (client) => {
    var filteredList = [];
    const masterList = (this.state.masterList) ? this.state.masterList : [];
    if(client == 'all') {
      this.setState({ filteredList: masterList, selectedClient: 'All Clients' });
    } else if(client && masterList) {
      masterList.forEach(function(request) {
        if(request.location.clientId == client.clientId) {
          console.log('match', request.location.clientId, client.clientId);
          filteredList.push(request);
        }
      });
      this.setState({ filteredList: filteredList, selectedClient: client.name });
    }
  };

  toStoreRequestDetail = (request) => {
    this.props.selectRequest(request);
    this.props.navigation.navigate('StoreRequest');
  }

}

const RequestListItem = ({ store, requestedTimestamp, forTimestamp, productCount, onPress }) => {
  return (
    <View style={{ flexDirection: 'row', alignItems: 'flex-start', paddingTop: 20, paddingBottom: 20, borderBottomColor: styles.palette.borderGray, borderBottomWidth: 1 }}>
      <View style={{ flex: 1 }}>
        <TouchableOpacity
          onPress={onPress}>
          <Text style={[styles.fonts.mediumLink, { marginBottom: 5 }]}>{store}</Text>
        </TouchableOpacity>
        <Text style={styles.fonts.body2}>Requested {utils.dateTime.getFormattedDateWithYear(requestedTimestamp)}</Text>
        <Text style={styles.fonts.body2}>{utils.dateTime.getFormattedTimestamp(requestedTimestamp)}</Text>
      </View>
      <View style={{ flex: 1, alignItems: 'flex-end' }}>
        <Text style={[styles.fonts.body3Bold, { marginBottom: 5 }]}>for {utils.dateTime.getFormattedDateWithYear(forTimestamp)}</Text>
        <Text style={styles.fonts.body2}>{productCount} products</Text>
      </View>
    </View>
  );
}

const mapStateToProps = ({ storeRequestReducer, userReducer }) => {
  return { storeRequestData: storeRequestReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getManagerStoreRequestList, selectRequest })(ManagerStoreRequestView);
