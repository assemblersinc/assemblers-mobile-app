import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';
import numeral from 'numeral';
import styles from '../../../styles';

const StoreRequestLineItem = ({ productName, sku, upc, requester, quantity, price }) => {
  return(
    <View style={{ flexDirection: 'row', margin: 20, marginBottom: 0, paddingBottom: 20, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>
      <View style={{ flex: 3 }}>
        <Text
          style={[styles.fonts.boldItemName, { marginBottom: 10 }]}
          numberOfLines={3}
          ellipseMode={'tail'}>
          {productName}
        </Text>
        {(sku)
          ? <Text style={styles.fonts.body1}>{sku}</Text>
          : (upc)
            ? <Text style={styles.fonts.body1}>{upc}</Text>
            : <Text style={styles.fonts.body1}>No SKU/UPC</Text>
        }
      </View>
      <View style={{ flex: 1, marginLeft: 10, justifyContent: 'space-between' }}>
        <Text style={styles.fonts.qty}>QTY {quantity}</Text>
        <Text style={styles.fonts.body1}>{numeral(price).format('$0.00')}</Text>
      </View>
    </View>
  );
}

export default StoreRequestLineItem;
