import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  ListView,
  TouchableOpacity
} from 'react-native';
import Communications from 'react-native-communications';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { getStoreRequest } from '../../../redux/actions';
import { ErrorScreen } from '../';
import { BasicPageHeader, DrawerPageHeader } from '../drawer-nav';
import StoreRequestLineItem from './StoreRequestLineItem';
import styles from '../../../styles';
import constants from '../../../constants.json';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class StoreRequestView extends Component {

  componentWillMount() {
    //if this is a tech, get the store request for the store he is currently in
    //if this is a manager, show the store request list for the store that routed him here
    const { userData, getStoreRequest } = this.props;
    if(userData.roleId == constants.roleIds.tech || userData.roleId == constants.roleIds.subcontractor) {
      getStoreRequest(userData.token, userData.timesheet.locationId);
    }
  }

  render() {
    const { userData, storeRequestData, getStoreRequest, navigation } = this.props;

    const rightButton =
      <TouchableOpacity onPress={() => this.callStore(storeRequestData.location)}>
          <Icon name={'phone'} size={25} style={{ marginRight: 20 }} color={styles.palette.mediumBlue} />
      </TouchableOpacity>;

    return(
      <View style= {{ flex: 1, backgroundColor: styles.palette.white }}>

        {(storeRequestData.location && storeRequestData.location.phoneNumber)
          ? (userData.roleId == constants.roleIds.areaManager || userData.roleId == constants.roleIds.fieldops || userData.roleId == constants.roleIds.contractor)
            ? <BasicPageHeader navigation={navigation} rightButton={rightButton} />
            : <DrawerPageHeader navigation={navigation} />
          : (userData.roleId == constants.roleIds.areaManager || userData.roleId == constants.roleIds.fieldops || userData.roleId == constants.roleIds.contractor)
            ? <BasicPageHeader navigation={navigation} />
            : <DrawerPageHeader navigation={navigation} />
        }

        {(storeRequestData.location)
          ? <View>
              <View style={{ margin: 20 }}>
                <Text style={styles.fonts.headerBlueGrey}>Store Request</Text>
                <Text style={[styles.fonts.h2, { marginTop: 10 }]}>{storeRequestData.location.Location}</Text>
              </View>

              <View style={{ margin: 20, marginTop: 10, marginBottom: 0, paddingBottom: 30, borderBottomWidth: 1, borderBottomColor: styles.palette.paleGray }}>
                <Text style={styles.fonts.body2}>{storeRequestData.location.address}</Text>
                <Text style={styles.fonts.body2}>{storeRequestData.location.city}, {storeRequestData.location.state} {storeRequestData.location.zip}</Text>
              </View>
            </View>
          : <View style={{ margin: 20 }}>
              <Text style={styles.fonts.headerBlueGrey}>Store Request</Text>
            </View>
        }


        {(storeRequestData.items && storeRequestData.items.length > 0)
          ? <View>
              <View style={{ flexDirection: 'row', height: 60, backgroundColor: styles.palette.paleGray, paddingBottom: 10, paddingLeft: 20, alignItems: 'flex-end' }}>
                <Text style={styles.fonts.sectionHeader}>Store request items</Text>
              </View>
              <ScrollView>
                <ListView
                  dataSource={ds.cloneWithRows(storeRequestData.items)}
                  renderRow={this.renderRow}
                  />
              </ScrollView>
            </View>
          : <Text style={[{ margin: 20 }, styles.fonts.body2]}>No items currently requested at this location.</Text>
        }

      </View>
    );
  }

  callStore = (location) => {
    if(location && location.phoneNumber) {
      Communications.phonecall(location.phoneNumber, true);
    }
  }

  renderRow = (item) => {
    if(item && item.product) {
      return(
        <StoreRequestLineItem
          productName={item.product.name}
          sku={item.product.sku.sku}
          upc={item.product.upc}
          requester={item.user}
          quantity={String(item.qtyTotal)}
          price={item.product.pricing.price}
        />
      )
    } else if(item && item.service) {
      return(
        <StoreRequestLineItem
          productName={item.service.name}
          sku={null}
          upc={null}
          requester={item.user}
          quantity={String(item.qtyTotal)}
          price={item.service.price}
        />
      )
    } else return <Text style={styles.fonts.body1}>Invalid product type</Text>;
  }
}

const mapStateToProps = ({ userReducer, storeRequestReducer }) => {
  return { userData: userReducer, storeRequestData: storeRequestReducer };
};
export default connect(mapStateToProps, { getStoreRequest })(StoreRequestView);
