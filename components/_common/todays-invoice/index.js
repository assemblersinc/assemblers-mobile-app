import CantGetFinalizedView from './CantGetFinalizedView';
import GetKeyRecView from './GetKeyRecView';
import GetPOCodeView from './GetPOCodeView';
import InvoiceDetailLineItem from './InvoiceDetailLineItem';
import InvoiceDetailView from './InvoiceDetailView';
import InvoiceLineItem from './InvoiceLineItem';
import PrintNameView from './PrintNameView';
import SubmitInvoiceView from './SubmitInvoiceView';
import SignatureView from './SignatureView';
import StartInvoiceView from './StartInvoiceView';
import SurveyQuestion from './SurveyQuestion';
import SurveyView from './SurveyView';
import ThankYouView from './ThankYouView';

module.exports = {
  CantGetFinalizedView: CantGetFinalizedView,
  GetKeyRecView: GetKeyRecView,
  GetPOCodeView: GetPOCodeView,
  InvoiceDetailLineItem: InvoiceDetailLineItem,
  InvoiceDetailView: InvoiceDetailView,
  InvoiceLineItem: InvoiceLineItem,
  PrintNameView: PrintNameView,
  SignatureView: SignatureView,
  SubmitInvoiceView: SubmitInvoiceView,
  StartInvoiceView: StartInvoiceView,
  SurveyQuestion: SurveyQuestion,
  SurveyView: SurveyView,
  ThankYouView: ThankYouView
}
