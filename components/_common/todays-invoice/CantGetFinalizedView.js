import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
} from 'react-native';
import {
  MKRadioButton,
  mdl,
} from 'react-native-material-kit';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { sendGeneratedMessage } from '../../../redux/actions';
import FloatingActionButton from '../FloatingActionButton';
import RadioButtonLineItem from '../RadioButtonLineItem';
import styles from '../../../styles';
import constants from '../../../constants.json';

const radioGroup = new MKRadioButton.Group();
var checkedReason;

class CantGetFinalizedView extends Component {

  componentWillMount() {
    this.checkedReason = null;
  }

  render() {

    const { clockedInStore } = this.props.clockingData;

    return(
      <View style= {{ flex: 1 , padding: 20, paddingTop: 50, backgroundColor: 'white', justifyContent: 'space-between' }}>

        <View style={{ paddingBottom: 30, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>
          <Text style={styles.fonts.itemNoLink}>Why can{"\'"}t you get today{"\'"}s invoice finalized?</Text>
        </View>

        <View>
          <RadioButtonLineItem radioGroup={radioGroup} label={"Can't get signature"} onCheckedChange={(e) => this.onCheckedChange(e, "Can't get signature")} />
          <RadioButtonLineItem radioGroup={radioGroup} label={"Can't get PO code"} onCheckedChange={(e) => this.onCheckedChange(e, "Can't get PO code")} />
          <RadioButtonLineItem radioGroup={radioGroup} label={"Can't get key rec"} onCheckedChange={(e) => this.onCheckedChange(e, "Can't get key rec")} />
          <RadioButtonLineItem radioGroup={radioGroup} label={"Can't get Wal-Mart stamp"} onCheckedChange={(e) => this.onCheckedChange(e, "Can't get Wal-Mart stamp")} />
          <RadioButtonLineItem radioGroup={radioGroup} label={"Other"} onCheckedChange={(e) => this.onCheckedChange(e, "Other")} />
        </View>

        <FloatingActionButton color={styles.palette.white} backgroundColor={styles.palette.mediumBlue} onPress={this.sendToChat} />

      </View>
    );
  }

  onCheckedChange = (e, reason) => {
    if(e.checked) {
      this.checkedReason = reason;
    }
  }

  sendToChat = () => {
    const { userData, sendGeneratedMessage } = this.props;

    if(this.checkedReason != null) {
      console.log(this.checkedReason);
      //TODO: send message to chat
      const message = "Can't get invoice finalized. " + this.checkedReason + " at ";
      sendGeneratedMessage(message, userData.timesheet.location.Location);
      this.props.navigation.navigate('Chat');
    }
  }
}


const mapStateToProps = ({ userReducer, clockingReducer }) => {
  return { clockingData: clockingReducer, userData: userReducer };
};

export default connect(mapStateToProps, { sendGeneratedMessage })(CantGetFinalizedView);

// {(clockedInStore.clientId == constants.HomeDepotID)
//   ? <View>
//     </View>
//   : (clockedInStore.clientId == constants.WalMartID)
//     ? <View>
//       </View>
//     : <Text>How did you get here?</Text>
// }

// <View>
//   <RadioButtonLineItem radioGroup={radioGroup} label={"Can't get signature"} onCheckedChange={(e) => this.onCheckedChange(e, "Can't get signature")} />
//   <RadioButtonLineItem radioGroup={radioGroup} label={"Can't get PO code"} onCheckedChange={(e) => this.onCheckedChange(e, "Can't get PO code")} />
//   <RadioButtonLineItem radioGroup={radioGroup} label={"Can't get key rec"} onCheckedChange={(e) => this.onCheckedChange(e, "Can't get key rec")} />
//   <RadioButtonLineItem radioGroup={radioGroup} label={"Other"} onCheckedChange={(e) => this.onCheckedChange(e, "Other")} />
// </View>
//
// <View>
//   <RadioButtonLineItem radioGroup={radioGroup} label={"Can't get signature"} onCheckedChange={(e) => this.onCheckedChange(e, "Can't get signature")} />
//   <RadioButtonLineItem radioGroup={radioGroup} label={"Can't get Wal-Mart stamp"} onCheckedChange={(e) => this.onCheckedChange(e, "Can't get Wal-Mart stamp")} />
//   <RadioButtonLineItem radioGroup={radioGroup} label={"Other"} onCheckedChange={(e) => this.onCheckedChange(e, "Other")} />
// </View>
