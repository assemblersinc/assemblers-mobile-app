import React, { Component } from 'react';
import {
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import BasicPageHeader from '../drawer-nav/BasicPageHeader';
import FloatingActionButton from '../FloatingActionButton';
import SurveyQuestion from './SurveyQuestion';
import styles from '../../../styles'

const SurveyComponent = ({ onBackFunction, index, totalQuestions, questionText, questionType, rating, onStarSelection}) => {
  return(
    <View style= {{ flex: 1, justifyContent: 'space-between', backgroundColor: styles.palette.white }}>

      <BasicPageHeader navigation={this.props.navigation} onBackFunction={this.props.clearSurveyAnswers} />

      <View style={{ flex: 1, padding: 20 }}>

        <SurveyQuestion
          index={index}
          totalQuestions={totalQuestions}
          questionText={questionText}
          questionType={questionType}
          rating={rating}
          onStarSelection={(index, rating) => onStarSelection(index, rating)}
         />

      </View>

    </View>
  );
}
export default SurveyComponent;
