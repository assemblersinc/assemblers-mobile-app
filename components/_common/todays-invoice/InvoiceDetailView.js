import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  ScrollView,
  Image
} from 'react-native';
import numeral from 'numeral';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Card, Button, ErrorScreen } from '../';
import { uploadStampImage, decreaseInvoiceItemQuantity } from '../../../redux/actions';
import InvoiceDetailLineItem from './InvoiceDetailLineItem';
import styles from '../../../styles';
import utils from '../../../utils';
import constants from '../../../constants.json';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
var nextStep = null;

class InvoiceDetailView extends Component {

  state = {
    "editable": true
  }

  componentWillMount() {
    const { invoiceData } = this.props;
    //if you got here from the invoice history view OR if the invoice has been submitted,
    //DO NOT allow editing of the invoice
    const { params } = this.props.navigation.state;
    if(params.origin == 'history' || (invoiceData.currentInvoice.invoiceCompletedTime != null)) {
      this.setState({ editable: false });
    }
  }


  render() {

    const { invoiceData, userData, navigation } = this.props;
    const index = invoiceData.index;
    const currentInvoice = invoiceData.currentInvoice;
    console.log(currentInvoice);

    nextStep = utils.clientAttributeUtil.getNextStep(currentInvoice);

    return(
      <View style= {{ flex: 1, backgroundColor: styles.palette.white }}>
        <View style={{ height: 70, width: '100%', backgroundColor: styles.palette.white, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', paddingTop: 30 }}>
          <Icon name="chevron-left" size={30}  color={styles.palette.mediumBlue} style={{ paddingLeft: 20 }} onPress={() => this.props.navigation.goBack()} />
          <Text style={[{ flex: 1, textAlign: 'center', paddingRight: 50 }, styles.fonts.body1]}>{utils.dateTime.getDate(currentInvoice.invoiceGeneratedTime)}</Text>
        </View>
        <View style={{ margin: 20, marginBottom: 0 }}>
          <View style={{ flexDirection: 'row', marginBottom: 10 }}>
            <View style={{ flex: 1 }}>
              <Text style={[styles.fonts.h2, { marginBottom: 5 }]}>Invoice # { currentInvoice.invoiceNumber === 'N/A' ? currentInvoice.invoiceId : currentInvoice.invoiceNumber}</Text>
              <Text style={styles.fonts.h2}>{numeral(currentInvoice.total).format('$0.00')}</Text>
            </View>
            {(this.state.editable && currentInvoice.signatureImageUrl && nextStep && nextStep.title)
              ? <View style={{ flex: 1 }}>
                  <Button label={this.getStepLabel()} onPress={this.getOnPress()} color={styles.palette.mediumBlue} />
                </View>
              : null
            }
          </View>
          <View style={{ paddingBottom: 20, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>
            <Text style={styles.fonts.body2}>{this.getTotalAssemled()} items assembled</Text>
          </View>
        </View>
        <ScrollView>
          <View style={{ margin: 20, marginBottom: 30 }}>
            <View>
              <Text style={styles.fonts.body2}>{currentInvoice.client.name}</Text>
              <Text style={styles.fonts.body2}>{currentInvoice.location.address}</Text>
              <Text style={styles.fonts.body2}>{currentInvoice.location.city}, {currentInvoice.location.state} {currentInvoice.location.zip}</Text>
            </View>
            <View style={{ marginTop: 30 }}>
              <Text style={styles.fonts.body2}>Tech ID {currentInvoice.user.techId}</Text>
              <Text style={[styles.fonts.body2, { marginTop: 5, marginBottom: 5 }]}>{currentInvoice.user.firstName} {currentInvoice.user.lastName.slice(0, 1)}</Text>
              <Text style={styles.fonts.body2}>{utils.dateTime.getFormattedTimestamp(currentInvoice.invoiceGeneratedTime)} {currentInvoice.invoiceCompletedTime ? '- ' + utils.dateTime.getFormattedTimestamp(currentInvoice.invoiceCompletedTime) : ''}</Text>
            </View>
          </View>
          <View style={{ backgroundColor: styles.palette.paleGray, height: 60, paddingLeft: 20, paddingTop: 20 }}>
            <Text style={styles.fonts.sectionHeader}>Invoice items</Text>
          </View>
          {(currentInvoice && currentInvoice.invoiceproduct && currentInvoice.invoiceproduct.length > 0)
            ? <ListView
                style={{ margin: 20 }}
                renderRow={this.renderRow}
                enableEmptySections={true}
                dataSource={ds.cloneWithRows(currentInvoice.invoiceproduct)} />
            : <Text style={[styles.fonts.body1, { padding: 20 }]}>No invoice products to show</Text>
          }
          {(currentInvoice.stamp) &&
            <View style={{ flexDirection: 'row', margin: 20}}>
              <Image style={{ flex: 1, resizeMode: 'contain', height: 150, width: 150 }} source={{uri: currentInvoice.stamp }} />
              <View style={{ flex: 1, justifyContent:'center', alignItems:'center' }}>
                <Text style={styles.fonts.blueTextMedium}>Walmart Stamp</Text>
              </View>
            </View>
          }
        </ScrollView>
      </View>
    );
  }

  getStepLabel = () => {
    if (nextStep) {
      return nextStep.title;
    } else {
      return "";
    }
  }

  getTotalAssemled = () => {
    const { invoiceData } = this.props;
    const currentInvoice = invoiceData.currentInvoice;
    let count = 0;
    currentInvoice.invoiceproduct.forEach( function(element) {
      count += element.qty;
    });

    return count;
  }

  getOnPress = () => {

    switch(nextStep.key) {
      case'reqKeyRec':
        return this.getKeyRec;
      case 'reqPoNumber':
        return this.getPOCode;
      case 'reqInvoiceStamp':
        return this.getStamp;
      case 'reqWeeklyCount':
        break;
      default:
        break;
    }
  }

  getPOCode = () => {
    this.props.navigation.navigate('GetPOCode', { currentInvoice: this.props.invoiceData.currentInvoice });
  }

  getKeyRec = () => {
    this.props.navigation.navigate('GetKeyRec', { currentInvoice: this.props.invoiceData.currentInvoice });
  }

  getStamp = () => {
    this.props.navigation.navigate('Camera', { cameraType: constants.cameraTypes.photo, photoCallback: this.getStampCallback });
  }

  decreaseQty = (item) => {
    if (item.qty <= 1) {
      item.qty = 0;
    } else {
      item.qty -= 1;
    }
    this.props.decreaseInvoiceItemQuantity(item, this.props.userData.token);
  }

  getStampCallback = (imagePath) => {
    var token = this.props.userData.token;
    var invoice = this.props.invoiceData.currentInvoice;
    var key = this.props.navigation.state.key;

    this.props.uploadStampImage({token, invoice, imagePath}, (i) => {
      const backAction = NavigationActions.back({
        key: key,
        params:{ origin: 'InvoiceHistory', index: this.props.invoiceData.invoices.indexOf(invoice) + 1}
      });

      this.props.navigation.dispatch(backAction);
    });
  }

  renderRow = (item) => {
    return(
      <InvoiceDetailLineItem
        name={item.name}
        sku={item.sku}
        qty={item.qty}
        aiCode = {item.ai_code}
        priceEach={numeral(item.pricePerUnit).format('$0.00')}
        priceTotal={numeral(item.pricePerUnit * item.qty).format('$0.00')}
        isEditable={this.state.editable}
        decreaseQty={this.decreaseQty}
        item={item}
        entryType={item.entryType}
        requestType={(item.request && item.request.requesttype) ? item.request.requesttype.request : null}
      />
    )
  }

  toSignature = () => {
    this.props.navigation.navigate('PrintName');
  }
}

const mapStateToProps = ({ invoiceReducer, userReducer }) => {
  return { invoiceData: invoiceReducer, userData: userReducer };
}

export default connect(mapStateToProps, { uploadStampImage, decreaseInvoiceItemQuantity })(InvoiceDetailView);
