import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  Image,
  ScrollView
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import numeral from 'numeral';
import { connect } from 'react-redux';
import { findManagerStore, getTodaysInvoices, setCurrentInvoice, clockOutUser, completeInvoice, enterClockInFlow } from '../../../redux/actions';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { DrawerPageHeader } from '../../_common/drawer-nav';
import InvoiceLineItem from './InvoiceLineItem';
import Button from '../Button';
import Card from '../CardComponent';
import ManagerLocationErrorComponent from '../../manager-common/ManagerLocationErrorComponent';
import chatIcon from '../../../assets/chatIcon@3x.png';
import styles from '../../../styles';
import constants from '../../../constants.json';
import utils from '../../../utils';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class StartInvoiceView extends Component {

  state = {
    toClockIn: false
  }

  componentDidMount() {
    const { invoiceData, userData } = this.props;
    if(userData.timesheet && userData.timesheet.locationId) {
      this.getTodaysInvoices();
    }
  }

  getTodaysInvoices = () => {
    const { userData, getTodaysInvoices } = this.props;
    console.log(userData);
    getTodaysInvoices(
      {
        'userId': userData.userId,
        'clientId': userData.timesheet.location.clientId,
        'locationId': userData.timesheet.locationId
      },
      userData.token
    );
  }

  render() {
    const { invoiceData, userData, getTodaysInvoices, findManagerStore } = this.props;
    const clientAttributes = utils.clientAttributeUtil.getClientAttributes(invoiceData.invoices);
    const isCompletedInvoices = utils.clientAttributeUtil.isCompletedInvoices(invoiceData.invoices, clientAttributes);
    const hasKeyRecOrPOAttr = utils.clientAttributeUtil.hasKeyRecOrPOAttr(clientAttributes);

    if(this.state.toClockIn) {
      this.props.navigation.navigate('ClockIn');
      this.setState({ toClockIn: false })
    }

    if((userData.roleId == constants.roleIds.areaManager || userData.roleId == constants.roleIds.fieldops
      || userData.roleId == constants.roleIds.contractor) && userData.timesheet == null) {
      return(
        <View style={{ flex: 1 }}>
          <DrawerPageHeader navigation={this.props.navigation} />
          <ManagerLocationErrorComponent
            label={"My Open Invoices"}
            retry={() => findManagerStore(userData.token, this.getTodaysInvoices, null)} />
        </View>
      );
    }

    if(invoiceData.error) {
      return (
        <View style={{ flex: 1 }}>
          <ErrorScreen
              errorText={constants.errorMessages.todaysInvoiceError}
              actionLabel={'REFRESH'}
              action={this.getTodaysInvoices}
            />
        </View>
      )
    }

    return(
      <View style= {{ flex: 1, justifyContent: 'space-between', backgroundColor: styles.palette.white }}>

        {(userData.roleId == constants.roleIds.tech || userData.roleId == constants.roleIds.subcontractor)
          ?
            <View style={{height: 70, backgroundColor: styles.palette.white, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between', paddingLeft: 20, paddingRight: 20, paddingTop: 20, paddingBottom: 5 }}>
              <TouchableOpacity
                onPress={ () => this.props.navigation.navigate('DrawerOpen') }>
                <Icon name="menu" size={30} color={styles.palette.mediumBlue} />
              </TouchableOpacity>
              {(hasKeyRecOrPOAttr && !isCompletedInvoices) &&
                <TouchableOpacity
                onPress={() => this.props.navigation.navigate('CantGetFinalized')}
                style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={[{ marginRight: 10 }, styles.fonts.smallLink]}>Can{"\'"}t get invoice finalized</Text>
                  <Image source={chatIcon} style={{ width: 25, height: 25 }} />
                </TouchableOpacity>
              }
            </View>
          : <DrawerPageHeader navigation={this.props.navigation} />
        }
        <View style={{ margin: 20 }}>
          <Text style={[styles.fonts.headerBlueGrey, { marginBottom: 10 }]}>My Open Invoices</Text>
          {clientAttributes.map((attr, index) => {

              if (attr.isCompleted) {
                return (<View key={attr.key} style={{ flexDirection: 'row', paddingTop: 5, paddingBottom: 5 }}>
                          <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 10, justifyContent: 'center', height: 35 }}>
                            <Icon style={{ height: 30, width: 30, borderWidth: 2, borderColor: styles.palette.completedGray, borderRadius: 15 }} name={'circle'} size={25} color={styles.palette.white} ></Icon>
                            <Icon style={{ backgroundColor: 'transparent', marginLeft: -30, paddingLeft: 0, width: 30, textAlign: 'center', color: styles.palette.completedGray}} name={'check'} size={20} color={styles.palette.completedGray} ></Icon>
                          </View>
                          <Text style={[styles.fonts.body1,  { lineHeight: 35, color: styles.palette.completedGray }]}>{attr.text}</Text>
                        </View>
                );

              } else {
                return (<View key={attr.key} style={{ flexDirection: 'row', paddingTop: 5, paddingBottom: 5  }}>
                          <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 10, justifyContent: 'center', height: 35 }}>
                            <Icon style={{ width: 30, borderWidth: 2, borderColor: styles.palette.mediumBlue, borderRadius: 15.5 }} name={'circle'} size={25} color={styles.palette.white} ></Icon>
                            <Text style={{ backgroundColor: 'transparent', marginLeft: -30, paddingLeft: 0, width: 30, textAlign: 'center', color: styles.palette.mediumBlue}}>{index + 1}</Text>
                          </View>
                          <Text style={[styles.fonts.body1,  { lineHeight: 35 }]}>{attr.text}</Text>
                        </View>
                );
              }
            })
          }
        </View>

        <ScrollView>

          {(invoiceData && invoiceData.invoices && invoiceData.invoices.length > 0)
            ? <ListView
              dataSource={ds.cloneWithRows(invoiceData.invoices)}
              renderRow={this.renderRow}
              enableEmptySections={true} />
            : <Text style={[styles.fonts.body2, { padding: 20 }]}>You do not have any unsubmitted invoices at this location.</Text>
          }
        </ScrollView>

        {(invoiceData && invoiceData.invoices && invoiceData.invoices.length > 0 && isCompletedInvoices)
          ? <Card direction={'top'}>
              <View
                style={{ padding: 20, alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', backgroundColor: styles.palette.paleGray }}>
                <View style={{ flex: 1, marginRight: 20 }}>
                  <Text style={styles.fonts.body1}>Total of {invoiceData.invoices.length} invoices</Text>
                  <Text style={styles.fonts.body1}>{numeral(this.getInvoiceTotal()).format('$0.00')}</Text>
                </View>
                  <View style={{ flex: 1 }}>
                    <Button label={'SUBMIT'} onPress={this.submitCompletedInvoice} color={styles.palette.mediumBlue} />
                  </View>
              </View>
            </Card>
          : <Card direction={'top'}>
              <View
                style={{ padding: 20, alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', backgroundColor: styles.palette.paleGray }}>
                <View style={{ flex: 1, marginRight: 20 }}>
                  <Text style={styles.fonts.body1}>Total of {this.props.invoiceData.invoices.length} invoices</Text>
                  <Text style={styles.fonts.body1}>{numeral(this.getInvoiceTotal()).format('$0.00')}</Text>
                </View>
                {(invoiceData && invoiceData.invoices.length > 0 && !invoiceData.invoices[0].signatureImageUrl) &&
                  <View style={{ flex: 1 }}>
                    <Button label={'SIGN'} onPress={this.toPrintName} color={styles.palette.mediumBlue} />
                  </View>
                }
              </View>
            </Card>
        }
      </View>

    );
  }

  getInvoiceTotal = () => {
    var total = 0;

    for(var i = 0; i < this.props.invoiceData.invoices.length; i ++) {
      total += this.props.invoiceData.invoices[i].total;
    }

    //TODO: Should we really being formatting values on the client?
    return total.toFixed(2);
  }

  renderRow = (item) => {

    var nextStep = utils.clientAttributeUtil.getNextStep(item);

    return(
      <InvoiceLineItem
        index={this.props.invoiceData.invoices.indexOf(item) + 1}
        total={item.total}
        itemCount={item.invoiceproduct.length}
        invoiceType={""}
        toInvoiceDetail={() => this.toInvoiceDetail(item)}
        actionText={ (nextStep) ? nextStep.text : null }
        actionTitle={ (nextStep) ? nextStep.title : null }
        isCompleted={ nextStep ? false : true }
        hideButton={ (nextStep && !nextStep.title) ? true : false }
        hasSignature={ item.signatureImageUrl }
        date={utils.dateTime.getFormattedDate(item.invoiceGeneratedTime)}
        />
    );
  }

  toInvoiceDetail = (invoice) => {
    this.props.setCurrentInvoice(invoice, this.props.invoiceData.invoices.indexOf(invoice) + 1);
    this.props.navigation.navigate('InvoiceDetail', { invoiceData: this.props.invoiceData, currentInvoice: invoice, index: this.props.invoiceData.invoices.indexOf(invoice) + 1 });
  }

  toPrintName = () => {
    this.props.navigation.navigate('PrintName', { invoiceData: this.props.invoiceData });
  }

  toClockOut = () => {
    this.submitCompletedInvoice('toClockOut');
  }

  toOtherStore = () => {
    this.submitCompletedInvoice('toOtherStore');
  }

  submitCompletedInvoice = () => {
    console.log("submitCompletedInvoice");
    const { userData, completeInvoice, invoiceData } = this.props;

    var invoices = invoiceData.invoices;
    var token = userData.token;

    for(var i = 0; i < invoices.length; i++){
      invoices[i].invoiceCompletedTime = Date.now();
    }

    completeInvoice({invoices, token}, () => {
      if(userData.roleId == constants.roleIds.tech) {
        this.props.navigation.navigate('SubmitInvoice', { toClockIn: this.navToClockIn });
      }
    });
  }

  navToClockIn = () => {
    console.log('navToClockIn');
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'StartInvoice' }),
      ]
    })
    this.props.navigation.dispatch(resetAction);

    this.setState({ toClockIn: true });
  }
}

const mapStateToProps = ({ invoiceReducer, userReducer }) => {
  return { invoiceData: invoiceReducer, userData: userReducer };
};

export default connect(mapStateToProps, { findManagerStore, getTodaysInvoices, setCurrentInvoice, completeInvoice, clockOutUser, enterClockInFlow })(StartInvoiceView);
