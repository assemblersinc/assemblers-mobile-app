import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';

import styles from '../../../styles';
import decreaseIcon from '../../../assets/minusQty@3x.png';

const InvoiceDetailLineItem = ({ name, sku, qty,aiCode, priceEach, priceTotal, isEditable, decreaseQty, item,entryType,requestType }) => {
  return(
    <View style={{ paddingTop: 30, paddingBottom: 30, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>
      <View style={{ flexDirection: 'row' }}>
        <View style={{ flex: 3, justifyContent: 'space-between' }}>
          <Text style={styles.fonts.boldItemName}>{name}</Text>
          {(sku)
            ? <Text style={styles.fonts.body1}>SKU: {sku}</Text>
            : null
          }
          {(aiCode)
            ? <Text style={styles.fonts.body1}>AI CODE: {aiCode}</Text>
            : null
          }
          {(entryType)
            ? <Text style={styles.fonts.body1}>TYPE: '{entryType}' </Text>
            : null
          }
        </View>
        {(isEditable)
          ? <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity onPress={() => decreaseQty(item)}>
                <Image source={decreaseIcon} style={{ width: 30, height: 30 }}/>
              </TouchableOpacity>
              <Text style={[styles.fonts.qty, {marginLeft: 10, marginTop: 3}] }>{qty}</Text>
            </View>
          : <Text style={[ styles.fonts.qty, {textAlign: 'right'}]}>QTY {qty}</Text>
        }
      </View>

      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
        {(requestType)
          ? <Text style={[ styles.fonts.smallLink, { marginTop: 10 }]}>{requestType}</Text>
          : <View />
        }
        <View style={{ alignItems: 'flex-end', marginTop: 20 }}>
          <Text style={[styles.fonts.body1, { marginBottom: 5 }]}>each {priceEach}</Text>
          <Text style={styles.fonts.body3Bold}>total {priceTotal}</Text>
        </View>
      </View>
    </View>
  );
}

export default InvoiceDetailLineItem;
