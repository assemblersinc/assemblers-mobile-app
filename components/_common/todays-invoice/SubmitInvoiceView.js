import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Text,
  Platform
} from 'react-native';
import {
  MKProgress,
  MKSpinner,
} from 'react-native-material-kit';
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux';
import { setCurrentInvoice, clockOutUser, completeInvoice, enterClockInFlow } from '../../../redux/actions';
import styles from '../../../styles';
import utils from '../../../utils';
import { Button } from '../../_common';

class SubmitInvoiceView extends Component {

  render() {

      return(
        <View style={{ flex: 1, padding: 40, justifyContent: 'center', alignItems: 'center', backgroundColor: styles.palette.white }}>

          <View style={{ marginTop: 10, marginBottom: 60, alignItems: 'center' }}>
            <Text style={{ textAlign: 'center' }, styles.fonts.body2}>You have submitted all open invoices for this location.</Text>
          </View>

          <View style={{ marginBottom: 20, height: 50, flexDirection: 'row' }}>
            <Button label={'TRAVELING TO OTHER STORE'} onPress={this.toOtherStore} color={styles.palette.mediumBlue} />
          </View>

          <View style={{ height: 50, flexDirection: 'row' }}>
            <Button label={'CLOCK OUT'} onPress={this.toClockOut} color={styles.palette.mediumGrey} />
          </View>

        </View>
      );
    }

    toClockOut = () => {
      const { userData, clockOutUser } = this.props;
      clockOutUser(userData.userId, userData.timesheet.timeId, userData.token, this.clockOutCallback);
    }

    clockOutCallback = () => {
      const navigateAction = NavigationActions.navigate({
        index: 0,
        action: [
          NavigationActions.back({ key: null }),
          NavigationActions.navigate({ routeName: 'Announcements'})
        ]
      })
      this.props.navigation.dispatch(navigateAction);
    }

    toOtherStore = () => {
      console.log('toOtherStore');
      const { params } = this.props.navigation.state;
      this.props.enterClockInFlow('invoicing', () => params.toClockIn());
    }

}

const mapStateToProps = ({ invoiceReducer, userReducer }) => {
  return { invoiceData: invoiceReducer, userData: userReducer };
};

export default connect(mapStateToProps, { completeInvoice, clockOutUser, enterClockInFlow })(SubmitInvoiceView);
