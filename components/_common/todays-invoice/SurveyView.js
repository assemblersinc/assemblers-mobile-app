import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { answerSurveyQuestion, clearSurveyAnswers, getSurveyQuestions } from '../../../redux/actions';
import BasicPageHeader from '../drawer-nav/BasicPageHeader';
import { FloatingActionButton, ErrorScreen } from '../';
import SurveyQuestion from './SurveyQuestion';
import Button from '../Button';
import styles from '../../../styles';
import constants from '../../../constants.json';

class SurveyView extends Component {

  state = {
    currentQuestionIndex: 0
  }

  componentWillMount() {

    const { userData, getSurveyQuestions } = this.props;
    getSurveyQuestions(userData.token);

  }

  componentDidMount() {
    const { surveyData, userData, getSurveyQuestions } = this.props;

    if(surveyData.questions.length == 0) {
      getSurveyQuestions(userData.token);
    }
  }

  render() {
    const { invoiceData, userData, surveyData, navigation, getSurveyQuestions } = this.props;

    if(surveyData && surveyData.questions && surveyData.questions.length > this.state.currentQuestionIndex) {
      return(
        <View style= {{ flex: 1, backgroundColor: styles.palette.white }}>
          <View style={{ height: 70, backgroundColor: styles.palette.white, flexDirection: 'row', alignItems: 'center', paddingTop: 30 }}>
            <TouchableOpacity>
              <Icon name="chevron-left" size={30}  color={styles.palette.mediumBlue} style={{ paddingLeft: 20 }} onPress={this.goBackFunction} />
            </TouchableOpacity>
          </View>

          <SurveyQuestion
            index={this.state.currentQuestionIndex}
            totalQuestions={surveyData.questions.length}
            questionText={surveyData.questions[this.state.currentQuestionIndex].question}
            questionType={surveyData.questions[this.state.currentQuestionIndex].answerType}
            rating={surveyData.questions[this.state.currentQuestionIndex].answer}
            onStarSelection={(index, rating) => this.answerSurveyQuestion(index, rating, surveyData.questions.length)}
           />
        </View>
      );
    }

    return(
      <View style={{ flex: 1, backgroundColor: styles.palette.white }}>
        <BasicPageHeader navigation={this.props.navigation} />
        <ErrorScreen
            errorText={constants.errorMessages.surveyError}
            actionLabel={'TRY AGAIN'}
            action={() => getSurveyQuestions(userData.token)}/>
      </View>
    )

  }

  goBackFunction = () => {
    console.log('goBackFunction', this.state.currentQuestionIndex);
    if(this.state.currentQuestionIndex > 0) {
      this.setState({ currentQuestionIndex: --this.state.currentQuestionIndex })
    } else {
      this.props.clearSurveyAnswers();
      this.props.navigation.goBack();
    }
  }

  answerSurveyQuestion = (index, rating, totalQuestions) => {
    this.props.answerSurveyQuestion(index, rating);
    var that = this;
    setTimeout(function() {
      if(index == (--totalQuestions)) {
        //TODO: capture survey data and keep track of current invoice
        const { params } = that.props.navigation.state;
        that.props.navigation.navigate('Signature');
        that.setState({ currentQuestionIndex: 0 })
      } else {
        that.setState({ currentQuestionIndex: ++that.state.currentQuestionIndex })
      }
    }, 300);
  }

}

const mapStateToProps = ({ surveyReducer, invoiceReducer, userReducer }) => {
  return { surveyData: surveyReducer, invoiceData: invoiceReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getSurveyQuestions, answerSurveyQuestion, clearSurveyAnswers })(SurveyView);
