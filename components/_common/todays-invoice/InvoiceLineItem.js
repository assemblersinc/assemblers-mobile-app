import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Button from '../Button';
import Card from '../CardComponent';
import styles from '../../../styles';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import numeral from 'numeral';

const InvoiceLineItem = ({ index, total, itemCount, invoiceType, toInvoiceDetail, actionText, actionTitle, isCompleted, hideButton, hasSignature, date }) => {
  return(
    <Card direction={'center'}>
      <TouchableOpacity
        style={{ padding: 20, backgroundColor: styles.palette.white }}
        onPress={toInvoiceDetail}>
        <View style={{ alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row' }}>
          <View style={{ flex: 1 }}>
            <Text style={styles.fonts.mediumLink}>Invoice #{index}</Text>
            <Text style={[styles.fonts.sectionHeader, { marginTop: 5, marginBottom: 10 }]}>{numeral(total).format('$0.00')}</Text>
            <Text style={[styles.fonts.body2, { marginBottom: 5 }]}>Started on {date}</Text>
            {(hasSignature)
              ? <Text style={styles.fonts.body2}>{ actionText }</Text>
              : null
            }
          </View>
          {(hasSignature && actionTitle && !isCompleted && !hideButton) &&
            <View style={{ flex: 1, height: 50 }}>
              <Button label={ actionTitle } onPress={toInvoiceDetail} color={styles.palette.mediumBlue} />
            </View>
          }
          {(isCompleted) &&
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 5, justifyContent: 'center', height: 35 }}>
                <Icon style={{ width: 30, borderWidth: 2, borderColor: styles.palette.completedGray, borderRadius: 15.5 }} name={'circle'} size={25} color={styles.palette.white} ></Icon>
                <Icon style={{ backgroundColor: 'transparent', marginLeft: -30, paddingLeft: 0, width: 30, textAlign: 'center', color: styles.palette.completedGray}} name={'check'} size={20} color={styles.palette.completedGray} ></Icon>
              </View>
              <Text style={[styles.fonts.body1,  { lineHeight: 35, color: styles.palette.completedGray }]}>{'Submitted'}</Text>
            </View>
          }
        </View>
      </TouchableOpacity>
    </Card>
  );
}

export default InvoiceLineItem;
