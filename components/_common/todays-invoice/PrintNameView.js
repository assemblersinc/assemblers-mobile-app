import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FloatingActionButton from '../FloatingActionButton';
import styles from '../../../styles';
import { addSurveyPrintedName } from '../../../redux/actions';

class PrintNameView extends Component {

  state = {
    text: ""
  }

  render() {

    return(
      <TouchableWithoutFeedback style={{ flex: 1 }} onPress={Keyboard.dismiss}>
        <View style={{ flex: 1, padding: 20, backgroundColor: styles.palette.white }}>
          <KeyboardAwareScrollView>
              <Text style={[{ marginBottom: 50, paddingTop: 50 }, styles.fonts.h2]}>Print your Name</Text>

              <View style={{ borderBottomWidth: 1, borderBottomColor: styles.palette.black, marginBottom: 100 }}>
                <TextInput
                  placeholder={'Name'}
                  style={[{ height: 50, backgroundColor: styles.palette.white, width: '100%' }, styles.fonts.formInput]}
                  underlineColorAndroid='transparent'
                  autoCapitalize={'words'}
                  onChangeText={(text) => this.setState({text})}
                  value={this.state.text}
                />
              </View>

          </KeyboardAwareScrollView>

          {(this.state.text && this.state.text.length > 1)
            ? <FloatingActionButton color={styles.palette.white} backgroundColor={styles.palette.mediumBlue} onPress={this.toSurvey} />
            : null
          }

        </View>
      </TouchableWithoutFeedback>


    );
  }

  toSurvey = () => {
    this.props.addSurveyPrintedName(this.state.text);
    this.props.navigation.navigate('Survey');
  }

}

const mapStateToProps = ({ surveyReducer }) => {
  return { surveyDate: surveyReducer };
}

export default connect(mapStateToProps, { addSurveyPrintedName })(PrintNameView);
