import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import {
  MKRadioButton,
  mdl,
} from 'react-native-material-kit';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import StarRating from 'react-native-star-rating';
import RadioButtonLineItem from '../RadioButtonLineItem';

import styles from '../../../styles'
const radioGroup = new MKRadioButton.Group();

const SurveyQuestion = ({ index, totalQuestions, questionText, questionType, rating, onStarSelection }) => {
  return(
    <View style={{ flex: 1, backgroundColor: styles.palette.white, justifyContent: 'space-between' }}>

      <View style={{ flex: 1, margin: 20 }} >
        <View style={{ paddingBottom: 20, borderBottomWidth: 1, borderBottomColor: styles.palette.paleGray }}>
          <Text style={[{ marginBottom: 10 }, styles.fonts.itemNoLink]}>{index + 1} of {totalQuestions}</Text>
          <Text style={styles.fonts.itemNoLink}>{questionText}</Text>
        </View>

        <View style={{ marginTop: 100 }}>
          {(questionType == 'stars')
            ?
              <StarRating
                disabled={false}
                maxStars={5}
                rating={rating}
                selectedStar={(rating) => onStarSelection(index, rating)}
                emptyStar={'ios-star-outline'}
                fullStar={'ios-star'}
                iconSet={'Ionicons'}
                starColor={styles.palette.mediumBlue}
              />

            : (questionType == 'boolean')
              ? <View>
                  <RadioButtonLineItem
                    label={'Yes'}
                    radioGroup={radioGroup}
                    onCheckedChange={(e) => {if(e.checked) onStarSelection(index, true)}} />
                  <RadioButtonLineItem
                    label={'No'}
                    radioGroup={radioGroup}
                    onCheckedChange={(e) => {if(e.checked) onStarSelection(index, false)}} />
                </View>
              : <Text style={styles.fonts.itemNoLink}>Invalid question type.</Text>
          }
        </View>

      </View>
    </View>
  );
}

export default SurveyQuestion;
