import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Platform
} from 'react-native';
import SignatureCapture from 'react-native-signature-capture';
import { connect } from 'react-redux';
import { Button } from '../';
import { saveSurveyWithSignature } from '../../../redux/actions';
import styles from '../../../styles';

const heightOffset = Platform.OS === 'ios'? 64 : 54;
const bottomBarHeight = 100;
const instructionHeight = 70;
const signatureHeight = Dimensions.get('window').height - heightOffset - bottomBarHeight - instructionHeight;
const signatureWidth = Dimensions.get('window').width;

class SignatureView extends Component {

  render() {
    const { invoiceData, userData, surveyData } = this.props;
    console.log(invoiceData);

    return(
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', backgroundColor: styles.palette.white }}>
        <Text style={[styles.fonts.h2, {height: instructionHeight, padding: 20}]}>Sign your Name</Text>
        <SignatureCapture
            style={{ height: signatureHeight, width: signatureWidth }}
            ref="sign"
            onSaveEvent={this.onSaveEvent}
            onDragEvent={this.onDragEvent}
            saveImageFileInExtStorage={false}
            showNativeButtons={false}
            showTitleLabel={true}
            viewMode={"portrait"}/>

        <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: styles.palette.white, height: bottomBarHeight, marginBottom: 10 }}>
          <Button
            onPress={this.saveSign}
            color={styles.palette.mediumBlue}
            label={'SAVE'} />
          <Button
            onPress={this.resetSign}
            color={styles.palette.mediumBlue}
            label={'RESET'} />
        </View>

      </View>
    );
  }

  saveSign = () => {
    this.refs["sign"].saveImage();
  }

  resetSign = () => {
      this.refs["sign"].resetImage();
  }

  onSaveEvent = (result) => {
      //result.encoded - for the base64 encoded png
      //result.pathName - for the file path name
      var survey = this.getSurveyAnswers()
      var fileName = result.pathName.split('.');
      var fileType = fileName[fileName.length - 1];
      var token = this.props.userData.token;

      var data = {
        "invoices": survey.invoices,
        "survey": survey.answers,
        "signature": {
          "data": result.encoded,
          "type": fileType
        }
      }

      this.props.saveSurveyWithSignature({ data, token }, this.toThankYou);
  }

  toThankYou = () => {
    this.props.navigation.navigate('ThankYou');
  }

  getSurveyAnswers = () => {

    var surveyAnswers = [];
    var invoices = [];

    for (var i = 0; i < this.props.invoiceData.invoices.length; i ++) {

      var invoice = this.props.invoiceData.invoices[i];

      invoices.push(invoice.invoiceId);

      for (var s = 0; s < this.props.surveyData.questions.length; s++) {

        var question = this.props.surveyData.questions[s];

        surveyAnswers.push({
          'userId': invoice.userId,
          'locationId': invoice.locationId,
          'invoiceId': invoice.invoiceId,
          'questionId': question.questionId,
          'response': this.getResponse(question.answer),
          'enteredByName': this.props.surveyData.name
        });
      }
    }

    return {
      'answers' : surveyAnswers,
      'invoices' : invoices
    };

  }

  getResponse = (response) => {

    if (response === true || response === false) {
      return true ? 1 : 0;
    } else {
      return response;
    }

  }

  onDragEvent = () => {
       // This callback will be called when the user enters signature
      console.log("dragged");
    }
}

const mapStateToProps = ({ surveyReducer, invoiceReducer, userReducer }) => {
  return { surveyData: surveyReducer, invoiceData: invoiceReducer, userData: userReducer };
}

export default connect(mapStateToProps, { saveSurveyWithSignature })(SignatureView);
