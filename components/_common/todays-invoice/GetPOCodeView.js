import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import { invoiceAddPO } from '../../../redux/actions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import FloatingActionButton from '../FloatingActionButton'
import styles from '../../../styles';

class GetPOCodeView extends Component {

  componentWillMount() {
    const { params } = this.props.navigation.state;
    this.currentInvoice = params.currentInvoice;
    this.state = { text: '' }
  }

  render() {

    const { userData } = this.props;

    return(
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={{ flex: 1, padding: 20, paddingBottom: 0, backgroundColor: styles.palette.mediumBlue}}>
          <KeyboardAwareScrollView>
            <View style={{ marginTop: 40, marginBottom: 20 }}>
              <Text style={styles.fonts.h2White}>Invoice</Text>
              <Text style={styles.fonts.h2White}>Enter PO code</Text>
            </View>

            <TextInput
              placeholder={'PO Code'}
              style={{ width: '100%', height: 60, paddingLeft: 20, backgroundColor: styles.palette.white }}
              underlineColorAndroid='transparent'
              onChangeText={(text) => this.setState({text})}
              autoFocus={true}
              value={this.state.text}
              keyboardType={'numeric'}
              />
          </KeyboardAwareScrollView>

          <FloatingActionButton color={styles.palette.mediumBlue} backgroundColor={styles.palette.white} onPress={this.submitPOCode} />
        </View>
      </TouchableWithoutFeedback>
    )
  }

  submitPOCode = () => {
    var invoice = this.currentInvoice;
    var token = this.props.userData.token;

    invoice.poNumber = this.state.text;

    this.props.invoiceAddPO({invoice, token}, this.goBack);

  }

  goBack = () => {
    this.props.navigation.goBack();
  }
}

const mapStateToProps = ({ invoiceReducer, userReducer }) => {
  return { invoiceData: invoiceReducer, userData: userReducer };
}

export default connect(mapStateToProps, { invoiceAddPO })(GetPOCodeView);
