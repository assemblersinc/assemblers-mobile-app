import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';
import { NavigationActions } from 'react-navigation'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Button from '../Button.js';
import styles from '../../../styles';
import logo from '../../../assets/aiLogo@3x.png';

export default class ThankYouView extends Component {

  render() {

    return(
      <View style= {{ flex: 1, justifyContent: 'space-between', alignItems: 'center', backgroundColor: styles.palette.white, paddingBottom: 100 }}>
        <View style={{ width: '100%', height: 70, backgroundColor: styles.palette.white, flexDirection: 'row', alignItems: 'center', paddingTop: 30  }}>
          <TouchableOpacity>
            <Icon name="close" size={30}  color={styles.palette.mediumBlue} style={{ paddingLeft: 10 }} onPress={this.finishInvoice} />
          </TouchableOpacity>
        </View>

          <Text style={styles.fonts.bold28}>Thanks!</Text>

          <Text style={styles.fonts.body2}>Signed invoice received.</Text>

          <Image source={logo} style={{ width: 250, height: 250 }} />

      </View>

    );
  }

  finishInvoice = () => {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'StartInvoice' })
      ]
    })
    this.props.navigation.dispatch(resetAction);
  }

}
