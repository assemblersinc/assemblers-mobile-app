import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { invoiceAddKeyRec } from '../../../redux/actions';

import FloatingActionButton from '../FloatingActionButton'
import styles from '../../../styles';

class GetKeyRecView extends Component {

  componentWillMount() {
    const { params } = this.props.navigation.state;
    this.currentInvoice = params.currentInvoice;
    this.state = { text: '' }
  }

  render() {

    const { userData } = this.props;

    return(
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={{ flex: 1, padding: 20, backgroundColor: styles.palette.mediumBlue}}>
          <KeyboardAwareScrollView>
            <View style={{ marginTop: 40, marginBottom: 20 }}>
              <Text style={styles.fonts.h2White}>Invoice</Text>
              <Text style={styles.fonts.h2White}>Enter key rec</Text>
            </View>

            <TextInput
              placeholder={'Key Rec'}
              style={{ width: '100%', height: 60, paddingLeft: 20, backgroundColor: styles.palette.white }}
              underlineColorAndroid='transparent'
              onChangeText={(text) => this.setState({text})}
              autoFocus={true}
              value={this.state.text}
              />
          </KeyboardAwareScrollView>

          <FloatingActionButton color={styles.palette.mediumBlue} backgroundColor={styles.palette.white} onPress={this.submitKeyRec} />
        </View>
      </TouchableWithoutFeedback>
    )
  }

  submitKeyRec = () => {
    var invoice = this.currentInvoice;
    var token = this.props.userData.token;

    invoice.keyRecNumber = this.state.text;
    invoice.keyRecTime = Date.now();

    this.props.invoiceAddKeyRec({invoice, token}, this.goBack);
  }

  goBack = (invoice) => {
    this.props.navigation.goBack();
  }
}

const mapStateToProps = ({ invoiceReducer, userReducer }) => {

  return { invoiceData: invoiceReducer, userData: userReducer };
}

export default connect(mapStateToProps, { invoiceAddKeyRec })(GetKeyRecView);