import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from '../../styles';
import Button from './Button';

const ServiceSKUBottomBar = ({ enterService, enterSku } )=> {
  return(
    <View style={{ flexDirection: 'row', alignItems: 'center', height: 100, paddingBottom: 10, backgroundColor: styles.palette.white }}>
      <Button onPress={enterService} label={'ADD SERVICE'} color={styles.palette.mediumBlue} />
      <Button onPress={enterSku} label={'ENTER SKU'} color={styles.palette.mediumBlue} />
    </View>
  );
}

export default ServiceSKUBottomBar;
