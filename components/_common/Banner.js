import React, { Component } from 'react';
import {
  TouchableOpacity,
  Text,
  View
} from 'react-native';

import styles from '../../styles';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Banner = ({ isVisible, text, color, textColor, close }) => {

  if(isVisible) {
    return(
      <View style={{ backgroundColor: color, padding: 20, paddingTop: 30, height: 70, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
        <Text style={textColor}>{text}</Text>
        <TouchableOpacity
          onPress={close}
          style={{ marginLeft: 10}}>
          <Icon name={'close'} size={30} color={styles.palette.white} />
        </TouchableOpacity>
      </View>
    );
  } else return null;

}

export default Banner;
