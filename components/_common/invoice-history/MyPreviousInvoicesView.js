import React, { Component } from 'react';
import {
  View,
  Text,
  ListView,
  ScrollView
} from 'react-native';
import numeral from 'numeral';
import { connect } from 'react-redux';
import { getMyInvoiceHistory, setCurrentInvoice } from '../../../redux/actions';
import InvoiceHistoryLineItem from './InvoiceHistoryLineItem';
import { SectionHeader, ErrorScreen } from '../';

import styles from '../../../styles';
import utils from '../../../utils';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
var query = { };

class MyPreviousInvoicesView extends Component {

  state = {
    submittedInvoices: [],
    unsubmittedInvoices: []
  }

  componentDidMount() {
    const { userData, getMyInvoiceHistory } = this.props;
    const { params } = this.props.navigation.state;
    if(params && params.tech) {
      query = { 'userId': params.tech.userId }
    } else {
      query = { 'userId': userData.userId }
    }
    getMyInvoiceHistory(query, userData.token, this.processInvoices);
  }

  processInvoices = () => {
    const { invoiceHistoryData, userData } = this.props;
    var unsubmittedInvoices = [];
    var submittedInvoices = [];
    invoiceHistoryData.invoices.forEach(function(element) {
      if(element.invoiceCompletedTime) {
        submittedInvoices.push(element);
      } else unsubmittedInvoices.push(element);
    });
    this.setState({ unsubmittedInvoices: unsubmittedInvoices, submittedInvoices: submittedInvoices });
  }

  render() {

    const { invoiceHistoryData, userData, getMyInvoiceHistory } = this.props;
    const { params } = this.props.navigation.state;

    return(
      <View style= {{ flex: 1, backgroundColor: styles.palette.white }}>
        <ScrollView>

          <Text style={[styles.fonts.headerBlueGrey, { margin: 20 }]}>Invoice History</Text>
          {(params && params.tech)
            ? <Text style={[{ margin: 20, marginTop: 0 }, styles.fonts.h2]}>{params.tech.firstName} {params.tech.lastName}</Text>
            : null
          }

          <SectionHeader header={'Unsubmitted Invoices'} badgeCount={this.state.unsubmittedInvoices.length}/>
          {(this.state.unsubmittedInvoices.length > 0)
            ? <ListView
              style={{ padding: 20, paddingBottom: 0 }}
              dataSource={ds.cloneWithRows(this.state.unsubmittedInvoices)}
              renderRow={this.renderRow}
            />
            : <Text style={[styles.fonts.body1, { padding: 20 }]}>You have no unsubmitted invoices.</Text>
          }

          <SectionHeader header={'Submitted Invoices'} />
          {(this.state.submittedInvoices.length > 0)
            ? <ListView
              style={{ padding: 20}}
              dataSource={ds.cloneWithRows(this.state.submittedInvoices)}
              renderRow={this.renderRow}
            />
            : <Text style={[styles.fonts.body1, { padding: 20 }]}>You have no recent submitted invoices.</Text>
          }

        </ScrollView>
      </View>
    );
  }

  renderRow = (item) => {
    if (item.user == null) {
      return null;
    }
    return(
      <InvoiceHistoryLineItem
        onPress={() => this.onRowPress(item)}
        missingStep={null}
        date={item.createdAt}
        invoiceId = {item.invoiceId}
        invoiceNumber={item.invoiceNumber}
        firstName={item.user.firstName}
        lastName={item.user.lastName}
        techId={item.user.techId}
        location={item.client.name}
        total={numeral(item.total).format('$0.00')}
        needsRec={this.needsRec(item)}
       />
    );
  }

  needsRec = (item) => {
    var rec = utils.clientAttributeUtil.getNextStep(item);
    if (rec) {
      return rec.text;
    }
    //Does not require warning (no key rec or completed)
    return '';
  }

  onRowPress = (invoice) => {
    this.props.setCurrentInvoice(invoice);
    this.props.navigation.navigate('InvoiceDetail', { origin: 'history' });
  }

}

const mapStateToProps = ({ invoiceHistoryReducer, userReducer }) => {
  return { invoiceHistoryData: invoiceHistoryReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getMyInvoiceHistory, setCurrentInvoice })(MyPreviousInvoicesView);
