import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import utils from '../../../utils';
import styles from '../../../styles';

const InvoiceHistoryLineItem = ({ onPress, missingStep, date, invoiceNumber,invoiceId, firstName, lastName, techId, location, total, needsRec }) => {
  return(
    <View style={{ paddingTop: 20, paddingBottom: 20, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>
      <TouchableOpacity
        onPress={onPress}>

        {(missingStep)
          ? <Text>Missing Step</Text>
          : null
        }

        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 3 }}>
            {(needsRec)
              ? <Text style={styles.fonts.orangeText}>{needsRec}</Text>
              : null
            }
            <Text style={[styles.fonts.mediumLink, { marginBottom: 5, marginTop: 5 }]}>{utils.dateTime.getFormattedDateWithYear(date)}</Text>
            <Text style={styles.fonts.body1}>Invoice #{invoiceNumber == 'N/A' ? invoiceId : invoiceNumber }</Text>
            <Text style={styles.fonts.body1}>{firstName} {lastName.slice(0, 1)} - TechId {techId}</Text>
            <Text style={styles.fonts.body1}>{location}</Text>
          </View>
          <View style={{ flex: 1, alignItems: 'flex-end' }}>
            <Text style={styles.fonts.body3Bold}>{total}</Text>
          </View>
        </View>

      </TouchableOpacity>
    </View>
  );
}

export default InvoiceHistoryLineItem;
