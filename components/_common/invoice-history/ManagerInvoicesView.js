import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import numeral from 'numeral';
import { connect } from 'react-redux';
import { getManagerInvoiceList, setCurrentInvoice } from '../../../redux/actions';
import { SectionHeader } from '../';
import InvoiceHistoryLineItem from './InvoiceHistoryLineItem';
import styles from '../../../styles';
import utils from '../../../utils';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class ManagerInvoicesView extends Component {

  state = {
    "selectedClient": "All Clients",
    "unsubmittedInvoicesMaster": [],
    "unsubmittedInvoicesFiltered": [],
    "submittedInvoicesMaster": [],
    "submittedInvoicesFiltered": []
  }

  componentWillMount() {
    const { userData, getManagerInvoiceList } = this.props;
    getManagerInvoiceList(userData.token, userData.areaId, this.processInvoices)
  }

  processInvoices = () => {
    const { invoiceHistoryData } = this.props;
    var submittedInvoices = [];
    var unsubmittedInvoices = [];

    if(invoiceHistoryData && invoiceHistoryData.managerList) {
      invoiceHistoryData.managerList.forEach(function(invoice) {
        if(invoice.invoiceCompletedTime) {
          submittedInvoices.push(invoice);
        } else {
          unsubmittedInvoices.push(invoice);
        }
      })
    }
    this.setState({ "unsubmittedInvoicesMaster": unsubmittedInvoices, "unsubmittedInvoicesFiltered": unsubmittedInvoices, "submittedInvoicesMaster": submittedInvoices, "submittedInvoicesFiltered": submittedInvoices });
  }

  render() {

    const { navigation, invoiceHistoryData } = this.props;

    return(
      <View style={{ flex: 1 }}>
        <View style={{ backgroundColor: styles.palette.paleGray, padding: 20, paddingTop: 30 }}>
          <View style={{height: 70, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
            <TouchableOpacity onPress={ () => navigation.navigate('DrawerOpen') }>
              <Icon name="menu" size={30} color={styles.palette.mediumBlue} />
            </TouchableOpacity>
            <Text style={[styles.fonts.headerBlueGrey, { marginLeft: 20 }]}>Invoices</Text>
          </View>

          <TouchableOpacity
            onPress={this.toClientList}
            style={{ backgroundColor: styles.palette.white, marginTop: 20, marginBottom: 10, padding: 10, flexDirection: 'row', alignItems: 'center' }}>
            <Icon name="account-multiple" size={30} color={styles.palette.mediumGray} />
            <Text style={[styles.fonts.formInput, { marginLeft: 10 }]}>{this.state.selectedClient}</Text>
          </TouchableOpacity>
        </View>
        <ScrollView style={{ backgroundColor: styles.palette.white, flex: 1 }}>

        <SectionHeader header={'Unsubmitted Invoices'} badgeCount={this.state.unsubmittedInvoicesFiltered.length}/>

          {(!invoiceHistoryData.managerList)
            ? <Text style={[styles.fonts.body1, { padding: 20 }]}>Could not get invoice information.</Text>
            : (this.state.unsubmittedInvoicesFiltered && this.state.unsubmittedInvoicesFiltered.length > 0)
              ? <ListView
                style={{ padding: 20 }}
                renderRow={this.renderRow}
                dataSource={ds.cloneWithRows(this.state.unsubmittedInvoicesFiltered)} />
              : <Text style={[styles.fonts.body1, { padding: 20 }]}>No unsubmitted invoices to show.</Text>
          }

          <SectionHeader header={'Submitted Invoices'} />

          {(!invoiceHistoryData.managerList)
            ? <Text style={[styles.fonts.body1, { padding: 20 }]}>Could not get invoice information.</Text>
            : (this.state.submittedInvoicesFiltered && this.state.submittedInvoicesFiltered.length > 0)
              ? <ListView
                style={{ padding: 20 }}
                renderRow={this.renderRow}
                dataSource={ds.cloneWithRows(this.state.submittedInvoicesFiltered)} />
              : <Text style={[styles.fonts.body1, { padding: 20 }]}>No submitted invoices to show.</Text>
          }

        </ScrollView>
      </View>
    );
  }

  toClientList = () => {
    this.props.navigation.navigate('ClientSelection', { onPressAction: this.filterList });
  }

  renderRow = (item) => {
    return(
      <InvoiceHistoryLineItem
        onPress={() => this.toInvoiceDetail(item)}
        date={item.invoiceGeneratedTime}
        invoiceNumber={item.invoiceNumber}
        firstName={item.user.firstName}
        lastName={item.user.lastName}
        techId={item.user.techId}
        invoiceId = {item.invoiceId}
        location={item.location.Location}
        total={numeral(item.total).format('$0.00')}
       />
    );
  }

  filterList = (client) => {
    var unsubmitted = [];
    var submitted = [];
    if(client == 'all') {
      this.setState({
        selectedClient: 'All Clients',
        submittedInvoicesFiltered: this.state.submittedInvoicesMaster,
        unsubmittedInvoicesFiltered: this.state.unsubmittedInvoicesMaster
      });
    } else if(client) {
      this.state.unsubmittedInvoicesMaster.forEach(function(invoice) {
        if(invoice.clientId == client.clientId) {
          unsubmitted.push(invoice);
        }
      });
      this.state.submittedInvoicesMaster.forEach(function(invoice) {
        if(invoice.clientId == client.clientId) {
          submitted.push(invoice);
        }
      });
      this.setState({
        unsubmittedInvoicesFiltered: unsubmitted,
        submittedInvoicesFiltered: submitted,
        selectedClient: client.name,
       });
    }
  };

  toInvoiceDetail = (invoice) => {
    this.props.setCurrentInvoice(invoice);
    this.props.navigation.navigate('InvoiceDetail', { origin: 'history' });
  }

}

const mapStateToProps = ({ invoiceHistoryReducer, userReducer }) => {
  return { invoiceHistoryData: invoiceHistoryReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getManagerInvoiceList, setCurrentInvoice })(ManagerInvoicesView);
