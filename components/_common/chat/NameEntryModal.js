import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  TouchableOpacity,
  Text
} from 'react-native';
import {
  MKTextField,
  mdl
} from 'react-native-material-kit';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Button from '../Button';
import styles from '../../../styles';

export default class NameEntryModal extends Component {

  state = {
    nameText: ''
  }

  render() {
    return(
      <Modal
        animationType={"fade"}
        transparent={true}
        visible={this.props.isVisible}
        onRequestClose={() => console.log('onRequestClose')}>
        <View style={styles.modal.container}>
          <View style={styles.modal.modal}>

            <Text>Enter your name for chat display.</Text>

            <MKTextField
              floatingLabelEnabled={true}
              tintColor={styles.palette.mediumBlue}
              textInputStyle={{ flex: 1 }}
              placeholder='My Name'
              placeholderTextColor={styles.palette.mediumBlue}
              highlightColor={styles.palette.mediumBlue}
              style={{ height: 55, width: '100%', marginTop: 40 }}
              value={this.state.nameText}
              onTextChange={ noteText =>  this.setState({ nameText }) }
              underlineColorAndroid='transparent'
            />

            <View style={{ marginTop: 40, flexDirection: 'row'}}>
              <Button label="SAVE" onPress={() => this.props.saveName(this.state.nameText)} color={styles.palette.mediumBlue} />
            </View>

          </View>
        </View>
      </Modal>
    );
  }
}
