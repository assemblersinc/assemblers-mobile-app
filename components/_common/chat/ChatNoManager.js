import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';
import Communications from 'react-native-communications';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { BigChatBubble } from '../../../assets';
import styles from '../../../styles';
import config from '../../../app/config';

const ChatNoManager = () => {

  return(
    <View style={{ flex: 1, backgroundColor: styles.palette.white, justifyContent: 'space-around', alignItems: 'center', padding: 40 }}>
      <Image source={BigChatBubble} style={{ width: 250, height: 250, justifyContent: 'center', padding: 20 }} resizeMode={'contain'}>
        <Text style={[styles.fonts.sectionHeader, { textAlign: 'center', backgroundColor: 'transparent' }]}>Your area manager hasn{"\'"}t added you to chat yet.</Text>
      </Image>
      <Text style={[styles.fonts.body2, { textAlign: 'center' }]}>You can remind your area manger to add you to chat.{"\n\n"}If you don’t know who your area manager is you can get in touch with the help desk.</Text>

      <TouchableOpacity
        onPress={this.contactSupport}
        style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Text style={styles.fonts.smallLink}>Call help desk</Text>
        <Icon name="phone-outgoing" color={styles.palette.mediumBlue} size={30} style={{ marginLeft: 10 }}/>
      </TouchableOpacity>
    </View>
  );
  
}

contactSupport = () => {
    Communications.phonecall(config.app.customerServicePhoneNumber, true);
}

export default ChatNoManager;
