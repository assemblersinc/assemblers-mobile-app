import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  TextInput,
  ScrollView,
  Dimensions,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
  StatusBar,
  Platform
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import InvertibleScrollView from 'react-native-invertible-scroll-view';import { connect } from 'react-redux';
import { sendMessage, messageReceived, getChatMessages, messageTextChange, updateChatMessageCounts, getLastHistoryMessages, clearCurrentText } from '../../../redux/actions';

import FloatingActionButton from '../FloatingActionButton';
import { BasicPageHeader } from '../drawer-nav';
import NameEntryModal from './NameEntryModal';
import ChatMessageComponent from './ChatMessageComponent';
import styles from '../../../styles';
import constants from '../../../constants.json';

const HEADER_HEIGHT = 70;
const INPUT_HEIGHT = 60;
const MESSGAES_HEIGHT = Dimensions.get('window').height - HEADER_HEIGHT - INPUT_HEIGHT - 20;
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
const receiverId = null;

class ChatView extends Component {

  componentWillMount() {
    //this.refs.chatList.scrollTo({animated: true});
    this.loadChatMessages();
  }

  onBackFunction = () => {
    const { userData, getLastHistoryMessages, clearCurrentText, updateChatMessageCounts } = this.props;
    getLastHistoryMessages(userData.token, userData.userId);
    clearCurrentText();
    updateChatMessageCounts(0);
  }

  render() {
    const { userData, chatData, navigation, keyboardType } = this.props;

    return(
      <View style={{ flex: 1, backgroundColor: styles.palette.white }}>

        <BasicPageHeader
          navigation={this.props.navigation}
          title={navigation.state.params ? navigation.state.params.label : '' }
          onBackFunction={this.onBackFunction}
        />

        {(userData.roleId == 'subcontractor')
          ? <NameEntryModal
            saveName={this.saveSubcontractorName}
            isVisible={chatData.nameModalVisible} />
          : null
        }

        <KeyboardAwareScrollView
          scrollEnabled={false}
          extraHeight= {80}>

          <ListView
            keyboardDismissMode={'on-drag'}
            style={{ padding: 10, backgroundColor: styles.palette.white, height: MESSGAES_HEIGHT }}
            dataSource={ds.cloneWithRows(chatData.chatmessages)}
            removeClippedSubviews={true}
            showsVerticalScrollIndicator={false}
            ref="chatList"
            enableEmptySections={true}
            renderScrollComponent={props => <InvertibleScrollView {...props} inverted />}
            renderRow={this.renderRow}
          />

          <View style={{ borderTopWidth: 3, margin: 10,borderColor: styles.palette.paleGray,  alignItems:'center', justifyContent:'center',height: INPUT_HEIGHT,flexDirection:'row'}}>
            <TextInput
              placeholder={'Message...'}
              style={{height: INPUT_HEIGHT,flex:4, backgroundColor: styles.palette.white, paddingLeft: 20, paddingRight: 20 }}
              value={chatData.currentMessageText}
              keyboardType={this.props.keyboardType}
              returnKeyType={'send'}
              blurOnSubmit={true}
              onChangeText={(text) => this.onMessageTextChange(text)}
              onSubmitEditing={this.prepareMessageToSend}
              underlineColorAndroid='transparent'
            />
          </View>

        </KeyboardAwareScrollView>

      </View>

    );
  }


  renderRow = (chatItem) => {
    const { userData } = this.props;
    var messageType;
    if(chatItem.senderId == userData.userId) {
      messageType = constants.chatTypes.outgoing;
    } else {
      messageType = constants.chatTypes.incoming;
    }
    return(
      <ChatMessageComponent messageType={messageType} messageData={chatItem} />
    );
  }

  prepareMessageToSend = () =>{
    const { chatData, userData, navigation, sendMessage } = this.props;
    const receiverId = (navigation.state.params.receiverId == userData.userId) ?  navigation.state.params.senderId : navigation.state.params.receiverId ;

    if(chatData.currentMessageText != null && chatData.currentMessageText != ''){
      let message = {
        message: chatData.currentMessageText,
        senderId: userData.userId,
        receiverId: receiverId,
        locationId: (userData.timesheet) ? userData.timesheet.locationId : null
      }
      sendMessage(userData.token, message, this.loadChatMessages);
    }
  }

  onMessageTextChange = (text) => {
    const { messageTextChange } = this.props;
    messageTextChange(text);
  }

  loadChatMessages = () => {
    const { navigation, userData, getChatMessages } = this.props;
    if(navigation.state.params){
      const receiverId = (navigation.state.params.receiverId == userData.userId) ?  navigation.state.params.senderId : navigation.state.params.receiverId ;
      navigation.state.params.userId = userData.userId;
      getChatMessages(navigation.state.params, userData.token);
    }
  }

  /*saveSubcontractorName = (name) => {
    //TODO: move this to redux
    this.setState({ nameModalVisible: false });
  }*/

}

const mapStateToProps = ({ userReducer, chatReducer }) => {
  return { userData: userReducer, chatData: chatReducer };
};
export default connect(mapStateToProps, {messageTextChange, sendMessage, messageReceived, updateChatMessageCounts, getChatMessages, getLastHistoryMessages, clearCurrentText })(ChatView);
