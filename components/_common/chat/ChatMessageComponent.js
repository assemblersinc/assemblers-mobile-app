import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';

import styles from '../../../styles';
import utils from '../../../utils';
import constants from '../../../constants.json';


export default class ChatMessageComponent extends Component {

  render() {
    const { messageType, messageData } = this.props;
    switch(messageType) {
      case constants.chatTypes.incoming:
        return(
          <IncomingMessage messageData={messageData} timeStamp={this.getChatTimeStamp(messageData.createdAt)}/>
        );
      case constants.chatTypes.outgoing:
        return(
          <OutgoingMessage messageData={messageData} timeStamp={this.getChatTimeStamp(messageData.createdAt)}/>
        );
      default:
        return(
          <Text>Invalid Message Type</Text>
        );
    }
  }

  getChatTimeStamp = (timeStamp) => {
    if(moment().isSame(timeStamp, 'day')) {
      return utils.dateTime.getFormattedTimestamp(timeStamp);
    } else {
      return utils.dateTime.getFormattedDate(timeStamp) + " at " + utils.dateTime.getFormattedTimestamp(timeStamp);
    }
  }

}

class IncomingMessage extends Component {

  render() {
    const { messageData, timeStamp } = this.props;
    return(
      <View style={{ margin: 10 }}>
        <View style={{ marginLeft: 60, marginBottom: 5}}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={[styles.fonts.body1, { marginRight: 10 }]}>{messageData.sender.firstName} {messageData.sender.lastName ? messageData.sender.lastName.slice(0, 1) : ''}</Text>
            <Text style={styles.fonts.body1}>{timeStamp}</Text>
          </View>
          {( messageData.location && messageData.location.Location)
          ? <Text style={styles.fonts.body1}>Sent from {messageData.location.Location}</Text>
          : <Text style={styles.fonts.body1}>Sent from unknown location</Text>
          }
        </View>

        <View style={{ flexDirection: 'row', marginBottom: 20, justifyContent: 'flex-start', alignItems: 'center' }}>
          <View style={{ padding: 10, backgroundColor: styles.palette.mediumBlue30, height: 50, width: 50, borderRadius: 25, justifyContent: 'center', marginRight: 10 }}>
            <Text style={{  color: 'white', textAlign: 'center', fontWeight: 'bold', fontSize: 16 }}>{messageData.sender.firstName.slice(0, 1)}{messageData.sender.lastName.slice(0, 1)}</Text>
          </View>
          <View style={{ marginTop: 5, maxWidth: 250, backgroundColor: styles.palette.paleGray, borderRadius: 10, overflow: 'hidden' }}>
            <Text style={[{ textAlign: 'left', padding: 15 }, styles.fonts.body1]}>{messageData.message}</Text>
          </View>
        </View>

      </View>
    );
  }

}

class OutgoingMessage extends Component {

  render() {
    const { messageData, timeStamp } = this.props;
    return(
      <View style={{ margin: 10, marginBottom: 20 }}>
        <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', marginBottom: 5 }}>
          <Text style={styles.fonts.body1}>{timeStamp}</Text>
          {( messageData.location && messageData.location.Location)
          ? <Text style={styles.fonts.body1}>Sent from {messageData.location.Location}</Text>
          : <Text style={styles.fonts.body1}>Sent from unknown location</Text>
          }
        </View>
        <View style={{ marginTop: 5, maxWidth: 250, backgroundColor: styles.palette.mediumBlue, alignSelf: 'flex-end', borderRadius: 20, overflow: 'hidden' }}>
          <Text style={[{ textAlign: 'right', alignSelf: 'flex-end', padding: 15 }, styles.fonts.whiteText]}>{messageData.message}</Text>
        </View>
      </View>
    );
  }
  
}
