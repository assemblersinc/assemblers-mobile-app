import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from '../../../styles';

const ChatListItem = ({conversationId, firstName, lastName, lastMessage, timeStamp, isRead, isPriority, senderId, receiverId, toDetailView }) => {

  return(
    <TouchableOpacity
      onPress={() => toDetailView()}
      style={{ flexDirection: 'row', padding: 10, paddingTop: 20, paddingBottom: 20, backgroundColor: styles.palette.white, borderBottomWidth: 1, borderBottomColor: styles.palette.lightGray }}>
      {
        (isPriority)
        ? <Icon name="circle" size={15} color={styles.palette.red}  />
        :(isRead)
        ? <Icon name="circle" size={15} color={'transparent'}  />
        : <Icon name="circle" size={15} color={styles.palette.mediumBlue}  />
      }

      <View style={{ flex: 4, marginLeft: 10, marginRight: 10 }}>
        <Text style={[{ marginBottom: 5 }, styles.fonts.body3Bold]}>{firstName} {lastName}</Text>
        <Text numberOfLines={2} ellipseMode={'tail'} style={styles.fonts.body2}>{lastMessage}</Text>
      </View>
      <Text style={[{ flex: 1 }, styles.fonts.body1]}>
        {timeStamp}
      </Text>
    </TouchableOpacity>
  );
}

export default ChatListItem;
