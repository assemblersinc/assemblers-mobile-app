import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  ScrollView
} from 'react-native';
import moment from 'moment';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { messageReceived, updateChatMessageCounts, getLastHistoryMessages, clearQuickMessage } from '../../../redux/actions';
import ChatListItem from './ChatListItem';
import ChatNoManager from './ChatNoManager';
import { DrawerPageHeader } from '../drawer-nav';
import { ErrorScreen } from '../';
import utils from '../../../utils';
import styles from '../../../styles';
import constants from '../../../constants';

const HEADER_HEIGHT = 100;
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class ChatListView extends Component {

  componentWillMount() {
    const { params } = this.props.navigation.state;
    const { userData, chatData, getLastHistoryMessages,updateChatMessageCounts, navigation } = this.props;
    getLastHistoryMessages(userData.token, userData.userId);
    updateChatMessageCounts(0);
  }

  componentDidMount() {
    const { chatData, updateChatMessageCounts } = this.props;
    if(chatData.messages && chatData.messages.length == 1) {
      this.toDetailView(chatData.messages[0]);
    }
  }

  render() {
    const { userData, chatData, getLastHistoryMessages, navigation, clearQuickMessage } = this.props;
    // in case we need to navigate to actual chat.
    const currentRouteKey = navigation.state.routeName;

    //checks if needs to navigate to quick message
    if(chatData.messages && chatData.messages.length > 0 && userData.claimedby &&  chatData.quickMessage) {
      var chatPos = chatData.messages.find((message) => {
        return message.chatName === userData.claimedby.chatName;
      });
      clearQuickMessage();
      this.toDetailView(chatPos);
    }
    // for area managers
    else if(chatData.chatWithTech){
      var chatPos = chatData.messages.find( (chat) => {
        if(chatData.tech == null) return false;
        let isValidChat = chat.chatName === (chatData.tech.claimedby ? chatData.tech.claimedby.chatName : '');
        return isValidChat;
      });

      clearQuickMessage();
      this.toDetailView(chatPos);
    }

    return(
      <View style={{ flex: 1, backgroundColor: styles.palette.white }}>

        {(userData.roleId == constants.roleIds.tech || userData.roleId == constants.roleIds.subcontractor)
          ? <DrawerPageHeader navigation={this.props.navigation} />
          : <View style={{ backgroundColor: styles.palette.white, height: 70, paddingTop: 15 }}>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', margin: 20, marginTop: 25 }}>

                <TouchableOpacity onPress={ () => this.props.navigation.navigate('DrawerOpen') }>
                  <Icon name="menu" size={30}  color={styles.palette.mediumBlue}  />
                </TouchableOpacity>

                {(chatData && chatData.countOfPriorityMsgs && parseInt(chatData.countOfPriorityMsgs) > 0)
                  ? <Icon.Button name={'bell-outline'} color={styles.palette.orange} backgroundColor={'transparent'}>
                      <Text style={[{ backgroundColor: 'transparent', textAlign: 'center' }, styles.fonts.orangeText]}>{String(chatData.countOfPriorityMsgs)}</Text>
                    </Icon.Button>
                  : <Icon name={'bell-outline'} color={styles.palette.lightGray} backgroundColor={'transparent'} size={30} />
                }

              </View>
            </View>
        }

        {(chatData.messages && (chatData.messages.length > 0))
          ? <View  style={{ flex: 1 }}>
            <Text style={[{ paddingLeft: 20, paddingTop: 20 }, styles.fonts.headerBlueGrey]}>Chat</Text>
            <ScrollView>
              <ListView
                style={{ backgroundColor: styles.palette.white, flex: 1 }}
                dataSource={ds.cloneWithRows(chatData.messages)}
                renderRow={this.renderRow}
                removeClippedSubviews={false} />
            </ScrollView>
            </View>
          : (userData.roleId == constants.roleIds.tech || userData.roleId == constants.roleIds.subcontractor)
            ? <ChatNoManager />
            : <ErrorScreen
                errorText={constants.errorMessages.noChatsError}
                actionLabel={'REFRESH'}
                action={() => getLastHistoryMessages(userData.token, userData.userId)} />
        }

      </View>

    );
  }

  renderRow = (item) => {
    const { userData } = this.props;
    return(
      <ChatListItem
        conversationId= {item.conversationId}
        firstName={ item.chatReceiverName}
        lastName={item.chatReceiverLastName}
        lastMessage={item.message}
        timeStamp={this.getChatTimeStamp(item.timeStamp)}
        isRead={Boolean(item.readMessage)}
        isPriority={Boolean(item.priority)}
        senderId={item.sender.senderId}
        receiverId={item.receiver.receiverId}
        toDetailView={() => this.toDetailView(item)} />
    );
  }

  toDetailView = (item) => {
    this.props.navigation.navigate('ChatDetail',  {conversationId: item.conversationId, senderId: item.sender.senderId, label: (item.chatReceiverName + ' ' + item.chatReceiverLastName) ,receiverId: item.receiver.receiverId});
  }

  getChatTimeStamp = (timeStamp) => {
    if(moment().isSame(timeStamp, 'day')) {
      return utils.dateTime.getFormattedTimestamp(timeStamp);
    } else {
      return utils.dateTime.getFormattedDate(timeStamp) + " at " + utils.dateTime.getFormattedTimestamp(timeStamp);
    }
  }

}

const mapStateToProps = ({ userReducer, chatReducer }) => {
  return { userData: userReducer, chatData: chatReducer };
};

export default connect(mapStateToProps, {getLastHistoryMessages, messageReceived, updateChatMessageCounts, clearQuickMessage })(ChatListView);
