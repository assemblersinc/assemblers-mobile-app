import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal
} from 'react-native';
import {
  MKProgress,
  MKSpinner,
} from 'react-native-material-kit';
import styles from '../../styles';

export default class LoadingModal extends Component {

  render() {
    return(
      <Modal
        animationType={"fade"}
        transparent={true}
        visible={this.props.isVisible}
        onRequestClose={() => console.log('onRequestClose')}>
        <View style={styles.modal.container}>
          <MKSpinner style={{ alignSelf: 'center' }} />
        </View>
      </Modal>
    );
  }
}
