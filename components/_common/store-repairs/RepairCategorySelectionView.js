import React, { Component } from 'react';
import {
  Text,
  View,
  ListView,
} from 'react-native';
import {
  MKRadioButton,
  mdl,
} from 'react-native-material-kit';
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux';
import { getRepairCategories, repairCategorySearch, repairCategorySelected, uploadRepairPhoto } from '../../../redux/actions';
import { PredictiveSearchComponent, FloatingActionButton, RadioButtonLineItem } from '../../_common';
import styles from '../../../styles';
import constants from '../../../constants.json';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class RepairCategorySelectionView extends Component {

  state = {
    "text": "",
    "masterList": [],
    "filteredList": []
  }

  componentDidMount() {
    const { userData, repairsData, getRepairCategories } = this.props;
    getRepairCategories(userData.token, this.setListState);
    this.radioGroup = new MKRadioButton.Group();
  }

  setListState = () => {
    const { repairsData, getRepairCategories } = this.props;
    this.setState({ "masterList": repairsData.categories, "filteredList": repairsData.categories }, () => console.log('setState'));
  }

  render() {
    const { navigation, repairsData, getRepairCategories } = this.props;

    return (
      <View style= {{ flex: 1, backgroundColor: styles.palette.white }}>

        <PredictiveSearchComponent
          goBack={this.goBack}
          text={this.state.text}
          onChangeText={this.onChangeText}
          label={'What needs repair?'}
          keyboardType={'default'}/>

        {(this.state.masterList && this.state.masterList.length > 0)
          ? <ListView
            style={{ padding: 20, paddingBottom: 0 }}
            dataSource={ds.cloneWithRows(this.state.filteredList)}
            renderRow={this.renderRow} />
          : <Text style={[styles.fonts.body1, { padding: 20 }]}>Unable to get repair categories.</Text>
        }

        {(repairsData.selectedCategory)
          ? <View style={{ marginLeft: 20, marginRight: 20 }}>
              <FloatingActionButton backgroundColor={styles.palette.mediumBlue} color={styles.palette.white} onPress={this.toPhoto} />
            </View>
          : null
        }

       </View>
    );
  }

  goBack = () => {
    this.props.repairCategorySelected(null);
    this.props.navigation.goBack();
  }

  renderRow = (item) => {
    return(
      <RadioButtonLineItem
        radioGroup={this.radioGroup}
        onCheckedChange={(e) => this.onCheckedChange(e, item)}
        label={item.name} />
    )
  }

  onChangeText = (text) => {
    var matches = [];
    const masterList = (this.state.masterList) ? this.state.masterList : [];
    masterList.forEach(function(element) {
      if(element.name.toLowerCase().indexOf(text.toLowerCase()) > -1) {
        matches.push(element);
      }
    });
    this.setState({ filteredList: matches, text: text });
  }

  onCheckedChange = (e, category) => {
    if(e.checked) {
      this.props.repairCategorySelected(category);
    }
  }

  toPhoto = () => {
    const { repairsData } = this.props;
    const { params } = this.props.navigation.state;
      const instructionBanner =
        <View style={{ backgroundColor: styles.palette.darkGray, width: '100%' }}>
          <Text style={[styles.fonts.whiteText, { padding: 20, textAlign: 'center' }]}>Photograph the area that needs repair</Text>
        </View>;
      this.props.navigation.navigate('Camera', { cameraType: constants.cameraTypes.photo, label: 'Photograph the area that needs repair.', photoCallback: this.photoCallback, instructionBanner: instructionBanner, origin: params.origin});
  }

  photoCallback = (photoPath) => {
    const { uploadRepairPhoto, repairsData, userData } = this.props;
    uploadRepairPhoto(userData.token, photoPath, this.uploadPhotoCallback);
  }

  uploadPhotoCallback = () => {
    const { params } = this.props.navigation.state;
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'SelectRepairCategory'})
      ]
    })
    this.props.navigation.dispatch(resetAction)
    this.props.navigation.navigate('AddRepairNotes', { origin: params.origin });
  }
}


const mapStateToProps = ({ repairsReducer, userReducer }) => {
  return { repairsData: repairsReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getRepairCategories, repairCategorySearch, repairCategorySelected, uploadRepairPhoto })(RepairCategorySelectionView);
