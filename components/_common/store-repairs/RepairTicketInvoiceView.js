import React, { Component } from 'react';
import {
  Text,
  View,
  ListView
} from 'react-native';
import {
  MKCheckbox,
  mdl,
} from 'react-native-material-kit';
import { connect } from 'react-redux';
import { addRepairToInvoice, repairCommentAdded, updateRepairTicketWithPhoto, uploadRepairPhoto } from '../../../redux/actions';
import { NavigationActions } from 'react-navigation';
import { FloatingActionButton } from '../../_common';
import styles from '../../../styles';
import constants from '../../../constants.json';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class RepairTicketInvoiceView extends Component {

  state = {
    selectedRepairTicket: null
  }

  componentWillMount() {
    const { repairsData } = this.props;
    this.setState({ selectedRepairTicket: JSON.parse(JSON.stringify(repairsData.selectedRepairTicket)) });
  }

  render() {

    return (
      <View style= {{ flex: 1, justifyContent: 'space-between', backgroundColor: styles.palette.white, padding: 20 }}>
        <View style={{ paddingBottom: 30, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>
          <Text style={styles.fonts.itemNoLink}>Which repairs did you make to the item?</Text>
        </View>

        <ListView
          dataSource={ds.cloneWithRows(this.state.selectedRepairTicket.repairs)}
          renderRow={this.renderRow} />

        <FloatingActionButton backgroundColor={styles.palette.mediumBlue} color={styles.palette.white} onPress={this.toCameraView} />

      </View>
    );
  }

  renderRow = (item) => {
    return(
      <View style={{ flexDirection: 'row', alignItems: 'center', padding:20 }}>
        <MKCheckbox
          borderOnColor={styles.palette.mediumBlue}
          borderOffColor={styles.palette.mediumGray}
          fillColor={styles.palette.mediumBlue}
          onCheckedChange={() => item.checked = !item.checked }
          checked={item.isChecked} />
        <Text
          numberOfLines={2}
          ellipseMode={'tail'}
          style={[styles.fonts.body3Bold, { marginLeft: 10 }]}>{item.repair.name}</Text>
      </View>
    )
  }


  toCameraView = () => {
    const instructionBanner =
      <View style={{ backgroundColor: styles.palette.darkGray, width: '100%' }}>
        <Text style={[styles.fonts.whiteText, { padding: 20, textAlign: 'center' }]}>Photograph the area that you repaired.</Text>
      </View>;
    this.props.navigation.navigate('Camera', { cameraType: constants.cameraTypes.photo, label: 'Photograph the area that you repaired.', photoCallback: this.photoCallback, instructionBanner: instructionBanner, origin: 'Repairs'});
  }

  photoCallback = (photoPath) => {
    const { uploadRepairPhoto, repairCommentAdded, addRepairToInvoice, userData } = this.props;
    var commentString = "Repairs made: ";
    this.state.selectedRepairTicket.repairs.forEach(function(element, index, array) {
      if(element.checked) {
        commentString += element.repair.name;
        if(index < array.length - 1) {
          element += ", ";
        }
      }
    });
    repairCommentAdded(commentString);
    uploadRepairPhoto(userData.token, photoPath, this.uploadPhotoCallback);
  }

  uploadPhotoCallback = () => {
    const { updateRepairTicketWithPhoto, repairsData, userData } = this.props;
    const currentRepairTicket = this.state.selectedRepairTicket;
    const photo = { "imageUrl" : repairsData.photo, "comment": this.state.commentText};
    updateRepairTicketWithPhoto(userData.token, userData.timesheet.locationId, currentRepairTicket.storeRepairId, photo, this.addRepairsToInvoice);
  }

  addRepairsToInvoice = () => {
    var repairsToInvoice = [];
    this.state.selectedRepairTicket.repairs.forEach(function(element) {
      if(element.checked && element.checked != undefined) {
        repairsToInvoice.push(element);
      }
    });
    const { userData, repairsData, addRepairToInvoice } = this.props;
    addRepairToInvoice(userData.token, repairsToInvoice, this.resetToRepairs);
  }

  resetToRepairs = () => {
    const { params } = this.props.navigation.state;
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Repairs'}),
      ]
    })
    this.props.navigation.dispatch(resetAction);
  }
}

const mapStateToProps = ({ userReducer, repairsReducer }) => {
  return { userData: userReducer, repairsData: repairsReducer };
};

export default connect(mapStateToProps, { addRepairToInvoice, repairCommentAdded, updateRepairTicketWithPhoto, uploadRepairPhoto })(RepairTicketInvoiceView);
