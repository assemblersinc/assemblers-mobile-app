import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ListView,
  Dimensions,
  ScrollView
} from 'react-native';
import { NavigationActions } from 'react-navigation'
import Toast from 'react-native-root-toast';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { getStoreRepairsList, searchScannedBarcode, selectRepairTicket, searchScannedRepairSticker } from '../../../redux/actions';

import { Card, Button } from '../';
import RepairsLineItem from './RepairsLineItem';
import styles from '../../../styles';
import { AddButton, ScanButton, BannerBackground } from '../../../assets';
import utils from '../../../utils';
import constants from '../../../constants.json';

const dimensions = Dimensions.get('window');
const bannerWidth = dimensions.width - 20;
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class RepairsView extends Component {

  componentWillMount() {
    const { userData, getStoreRepairsList } = this.props;
    if(userData.roleId == constants.roleIds.tech || userData.roleId == constants.roleIds.subcontractor) {
      getStoreRepairsList(userData.token, userData.timesheet.locationId);
    }
  }

  render() {

    const { repairsData } = this.props;
    const { params } = this.props.navigation.state;

    return(
      <View style= {{ flex: 1,backgroundColor: styles.palette.white }}>

          <View style={{ margin: 20 }}>
            <Text style={[styles.fonts.headerBlueGrey, { marginBottom: 20 }]}>Store repairs list</Text>

            {(repairsData && repairsData.location)
              ? <View>
                  <Text style={styles.fonts.body2}>{repairsData.location.Location}</Text>
                  <Text style={styles.fonts.body2}>{repairsData.location.address}</Text>
                  <Text style={styles.fonts.body2}>{repairsData.location.city}, {repairsData.location.state} {repairsData.location.zip}</Text>
                </View>
              : null
            }

            <TouchableOpacity
              onPress={this.scanToCreate}
              style={{ marginTop: 20, alignItems: 'center', flexDirection: 'row' }}>
              <Image source={AddButton} style={{ width: 30, height: 30, marginRight: 20 }} />
              <Text style={styles.fonts.smallLink}>CREATE NEW REPAIR TICKET</Text>
            </TouchableOpacity>
          </View>

          {(repairsData.items && repairsData.items.length > 0)
            ?
            <Card direction={'bottom'}>
                <Image
                  resizeMode='cover'
                  source={BannerBackground}
                  style={{ flexDirection: 'row', alignItems: 'flex-start', padding: 10, paddingLeft: 20, height: 85, width: bannerWidth }}>
                  <Text style={[{ flex: 1, backgroundColor: 'transparent', marginTop: 5 }, styles.fonts.whiteText]}>Scan to repair item or select from the list below</Text>
                  <TouchableOpacity onPress={this.scanToRepair} style={{ width: 70, height: 70, marginLeft: 30, backgroundColor: 'transparent'}}>
                    <Image
                      source={ScanButton}
                      style={{ width: 70, height: 70 }}
                      shadowColor={'black'}
                      shadowOpacity={0.3}
                      />
                  </TouchableOpacity>
                </Image>
              <ScrollView>
                <ListView
                  dataSource={ds.cloneWithRows(repairsData.items)}
                  renderRow={this.renderRow}
                />
              </ScrollView>
            </Card>
            : <Text style={[ styles.fonts.body1 ], { padding: 20 }}>No active repair tickets at this location.</Text>
          }

      </View>
    );
  }

  //REPAIR TICKET CREATION
  scanToCreate = () => {
    const bottomBarComponent = <View style={{ flexDirection: 'row', height: 100, backgroundColor: styles.palette.white, padding: 20 }}><Button color={styles.palette.mediumBlue} onPress={this.enterRepairProductDetails} label={'ENTER PRODUCT INFO'}/></View>
    this.props.navigation.navigate('Camera', { cameraType: constants.cameraTypes.barcode, onBarcodeFound: this.productBarcodeCallback, bottomBarComponent: bottomBarComponent  })
  }

  productBarcodeCallback = (product) => {
    console.log('productBarcodeCallback', product);
    const { userData, searchScannedBarcode } = this.props;
    searchScannedBarcode(userData.token, product, userData.timesheet.location.clientId, this.productBarcodeSearchCallback, null);
  }

  productBarcodeSearchCallback = (product) => {
    if(product) {
      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'Repairs'})
        ]
      })
      this.props.navigation.dispatch(resetAction);
      this.props.navigation.navigate('RepairProductDetails', { origin: 'Repairs', product: product });
    }
    else {
      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'Repairs'})
        ]
      })
      this.props.navigation.dispatch(resetAction);
      this.props.navigation.navigate('RepairProductDetails', { origin: 'Repairs', product: product, message: "We could not find your scanned product SKU. Please enter product info below." });
    }
  }

  enterRepairProductDetails = (product) => {
    this.props.navigation.navigate('RepairProductDetails', { origin: 'Repairs' });
  }

  //WORK ON EXISTING TICKETS
  scanToRepair = () => {
    const bottomBarComponent = <View style={{ flexDirection: 'row', height: 100, backgroundColor: styles.palette.white, padding: 20 }}><Button color={styles.palette.mediumBlue} onPress={this.enterRepairTicketNumber} label={'ENTER TICKET NUMBER'}/></View>


    this.props.navigation.navigate('Camera', { cameraType: constants.cameraTypes.barcode, onBarcodeFound: this.ticketBarcodeCallback, bottomBarComponent: bottomBarComponent  })
  }

  ticketBarcodeCallback = (sticker) => {
    const { userData, searchScannedRepairSticker } = this.props;
    searchScannedRepairSticker(userData.token, sticker, this.ticketBarcodeSearchCallback, this.ticketBarcodeSearchErrorCallback);
  }

  ticketBarcodeSearchCallback = (ticket) => {
    const { navigation, selectRepairTicket } = this.props;
    if(ticket) {

      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'Repairs'})
        ]
      })
      navigation.dispatch(resetAction)

      selectRepairTicket(ticket);
      navigation.navigate('RepairTicketDetail');
    }
  }

  ticketBarcodeSearchErrorCallback = () => {
    let toast = Toast.show('Could not find a repair ticket with that barcode.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }

  enterRepairTicketNumber = () => {
    this.props.navigation.navigate('EnterStickerNumber', { origin: 'search' });
  }

  onRowPress = (item) => {
    console.log('onRowPress ' + item.name);
    const { selectRepairTicket, navigation } = this.props;
    selectRepairTicket(item);
    navigation.navigate('RepairTicketDetail');
  }

  renderRow = (item) => {
    console.log(item);
      return(
        <RepairsLineItem
          name={item.productName}
          sku={item.productSku}
          addedByName={item.user.firstName + ' ' + item.user.lastName.slice(0,1)}
          addedById={item.user.techId}
          date={utils.dateTime.getFormattedDate(item.createdAt)}
          isOver90={utils.dateTime.isOver90Days(item.createdAt)}
          onPress={() => this.onRowPress(item)}
        />
      );
  }
}

const mapStateToProps = ({ repairsReducer, userReducer }) => {
  return { repairsData: repairsReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getStoreRepairsList, searchScannedBarcode, selectRepairTicket, searchScannedRepairSticker })(RepairsView);
