import React, { Component } from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native';
import Toast from 'react-native-root-toast';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { searchScannedRepairSticker, clearStickerError } from '../../../redux/actions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Button } from '../';
import styles from '../../../styles';

class EnterStickerNumberView extends Component {

  state = {
    "stickerText": ""
  }

  componentWillMount() {
    this.props.clearStickerError();
  }

  render() {
    const { repairsData } = this.props;
    const { params } = this.props.navigation.state;

    return (
      <TouchableWithoutFeedback  style={{ flex: 1, justifyContent: 'space-between' }} onPress={Keyboard.dismiss}>
        <View style= {{ flex: 1, backgroundColor: styles.palette.white }}>
          <KeyboardAwareScrollView style={{ flex: 1, padding: 20 }}>

            <Text style={[styles.fonts.h2, { marginTop: 80, marginBottom: 80 }]}>Enter the barcode number from the repair sticker</Text>

            <View style={{ borderBottomWidth: 1, borderBottomColor: styles.palette.mediumGray, marginBottom: 80 }}>
              <TextInput
                value={this.state.stickerText}
                placeholder={'E.g. 123456789'}
                style={[{ height: 60, width: '100%' }, styles.fonts.inputText]}
                underlineColorAndroid='transparent'
                onChangeText={(text) => this.setState({ stickerText: text })}
                keyboardType={'numeric'}
              />
            </View>

            {(repairsData.stickerSearchError)
              ? <Text style={[styles.fonts.body1, { padding: 20 }]}>Could not find a repair ticket matching your search.</Text>
              : null
            }

          </KeyboardAwareScrollView>

          {(params && params.origin == 'attach')
            ? <View style={{ flexDirection: 'row', marginBottom: 20, padding: 10 }}>
                <Button color={styles.palette.mediumBlue} onPress={this.createRepairTicket} label={'CREATE TICKET'} />
              </View>
            : <View style={{ flexDirection: 'row', marginBottom: 20, padding: 10 }}>
                <Button color={styles.palette.mediumBlue} onPress={this.searchStickerNumber} label={'SEARCH'} />
              </View>
          }


        </View>
      </TouchableWithoutFeedback>
    );
  }

  createRepairTicket = () => {
    const { params } = this.props.navigation.state;
    if(params && params.callback) {
      params.callback(this.state.stickerText);
    }
  }

  searchStickerNumber = () => {
    const { userData, searchScannedRepairSticker } = this.props;
    searchScannedRepairSticker(userData.token, this.state.stickerText, this.searchStickerCallback, this.searchStickerError);
  }

  searchStickerCallback = (ticket) => {
    this.props.navigation.navigate('RepairTicketDetail');
  }

  searchStickerError = () => {
    let toast = Toast.show('Could not find a repair ticket with that barcode.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }
}

const mapStateToProps = ({ repairsReducer, userReducer }) => {
  return { repairsData: repairsReducer, userData: userReducer };
};

export default connect(mapStateToProps, { searchScannedRepairSticker, clearStickerError })(EnterStickerNumberView);
