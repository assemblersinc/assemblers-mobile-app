import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from '../../../styles';

const RepairsLineItem = ({ name, sku, upc, addedByName, addedById, date, isOver90, onPress }) => {
  return(
    <View style={{ margin: 10, paddingTop: 10, paddingBottom: 20, marginBottom: 0, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>
      <TouchableOpacity
        style={{ marginBottom: 10 }}
        onPress={onPress}>
        <Text
          numberofLines={2}
          ellipseMode={'tail'}
          style={styles.fonts.mediumLink}>
          {name}
        </Text>
      </TouchableOpacity>

      <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
        <View style={{ flex: 3 }}>
          {(sku && sku.sku)
            ? <Text style={styles.fonts.body1}>{sku.sku}</Text>
            : (upc)
              ? <Text style={styles.fonts.body1}>{upc}</Text>
              : <Text style={styles.fonts.body1}>No SKU/UPC</Text>

          }
          <Text style={styles.fonts.body1}>Added by {addedByName} - ID{addedById}</Text>
        </View>
        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
          <Text style={[{ textAlign: 'right' }, styles.fonts.body3Bold]}>{date}</Text>
        </View>
      </View>

      {(isOver90)
        ? <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Icon name="circle" size={10} color={styles.palette.red} />
            <Text style={[{ marginLeft: 10 }, styles.fonts.body1]}>Over 90 days old</Text>
          </View>
        : null
      }
    </View>
  );
}
export default RepairsLineItem;
