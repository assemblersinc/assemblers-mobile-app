import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView
} from 'react-native';
import { connect } from 'react-redux';
import { getManagerRepairsList, selectRepairLocation } from '../../../redux/actions';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from '../../../styles';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class ManagerRepairsView extends Component {

  state = {
    "filteredList": [],
    "masterList": [],
    "selectedClient": "All Clients"
  }

  componentDidMount() {
    const { userData, repairsData, getManagerRepairsList } = this.props;
    getManagerRepairsList(userData.token, userData.areaId,
    () => this.setState({ "filteredList": repairsData.managerList, "masterList": repairsData.managerList }) );
  }

  render() {

    const { navigation, repairsData } = this.props;

    return(
      <View style={{ flex: 1 }}>
        <View style={{ backgroundColor: styles.palette.paleGray, padding: 20, paddingTop: 30 }}>
          <View style={{height: 70, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
            <TouchableOpacity onPress={ () => navigation.navigate('DrawerOpen') }>
              <Icon name="menu" size={30} color={styles.palette.mediumBlue} />
            </TouchableOpacity>
            <Text style={[styles.fonts.headerBlueGrey, { marginLeft: 20 }]}>Store repairs list</Text>
          </View>

          <TouchableOpacity
            onPress={this.toClientList}
            style={{ backgroundColor: styles.palette.white, marginTop: 20, marginBottom: 10, padding: 10, flexDirection: 'row', alignItems: 'center' }}>
            <Icon name="account-multiple" size={30} color={styles.palette.mediumGray} />
            <Text style={[styles.fonts.formInput, { marginLeft: 10 }]}>{this.state.selectedClient}</Text>
          </TouchableOpacity>
        </View>
        <View style={{ backgroundColor: styles.palette.white, flex: 1 }}>
          {(!this.state.masterList || this.state.masterList.length == 0)
            ? <Text style={[styles.fonts.body1, { padding: 20 }]}>Could not get store repairs information.</Text>
            : (this.state.filteredList && this.state.filteredList.length > 0)
              ? <ListView
                style={{ padding: 20 }}
                renderRow={this.renderRow}
                dataSource={ds.cloneWithRows(this.state.filteredList)} />
              : <Text style={[styles.fonts.body1, { padding: 20 }]}>No store repairs to show for {this.state.selectedClient}.</Text>
          }
        </View>
      </View>
    );
  }

  toClientList = () => {
    this.props.navigation.navigate('ClientSelection', { onPressAction: this.filterList });
  }

  renderRow = (item) => {
    return(
      <RepairListItem
        store={item.Location}
        address={item.address}
        city={item.city}
        state={item.state}
        ticketCount={item.repairs.length}
        onPress={() => this.toStoreRepairDetail(item)} />
    );
  }

  filterList = (client) => {
    const masterList = this.state.masterList;
    var filteredList = [];
    if(client == 'all') {
      this.setState({ filteredList: masterList, selectedClient: 'All Clients' });
    } else if(masterList){
      masterList.forEach(function(request) {
        if(request.clientId == client.clientId) {
          filteredList.push(request);
        }
      });
      this.setState({ filteredList: filteredList, selectedClient: client.name });
    }
  }

  toStoreRepairDetail = (repair) => {
    this.props.selectRepairLocation(repair);
    this.props.navigation.navigate('Repairs');
  }
}

const RepairListItem = ({ store, address, city, state, ticketCount, onPress }) => {
  return (
    <View style={{ flexDirection: 'row', alignItems: 'flex-start', paddingTop: 20, paddingBottom: 20, borderBottomColor: styles.palette.borderGray, borderBottomWidth: 1 }}>
      <View style={{ flex: 1 }}>
        <TouchableOpacity onPress={onPress}>
          <Text style={[styles.fonts.mediumLink, { marginBottom: 5 }]}>{store}</Text>
        </TouchableOpacity>
        <Text style={styles.fonts.body2}>{address}</Text>
        <Text style={styles.fonts.body2}>{city}, {state}</Text>
      </View>
      <View style={{ flex: 1, alignItems: 'flex-end' }}>
        <Text style={styles.fonts.body1}>{ticketCount} tickets</Text>
      </View>
    </View>
  );
}

const mapStateToProps = ({ repairsReducer, userReducer }) => {
  return { repairsData: repairsReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getManagerRepairsList, selectRepairLocation })(ManagerRepairsView);
