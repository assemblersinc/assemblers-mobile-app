import AbleToAssembleView from './AbleToAssembleView';
import AddRepairNotesView from './AddRepairNotesView';
import AttachStickerView from './AttachStickerView';
import EnterStickerNumberView from './EnterStickerNumberView';
import ManagerRepairsView from './ManagerRepairsView';
import RepairCategorySelectionView from './RepairCategorySelectionView';
import RepairProductDetailsView from './RepairProductDetailsView';
import RepairsView from './RepairsView';
import RepairTicketInvoiceView from './RepairTicketInvoiceView';
import RepairTicketDetailView from './RepairTicketDetailView';

module.exports = {
  AbleToAssembleView: AbleToAssembleView,
  AddRepairNotesView: AddRepairNotesView,
  AttachStickerView: AttachStickerView,
  EnterStickerNumberView: EnterStickerNumberView,
  ManagerRepairsView: ManagerRepairsView,
  RepairCategorySelectionView: RepairCategorySelectionView,
  RepairProductDetailsView: RepairProductDetailsView,
  RepairTicketInvoiceView: RepairTicketInvoiceView,
  RepairsView: RepairsView,
  RepairTicketDetailView: RepairTicketDetailView
}
