import React, { Component } from 'react';
import {
  Text,
  View,
  Image
} from 'react-native';
import Toast from 'react-native-root-toast';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { createRepairTicket } from '../../../redux/actions';
import { Card, Button } from '../';
import { PhoneQRImage } from '../../../assets';
import styles from '../../../styles';
import constants from '../../../constants.json';

class AttachStickerView extends Component {

  render() {
    const { userData, repairsData } = this.props;
    const { params } = this.props.navigation.state;

    return (
      <View style= {{ flex: 1, justifyContent: 'space-between', backgroundColor: styles.palette.white }}>

        <View style={{ padding: 20 }}>
          <Text style={styles.fonts.itemNoLink}>Last step</Text>
          <Text style={[styles.fonts.bold28, { marginTop: 10, marginBottom: 10 }]}>Attach a repair sticker{"\n"}to the item</Text>
          <Text style={styles.fonts.itemNoLink}>Then hit scan below and point the screen at the new sticker</Text>
        </View>

        <Image
          source={PhoneQRImage}
          resizeMode={'contain'}
          style={{ width: 200, height: 200, alignSelf: 'center' }} />

        <Card direction={'top'}>
          <View style={{ flexDirection: 'row', padding: 10, marginBottom: 20, marginTop: 10, flexDirection: 'row', alignItems: 'center' }}>
            <Button color={styles.palette.mediumGrey} label={'ENTER STICKER'} onPress={this.enterRepairSticker} />
            <Button color={styles.palette.mediumBlue} label={'SCAN'} onPress={this.scanRepairSticker} />
          </View>
        </Card>

      </View>
    );
  }

  scanRepairSticker = () => {
    this.props.navigation.navigate('Camera', {cameraType: constants.cameraTypes.barcode, onBarcodeFound: this.scanStickerCallback });
  }

  scanStickerCallback = (barcode) => {
    const { userData, repairsData, createRepairTicket } = this.props;
    var imageData = { "imageUrl": repairsData.photo, "comment": repairsData.comment };
    createRepairTicket(userData.token, userData.userId, userData.timesheet.locationId, barcode, repairsData.product,
      repairsData.selectedCategory, imageData, this.onRepairTicketCreated);
  }

  onRepairTicketCreated = () => {
    this.showConfirmationToast();
    this.backToOrigin();
  }

  enterRepairSticker = () => {
    this.props.navigation.navigate('EnterStickerNumber', { origin: 'attach', callback: this.scanStickerCallback });
  }

  showConfirmationToast = () => {
    let toast = Toast.show('Repair ticket created.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }

  backToOrigin = () => {
    const { params } = this.props.navigation.state;
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: params.origin}),
      ]
    })
    this.props.navigation.dispatch(resetAction);
  }
}

const mapStateToProps = ({ repairsReducer, userReducer }) => {
  return { repairsData: repairsReducer, userData: userReducer };
};

export default connect(mapStateToProps, { createRepairTicket })(AttachStickerView);
