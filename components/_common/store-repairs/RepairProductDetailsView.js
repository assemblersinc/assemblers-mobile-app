import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';
import {
  MKTextField,
  mdl,
} from 'react-native-material-kit';
import { connect } from 'react-redux';
import _ from 'lodash';
import { productInfoAdded, clearProductInfo } from '../../../redux/actions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import BasicPageHeader from '../drawer-nav/BasicPageHeader';
import { FloatingActionButton } from '../../_common';

import styles from '../../../styles';

class RepairProductDetailsView extends Component {

  state = {
    productId: null,
    name: null,
    category: null,
    sku: null,
    serialNumber: null
  }

  componentWillMount() {
    const { params } = this.props.navigation.state;
    const { workListData } = this.props;
    var product = null;

    if(params.origin == 'WorkList') {
      product = workListData.selectedItem;
    } else if(params && params.product) {
      product = params.product;
    }

    if(product) {
      if(product.productcategory) {
        this.setState({ category: product.productcategory.categoryId + '', upc: product.upc, serialNumber: product.serialNumber });
      } else if (product.categoryId) {
        this.setState({ category: product.categoryId + '', upc: product.upc, serialNumber: product.serialNumber });
      } else {
        this.setState({ upc: product.upc, serialNumber: product.serialNumber });
      }

      if(product.name) {
        this.setState({ name: product.name });
      } else if(product.productName) {
        this.setState({ name: product.productName });
      }

      if(typeof product.sku === 'object' ) {
        this.setState({ sku: product.sku.sku });
      } else {
        this.setState({ sku: product.sku });
      }

      if(product.itemId) {
        this.setState({ productId: product.itemId });
      } else if(product.productId) {
        this.setState({ productId: product.productId });
      }
    }

  }

  render() {

    const { navigation } = this.props;
    const { params } = this.props.navigation.state;

    return(
      <View style={{ flex: 1, justifyContent: 'space-between' }}>
        <BasicPageHeader navigation={navigation} onBackFunction={this.onBack} />

        <KeyboardAwareScrollView
          style={{ flex: 1, backgroundColor: styles.palette.white, padding: 20 }}>
          <Text style={[ styles.fonts.headerBlueGrey, { marginBottom: 20 }]}>Repair Item Info</Text>

          {(params && params.message)
            ? <Text style={styles.fonts.body1}>{params.message}</Text>
            : null
          }

          <MKTextField
            floatingLabelEnabled={true}
            tintColor={styles.palette.mediumGray}
            textInputStyle={[styles.fonts.itemNoLink, { height: 70, flex: 1 }]}
            placeholder={'Item name'}
            placeholderTextColor={styles.palette.black}
            highlightColor={styles.palette.black}
            style={{ height: 55, marginTop: 40, marginBottom: 20 }}
            value={this.state.name}
            onTextChange={(input) => this.setState({ name: input})}
          />

          <View style={{ opacity: .3 }} >
            <MKTextField
              editable={false}
              floatingLabelEnabled={true}
              tintColor={styles.palette.mediumGray}
              textInputStyle={[styles.fonts.itemNoLink, { height: 70, flex: 1 }]}
              placeholder={'Product category'}
              placeholderTextColor={styles.palette.black}
              highlightColor={styles.palette.black}
              style={{ height: 55, marginTop: 40, marginBottom: 20 }}
              value={this.state.category}
              onTextChange={(input) => this.setState({ category: input})}
            />
          </View>

          <MKTextField
            floatingLabelEnabled={true}
            tintColor={styles.palette.mediumGray}
            textInputStyle={[styles.fonts.itemNoLink, { height: 70, flex: 1 }]}
            placeholder={'SKU'}
            placeholderTextColor={styles.palette.black}
            highlightColor={styles.palette.black}
            style={{ height: 55, marginTop: 40, marginBottom: 40 }}
            value={this.state.sku}
            keyboardType={'numeric'}
            onTextChange={(input) => this.setState({ sku: input})}
          />

          <MKTextField
            floatingLabelEnabled={true}
            tintColor={styles.palette.mediumGray}
            textInputStyle={[styles.fonts.itemNoLink, { height: 70, flex: 1 }]}
            placeholder={'Serial number (optional)'}
            placeholderTextColor={styles.palette.black}
            highlightColor={styles.palette.black}
            style={{ height: 55, marginTop: 40, marginBottom: 40 }}
            value={this.state.serialNumber}
            keyboardType={'default'}
            onTextChange={(input) => this.setState({ serialNumber: input})}
          />

          {(this.state.name && this.state.sku)
            ? <View style={{ marginBottom: 40 }}>
                <FloatingActionButton
                  color={styles.palette.white}
                  backgroundColor={styles.palette.mediumBlue}
                  onPress={this.continueRepairTicket} />
              </View>
            : null
          }

        </KeyboardAwareScrollView>
      </View>
    );
  }

  onBack = () => {
    this.props.clearProductInfo();
  }

  continueRepairTicket = () => {
    const { productInfoAdded, navigation } = this.props;
    const { params } = this.props.navigation.state;
    productInfoAdded(this.state);
    navigation.navigate('SelectRepairCategory', { origin: params.origin });
  }
}

const mapStateToProps = ({ workListReducer, userReducer }) => {
  return { workListData: workListReducer, userData: userReducer };
};
export default connect(mapStateToProps, { productInfoAdded, clearProductInfo })(RepairProductDetailsView);
