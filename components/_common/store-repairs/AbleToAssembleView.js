import React, { Component } from 'react';
import {
  Text,
  View,
} from 'react-native';
import {
  MKRadioButton,
  mdl,
} from 'react-native-material-kit';
import Toast from 'react-native-root-toast';
import { connect } from 'react-redux';
import { addItemToInvoice } from '../../../redux/actions';
import { RadioButtonLineItem, FloatingActionButton } from '../';
import { PhoneQRImage } from '../../../assets';
import styles from '../../../styles';

class AbleToAssembleView extends Component {

  componentWillMount() {
    this.radioGroup = new MKRadioButton.Group();
    this.checked = null;
  }

  render() {
    return (
      <View style= {{ flex: 1, justifyContent: 'space-between', backgroundColor: styles.palette.white, padding: 20 }}>
        <View style={{ paddingBottom: 30, borderBottomWidth: 1, borderBottomColor: styles.palette.borderGray }}>
          <Text style={styles.fonts.itemNoLink}>Were you able to assemble the item?</Text>
        </View>
        <View>
          <RadioButtonLineItem radioGroup={this.radioGroup} onCheckedChange={(e) => this.onCheckedChanged(e, "yes")} label={'Yes, add it to my invoice'} />
          <RadioButtonLineItem radioGroup={this.radioGroup} onCheckedChange={(e) => this.onCheckedChanged(e, "no")} label={"No, don't add it to my invoice"} />
        </View>
        <FloatingActionButton backgroundColor={styles.palette.mediumBlue} color={styles.palette.white} onPress={this.toNextStep} />
      </View>
    );
  }

  onCheckedChanged = (e, answer) => {
    if(e.checked) {
      this.checked = answer;
    }
  }

  toNextStep = () => {
      //if done with repair add to invoice and reset back to worklist
      if(this.checked == 'yes'){
        this.addToInvoice();
      }
      //if not done with repair, send to sticker screen:
      else if(this.checked == 'no') {
        this.scanRepairSticker();
      }
  }

  scanRepairSticker = () => {
    const { params } = this.props.navigation.state;
    this.props.navigation.navigate('AttachSticker', { origin: params.origin });
  }

  addToInvoice = () => {
    //TODO: service call to add to invoice
    const { userData, workListData, addItemToInvoice } = this.props;
    addItemToInvoice(userData.token, userData.userId, workListData.selectedItem.requestProductId, userData.timesheet.location.clientId,
      userData.timesheet.locationId, workListData.selectedItem.isService, workListData.selectedItem.serialNumber, this.invoiceConfirmation, this.invoiceFail);
  }

  invoiceConfirmation = () => {
    let toast = Toast.show('Item added to your invoice', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });

    this.scanRepairSticker();
  }

  invoiceFail = () => {
    let toast = Toast.show('Unable to add item to invoice. Please try again.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }
}

const mapStateToProps = ({ repairsReducer, userReducer, workListReducer }) => {
  return { repairsData: repairsReducer, userData: userReducer, workListData: workListReducer };
};

export default connect(mapStateToProps, { addItemToInvoice })(AbleToAssembleView);
