import React, { Component } from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { repairCommentAdded, updateRepairTicket } from '../../../redux/actions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Button } from '../';
import styles from '../../../styles';

class AddRepairNotesView extends Component {

  state = {
    "commentText": ""
  }

  render() {
    return (
      <TouchableWithoutFeedback  style={{ flex: 1, justifyContent: 'space-between' }} onPress={Keyboard.dismiss}>
        <View style= {{ flex: 1, backgroundColor: styles.palette.white }}>
          <KeyboardAwareScrollView style={{ flex: 1, padding: 20 }}>

            <Text style={[styles.fonts.h2, { marginTop: 80, marginBottom: 80 }]}>Describe what needs repair</Text>

            <View style={{ borderBottomWidth: 1, borderBottomColor: styles.palette.mediumGray, marginBottom: 80 }}>
              <TextInput
                value={this.state.commentText}
                placeholder={'E.g. Bike is missing screws'}
                style={[{ height: 60, width: '100%' }, styles.fonts.inputText]}
                underlineColorAndroid='transparent'
                onChangeText={(text) => this.setState({ commentText: text })}
              />
            </View>

          </KeyboardAwareScrollView>

          <View style={{ flexDirection: 'row', marginBottom: 20, padding: 10 }}>
            <Button color={styles.palette.mediumBlue} onPress={this.toNextStep} label={'DONE'} />
          </View>

        </View>
      </TouchableWithoutFeedback>
    );
  }

  toNextStep = () => {
    const { repairCommentAdded, updateRepairTicket, userData, repairsData } = this.props;
    repairCommentAdded(this.state.commentText);

    const { params } = this.props.navigation.state;
    //creating repair ticket from work list, have to go to AbleToAssemble before attaching sticker
    if(params.origin == 'WorkList') {
      this.props.navigation.navigate('AbleToAssemble', { origin: params.origin });
    }
    //updating repair ticket
    else if(params.origin == 'RepairTicketDetail') {
      const imageData = { "imageUrl" : repairsData.photo, "comment": this.state.commentText};
      updateRepairTicket(userData.token, repairsData.selectedRepairTicket.storeRepairId, userData.userId,
        repairsData.selectedCategory.repairId, userData.timesheet.locationId, imageData, this.resetToRepairs);
    }
    //creating repair ticket
    else {
      this.props.navigation.navigate('AttachSticker', { origin: params.origin });
    }
  }

  resetToRepairs = () => {
    const { params } = this.props.navigation.state;
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Repairs'}),
      ]
    })
    this.props.navigation.dispatch(resetAction);
  }
}

const mapStateToProps = ({ repairsReducer, userReducer }) => {
  return { repairsData: repairsReducer, userData: userReducer };
};

export default connect(mapStateToProps, { repairCommentAdded, updateRepairTicket })(AddRepairNotesView);
