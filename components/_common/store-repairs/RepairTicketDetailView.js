import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ListView,
  ScrollView,
  Dimensions
} from 'react-native';
import Toast from 'react-native-root-toast';
import Communications from 'react-native-communications';
import { connect } from 'react-redux';
import { addRepairToInvoice, removeRepairFromTicket, getStoreRepairsList } from '../../../redux/actions';

import { Card, Button, PhotoNoteLineItem, AddNotesModal } from '../';
import { BannerBackground, AddButton } from '../../../assets';
import styles from '../../../styles';
import utils from '../../../utils';
import constants from '../../../constants.json';
const dimensions = Dimensions.get('window');
const bannerWidth = dimensions.width - 20;
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class RepairTicketDetailView extends Component {

  state = {
    notesModalVisible: false
  }

  render() {

    const { repairsData } = this.props;
    const currentRepairTicket = repairsData.selectedRepairTicket;

    if(currentRepairTicket) {
      return(
        <View style={{ flex: 1, backgroundColor: styles.palette.white }}>

          <AddNotesModal isVisible={this.state.notesModalVisible} hideModal={this.hideNotesModal} saveNote={this.saveNote}/>

          <ScrollView style={{ flex: 1 }}>

            <View style={{ padding: 20 }}>
              {(currentRepairTicket.productName)
                ? <Text style={styles.fonts.itemNoLink}>{currentRepairTicket.productName}</Text>
                : null
              }
              {(currentRepairTicket.productSku)
                ? <Text style={[styles.fonts.qty], { marginTop: 20, marginBottom: 20 }}>SKU {currentRepairTicket.productSku}</Text>
                : <Text style={[styles.fonts.qty], { marginTop: 20, marginBottom: 20 }}>No SKU Available</Text>
              }
              {(currentRepairTicket.user)
                ? <View>
                    <Text style={styles.fonts.body1}>Created by {currentRepairTicket.user.firstName} {currentRepairTicket.user.lastName.slice(0, 1)} - ID{currentRepairTicket.user.techId}</Text>
                    <Text style={styles.fonts.body1}>{utils.dateTime.getFormattedDate(currentRepairTicket.createdAt)}</Text>
                  </View>
                : null
              }
            </View>

            <Card direction={'bottom'}>
              <Image
                resizeMode='cover'
                source={BannerBackground}
                style={{ flexDirection: 'row', padding: 10, paddingLeft: 20, marginBottom: 0, height: 85, width: bannerWidth }}>
                <Text style={[{ flex: 1, marginTop: 15, backgroundColor: 'transparent' }, styles.fonts.whiteText]}>Before & after photos</Text>
              </Image>
              <View style={{ padding: 20 }}>
                { (currentRepairTicket.imageData && currentRepairTicket.imageData.length > 0)
                  ? <ListView
                      dataSource={ds.cloneWithRows(currentRepairTicket.imageData)}
                      renderRow={this.renderPhotoRow}
                    />
                  : <Text style={styles.fonts.body2}>Photos and descriptions for this repair ticket.</Text>
                }
              </View>
            </Card>

            <Card direction={'bottom'}>
              <Image
                resizeMode='cover'
                source={BannerBackground}
                style={{ flexDirection: 'row', padding: 10, paddingLeft: 20, marginBottom: 0, height: 85, width: bannerWidth }}>
                <Text style={[{ flex: 1, marginTop: 15, backgroundColor: 'transparent' }, styles.fonts.whiteText]}>Repair items</Text>
                <TouchableOpacity onPress={this.addRepair} style={{ width: 70, height: 70, marginLeft: 30, backgroundColor: 'transparent'}}>
                  <Image
                    source={AddButton}
                    style={{ width: 70, height: 70 }}
                    shadowColor={'black'}
                    shadowOpacity={0.3}
                    />
                </TouchableOpacity>
              </Image>
              <View style={{ padding: 20 }}>
                { (currentRepairTicket.repairs && currentRepairTicket.repairs.length > 0)
                  ? <ListView
                      dataSource={ds.cloneWithRows(currentRepairTicket.repairs)}
                      renderRow={this.renderRepairRow}
                    />
                  : <Text style={styles.fonts.body2}>There are no pending repairs for this repair ticket.</Text>
                }
              </View>
            </Card>

            <TouchableOpacity
              style={{ backgroundColor: styles.palette.paleGray, alignItems: 'center', marginTop: 20, marginBottom: 20 }}
              onPress={this.toPartsOrderForm}>
              <Text style={[{ padding: 20 }, styles.fonts.smallLink]}>Order parts for this repair</Text>
            </TouchableOpacity>

          </ScrollView>

          <Card direction={'top'}>
            <View style={{ flexDirection: 'row', padding: 20, paddingTop: 10, paddingBottom: 10 }}>
              <Button label={'REPAIR ITEM & INVOICE'} onPress={this.repairItem} color={styles.palette.mediumBlue} />
            </View>
          </Card>

        </View>
      );
    } else {
      return(
        <View style={{ flex: 1, backgroundColor: styles.palette.white }}>
          <Text style={styles.fonts.body1}>Error loading selected repair ticket.</Text>
        </View>
      );

    }

  }

  renderPhotoRow = (item) => {
    return(
      <PhotoNoteLineItem
        item={item}
        showNotesModal={this.showNotesModal}
      />
    )
  }

  renderRepairRow = (item) => {
    if(item) {
      return(
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingTop: 10, paddingBottom: 10 }}>
          <Text
            numberOfLines={2}
            ellipseMode={'tail'}
            style={[styles.fonts.body1, { flex: 3, marginRight: 5 }]}>{item.repair.name}</Text>
          <TouchableOpacity
            onPress={() => this.removeRepair(item)}
            style={{ flex: 1 }}>
            <Text style={styles.fonts.mediumLink}>Remove</Text>
          </TouchableOpacity>
        </View>
      );
    }
  }

  addRepair = () => {
    this.props.navigation.navigate('SelectRepairCategory', { origin: 'RepairTicketDetail' });
  }

  removeRepair = (item) => {
    const { repairsData, userData, removeRepairFromTicket } = this.props;
    removeRepairFromTicket(userData.token, item.id,
      this.removeRepairConfirmation, this.ticketClosed, this.removeRepairError);
  }

  removeRepairConfirmation = () => {
    let toast = Toast.show('Repair item successfully removed.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }

  removeRepairError = () => {
    let toast = Toast.show('Unable to remove repair item. Please try again.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }

  ticketClosed = () => {
    let toast = Toast.show('Repair item successfully removed and repair ticket closed.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
    const { userData, getStoreRepairsList } = this.props;
    getStoreRepairsList(userData.token, userData.timesheet.locationId);
    this.props.navigation.goBack();
  }

  toPartsOrderForm = () => {
    const { repairsData } = this.props;
    const currentRepairTicket = repairsData.selectedRepairTicket;

    let partsLink;
    if(currentRepairTicket.location && currentRepairTicket.location.client && currentRepairTicket.location.client.clientattributes) {
      currentRepairTicket.location.client.clientattributes.forEach(function(element) {
        if(element.key == 'partsLink') {
          partsLink = element.value;
        }
      })
    } else {
      partsLink = constants.urls.parts;
    }

    Communications.web(partsLink);
  }

  repairItem = () => {
    this.props.navigation.navigate('RepairTicketInvoice', { origin: 'RepairTicketDetail' });
  }
}

const mapStateToProps = ({ userReducer, repairsReducer }) => {
  return { userData: userReducer, repairsData: repairsReducer };
};
export default connect(mapStateToProps, { addRepairToInvoice, removeRepairFromTicket, getStoreRepairsList })(RepairTicketDetailView);
