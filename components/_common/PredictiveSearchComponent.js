import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from '../../styles';

const PredictiveSearchComponent = ({ goBack, text, onChangeText, keyboardType, label }) => {

  return(
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={{ backgroundColor: styles.palette.mediumBlue }}>
        <View style={{ flexDirection: 'row', backgroundColor: styles.palette.white, marginTop: 40, marginLeft: 10, marginRight: 10, marginBottom: 30, alignItems: 'center' }}>
          <TouchableOpacity onPress={() => goBack()} style={{ width: 40}}>
            <Icon name="arrow-left" size={25}  color={styles.palette.mediumGray} style={{ marginLeft: 10 }} />
          </TouchableOpacity>

          <TextInput
            placeholder={'Search...'}
            style={[{flex: 1, height: 50, backgroundColor: 'white', marginLeft: 20}, styles.fonts.textInput]}
            onChangeText={onChangeText}
            value={text}
            keyboardType={keyboardType}
            underlineColorAndroid='transparent'
          />
        </View>
        <Text style={[{ marginLeft: 10, marginBottom: 30 }, styles.fonts.whiteText]}>{label}</Text>
      </View>
      </TouchableWithoutFeedback>
  );
}

export default PredictiveSearchComponent;
