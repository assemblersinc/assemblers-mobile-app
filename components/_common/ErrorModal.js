import React, { Component } from 'react';
import {
  View,
  Modal,
  Text
} from 'react-native';
import Button from './Button';
import styles from '../../styles';

const ErrorModal = ({ isVisible, errorText, closeModal }) => {
  return(
    <Modal
      animationType={"fade"}
      transparent={true}
      visible={isVisible}
      onRequestClose={() => console.log('onRequestClose')}>
      <View style={styles.modal.container}>
        <View style={styles.modal.modal}>

          <Text style={styles.fonts.h2}>Something went wrong</Text>

          <Text style={styles.fonts.body2}>{errorText}</Text>

          <Text style={styles.fonts.body2}>Please try your request again.</Text>

          <View style={{ marginTop: 40, flexDirection: 'row'}}>
            <Button label="OKAY" onPress={closeModa} color={styles.palette.mediumBlue} />
          </View>

        </View>
      </View>
    </Modal>
  );
}

export default ErrorModal;
