import React, { Component } from 'react';
import {
  View,
} from 'react-native';
import styles from '../../styles';

const Card = (props) => {

  if(props.direction == "top"){
    return(
      <View
        style={{ backgroundColor: styles.palette.white }}
        shadowColor={styles.palette.black}
        shadowOpacity={0.3}
        shadowRadius={3}
        shadowOffset={{ width: 0, height: -3 }}
        elevation={3}>

        {props.children}

      </View>
    );
  }
  if(props.direction == 'center') {
    return(
      <View
        style={{ margin: 10, backgroundColor: styles.palette.white }}
        shadowColor={styles.palette.black}
        shadowOpacity={0.3}
        shadowRadius={3}
        shadowOffset={{ width: 0, height: 1 }}
        elevation={3}>

        {props.children}

      </View>
    );
  }
  else return(
    <View
      style={{ flex: 1, margin: 10, backgroundColor: styles.palette.white }}
      shadowColor={styles.palette.black}
      shadowOpacity={0.3}
      shadowRadius={3}
      shadowOffset={{ width: 0, height: 5 }}
      elevation={3}>

      {props.children}

    </View>
  );
}
 export default Card;
