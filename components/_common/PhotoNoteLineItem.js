import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import styles from '../../styles';
import utils from '../../utils';

const PhotoNoteLineItem = ({ item, showNotesModal }) => {
    return(
      <View key={item.id} style={{ marginBottom: 20 }}>
        <View style={{ flexDirection: 'row', padding: 10, alignItems: 'center' }}>
          <View style={{ alignItems: 'center', padding: 10, borderRadius: 5, backgroundColor: styles.palette.paleGray, marginRight: 30 }}>
            <Image
              source={{ uri: item.imageUrl }}
              style={{ width: 100, height: 133}}
              resizeMode= {'cover'}
            />
          </View>

          {(!item.comment && showNotesModal)
            ? <TouchableOpacity
                onPress={showNotesModal}
                style={{ flex: 1 }}>
                <Text style={styles.fonts.mediumLink}>Add comment</Text>
              </TouchableOpacity>
            : null
          }

        </View>
        <View style={{ margin: 10, marginTop: 0 }}>
          {(item.comment)
            ? <Text style={[{ flex: 1, backgroundColor: styles.palette.paleGray, borderRadius: 5, padding: 10, marginBottom: 10 }, styles.fonts.body1]}>{item.comment}</Text>
            : null
          }

          {(item.firstName && item.lastName)
            ? <Text style={styles.fonts.body1}>- {item.firstName} {item.lastName.slice(0, 1)},  {utils.dateTime.getFormattedDate(item.timeStamp)}  {utils.dateTime.getFormattedTimestamp(item.timeStamp)}</Text>
            : null
          }
        </View>
      </View>
    );
}

export default PhotoNoteLineItem;
