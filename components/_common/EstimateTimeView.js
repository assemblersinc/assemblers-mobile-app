import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import FloatingActionButton from './FloatingActionButton';
import RadioButtonLineItem from './RadioButtonLineItem';
import styles from '../../styles';

class EstimateTimeView extends Component {

  state = {
    selectedTime: ''
  }

  componentWillMount() {
    this.radioGroup = new MKRadioButton.Group();

    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.dataSource = ds.cloneWithRows(this.props.timeEstimate);
  }

  render() {

    const { params } = this.props.navigation.state;

    return(
      <View style= {{ flex: 1 , padding: 30, backgroundColor: 'white' }}>

        {(params.scanSku)
          ? <Text>Scanned SKU: {params.scanSku}</Text>
          : (params.item)
            ? <View>
                <Text>{params.item.name}</Text>
                <Text>SKU: {params.item.sku}</Text>
              </View>
            : null
        }
        <ListView
          dataSource={this.dataSource}
          style={{ marginTop: 20 }}
          renderRow={this.renderRow}
          />

        <FloatingActionButton color={styles.palette.white} backgroundColor={styles.palette.mediumBlue} onPress={this.addToWorklistInvoice} />

      </View>
    );
  }

  renderRow = (item) => {
    return(
      <RadioButtonLineItem
        radioGroup={this.radioGroup}
        onCheckedChange={this.updateSelectedTime}
        label={item}
      />
    );
  }

  updateSelectedTime = (time) => {
    this.setState({ selectedTime: time });
  }

  addToWorklistInvoice = () => {
    //TODO: add item to work list/invoice
    this.props.navigation.navigate('Confirmation');
  }
}

const mapStateToProps = state => {
  return { timeEstimate: state.timeEstimate };
};

export default connect(mapStateToProps)(EstimateTimeView);
