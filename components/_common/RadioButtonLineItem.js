import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
} from 'react-native';
import {
  MKRadioButton,
  mdl,
} from 'react-native-material-kit';

import styles from '../../styles';

const RadioLineItem = ({ radioGroup, onCheckedChange, label }) => {
    return(
      <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 10, paddingBottom: 10 }}>
        <MKRadioButton
          borderOnColor={styles.palette.mediumBlue}
          borderOffColor={styles.palette.mediumGray}
          fillColor={styles.palette.mediumBlue}
          checked={false}
          group={radioGroup}
          onCheckedChange={onCheckedChange}
        />
        <Text
          numberOfLines={2}
          ellipseMode={'tail'}
          style={[{ marginLeft: 10, flex: 1 }, styles.fonts.body2]}>{label}</Text>
      </View>
    );
}

export default RadioLineItem;
