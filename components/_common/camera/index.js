import BarcodeScanner from './BarcodeScanner';
import CameraView from './CameraView';
import PhotoCapture from './PhotoCapture';
import PhotoPreview from './PhotoPreview';

module.exports = {
  BarcodeScanner: BarcodeScanner,
  CameraView: CameraView,
  PhotoCapture: PhotoCapture,
  PhotoPreview: PhotoPreview
}
