import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Image,
  Linking
} from 'react-native';
import Camera from 'react-native-camera';
import Permissions from 'react-native-permissions';
import _ from 'lodash';

import { BasicPageHeader } from '../drawer-nav';
import BarcodeScanner from './BarcodeScanner';
import PhotoCapture from './PhotoCapture';
import { ErrorScreen } from '../';
import { ScanConfirmPopover } from '../../../assets';
import constants from '../../../constants.json';

/**
This is the sole camera for the application. Multiple cameras are NOT supported and will crash the app.

Camera type, title, and appropriate functions are passed from the routing screen.

**/

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

export default class CameraView extends Component {


  state = {
    hasCameraPermission: 'unknown',
    cameraType: null,
    cameraComponent: null,
    barcodeFound: false,
    barcodeCallback: null,
    bottomBarComponent: null,
    instructionBanner: null,
    photoCallback: null
  }

  componentWillMount() {
    this.camera = null;

    Permissions.getPermissionStatus('camera')
      .then(response => {
        this.setState({ hasCameraPermission: response });
      });

    _.throttle(this.onBarcodeFound, 5000);

    const { params } = this.props.navigation.state;
    if(params && params.cameraType) {
      this.setState({ cameraType: params.cameraType });
    }
    if(params && params.bottomBarComponent) {
      this.setState({ bottomBarComponent: params.bottomBarComponent });
    }
    if(params && params.onBarcodeFound) {
      this.setState({ barcodeCallback: params.onBarcodeFound });
    }
    if(params && params.instructionBanner) {
      this.setState({ instructionBanner: params.instructionBanner });
    }
    if(params && params.photoCallback) {
      this.setState({ photoCallback: params.photoCallback });
    }
  }

  componentWillUnmount() {
    this.setState({
      hasCameraPermission: 'unknown',
      cameraType: null,
      cameraComponent: null,
      barcodeFound: false,
      barcodeCallback: null,
      bottomBarComponent: null,
      instructionBanner: null,
      photoCallback: null
    });
  }

  render() {

    const { params } = this.props.navigation.state;

    if(this.state.hasCameraPermission == 'unknown') {
      return (
        <View style={{ flex: 1, backgroundColor: 'black' }} />
      );
    }
    if(!this.state.hasCameraPermission) {
      return(
        <View style={{ flex: 1 }}>
          <BasicPageHeader navigation={this.props.navigation} />
          <ErrorScreen
            errorText={constants.errorMessages.cameraPermissionError}
            actionLabel={'GO TO SETTINGS'}
            action={() => Linking.openURL('app-settings:')}
          />
        </View>
      );
    }
    if(this.state.barcodeFound) {
      return (
        <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, .36)', justifyContent: 'center' }}>
          <Image source={ScanConfirmPopover} style={{ alignSelf: 'center', width: 250, height: 250 }} />
        </View>
      );
    }

    return(
      <View style={styles.container}>
        <Camera
          ref={(cam) => {
            this.camera = cam;
          }}
          style={styles.camera}
          captureQuality={Camera.constants.CaptureQuality.high}
          aspect={Camera.constants.Aspect.fill}
          captureTarget={Camera.constants.CaptureTarget.disk}
          onBarCodeRead={this.onBarcodeFound} />

        <View style={styles.container}>

          {(this.state.cameraType && this.state.cameraType == constants.cameraTypes.photo)
            ?  <PhotoCapture
                navigation={this.props.navigation}
                instructionBanner={this.state.instructionBanner}
                takePhoto={this.takePhoto}
                cameraType={this.state.cameraType} />
            : (this.state.cameraType && this.state.cameraType == constants.cameraTypes.barcode)
                ? <BarcodeScanner
                  navigation={this.props.navigation}
                  bottomBarComponent={this.state.bottomBarComponent}
                  cameraType={this.state.cameraType} />
                : null
          }

        </View>

      </View>

    );
  }

  //barcode recognized by camera
  onBarcodeFound = (data) => {
    const { params } = this.props.navigation.state;

    if(this.state.cameraType == constants.cameraTypes.barcode && !this.state.barcodeFound) {
      console.log('barcodeFound', data.data, params);
      this.setState({ barcodeFound: true });
      this.state.barcodeCallback(data.data);

      const that = this;
      setTimeout( function() {
        that.setState({ barcodeFound: false })
      }, 5000);
    }
  }

  //PHOTO functions
  takePhoto = () => {
    const options = {};
    this.camera.capture({metadata: options})
      .then((data) => {
        console.log(data);
        this.props.navigation.navigate('PhotoPreview', { photoCallback: this.state.photoCallback, photoPreview: data.path });
      })
      .catch(err => console.error(err));
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    position: 'absolute',
    top: 0,
    left: 0
  },
  camera: {
    flex: 1,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    position: 'absolute',
    top: 0,
    left: 0
  }
});
