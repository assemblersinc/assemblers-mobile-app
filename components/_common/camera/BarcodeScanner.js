import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { ScanOverlay } from '../../../assets';
import palette from '../../../styles/palette';
import constants from '../../../constants.json';

var {height, width} = Dimensions.get('window');

export default class BarcodeScanner extends Component {

  state = {
    bottomBarComponent: null,
    cameraType: null
  }

  componentWillMount() {
    const { bottomBarComponent, cameraType } = this.props;
    this.setState({ bottomBarComponent: bottomBarComponent, cameraType: cameraType });
  }

  componentWillUnmount() {
    this.setState({ bottomBarComponent: null, cameraType: null });
  }

  render() {

    if(this.state.cameraType != constants.cameraTypes.barcode) {
      return null;
    }

    return(
      <View style={{ flex: 1 }}>
        <Image
          resizeMode={'stretch'}
          source={ScanOverlay}
          style={{ flex: 1, width: '100%', justifyContent: 'space-between' }} >

          <View style={{ height: 70, backgroundColor: 'transparent', flexDirection: 'row', alignItems: 'flex-start', alignSelf: 'flex-start' }}>
            <Icon name="close" size={40}  color={palette.white} style={{ padding: 10, paddingTop: 20 }} onPress={ () => this.props.navigation.goBack() } />
          </View>

          <View style={{ height: 200 }} />

          <View style={{ marginBottom: 40 }}>
            <Text style={styles.barcodeGuideText}>Point at code to scan</Text>
          </View>

          {(this.state.bottomBarComponent)
            ? this.state.bottomBarComponent
            : null
          }

        </Image>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  barcodeGuideText: {
    color: palette.white,
    marginTop: 40,
    backgroundColor: 'transparent',
    fontSize: 20,
    textAlign: 'center'
  }
});
