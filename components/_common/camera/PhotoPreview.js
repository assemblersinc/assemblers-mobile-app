import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';

import styles from '../../../styles';

export default class PhotoPreview extends Component {

  render() {
    const { params } = this.props.navigation.state;

    return(
      <View style={{ flex: 1 }}>
        <Image
          style={{ flex: 1, width: '100%' }}
          source={{ uri: params.photoPreview }} />
        <View style={{ backgroundColor: 'black', padding: 20, height: 90, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
          <TouchableOpacity onPress={ () =>
            this.props.navigation.goBack(null)
          }>
            <Text style={styles.fonts.whiteText}>retake photo</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={ () => {
              params.photoCallback(params.photoPreview);
            } }>
            <Text style={styles.fonts.whiteText}>use photo</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

}
