import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import palette from '../../../styles/palette.js';
import constants from '../../../constants.json';

export default class PhotoCapture extends Component {

  state = {
    instructionBanner: null,
    takePhoto: null,
    cameraType: null
  }

  componentWillMount() {
    const { instructionBanner, takePhoto, cameraType } = this.props;
    this.setState({ instructionBanner: instructionBanner, takePhoto: takePhoto, cameraType: cameraType })
  }

  componentWillUnmount() {
    this.setState({ instructionBanner: null, takePhoto: null, cameraType: null });
  }

  render() {
    if(this.state.cameraType != constants.cameraTypes.photo) {
      return null;
    }

    return(
      <View style={{ justifyContent: 'space-between', flex: 1 }}>
        <View style={{ height: 70, backgroundColor: 'transparent', flexDirection: 'row', alignItems: 'flex-start', alignSelf: 'flex-start' }}>
          <Icon name="close" size={40}  color={palette.white} style={{ padding: 10, paddingTop: 20 }} onPress={ () => this.props.navigation.goBack() } />
        </View>

        <View style={{ width: '100%', alignItems: 'center'}}>
          {(this.state.instructionBanner)
            ? this.state.instructionBanner
            : null
          }

          {(this.state.takePhoto)
            ? <View style={styles.actionContainer}>
                <TouchableOpacity onPress={this.state.takePhoto} style={styles.activeStyle}>
                  <Icon name="camera" size={40} color={palette.white} />
                </TouchableOpacity>
              </View>
            : null
          }

        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  actionContainer: {
    width: '100%',
    height: 80,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: palette.black
  },
  activeStyle: {
    height: 40,
    alignItems: 'center',
  }
});
