import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  StyleSheet
} from 'react-native';
import {
  MKTextField,
  mdl,
} from 'react-native-material-kit';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import HockeyApp from 'react-native-hockeyapp';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { connect } from 'react-redux';
import { loginIdChanged, loginPasswordChanged, loginUser, showPassword, hidePassword } from '../../redux/actions';

import LoadingModal from '../_common/LoadingModal';
import FloatingActionButton from '../_common/FloatingActionButton';
import styles from '../../styles';
import services from '../../services';
import utils from '../../utils';



//TODO: 100 characters max on username, 16 max on password
class LoginView extends Component {

  constructor(props){
    super(props);
    this._userNamedApppended = false; 
  }

  componentWillMount() {

    this.passwordUnderlineStyle = styles.login.passwordViewButton60

    HockeyApp.trackEvent("Login View");
    HockeyApp.addMetadata({ page:'login'});
  }

  render() {
    const { appData, loginData } = this.props;
    const { userId, password, isPasswordVisible, userIdError, passwordError, loginError } = loginData;

    return(
      <KeyboardAwareScrollView style={styles.login.container}>
        <View style={{ flex: 1 }}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={{ flex: 1, justifyContent: 'space-between' }}>

            <LoadingModal isVisible={appData.isLoading} />

            <TouchableOpacity onPress={() => this.props.navigation.navigate('NeedHelp')}>
              <Text style={{ textAlign: 'right', color: 'white' }}>Need help?</Text>
            </TouchableOpacity>

              <Text style={styles.login.header}>Log in</Text>

              <View>
                <MKTextField
                  floatingLabelEnabled={true}
                  tintColor={styles.palette.white60}
                  textInputStyle={styles.login.textInput}
                  placeholder='LOGIN ID'
                  placeholderTextColor={styles.palette.white}
                  highlightColor={styles.palette.white}
                  style={{ height: 55, marginTop: 30 }}
                  value={userId}
                  onTextChange={this.onUserIdChange}
                  autoCapitalize='none'
                />

                {userIdError
                  ? <View style={styles.login.errorView}><Text style={styles.login.errorText}>Please enter a valid user ID.</Text></View>
                  : <View style={styles.login.errorView}></View>
                }


                {isPasswordVisible
                  ? <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                      <MKTextField
                        floatingLabelEnabled={true}
                        tintColor={styles.palette.white60}
                        textInputStyle={styles.login.textInput}
                        placeholder='PASSWORD'
                        placeholderTextColor={styles.palette.white}
                        highlightColor={styles.palette.white}
                        style={{ height: 55, marginTop: 30, flex: 1 }}
                        value={password}
                        onFocus={this.setUnderlineFocus}
                        onBlur={this.setUnderlineBlur}
                        onTextChange={this.onPasswordChange}
                      />
                      <View style={styles.login.passwordViewButton60}>
                        <TouchableOpacity onPress={this.hidePassword}>
                          <Text style={styles.login.passwordViewText}>Hide</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  : <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <MKTextField
                        password={true}
                        floatingLabelEnabled={true}
                        tintColor={styles.palette.white60}
                        textInputStyle={styles.login.textInput}
                        placeholder='PASSWORD'
                        placeholderTextColor={styles.palette.white}
                        highlightColor={styles.palette.white}
                        style={{ height: 55, marginTop: 30, flex: 1 }}
                        value={password}
                        onFocus={this.setUnderlineFocus}
                        onBlur={this.setUnderlineBlur}
                        onTextChange={this.onPasswordChange}
                      />
                      <View style={styles.login.passwordViewButton60}>
                        <TouchableOpacity onPress={this.showPassword}>
                          <Text style={styles.login.passwordViewText}>Show</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                }

                {passwordError
                ? <View style={styles.login.errorView}><Text style={styles.login.errorText}>Please enter a valid password.</Text></View>
                : <View style={styles.login.errorView}></View>
                }
              </View>

              <Text style={{ color: styles.palette.white }}>{loginError}</Text>

              {(userIdError || passwordError || userId.length == 0 || password.length == 0)
                ? null
                : <FloatingActionButton color={styles.palette.mediumBlue} backgroundColor={styles.palette.white} onPress={this.onLoginPress} />
              }

            </View>
          </TouchableWithoutFeedback>
        </View>
      </KeyboardAwareScrollView>

    );
  }

  setUnderlineFocus = () => {
    this.passwordUnderlineStyle = styles.login.passwordViewButton100;
  }

  setUnderlineBlur = () => {
    this.passwordUnderlineStyle = styles.login.passwordViewButton60;
  }

  onUserIdChange = (text) => {
    // auto complete. It can be overriden after.

    if(text && text.indexOf('@') > 0 &&  !this._userNamedApppended ){
      this._userNamedApppended = true; 
      text = text.substring(0,text.indexOf('@')) + '@assemblersinc.net'; 
    }
    else if(text.length == 1){
      this._userNamedApppended = false; 
    }
    
    this.props.loginIdChanged(text);
  }

  onPasswordChange = (text) => {
    this.props.loginPasswordChanged(text);
  }

  showPassword = () => {
    this.props.showPassword();
  }

  hidePassword = () => {
    this.props.hidePassword();
  }

  onLoginPress = () => {
    const { userId, password } = this.props.loginData;

    this.props.loginUser({userId, password}, this.toPermissions);
  }

  toPermissions = () => {
    this.props.navigation.navigate('Permissions');
  }

}

const mapStateToProps = ({ appReducer, loginReducer }) => {
  return { appData: appReducer, loginData: loginReducer };
};
export default connect(mapStateToProps, { loginIdChanged, loginPasswordChanged, loginUser, showPassword, hidePassword })(LoginView);
