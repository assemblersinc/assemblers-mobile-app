import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native';

import Button from '../_common/Button';
import styles from '../../styles';
import services from '../../services';
import config from '../../app/config/';
import Communications from 'react-native-communications';

export default class ForgotPasswordView extends Component {

  state = {
    isLoading: false,
    emailText: null,
    emailError: false
  }

  render() {

    return(
      <View style={styles.login.container}>
        <View style={{ flex: 1, paddingTop: 40, paddingBottom: 40 }}>
          <Text style={[styles.login.header, {flex: 1}]}>Need help?</Text>

          <View style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', marginBottom: 30 }}>
              <Button label={'CONTACT SUPPORT'} onPress={this.contactSupport} color={styles.palette.mediumBlue} />
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Button label={'RESET PASSWORD'} onPress={this.resetPassword} color={styles.palette.mediumBlue} />
            </View>
          </View>
        </View>
      </View>
    );
  }

  resetPassword = () => {
    console.log('retrievePassword');
    this.props.navigation.navigate('ForgotPassword'); 
  }

  contactSupport = () => {
     console.log('calling support');
    Communications.phonecall(config.app.customerServicePhoneNumber, true);
  }
}
