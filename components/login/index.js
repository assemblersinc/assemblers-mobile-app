import LoginHelpView from './LoginHelpView';
import LoginView from './LoginView';
import PermissionsView from './PermissionsView';
import ForgotPasswordView from './ForgotPasswordView'; 

module.exports = {
  LoginHelpView: LoginHelpView,
  LoginView: LoginView,
  PermissionsView: PermissionsView, 
  ForgotPasswordView:ForgotPasswordView
}
