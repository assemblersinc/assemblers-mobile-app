import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Platform,
  PermissionsAndroid,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Permissions from 'react-native-permissions';
import PushNotification from 'react-native-push-notification';
import { connect } from 'react-redux';
import { permissionsCompleted, enterClockInFlow } from '../../redux/actions';
import styles from '../../styles';
import Button from '../_common/Button';
import HockeyApp from 'react-native-hockeyapp';

var permissionsArray = [];

class PermissionsView extends Component {

 componentDidMount()
  {
    HockeyApp.trackEvent("Permissions View");
    HockeyApp.addMetadata({userId:123, page:'Permissions'});
  }

  render() {

    return(
        <View style={{ flex: 1, padding: 40, paddingTop: 50, backgroundColor: styles.palette.white, justifyContent: 'space-around' }}>

          <Text style={styles.fonts.h2}>This app requires the following permissions:</Text>

          <View>
            <Text style={styles.fonts.body2}>Location services to find your current store and clock in.{"\n"}</Text>
            <Text style={styles.fonts.body2}>The camera to scan barcodes and take photos while you work.{"\n"}</Text>
            <Text style={styles.fonts.body2}>Push Notifications to alert you about incoming chat messages.</Text>
          </View>

          <View style={{ flexDirection: 'row' }}>
            <Button label={'OKAY'} color={styles.palette.mediumBlue} onPress={() => this.enableLocationPermissions(Platform.OS)} />
          </View>

        </View>
    );
  }

  // checkPermissions = (os) => {
  //   this.checkLocationPermission(os);
  // }
  //
  // checkLocationPermission = (os) => {
  //   Permissions.getPermissionStatus('location', 'always')
  //     .then(response => {
  //       permissionsArray.push({ "location": response });
  //       this.checkCameraPermission(os);
  //     })
  // }
  //
  // checkCameraPermission = (os) => {
  //   Permissions.getPermissionStatus('camera')
  //     .then(response => {
  //       permissionsArray.push({ "camera": response });
  //       this.checkPushNotificationPermission(os);
  //     });
  // }
  //
  // checkPushNotificationPermission = (os) => {
  //   if(os == 'ios') {
  //     Permissions.getPermissionStatus('notification', ['alert, badge'])
  //       .then(response => {
  //         permissionsArray.push({ "notification": response });
  //         this.checkPermissionsCallback(os);
  //       })
  //   } else if(os == 'android') {
  //
  //   } else {
  //     console.log('what did you do?');
  //   }
  //
  // }

  // checkPermissionsCallback = (os) => {
  //   console.log(permissionsArray);
  //   //TODO: do whatever we're going to do with the status, then
  //   permissionsArray = [];
  //   this.enableLocationPermissions(os);
  // }

  enableLocationPermissions = (os) => {
    Permissions.requestPermission('location', 'always')
      .then(response => {
        console.log(response);
        this.enableCameraPermissions(os);
      })
  }

  enableCameraPermissions = (os) => {
    Permissions.requestPermission('camera')
      .then(response => {
        console.log(response);
        this.enableNotificationPermissions(os);
      });
  }

  enableNotificationPermissions = (os) => {
    if(os == 'ios') {
      Permissions.requestPermission('notification')
        .then(response => {
          console.log(response);
          this.enterApp();
        })
    } else if(os == 'android') {
      //TODO: PermissionsAndroid check()
      this.enterApp();
    } else {
      console.log('what did you do?');
    }
  }

  enterApp = () => {
    const { userData, enterClockInFlow, permissionsCompleted } = this.props;
    if(userData.clockStatus == 'clock-in') {
      enterClockInFlow(null, null);
    }
    permissionsCompleted();
  }


}

const mapStateToProps = ({ appReducer, userReducer }) => {
  return { appData: appReducer, userData: userReducer };
};
export default connect(mapStateToProps, { permissionsCompleted, enterClockInFlow })(PermissionsView);
