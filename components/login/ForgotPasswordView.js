import React, { Component } from 'react';
import { WebView } from 'react-native';
import config from '../../app/config/';

export default class ForgotPasswordView extends Component {
  render() {
    return (
      <WebView
        source={{uri: config.app.forgotPasswordLink}}
      />
    );
  }
}