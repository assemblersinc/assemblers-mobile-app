import React, { Component } from 'react';
import {
  View,
  Text,
  ListView,
} from 'react-native';
import {
  MKRadioButton,
} from 'react-native-material-kit';
import { connect } from 'react-redux';
import { selectClient, getClients } from '../../../redux/actions';
import { RadioButtonLineItem, FloatingActionButton } from '../../_common';
import styles from '../../../styles';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class ClockInStoreListView extends Component {

  state = {
    checked: null
  }

  componentWillMount() {
    this.radioGroup = new MKRadioButton.Group();
    const { userData, getClients } = this.props;
    const query = {};
    getClients(query, userData.token);
  }

  render() {
    const { clientData } = this.props;

    return(
      <View style= {{ flex: 1 , paddingTop: 20, backgroundColor: styles.palette.white }}>

        {(clientData && clientData.clients && clientData.clients.length > 0)
          ? <ListView
              dataSource={ds.cloneWithRows(clientData.clients)}
              style={{ padding: 10 }}
              renderRow={this.renderRow}
              />
          : <Text style={[styles.fonts.body2, { padding: 20 }]}>Could not get clients list.</Text>
        }

        {(this.state.checked)
          ? <View style={{ margin: 20, marginTop: 0 }}>
              <FloatingActionButton color={styles.palette.white} backgroundColor={styles.palette.mediumBlue} onPress={this.backToClockin} />
            </View>
          : null
        }

      </View>
    );
  }

  renderRow = (item) => {
    return(
      <RadioButtonLineItem
        radioGroup={this.radioGroup}
        onCheckedChange={(e) => this.onCheckedChanged(e, item)}
        label={item.name}
      />
    );
  }

  onCheckedChanged = (e, location) => {
    if(e.checked) {
      this.setState({ checked: location })
    }
  }

  backToClockin = () => {
    const { selectClient, navigation } = this.props;
    selectClient(this.state.checked);
    navigation.goBack();
    this.setState({ checked: null });
  }
}

const mapStateToProps = ({ clientReducer, userReducer }) => {
  return { clientData: clientReducer, userData: userReducer };
};

export default connect(mapStateToProps, { getClients, selectClient })(ClockInStoreListView);
