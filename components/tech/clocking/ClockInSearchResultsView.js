import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  Alert
} from 'react-native';
import {
  MKRadioButton
} from 'react-native-material-kit';
import { connect } from 'react-redux';
import { selectStore, clockinStoreNumberChanged } from '../../../redux/actions';

import { NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FloatingActionButton from '../../_common/FloatingActionButton';
import RadioButtonLineItem from '../../_common/RadioButtonLineItem';
import styles from '../../../styles';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class ClockInSearchResultsView extends Component {

  state = {
    checked: null
  }


  componentWillMount() {
    this.radioGroup = new MKRadioButton.Group();
  }

  render() {

    const { clockingData } = this.props;
    console.log(clockingData);
    return(
      <View style= {{ flex: 1, padding: 20, backgroundColor: styles.palette.white, justifyContent: 'center' }}>
        {(clockingData && clockingData.searchResults && clockingData.searchResults.location && clockingData.searchResults.location.length > 0)
          ? <View style={{ flex: 1 }}>
              <ListView
                enableEmptySection={false}
                style={{ padding: 20, backgroundColor: styles.palette.white }}
                dataSource={ds.cloneWithRows(clockingData.searchResults.location)}
                renderRow={this.renderRow} />

              {(this.state.checked)
                ? <FloatingActionButton color={styles.palette.white} backgroundColor={styles.palette.mediumBlue} onPress={this.updateSelection} />
                : null
              }
            </View>
          : <Text style={styles.fonts.body2}>No locations match your input.{"\n\n"}Go back to update your search.</Text>
        }
      </View>
    )

  }

  renderRow = (item) => {
    return(
      <RadioButtonLineItem
        radioGroup={this.radioGroup}
        onCheckedChange={(e) => this.onCheckedChanged(e, item)}
        label={item.Location}
      />
    )
  }

  onCheckedChanged = (e, location) => {
    if(e.checked) {
      this.setState({ checked: location });
    }
  }

  updateSelection = () => {
    if(this.state.checked) {
      const { clockingData, selectStore, clockinStoreNumberChanged } = this.props;
      var storeObject = { name: this.state.checked.name, clientId: this.state.checked.clientId,
        location: [{ locationId: this.state.checked.locationId, clientId: this.state.checked.clientId, Location: this.state.checked.Location }] };
      selectStore(storeObject, this.state.checked.storeNumber);
      this.backToClockin();
    }
  }

  backToClockin = () => {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'ClockIn'}),
      ]
    })
    this.props.navigation.dispatch(resetAction);
  }
}

const mapStateToProps = ({ clockingReducer }) => {
  return { clockingData: clockingReducer };
};

export default connect(mapStateToProps, { selectStore, clockinStoreNumberChanged })(ClockInSearchResultsView);
