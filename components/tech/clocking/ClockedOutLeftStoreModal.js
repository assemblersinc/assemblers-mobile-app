import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Text,
  TouchableOpacity
} from 'react-native';
import {
  MKProgress,
  MKSpinner,
} from 'react-native-material-kit';

import styles from '../../../styles';
import Button from '../../_common/Button';

export default class ClockedOutLeftStoreModal extends Component {

  render() {
      return(
        <Modal
          animationType={"fade"}
          transparent={true}
          visible={this.props.isVisible}
          onRequestClose={this.props.closeModal}>
          <View style={styles.modal.container}>
            <View style={styles.modal.modal}>

              <Text style={styles.modal.text}>Looks like you{"\'"}ve left</Text>
              <Text style={styles.modal.boldText}>{this.props.store}</Text>
              <Text style={[styles.modal.text, { marginTop: 20, marginBottom: 40 }]}>Would you like to clock out?</Text>

              <View style={{ flexDirection: 'row', marginBottom: 20 }}>
                <Button label={'CLOCK OUT'} onPress={this.clockOut} color={styles.palette.mediumBlue} />
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Button label={"DON'T CLOCK OUT"} onPress={this.stayClockedIn} color={styles.palette.mediumGray} />
              </View>

            </View>
          </View>
        </Modal>
      );
  }

  clockOut = () => {
    this.props.closeModal();
    this.props.clockOutUser();
  }

  stayClockedIn = () => {
    this.props.closeModal();
  }
}
