import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Text
} from 'react-native';
import {
  MKProgress,
  MKSpinner,
} from 'react-native-material-kit';

import Button from '../../_common/Button';
import styles from '../../../styles';
import constants from '../../../constants.json';

export default class ClockOutPromptModal extends Component {

  render() {
      return(
        <Modal
          animationType={"fade"}
          transparent={true}
          visible={this.props.isVisible}
          onRequestClose={this.props.closeModal}>
          <View style={styles.modal.container}>

          {(this.props.roleId == constants.roleIds.tech)
            ? <View style={styles.modal.modal}>
               <Text style={[styles.modal.text, { marginTop: 40, marginBottom: 40 }]}>Are you sure you want to clock out?</Text>
               <View style={{ flexDirection: 'row', marginBottom: 20 }}>
                 <Button label={"YES, CLOCK OUT"} onPress={this.props.clockOut} color={styles.palette.mediumBlue} />
               </View>
               <View style={{ flexDirection: 'row' }}>
                 <Button label={"DON'T CLOCK OUT"} onPress={this.props.closeModal} color={styles.palette.lightGrey} />
               </View>
             </View>
            : <View style={styles.modal.modal}>
               <Text style={[styles.modal.text, { marginTop: 40, marginBottom: 40 }]}>Are you sure you want to check out?</Text>
               <View style={{ flexDirection: 'row', marginBottom: 20 }}>
                 <Button label={"YES, CHECK OUT"} onPress={this.props.clockOut} color={styles.palette.mediumBlue} />
               </View>
               <View style={{ flexDirection: 'row' }}>
                 <Button label={"DON'T CHECK OUT"} onPress={this.props.closeModal} color={styles.palette.lightGrey} />
               </View>
             </View>
          }
            
          </View>
        </Modal>
      );
  }
}
