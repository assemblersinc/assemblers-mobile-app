import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  Modal,
  Platform
} from 'react-native';
import {
  MKTextField,
  mdl,
} from 'react-native-material-kit';
import Toast from 'react-native-root-toast';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import _ from 'lodash';
import { submitGrillbarrow } from '../../../redux/actions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { FloatingActionButton } from '../../_common';

import styles from '../../../styles';

class GrillbarrowView extends Component {

  state = {
    activeCategory: null,
    onRackCount: "",
    holesCount: "",
    stagedCount: "",
    boxedCount: "",
    repairsCount: "",

    grillCount: "",
    wheelbarrowCount: ""
  }

  render() {
    return(
      <View style={{ flex: 1, backgroundColor: styles.palette.white }}>
        <KeyboardAwareScrollView
          style={{ flex: 1, backgroundColor: styles.palette.white, padding: 20 }}>
          <Text style={[ styles.fonts.headerBlueGrey, { marginTop: 40, marginBottom: 20 }]}>Required Store Walk</Text>
          <Text style={[styles.fonts.body2, { marginBottom: 20 }]}>Please click on each category and enter the required information. You must fill out each field before proceeding.</Text>

          <View style={{ marginTop: 20, marginBottom: 20 }}>
            <GrillbarrowHeader
              onPress={() => this.onCategorySelected('bicycles')}
              category={'bicycles'}
              activeCategory={this.state.activeCategory}
              label={'Bicycles'}
              isComplete={this.isBicycleSectionComplete()}/>
            {(this.state.activeCategory == 'bicycles')
              ? <View style={{marginLeft: 50}}>
                  <GrillbarrowInput label={'On Rack'} value={this.state.onRackCount} onChangeText={(input) => this.setState({ onRackCount: input })} />
                  <GrillbarrowInput label={'Holes'} value={this.state.holesCount} onChangeText={(input) => this.setState({ holesCount: input })} />
                  <GrillbarrowInput label={'Staged'} value={this.state.stagedCount} onChangeText={(input) => this.setState({ stagedCount: input })} />
                  <GrillbarrowInput label={'Boxed'} value={this.state.boxedCount} onChangeText={(input) => this.setState({ boxedCount: input })} />
                  <GrillbarrowInput label={'Repairs'} value={this.state.repairsCount} onChangeText={(input) => this.setState({ repairsCount: input })} />
                </View>
              : null
            }
          </View>

          <View style={{ marginTop: 20, marginBottom: 20 }}>
            <GrillbarrowHeader
              onPress={() => this.onCategorySelected('grills')}
              category={'grills'}
              activeCategory={this.state.activeCategory}
              label={'Grills'}
              isComplete={(this.state.grillCount.length > 0) ? true : false}/>
            {(this.state.activeCategory == 'grills')
              ? <View style={{ marginLeft: 50 }}>
                  <GrillbarrowInput label={'Assembled'} value={this.state.grillCount} onChangeText={(input) => this.setState({ grillCount: input })} />
                </View>
              : null
            }
          </View>


          <View style={{ marginTop: 20, marginBottom: 20 }}>
            <GrillbarrowHeader
              onPress={() => this.onCategorySelected('wheelbarrows')}
              category={'wheelbarrows'}
              activeCategory={this.state.activeCategory}
              label={'Wheelbarrows'}
              isComplete={(this.state.wheelbarrowCount.length > 0) ? true : false} />
            {(this.state.activeCategory == 'wheelbarrows')
              ? <View style={{ marginLeft: 50 }}>
                  <GrillbarrowInput label={'Assembled'} value={this.state.wheelbarrowCount} onChangeText={(input) => this.setState({ wheelbarrowCount: input })} />
                </View>
              : null
            }
          </View>

          {(this.state.onRackCount.length > 0 && this.state.holesCount.length > 0 && this.state.stagedCount.length > 0 && this.state.boxedCount.length > 0 && this.state.repairsCount.length > 0
            && this.state.grillCount.length > 0 && this.state.wheelbarrowCount.length > 0)
            ? <FloatingActionButton
              color={styles.palette.white}
              backgroundColor={styles.palette.mediumBlue}
              onPress={this.submitGrillbarrow} />
            : null
          }

        </KeyboardAwareScrollView>
      </View>
    );
  }

  isBicycleSectionComplete = () => {
    return this.state.onRackCount.length > 0 && this.state.holesCount.length > 0 && this.state.stagedCount.length > 0 && this.state.boxedCount.length > 0 && this.state.repairsCount.length > 0;
  }

  onCategorySelected = (category) => {
    if(category == this.state.activeCategory) {
      this.setState({ activeCategory: null });
    } else {
      this.setState({ activeCategory: category });
    }
  }

  submitErrorCallback = () => {
    let toast = Toast.show('Error submitting counts. Please try again.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }

  submitCallback = () => {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'ClockIn'}),
      ]
    })
    this.props.navigation.dispatch(resetAction);
  }

  submitGrillbarrow = () => {
    const { userData, clockingData, submitGrillbarrow } = this.props;
    submitGrillbarrow(userData.token, userData.userId, userData.timesheet.locationId,
      this.state.onRackCount, this.state.holesCount, this.state.stagedCount, this.state.boxedCount, this.state.repairsCount,
      this.state.grillCount, this.state.wheelbarrowCount, this.submitCallback, this.submitErrorCallback);
  }
}

const GrillbarrowInput = ({ label, value, onChangeText}) => {
  return(
    <View style={{ borderBottomWidth: 1, borderBottomColor: styles.palette.mediumGray, flexDirection: 'row', alignItems: 'center' }}>
      <Text style={[styles.fonts.body2Light, { flex: 1 }]}>{label}</Text>
      <TextInput
        placeholder={'Total Count'}
        style={{ flex: 2, height: 60, paddingLeft: 40, backgroundColor: styles.palette.white }}
        underlineColorAndroid='transparent'
        onChangeText={onChangeText}
        value={value}
        keyboardType={'numeric'}
        />
    </View>
  );
}

const GrillbarrowHeader = ({ onPress, category, activeCategory, label, isComplete }) => {
  return (
    <TouchableOpacity
      style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 10, justifyContent: 'space-between' }}
      onPress={onPress}>
      {(activeCategory == category)
        ? <Icon name={'minus'} size={30} color={styles.palette.mediumGray} />
        : <Icon name={'plus'} size={30} color={styles.palette.mediumGray} />
      }
      <Text style={[styles.fonts.itemNoLink, { marginLeft: 20, marginRight: 20, flex: 1 }]}>{label}</Text>
      {(isComplete)
        ? <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Icon name={'check-circle-outline'} size={20} color={styles.palette.mediumGray} />
            <Text style={[styles.fonts.body2Light, { marginLeft: 10 }]}>Completed</Text>
          </View>
        : null
      }
    </TouchableOpacity>
  );
}

const mapStateToProps = ({ clockingReducer, userReducer }) => {
  return { clockingData: clockingReducer, userData: userReducer };
};
export default connect(mapStateToProps, { submitGrillbarrow })(GrillbarrowView);
