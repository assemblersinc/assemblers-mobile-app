import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  Image
} from 'react-native';
import {
  MKTextField,
  mdl,
} from 'react-native-material-kit';
import { connect } from 'react-redux';
import _ from 'lodash';
import { exitClockInFlow,
  enterClockInFlow,
  findCurrentStore,
  clockinStoreNumberChanged,
  clockInUser,
  clockOutUser,
  stayClockedIn,
  getClockInLocation,
  setClockinError,
  checkInSubcontractor,
  clearSelectedStore,
  travelToAnotherStore } from '../../../redux/actions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { DrawerPageHeader } from '../../_common/drawer-nav';
import { Button, FloatingActionButton } from '../../_common';
import { AILogo } from '../../../assets';
import styles from '../../../styles';
import constants from '../../../constants.json';

class ClockInView extends Component {

  componentWillMount() {
    const { findCurrentStore, clockingData } = this.props;
    //If they have not selected a store already, see if they are in a store and pre-populate the form
    console.log(clockingData.selectedStore);

    if(_.isEmpty(clockingData.selectedStore) || !clockingData.selectedStore) {
      findCurrentStore(this.props.userData.token, false);
    }
  }

  componentWillUnmount() {
    this.props.enterClockInFlow(null, null);
  }

  render() {
    const { userData, clockingData, clockOutUser, stayClockedIn, exitClockInFlow, clockinStoreNumberChanged, getClockInLocation, navigation } = this.props;

    const rightButton = <TouchableOpacity
                          style={{ marginRight: 20 }}
                          onPress={ () => navigation.navigate('HavingTrouble') }>
                          <Text style={{ color: styles.palette.mediumGray, fontSize: 16 }}>Don{"\'"}t know the store number?</Text>
                        </TouchableOpacity>;

    console.log('RENDER', clockingData);

    if(clockingData.isClockedIn && (clockingData.origin && clockingData.origin != "invoicing")) {
      return(
        <View style={{ flex: 1, backgroundColor: styles.palette.white, alignItems: 'center', justifyContent: 'center', padding: 20 }}>

          <Text style={styles.fonts.h2}>You are already clocked in</Text>

          {(userData.timesheet && userData.timesheet.location && userData.timesheet.location.Location)
            ? <Text style={[styles.fonts.h2, { marginTop: 20, marginBottom: 60 }]}>at {userData.timesheet.location.Location} </Text>
            : null
          }

          <View style={{ flexDirection: 'row', marginBottom: 30 }}>
            <Button
              label={'CLOCK OUT'}
              onPress={() => clockOutUser(userData.userId, userData.timeSheetId, userData.token)}
              color={styles.palette.mediumBlue}/>
          </View>
          <View style={{ flexDirection: 'row'}}>
            <Button label={'STAY CLOCKED IN'} onPress={this.stayClockedIn}/>
          </View>
        </View>
      );
    }

  return(
    <View style={{ flex: 1, backgroundColor: styles.palette.white }}>
      <DrawerPageHeader navigation={navigation} rightButton={rightButton} />

      <KeyboardAwareScrollView>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={{ flex: 1 }}>
            <View style= {{ flex: 1, padding: 30, justifyContent: 'space-between' }}>

              <Image
                source={AILogo}
                resizeMode={'contain'}
                style={{ width: 150, height: 150, alignSelf: 'center', marginBottom: 20 }} />

              {(userData.roleId == constants.roleIds.tech)
                ? (clockingData.origin && clockingData.origin == "invoicing")
                  ? <Text style={styles.fonts.headerBlueGrey}>Travel to Another Store</Text>
                  : <Text style={styles.fonts.headerBlueGrey}>Clock in</Text>
                : (userData.roleId == constants.roleIds.areaManager || userData.roleId == constants.roleIds.contractor)
                  ? <Text style={styles.fonts.headerBlueGrey}>Enter store info</Text>
                  : <Text style={styles.fonts.headerBlueGrey}>Check in</Text>

              }

              <TouchableOpacity
                onPress={() => navigation.navigate('StoreList')}>
                <MKTextField
                  editable={false}
                  floatingLabelEnabled={true}
                  tintColor={styles.palette.mediumGray}
                  textInputStyle={styles.login.textInputDark}
                  placeholder= {'Store name'}
                  placeholderTextColor={styles.palette.black}
                  highlightColor={styles.palette.black}
                  style={{ height: 55, marginTop: 20 }}
                  value={clockingData.selectedStore.name}
                />
              </TouchableOpacity>
              <MKTextField
                floatingLabelEnabled={true}
                tintColor={styles.palette.mediumGray}
                textInputStyle={styles.login.textInputDark}
                placeholder={'Store number'}
                placeholderTextColor={styles.palette.black}
                highlightColor={styles.palette.black}
                style={{ height: 55, marginTop: 40, marginBottom: 20 }}
                value={String(clockingData.storeNumberText)}
                keyboardType={'numeric'}
                onTextChange={(input) => clockinStoreNumberChanged(input)}
                onBlur={this.getClockInLocation}
              />

              {this.validationError ?
              <Text>{String(this.validationError)}</Text>
              : null}

              {clockingData.error ?
              <Text>{String(clockingData.error)}</Text>
              : null
              }

              {(clockingData.hasValidation)
                ? <FloatingActionButton
                  color={styles.palette.white}
                  backgroundColor={styles.palette.mediumBlue}
                  onPress={() => this.clockInUser()} />
                : null
              }

            </View>

          </View>
        </TouchableWithoutFeedback>
      </KeyboardAwareScrollView>
    </View>
    );
  }

  stayClockedIn = () => {
    this.props.stayClockedIn();
    this.props.navigation.navigate('WorkList');
  }

  getClockInLocation = () => {
    const { userData, clockingData, getClockInLocation } = this.props;
    console.log(clockingData);
    if(!_.isNil(clockingData.storeNumberText) || clockingData.storeNumberText.length > 0) {
      getClockInLocation(clockingData.selectedStore.clientId, clockingData.storeNumberText, userData.token);
    }
  }

  clockInUser = () => {
    const { userData, clockingData, clockInUser, setClockinError, checkInSubcontractor, travelToAnotherStore, navigation } = this.props;

    console.log(clockingData);

    //validate form
    if(_.isEmpty(clockingData.selectedStore) ||
      _.isNil(clockingData.storeNumberText) ||
      clockingData.storeNumberText.length < 1) {
      setClockinError(constants.errorMessages.clockinFormError);
    }
    //traveling to another store
    else if((clockingData.origin && clockingData.origin == "invoicing") && userData.roleId == constants.roleIds.tech) {
      travelToAnotherStore(userData.token, userData.timesheet.timeId, userData.userId, userData.timesheet.locationId, userData.roleId);
    }
    //subcontractor, contractor, or area manager
    else if(userData.roleId == constants.roleIds.subcontractor
      || userData.roleId == constants.roleIds.areaManager
      || userData.roleId == constants.roleIds.contractor) {
        console.log(clockingData);
        checkInSubcontractor(clockingData.selectedStore.location[0]);
    }
    //tech
    else if(userData.roleId == constants.roleIds.tech){
      clockInUser(userData.userId, clockingData.selectedStore.location[0].locationId, userData.token, userData.roleId, () => navigation.navigate('Grillbarrow'));
    }
  }

}

const mapStateToProps = ({ clockingReducer, userReducer }) => {
  return { clockingData: clockingReducer, userData: userReducer };
};
export default connect(mapStateToProps, { exitClockInFlow, enterClockInFlow, findCurrentStore, clockinStoreNumberChanged, clockInUser, clockOutUser, stayClockedIn, getClockInLocation, setClockinError, checkInSubcontractor, clearSelectedStore, travelToAnotherStore })(ClockInView);
