import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Text
} from 'react-native';
import {
  MKProgress,
  MKSpinner,
} from 'react-native-material-kit';
import styles from '../../../styles';
import Button from '../../_common/Button';

export default class ClockInPromptModal extends Component {

  render() {

    if(this.props.store) {
      return(
        <Modal
          animationType={"fade"}
          transparent={true}
          visible={this.props.isVisible}
          onRequestClose={this.props.closeModal}
          key={Math.random()}>
          <View style={styles.modal.container}>

          {(this.props.isLoading)

            ? <View style={{ justifyContent: 'center', alignItems: 'center' }}><MKSpinner /></View>
            : <View style={styles.modal.modal}>
                <Text style={styles.modal.text}>Looks like you{"\'"}re at</Text>
                <Text style={styles.modal.boldText}>{this.props.store.name}</Text>
                <Text style={styles.modal.text}> at {this.props.store.address}.</Text>
                <Text style={[styles.modal.text, { marginTop: 20, marginBottom: 40 }]}>Would you like to clock in?</Text>

                {(this.props.isLoading)
                  ? <Text style={{ fontSize: 30 }}>Loading...</Text>
                  : null
                }
                <View style={{ flexDirection: 'row', marginBottom: 20 }}>
                  <Button label={'YES, CLOCK IN'} onPress={this.props.clockIn} color={styles.palette.mediumBlue}/>
                </View>

                <View style={{ flexDirection: 'row' }}>
                  <Button label={'NO'} onPress={this.props.closeModal} color={styles.palette.mediumGray}/>
                </View>
              </View>
            }
          </View>
        </Modal>
      );
    }

    else {
      return null;
    }

  }
}
