import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  Keyboard,
  ListView,
  Picker,
  ScrollView,
  Platform
} from 'react-native';
import Toast from 'react-native-root-toast';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { updateCityText, updateStateText, updateZipText, searchForStores } from '../../../redux/actions';

import FloatingActionButton from '../../_common/FloatingActionButton';
import styles from '../../../styles';
import constants from '../../../constants.json';

class ClockInHavingTroubleView extends Component {

  render() {
    
    const { updateCityText, updateStateText, updateZipText, clockingData, userData } = this.props;
    return(
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={{ flex: 1, justifyContent: 'space-between', backgroundColor: styles.palette.white }}>
          <View style={{ padding: 20, paddingTop: 10, backgroundColor: styles.palette.mediumBlue }}>
            <View style={{ height: 70, flexDirection: 'row', alignItems: 'center' }}>
              <Icon name="close" size={30}  color={styles.palette.white} onPress={ () => this.props.navigation.goBack() } />
            </View>

            <View style={{ backgroundColor: styles.palette.mediumBlue }}>
              <Text style={[styles.fonts.whiteText, {paddingTop: 20, marginBottom: 10 }]}>Search for a store to clock in.</Text>

              <View style={{ marginTop: 30 }}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('StoreList')}
                  style={{ marginBottom: 20 }}>
                  <TextInput
                    placeholder={'Store'}
                    value={clockingData.selectedStore.name}
                    style={{ height: 50, paddingLeft: 20, backgroundColor: styles.palette.white }}
                    editable={false}
                    underlineColorAndroid='transparent'
                    />
                </TouchableOpacity>

                <View style={{ flexDirection: 'row' }}>
                  <TextInput
                    placeholder={'City'}
                    style={{ flex: 1, marginRight: 10, height: 50, paddingLeft: 20, backgroundColor: styles.palette.white }}
                    underlineColorAndroid='transparent'
                    value={clockingData.cityText}
                    onChangeText={(input) => updateCityText(input)}
                    />
                  <TextInput
                    placeholder={'Zip Code'}
                    keyboardType={'numeric'}
                    style={{ flex: 1, marginLeft: 10, height: 50, paddingLeft: 20, backgroundColor: styles.palette.white }}
                    underlineColorAndroid='transparent'
                    value={clockingData.zipText}
                    onChangeText={(input) => updateZipText(input)}
                    />
                </View>
              </View>
            </View>
          </View>

          <Picker
            style={{ backgroundColor: styles.palette.white, margin: 5 }}
            selectedValue={clockingData.stateText}
            prompt={clockingData.stateText}
            onValueChange={(selection, index) =>  {
              console.log(selection);
              updateStateText(selection);
            }}>
              {constants.statesArray.map(function(name, index) {
                return <Picker.Item key={index} label={name} value={name} color={styles.palette.black} />
              })}
          </Picker>

          <View style={{ marginRight: 10 }}>
            <FloatingActionButton
              color={styles.palette.white}
              backgroundColor={styles.palette.mediumBlue}
              onPress={this.searchForStores} />
          </View>

        </View>
      </TouchableWithoutFeedback>
    );
  }

  searchForStores = () => {
    const { userData, clockingData, searchForStores } = this.props;
    if(clockingData.selectedStore) {
      var stateAbbrev = clockingData.stateText.slice(0, 2);
      searchForStores(userData.token, clockingData.selectedStore.clientId, clockingData.cityText, stateAbbrev, clockingData.zipText,
        this.viewCallback, this.viewEmptyCallback, this.viewErrorCallback);
    }
  }

  viewCallback = () => {
    this.props.navigation.navigate('SearchResults');
  }

  viewEmptyCallback = () => {
    let toast = Toast.show('No results found that match your search. Please try again.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }

  viewErrorCallback = () => {
    let toast = Toast.show('Error searching for stores. Please try again.', {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true
    });
  }
}

const mapStateToProps = ({ clockingReducer, userReducer }) => {
  return { clockingData: clockingReducer, userData: userReducer };
};

export default connect(mapStateToProps, { updateCityText, updateStateText, updateZipText, searchForStores })(ClockInHavingTroubleView);
