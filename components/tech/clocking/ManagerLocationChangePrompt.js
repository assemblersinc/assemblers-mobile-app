import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Text
} from 'react-native';

import styles from '../../../styles';
import Button from '../../_common/Button';

export default class ManagerLocationChangePrompt extends Component {

  render() {
    const { store, closeModal, checkIn } = this.props;
      return(
        <Modal
          animationType={"none"}
          transparent={true}
          visible={true}
          onRequestClose={closeModal}
          key={Math.random()}>
          <View style={styles.modal.container}>
            <View style={styles.modal.modal}>
                <Text style={styles.modal.text}>Looks like you{"\'"}re at</Text>
                <Text style={styles.modal.boldText}>{store.Location}</Text>
                <Text style={styles.modal.text}> at {store.address}.</Text>
                <Text style={[styles.modal.text, { marginTop: 20, marginBottom: 40 }]}>Would you like to check in?</Text>

                <View style={{ flexDirection: 'row', marginBottom: 20 }}>
                  <Button label={'YES, CHECK IN'} onPress={checkIn} color={styles.palette.mediumBlue}/>
                </View>

                <View style={{ flexDirection: 'row' }}>
                  <Button label={'NO'} onPress={closeModal} color={styles.palette.mediumGray}/>
                </View>
              </View>

          </View>
        </Modal>
      );
    }

}
