import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './redux/AppStore';
import config from './app/config/';
import { Platform } from 'react-native';
import HockeyApp from 'react-native-hockeyapp';


import AssemblersApp from './roles/_AssemblersApp';

export default class AssemblersIncMobile extends Component {

  componentWillMount() {
    
    if(Platform.OS == "android")
    {

      HockeyApp.configure(config.hockeyAndroidAppId
        ,config.autoSendReports
        ,config.authenticationType
        ,config.appAndroidSecret
      );

    }
    else 
    {
      
      HockeyApp.configure(config.hockeyIOSAppId
        ,config.autoSendReports
        ,config.authenticationType
        ,config.appIOSSecret
      );

    }


  }

  componentDidMount() {
      HockeyApp.trackEvent("INIT");
      HockeyApp.start();
      //HockeyApp.checkForUpdate(); // optional
  }

  render() {

    console.ignoredYellowBox = ['Warning: Can only update a mounted or mounting component.', 'Remote debugger is in a background tab which may cause apps to perform slowly.'];
    return(
      <Provider store={store}>
        <AssemblersApp />
      </Provider>
    );
  }

}
