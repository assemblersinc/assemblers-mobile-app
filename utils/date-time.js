import moment from 'moment';

function calculateHours(timeIn, timeOut) {
  var start = moment(timeIn);
  var end = moment(timeOut);
  var duration = moment.duration(end.diff(start));
  var hours = duration.asHours();
  return Math.round(hours);
}

function getDayOfWeek(timestamp) {
  var date = moment(timestamp);
  var day = date.day();

  switch(day){
    case 0:
      return "Sunday";
    case 1:
      return "Monday";
    case 2:
      return "Tuesday";
    case 3:
      return "Wednesday";
    case 4:
      return "Thursday";
    case 5:
      return "Friday";
    case 6:
      return "Saturday";
  }
}

function getDate(timestamp) {
  var date = moment(timestamp);
  return date.format('LL')
}

function getFormattedDate(timestamp) {
  var date = moment(timestamp);
  return date.format('MM-DD');
}

function getFormattedDateWithYear(timestamp) {
  var date = moment(timestamp);
  return date.format('MM-DD-YY');
}

/**
 * This returns an array of week objects
 * eg: [{monday: Date, description: 'Week of Aug 14 - 21, formattedForApi: 2017-08-14}]
 * @param numberOfWeeks number of weeks requested
 */
function getWeeks(numberOfWeeks) {

  let weeks = [];
  let startDate = getMonday(new Date());

  for (let i = 0; i < numberOfWeeks; i++) {
    let date = moment(startDate).subtract(i, 'week');
    weeks.push({monday: date, description: getFormattedWeekOf(date), formattedForApi: moment(date).format('YYYY-MM-DD')})
  }

  return weeks;
}

function getFormattedWeekOf(date) {
  let monday = moment(getMonday(date));
  return 'Week of ' + monday.format('MMM DD') + ' - ' + monday.add(1, 'week').format('DD');
}

function getFormattedTimestamp(timestamp) {
  var date = moment(timestamp);
  var hour = date.hour();
  var minute = date.minute();

  if(minute < 10) {
    minute = "0" + minute;
  }

  if(hour > 12) {
    hour = hour - 12;
    minute += " pm";
  } else { minute +=  "am"; }

  return hour + ':' + minute;
}

function isGrillbarrowDay() {
  var todayDOW = moment().weekday();
  console.log(todayDOW);
  if(todayDOW > 2 && todayDOW < 6) {
    return true;
  } else return false;
}

function isOver90Days(timestamp) {
  var todayTimestamp = moment().format('YYYY-MM-DD');
  var difference = moment(timestamp).diff(todayTimestamp, 'days');
  if(difference > 90) return true;
  else return false;
}

function getMonday(d) {
  d = new Date(d);
  let day = d.getDay(),
    diff = d.getDate() - day + (day === 0 ? -6:1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}


module.exports = {
  calculateHours: calculateHours,
  getDayOfWeek: getDayOfWeek,
  getFormattedDate: getFormattedDate,
  getFormattedTimestamp: getFormattedTimestamp,
  getFormattedDateWithYear: getFormattedDateWithYear,
  getFormattedWeekOf: getFormattedWeekOf,
  getDate: getDate,
  getMonday: getMonday,
  getWeeks, getWeeks,
  isGrillbarrowDay: isGrillbarrowDay,
  isOver90Days: isOver90Days
}
