import _ from 'lodash';

function getClientAttributes(invoices) {
	var attributes = [];

	if (invoices.length <= 0) return [];

	var clientAttributes = invoices[0].client.clientattributes;

	for (var i = 0; i < clientAttributes.length; i++) {

		var attr = clientAttributes[i];

		switch(attr.key) {
			case'reqKeyRec':
				attributes.push(getKeyRec(invoices));
				break;
			case 'reqPoNumber':
				attributes.push(getPoNumber(invoices));
				break;
			case 'reqInvoiceStamp':
				attributes.push(getInvoiceStamp(invoices));
				break;
			case 'reqWeeklyCount':
				//Ignore Weekly count Client Attribute
				break;
			default:
				break;

		}
	}
	console.log('before', clientAttributes);
	clientAttributes = _.sortBy(clientAttributes, 'index');
	console.log('after', clientAttributes);

	attributes.push(getSignature(invoices));

	return attributes.sort(function(x, y){return x.index > y.index ? 1 : x == y ? 0 : -1});
}

/*
* Test if invoice has Key Rec or PO for "Can't get invoice finalized" chat link display
*/
function hasKeyRecOrPOAttr(clientAttributes) {

	if (!clientAttributes || clientAttributes.length == 0) return false;

	for (var i = 0; i < clientAttributes.length; i++) {

		var attr = clientAttributes[i];

		switch(attr.key) {
			case'reqKeyRec':
				return true
			case 'reqPoNumber':
				return true;
			default:
				break;
		}
	}

	return false;
}

/*
* If an invoice has client attributes this function returns the next attribute that needs to be completed.
*/
function getNextStep(invoice) {

	var nextStep = null; //{ 'title': 'GET PO CODE', 'text': 'PO code needed'};
	var clientAttributes = invoice.client.clientattributes;
	clientAttributes = _.sortBy(clientAttributes, 'index');
	
	for (var i = 0; i < clientAttributes.length; i++) {
		var attr = clientAttributes[i];

		switch(attr.key) {
			case 'reqPoNumber':
				var poNumber = getPoNumber([invoice]);
				if (poNumber && !poNumber.isCompleted) {
					nextStep = { 'title':'GET PO CODE', 'text':'PO code needed', 'key': 'reqPoNumber' }
				}
				break;
			case'reqKeyRec':
				var keyRec = getKeyRec([invoice]);
				if (keyRec && !keyRec.isCompleted) {
					nextStep = { 'title':'KEY REC', 'text':'Key Rec needed', 'key': 'reqKeyRec' }
				}
				break;
			case 'reqInvoiceStamp':
				var stamp = getInvoiceStamp([invoice]);
				if (stamp && !stamp.isCompleted) {
					nextStep = { 'title':'GET STAMP', 'text':'Stamp needed', 'key': 'reqInvoiceStamp' }
				}
				break;
			case 'reqWeeklyCount':
				//Ignore Weekly count Client Attribute
				break;
			default:
				break;

		}

		if (nextStep) {
			return nextStep;
		};
	}

	if (!invoice.signatureImageUrl) {
		nextStep = { 'title': null, 'text': null, 'key': 'signature'};
	}

	return nextStep;
}

/*
* Used in StartInvoiceView to show 'All invoices submitted card'
*/
function isCompletedInvoices(invoices, clientAttributes) {

	for (var i = 0; i < clientAttributes.length; i++) {
		var attr = clientAttributes[i];

		if (!attr.isCompleted) return false;
	}

	return true;
}

module.exports = {
	getClientAttributes: getClientAttributes,
	getNextStep: getNextStep,
	isCompletedInvoices: isCompletedInvoices,
	hasKeyRecOrPOAttr: hasKeyRecOrPOAttr
}

/*
Private Functions
*/

function getKeyRec(invoices) {

	var isCompleted = true;

	for(var i = 0; i < invoices.length; i++) {
		var invoice = invoices[i];

		if (!invoice.keyRecNumber) {
			isCompleted = false;
			break;
		}
	}

	return {
			'index': 2,
			'key': 'reqKeyRec',
			'text': 'Enter the key rec code for each invoice',
			'isCompleted': isCompleted
		};
}

function getPoNumber(invoices) {

	var isCompleted = true;

	for(var i = 0; i < invoices.length; i++) {
		var invoice = invoices[i];

		if (!invoice.poNumber) {
			isCompleted = false;
			break;
		}
	}

	return {
			'index': 1,
			'key': 'reqPoNumber',
			'text': 'Enter the PO code for each invoice',
			'isCompleted': isCompleted
		};
}

function getInvoiceStamp(invoices) {

	var isCompleted = true;

	for(var i = 0; i < invoices.length; i++) {
		var invoice = invoices[i];

		if (!invoice.stamp) {
			isCompleted = false;
			break;
		}
	}

	return {
			'index': 3,
			'key': 'reqInvoiceStamp',
			'text': 'Missing invoice stamp for each invoice',
			'isCompleted': isCompleted
		};
}

/*
* DEPRECATED: Will never be used based on current assembler's process
*/
function getWeeklyCount(invoices) {

	var isCompleted = true;

	//TODO: Follow-up with Andres on this rec

	/*for(var i = 0; i < invoices.length; i++) {
		var invoice = invoices[i];

		if (!invoice.keyRecNumber) {
			isCompleted = false;
			break;
		}
	}*/

	return {
			'key': 'reqWeeklyCount',
			'text': 'Get weekly count',
			'isCompleted': isCompleted
		};
}

function getSignature(invoices) {
	var isCompleted = true;

	for(var i = 0; i < invoices.length; i++) {
		var invoice = invoices[i];

		if (!invoice.signatureImageUrl) {
			isCompleted = false;
			break;
		}
	}


	return {
			'index': 0,
			'key': 'reqSignature',
			'text': 'Get approved personnel signature',
			'isCompleted': isCompleted,
		};
}
